package fragments;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.CursorIndexOutOfBoundsException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import adapter.Area1NothingSelectedSpinnerAdapter;
import adapter.CustomRecyclerAdapterAdditional;
import data.GlobalVariables;
import implementation.DBHandler;
import implementation.FunctionJson;
import model.Additional;
import model.Additionals;
import model.RequestAdditional;
import model.RequestBasics;
import model.ResponseAdditional;
import nubaj.com.ocandroid.DocumentsActivity;
import nubaj.com.ocandroid.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class AdditionalFragment extends Fragment {

    private View root;
    private Dialog builder;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<Additional> items;
    public SearchView search;
    private Button button_Agregar_Referencia;
    private RelativeLayout relativeLayoutItemsZero;
    private RelativeLayout relativeLayoutInternet;
    private MenuItem searchViewItem;

    private Spinner spinner_Type_Reference;
    private int id_occupation, id_additional_types, getId_additional;
    private EditText editText_Telefono, editText_First_Name, editText_Second_Name, editText_Last_Name, editText_M_Last_Name;
    private Gson gson;
    private int position_additional, id=0;
    private FunctionJson functionJson;
    private DBHandler dbHandler;
    private TextView textViewInternet;
    private Additional additionals;
    private boolean is_add_additional = false, validate_number_length_phone=true, value_addtional=false;

    public AdditionalFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_additional, container, false);
        //search = (SearchView) root.findViewById(R.id.searchViewFilter);
        mRecyclerView = (RecyclerView) root.findViewById(R.id.reciclador);

        relativeLayoutItemsZero = (RelativeLayout) root.findViewById(R.id.relativeLayoutZeroItems);
        relativeLayoutInternet = (RelativeLayout) root.findViewById(R.id.relativeLayoutInternet);
        textViewInternet = (TextView) root.findViewById(R.id.textViewInternet);

        fillListAdditional();

        initialize();
        setHasOptionsMenu(true);
        return root;

    }

    private void insertAdditional()
    {
        Additionals additionals = new Additionals();
        try
        {
            additionals.setName1(editText_First_Name.getText().toString());
            additionals.setName2(editText_Second_Name.getText().toString());
            additionals.setLastName(editText_Last_Name.getText().toString());
            additionals.setmLastName(editText_M_Last_Name.getText().toString());
            additionals.setIdOccupation("1");
            additionals.setPhone(editText_Telefono.getText().toString());
            additionals.setIdAdditionalTypes(String.valueOf(id_additional_types));
            additionals.setIdName(String.valueOf(GlobalVariables.id_Name)); //IdName
            additionals.setComplete(getString(R.string.value_false));

            dbHandler = new DBHandler(getActivity());
            dbHandler.AddAdditional(additionals);
            getActivity().runOnUiThread(new Runnable()
            {
                public void run()
                {
                    Toast.makeText(getActivity(), "Alta", Toast.LENGTH_LONG).show();
                    is_add_additional = true;
                }
            });
        }
        catch (Exception e)
        {
            getActivity().runOnUiThread(new Runnable()
            {
                public void run()
                {
                    Toast.makeText(getActivity(), "Error", Toast.LENGTH_LONG).show();
                    is_add_additional = true;
                }
            });
            e.getStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        fillListAdditional();
        if(mRecyclerView.getAdapter()!=null)
        {
            ((CustomRecyclerAdapterAdditional) mAdapter).setOnItemClickListener(new CustomRecyclerAdapterAdditional
                    .MyClickListener()
            {
                @Override
                public void onItemClick(int position, View v)
                {
                    //Toast.makeText(getActivity(), "No hay información", Toast.LENGTH_SHORT).show();
                    GlobalVariables.state_Boolean = true;
                    position_additional = position;
                    GlobalVariables.additional.get(position).getIdADDITIONAL();
                    value_addtional = Boolean.parseBoolean(GlobalVariables.additional.get(position).getComplete());
                    dialogAddReference();
                }
            });
        }
    }

    void initialize(){
        button_Agregar_Referencia = (Button)root.findViewById(R.id.buttonAddReference);

        button_Agregar_Referencia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GlobalVariables.state_Boolean = false;
                dialogAddReference();
            }
        });
    }

    SearchView.OnQueryTextListener searchFilterListener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextChange(String query) {
            //query = query.toLowerCase();

            final ArrayList<Additional> filtered_List = new ArrayList<>();

            for (int i = 0; i < items.size(); i++) {

                final String text_Filter = items.get(i).getNombre().toString();
                //final String text = String.valueOf(items.get(i)).toString();
                if (text_Filter.toUpperCase().contains(query) || text_Filter.toLowerCase().contains(query)) {
                    filtered_List.add(items.get(i));
                }
            }

            mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            mAdapter = new CustomRecyclerAdapterAdditional(getActivity(), filtered_List);
            mRecyclerView.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();  // data set changed
            return true;

        }
        public boolean onQueryTextSubmit(String query) {
            return false;
        }
    };



    void dialogAddReference(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View layout = inflater.inflate(R.layout.custom_dialog_add_references, null);
        builder.setTitle(R.string.prompt_dialog_title_add_reference);

        editText_First_Name = (EditText) layout.findViewById(R.id.editTextPrimerNombre);
        editText_Second_Name = (EditText) layout.findViewById(R.id.editTextSegundoNombre);
        editText_Last_Name = (EditText) layout.findViewById(R.id.editTextPrimerApellido);
        editText_M_Last_Name = (EditText) layout.findViewById(R.id.editTextSegundoApellido);
        editText_Telefono = (EditText) layout.findViewById(R.id.editTextTelefono);
        spinner_Type_Reference = (Spinner) layout.findViewById(R.id.spinnerTypeReference);
        // Spinner Drop down elements type Genero
        List<String> list_Tipo_Adicional = new ArrayList<String>();
        list_Tipo_Adicional.add("Cónyuge");
        list_Tipo_Adicional.add("Arrendador de casa");
        list_Tipo_Adicional.add("Arrendador local (Negocio)");
        list_Tipo_Adicional.add("Referencia personal/familiar");
        list_Tipo_Adicional.add("Referencia comercial");
        list_Tipo_Adicional.add("Empleador");

        ArrayAdapter<String> adapter_Tipo_Adicional = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, list_Tipo_Adicional);
        adapter_Tipo_Adicional.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_Type_Reference.setAdapter(adapter_Tipo_Adicional);
        spinner_Type_Reference.setAdapter(new Area1NothingSelectedSpinnerAdapter(adapter_Tipo_Adicional, R.layout.area_1_spinner_row_nothing_selected, getContext()));
        spinner_Type_Reference.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
            {
                if(i>0)
                {
                    id_additional_types = i;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        initializeCapitalLetters();

        if (GlobalVariables.state_Boolean == true) {
            setDataAdditional();
        }

        builder.setView(layout);

        builder.setNegativeButton(R.string.prompt_cancel, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.setPositiveButton(R.string.prompt_acept, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which)
            {

                if (!TextUtils.isEmpty(editText_First_Name.getText().toString())
                        || !TextUtils.isEmpty(editText_Second_Name.getText().toString())
                        || !TextUtils.isEmpty(editText_Last_Name.getText().toString())
                        || !TextUtils.isEmpty(editText_M_Last_Name.getText().toString())
                        || !TextUtils.isEmpty(editText_Telefono.getText().toString())
                        || id_additional_types > 0)
                {
                    if(!TextUtils.isEmpty(editText_Telefono.getText()))
                    {
                        validate_number_length_phone = GlobalVariables.validateLengthPhone(editText_Telefono.getText().toString());
                    }
                    if (GlobalVariables.state_Boolean == true)
                    {
                        if (validate_number_length_phone == true)
                        {
                            updateDataAdditional();
                            GlobalVariables.isRefresh_additionals_list_boolean = true;
                            fillListAdditional();
                        }
                        else
                        {
                            Toast.makeText(getActivity(), R.string.error_phone_lenght, Toast.LENGTH_LONG).show();
                        }
                    }
                    else
                    {
                        if (GlobalVariables.id_Name > 0)
                        {
                            if (validate_number_length_phone == true)
                            {
                                insertAdditional();
                                GlobalVariables.isRefresh_additionals_list_boolean = true;
                                fillListAdditional();
                            }
                            else
                            {
                                Toast.makeText(getActivity(), R.string.error_phone_lenght, Toast.LENGTH_LONG).show();
                            }
                        }
                        else
                        {
                            Toast.makeText(getActivity(), R.string.error_name, Toast.LENGTH_LONG).show();
                        }
                    }
                }
                else
                {
                    Toast.makeText(getActivity(), R.string.error_empty_input, Toast.LENGTH_LONG).show();
                }
                dialog.cancel();
            }
        });

        builder.show();
    }

    void setDataAdditional()
    {
        //for(Additionals additional: GlobalVariables.additional)
        //{
        dbHandler = new DBHandler(getActivity());
        ArrayList<Additionals> additionals;
        try
        {
            additionals = dbHandler.GetAdditional(GlobalVariables.id_Name);
            editText_First_Name.setText(additionals.get(position_additional).getName1());
            editText_Second_Name.setText(additionals.get(position_additional).getName2());
            editText_Last_Name.setText(additionals.get(position_additional).getLastName());
            editText_M_Last_Name.setText(additionals.get(position_additional).getmLastName());
            editText_Telefono.setText(additionals.get(position_additional).getPhone());
            spinner_Type_Reference.setSelection(Integer.parseInt(additionals.get(position_additional).getIdAdditionalTypes()));
            id = Integer.parseInt(additionals.get(position_additional).getIdADDITIONAL());

        } catch (CursorIndexOutOfBoundsException e) {

        }

        //}
    }

    void updateDataAdditional()
    {
        dbHandler = new DBHandler(getActivity());
        Additionals additionals = new Additionals();

        additionals.setName1(editText_First_Name.getText().toString());
        additionals.setName2(editText_Second_Name.getText().toString());
        additionals.setLastName(editText_Last_Name.getText().toString());
        additionals.setmLastName(editText_M_Last_Name.getText().toString());
        additionals.setIdOccupation("1");
        additionals.setPhone(editText_Telefono.getText().toString());
        additionals.setIdAdditionalTypes(String.valueOf(id_additional_types));
        additionals.setIdName(String.valueOf(GlobalVariables.id_Name)); //IdName
        if(value_addtional==false)
        {
            additionals.setComplete(getString(R.string.value_false));
        }
        else
        {
            additionals.setComplete(getString(R.string.value_true));
        }
        additionals.setIdADDITIONAL(String.valueOf(id));
        dbHandler.UpdateAdditionals(additionals);
        Toast.makeText(getActivity(), R.string.msg_save_change, Toast.LENGTH_LONG).show();
    }

    private void initializeCapitalLetters() {
        GlobalVariables.capitalLetters(editText_First_Name);
        GlobalVariables.capitalLetters(editText_Last_Name);
        GlobalVariables.capitalLetters(editText_M_Last_Name);
        GlobalVariables.capitalLetters(editText_Second_Name);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_applicants, menu);

        searchViewItem = menu.findItem(R.id.action_search_simple);
        final SearchView searchViewAndroidActionBar = (SearchView) MenuItemCompat.getActionView(searchViewItem);
        searchViewAndroidActionBar.setOnQueryTextListener(searchFilterListener); // call the QuerytextListner.
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_camera){
            try {
                dbHandler = new DBHandler(getActivity());
                GlobalVariables.complementary = dbHandler.GetComplementary(GlobalVariables.id_Name);
                if(!TextUtils.isEmpty(GlobalVariables.CURP) && GlobalVariables.CURP!=null)
                {
                    GlobalVariables.CURP = GlobalVariables.complementary.getCurp();
                }
            } catch (CursorIndexOutOfBoundsException e) {

            }
            if(!TextUtils.isEmpty(GlobalVariables.CURP) && GlobalVariables.CURP!=null){
                Intent intent = new Intent(getActivity(), DocumentsActivity.class);
                startActivity(intent);
            }
            else
            {
                Toast.makeText(getActivity(), R.string.message_camera, Toast.LENGTH_LONG).show();
            }
        }else if (id == R.id.action_save){
            saveAction();
        }else if (id == R.id.action_sync){
            Toast.makeText(getActivity(), "Sincronizar", Toast.LENGTH_LONG).show();
        }else if (id == R.id.action_info){
            infoAction();
        }
        return super.onOptionsItemSelected(item);
    }

    void saveAction(){
        // Use the Builder class for convenient dialog construction
        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity());
        builder.setMessage("¿Desea guardar el solicitante?")
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //Action
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //Action
                    }
                });
        // Create the AlertDialog object and return
        builder.show();
    }

    void infoAction(){
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity());
        builder.setTitle("Politicas de privacidad")
                .setItems(R.array.politica_privacidad, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // The 'which' argument contains the index position
                        // of the selected item
                    }
                });

        builder.show();
    }

    void fillListAdditional(){

        dbHandler = new DBHandler(getActivity());
        GlobalVariables.additional = dbHandler.GetAdditional(GlobalVariables.id_Name);

        if (GlobalVariables.setServiceData == false){
            if (GlobalVariables.additional != null){
                if (is_add_additional == false ) {
                    GlobalVariables.additional.clear();
                }
            }
        }

        if(GlobalVariables.additional==null)
        {
            mRecyclerView.setVisibility(View.GONE);
            relativeLayoutInternet.setVisibility(View.VISIBLE);
            //textViewInternet.setText(GlobalVariables.responseNameUser.getMensaje().toString());
            textViewInternet.setText("No se encontró información");
        }
        else
        {
            items = new ArrayList<>();
            if (GlobalVariables.additional.size() > 0) {
                for (Additionals additional : GlobalVariables.additional) {
                    additionals = new Additional();
                    additionals.setNombre(additional.getName1() + " " + additional.getName2() + " " + additional.getLastName() + " " + additional.getmLastName());
                    String getTypeAdditional = String.valueOf(GlobalVariables.listTypeAdicional().get(Integer.parseInt(additional.getIdAdditionalTypes())));
                    additionals.setTipo(getTypeAdditional);
                    additionals.setIcon_Contact(R.mipmap.ic_type_adition);
                    items.add(additionals);
                }
            }

            if (items.size() != 0) {
                mRecyclerView.setHasFixedSize(true);
                mLayoutManager = new LinearLayoutManager(getActivity());
                mRecyclerView.setLayoutManager(mLayoutManager);
                mAdapter = new CustomRecyclerAdapterAdditional(getActivity(), items);
                mAdapter.notifyDataSetChanged();
                mRecyclerView.setAdapter(mAdapter);
                if (GlobalVariables.isRefresh_additionals_list_boolean == true){
                    GlobalVariables.isRefresh_additionals_list_boolean = false;
                    getFragmentManager().beginTransaction().detach(this).attach(this).commit();
                }
                //setHasOptionsMenu(true);
            } else {
                //Toast.makeText(getActivity(), "No tiene conexión a internet", Toast.LENGTH_SHORT).show();
                mRecyclerView.setVisibility(View.GONE);
                relativeLayoutInternet.setVisibility(View.VISIBLE);
            }
        }
    }
}
