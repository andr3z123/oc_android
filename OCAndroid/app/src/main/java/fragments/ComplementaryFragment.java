package fragments;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.CursorIndexOutOfBoundsException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import adapter.Area1NothingSelectedSpinnerAdapter;
import data.GlobalVariables;
import implementation.DBHandler;
import implementation.FunctionJson;
import model.Complementary;
import model.Economic;
import model.RequestComplementary;
import model.ResponseComplementary;
import nubaj.com.ocandroid.DocumentsActivity;
import nubaj.com.ocandroid.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ComplementaryFragment extends Fragment {
    ProgressDialog pDialog;
    private View root;
    private Gson gson;
    private FunctionJson functionJson;
    private int idSelection;
    private RadioGroup radio_Group_Options;
    private LinearLayout linearLayoutForm;
    private RadioButton radioButtonSi;
    private RadioButton radioButtonNo;
    private TextView textView_Title_Persona_Fisica, textView_Title_Ocupacion, textView_Title_Actividad_Economica, textView_Title_Tiempo_Realizando_Actividad, textView_Title_Realiza_Actividad, textView_Title_Aportacion, textView_Title_Otra_Fuente_Ingresos, textView_Title_Tiempo_Negocio_Actual, textView_Title_Local_Es;
    private AutoCompleteTextView editTex_Homoclave, editTex_NumeroFiel, editTex_CURP, editTex_Dependientes_Economicos, editTex_Correo_Electronico;
    private Spinner spinner_Ocupacion, spinner_Tiempo_Realizando_Actividad, spinner_Realiza_Actividad_Economica, spinner_Aportacion_Negocio, spinner_Otra_Fuente_Ingresos, spinner_Tiempo_Negocio_Actual, spinner_Es_Local;
    private String physical_person = "false",
            curp,
            dependent_number,
            email,
            homoclave,
            fiel;
    private int id_occupation,
            id_tiempo_realizando_activida,
            id_realiza_actividad_economica,
            id_aportacion_negocio,
            id_otra_fuente_ingresos,
            id_tiempo_negocio_actual,
            id_es_local,
            id_check;
    private String aportacion_negocio, otra_fuente_ingresos, tiempo_negocio_actual;
    private List<String> list_Aportacion_Negocio, list_Otra_Fuente_Ingresos, list_Tiempo_Negocio_Actual;
    DBHandler dbHandler;
    private boolean curp_validate=true, homoClave_validate=true, validateLengthFiel=true;
    Economic economic;
    Complementary complementary;

    public ComplementaryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_complementary, container, false);
        initializeComplementary();
        if (GlobalVariables.setServiceData == true) {
            setDataComplementary();
        }

        FloatingActionButton fab = (FloatingActionButton) root.findViewById(R.id.floatingActionButtonSave);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveAction();
            }
        });

        setHasOptionsMenu(true);
        return root;
    }

    private void insertEconomic()
    {
        Economic economic = new Economic();
        try
        {
            economic.setContributionLevel(String.valueOf(aportacion_negocio));
            economic.setIdLocalType(String.valueOf(id_es_local));
            economic.setCurrentBusinessTime(String.valueOf(tiempo_negocio_actual));
            economic.setAnotherSourceIncome(String.valueOf(otra_fuente_ingresos));
            economic.setIdName(String.valueOf(GlobalVariables.id_Name));
            economic.setEmail(editTex_Correo_Electronico.getText().toString());
            economic.setIdMakesActivity(String.valueOf(id_tiempo_realizando_activida));
            economic.setIdTimeActivity(String.valueOf(id_tiempo_realizando_activida));
            economic.setIdEconomicActivity(String.valueOf(GlobalVariables.activity_economic));
            economic.setComplete("false");
            dbHandler = new DBHandler(getActivity());
            dbHandler.AddEconomic(economic);
            Toast.makeText(getActivity(), "Alta", Toast.LENGTH_LONG).show();
        }
        catch (Exception ex)
        {
            Toast.makeText(getActivity(), "Error", Toast.LENGTH_LONG).show();
        }
    }

    private void insertComplementary()
    {
        Complementary complementary = new Complementary();

        boolean validateLengthFiel = GlobalVariables.validateLengthFiel(editTex_NumeroFiel.getText().toString());
        if (validateLengthFiel == true)
        {
            boolean homoClave_validate = GlobalVariables.validateHomoClave(editTex_Homoclave.getText().toString());
            if (homoClave_validate == true)
            {
                boolean curp_validate = GlobalVariables.curpValidate(editTex_CURP.getText().toString());
                if (curp_validate == true)
                {

                }
                else
                {
                    editTex_CURP.setError(getString(R.string.error_invalid_curp));
                }
            }
            else
            {
                editTex_Homoclave.setError(getString(R.string.error_invalid_homo_clave));
            }
        }
        else
        {
            editTex_NumeroFiel.setError(getString(R.string.error_invalid_number_fiel));
        }
        try
        {
            complementary.setPhysicalPerson(physical_person.toString());


            complementary.setCurp(editTex_CURP.getText().toString());
            complementary.setDependentNumber(editTex_Dependientes_Economicos.getText().toString());
            complementary.setIdOccupation(String.valueOf(id_occupation));
            complementary.setEmail(editTex_Correo_Electronico.getText().toString());
            complementary.setHomoclave(editTex_Homoclave.getText().toString());
            complementary.setFiel(editTex_NumeroFiel.getText().toString());
            complementary.setIdName(String.valueOf(GlobalVariables.id_Name)); //IdName
            complementary.setComplete(getString(R.string.value_false));
            dbHandler = new DBHandler(getActivity());
            dbHandler.AddComplementary(complementary);
            getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(getActivity(), "Alta", Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception e) {
            getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(getActivity(), "Error", Toast.LENGTH_LONG).show();
                }
            });
            e.getStackTrace();
        }
    }

    void initializeComplementary() {
        initializeComponents();
        initializeCapitalLetters();
        if (idSelection == R.id.radioButtonNo) {
            linearLayoutForm.setVisibility(View.GONE);
        }
        radio_Group_Options.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                //String opcion = "";
                id_check = checkedId;
                if (checkedId == R.id.radioButtonNo) {
                    physical_person = String.valueOf(false);
                } else {
                    physical_person = String.valueOf(true);
                }
                switch (checkedId) {
                    case R.id.radioButtonSi:
                        linearLayoutForm.setVisibility(View.VISIBLE);
                        break;
                    case R.id.radioButtonNo:
                        linearLayoutForm.setVisibility(View.GONE);
                        break;
                }

                //Toast.makeText(getActivity(),"ID opción seleccionada: " + opcion,Toast.LENGTH_LONG).show();
            }
        });

        ArrayAdapter<String> adapter_Ocupacion = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listOccupation());
        adapter_Ocupacion.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_Ocupacion.setAdapter(adapter_Ocupacion);
        spinner_Ocupacion.setAdapter(new Area1NothingSelectedSpinnerAdapter(adapter_Ocupacion, R.layout.area_1_spinner_row_nothing_selected, getContext()));
        spinner_Ocupacion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                id_occupation = i;
                GlobalVariables.id_occupation = id_occupation;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        // Spinner Drop down elements type Status
        List<String> list_Tiempo_Actividad = new ArrayList<String>();
        list_Tiempo_Actividad.add("Menor igual a 1 año");
        list_Tiempo_Actividad.add("De 2 a 3 años");
        list_Tiempo_Actividad.add("De 4 a 5 años");
        list_Tiempo_Actividad.add("Mas de 5 años");

        ArrayAdapter<String> adapter_Tiempo_Actividad = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, list_Tiempo_Actividad);
        adapter_Tiempo_Actividad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_Tiempo_Realizando_Actividad.setAdapter(adapter_Tiempo_Actividad);
        spinner_Tiempo_Realizando_Actividad.setAdapter(new Area1NothingSelectedSpinnerAdapter(adapter_Tiempo_Actividad, R.layout.area_1_spinner_row_nothing_selected, getContext()));
        spinner_Tiempo_Realizando_Actividad.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                id_tiempo_realizando_activida = i;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        // Spinner Drop down elements type Status
        List<String> list_Realiza_Actividad_Economica = new ArrayList<String>();
        list_Realiza_Actividad_Economica.add("Puesto en tianguis");
        list_Realiza_Actividad_Economica.add("Venta por catálogo");
        list_Realiza_Actividad_Economica.add("Puesto en vía pública");
        list_Realiza_Actividad_Economica.add("Ambulante/de casa en casa");
        list_Realiza_Actividad_Economica.add("Oficios a domicilio");
        list_Realiza_Actividad_Economica.add("En el domicilio del cliente");
        list_Realiza_Actividad_Economica.add("Puesto en mercado o techo común");
        list_Realiza_Actividad_Economica.add("Taller");
        list_Realiza_Actividad_Economica.add("En un vehículo");
        list_Realiza_Actividad_Economica.add("Tienda, accesoria, tendajon");

        ArrayAdapter<String> adapter_Realiza_Actividad_Economica = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, list_Realiza_Actividad_Economica);
        adapter_Realiza_Actividad_Economica.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_Realiza_Actividad_Economica.setAdapter(adapter_Realiza_Actividad_Economica);
        spinner_Realiza_Actividad_Economica.setAdapter(new Area1NothingSelectedSpinnerAdapter(adapter_Realiza_Actividad_Economica, R.layout.area_1_spinner_row_nothing_selected, getContext()));
        spinner_Realiza_Actividad_Economica.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
            {
                id_realiza_actividad_economica = i;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        // Spinner Drop down elements type Status
        list_Aportacion_Negocio = new ArrayList<String>();
        list_Aportacion_Negocio.add("El negocio es la principal fuente de ingreso del hogar");
        list_Aportacion_Negocio.add("El negocio ayuda a complementar el ingreso del hogar");

        ArrayAdapter<String> adapter_Aportacion_Negocio = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, list_Aportacion_Negocio);
        adapter_Aportacion_Negocio.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_Aportacion_Negocio.setAdapter(adapter_Aportacion_Negocio);
        spinner_Aportacion_Negocio.setAdapter(new Area1NothingSelectedSpinnerAdapter(adapter_Aportacion_Negocio, R.layout.area_1_spinner_row_nothing_selected, getContext()));
        spinner_Aportacion_Negocio.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
            {
                id_aportacion_negocio = i;
                if(i>0)
                {
                    aportacion_negocio = list_Aportacion_Negocio.get(i - 1);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        // Spinner Drop down elements type Status
        list_Otra_Fuente_Ingresos = new ArrayList<String>();
        list_Otra_Fuente_Ingresos.add("Salario");
        list_Otra_Fuente_Ingresos.add("Apoyo familiar");
        list_Otra_Fuente_Ingresos.add("Envío de dinero");
        list_Otra_Fuente_Ingresos.add("Pensión");
        list_Otra_Fuente_Ingresos.add("Apoyo de gobierno");
        list_Otra_Fuente_Ingresos.add("Otro negocio");
        list_Otra_Fuente_Ingresos.add("Otro");
        list_Otra_Fuente_Ingresos.add("Ninguno");

        ArrayAdapter<String> adapter_Otra_Fuente_Ingresos = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, list_Otra_Fuente_Ingresos);
        adapter_Otra_Fuente_Ingresos.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_Otra_Fuente_Ingresos.setAdapter(adapter_Otra_Fuente_Ingresos);
        spinner_Otra_Fuente_Ingresos.setAdapter(new Area1NothingSelectedSpinnerAdapter(adapter_Otra_Fuente_Ingresos, R.layout.area_1_spinner_row_nothing_selected, getContext()));
        spinner_Otra_Fuente_Ingresos.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                id_otra_fuente_ingresos = i;
                if(i>0)
                {
                    otra_fuente_ingresos = list_Otra_Fuente_Ingresos.get(i - 1);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        // Spinner Drop down elements type Status
        list_Tiempo_Negocio_Actual = new ArrayList<String>();
        list_Tiempo_Negocio_Actual.add("Menos igual a 1 año");
        list_Tiempo_Negocio_Actual.add("De 2 a 3 años");
        list_Tiempo_Negocio_Actual.add("De 4 a 5 años");
        list_Tiempo_Negocio_Actual.add("Mas de 5 años");

        ArrayAdapter<String> adapter_Tiempo_Negocio_Actual = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, list_Tiempo_Negocio_Actual);
        adapter_Tiempo_Negocio_Actual.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_Tiempo_Negocio_Actual.setAdapter(adapter_Tiempo_Negocio_Actual);
        spinner_Tiempo_Negocio_Actual.setAdapter(new Area1NothingSelectedSpinnerAdapter(adapter_Tiempo_Negocio_Actual, R.layout.area_1_spinner_row_nothing_selected, getContext()));
        spinner_Tiempo_Negocio_Actual.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                id_tiempo_negocio_actual = i;
                if(i>0)
                {
                    tiempo_negocio_actual = list_Tiempo_Negocio_Actual.get(i-1);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        // Spinner Drop down elements type Status
        List<String> list_Es_Local = new ArrayList<String>();
        list_Es_Local.add("Algo 1");
        list_Es_Local.add("Algo 2");
        list_Es_Local.add("Algo 3");
        list_Es_Local.add("Algo 4");

        ArrayAdapter<String> adapter_Es_Local = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, list_Es_Local);
        adapter_Es_Local.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_Es_Local.setAdapter(adapter_Es_Local);
        spinner_Es_Local.setAdapter(new Area1NothingSelectedSpinnerAdapter(adapter_Es_Local, R.layout.area_1_spinner_row_nothing_selected, getContext()));
        spinner_Es_Local.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                id_es_local = i;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private void initializeCapitalLetters() {
        GlobalVariables.capitalLetters(editTex_CURP);
        GlobalVariables.capitalLetters(editTex_Homoclave);
        GlobalVariables.capitalLetters(editTex_NumeroFiel);
    }

    void initializeComponents() {

        textView_Title_Persona_Fisica = (TextView) root.findViewById(R.id.textViewTitlePersonaFisica);
        textView_Title_Ocupacion = (TextView) root.findViewById(R.id.textViewTitleOcupacion);
        textView_Title_Actividad_Economica = (TextView) root.findViewById(R.id.textViewTitleActividadEconomica);
        textView_Title_Tiempo_Realizando_Actividad = (TextView) root.findViewById(R.id.textViewTitleTiempoRealizandoActividad);
        textView_Title_Realiza_Actividad = (TextView) root.findViewById(R.id.textViewTitleRealizaActividad);
        textView_Title_Aportacion = (TextView) root.findViewById(R.id.textViewTitleAportacion);
        textView_Title_Otra_Fuente_Ingresos = (TextView) root.findViewById(R.id.textViewTitleOtraFuenteIngresos);
        textView_Title_Tiempo_Negocio_Actual = (TextView) root.findViewById(R.id.textViewTitleTiempoNegocioActual);
        textView_Title_Local_Es = (TextView) root.findViewById(R.id.textViewTitleLocalEs);

        linearLayoutForm = (LinearLayout) root.findViewById(R.id.linearLayoutForm);
        editTex_CURP = (AutoCompleteTextView) root.findViewById(R.id.editTexCURP);
        editTex_Homoclave = (AutoCompleteTextView) root.findViewById(R.id.editTexHomoclave);
        editTex_NumeroFiel = (AutoCompleteTextView) root.findViewById(R.id.editTexNumeroFiel);
        editTex_Dependientes_Economicos = (AutoCompleteTextView) root.findViewById(R.id.editTexDependientesEconomicos);
        editTex_Correo_Electronico = (AutoCompleteTextView) root.findViewById(R.id.editTexCorreoElectronico);

        radio_Group_Options = (RadioGroup) root.findViewById(R.id.radioButtonOptions);
        radio_Group_Options.clearCheck();
        radio_Group_Options.check(R.id.radioButtonSi);
        radio_Group_Options.check(R.id.radioButtonNo);
        idSelection = radio_Group_Options.getCheckedRadioButtonId();

        radioButtonSi = (RadioButton) root.findViewById(R.id.radioButtonSi);
        radioButtonNo = (RadioButton) root.findViewById(R.id.radioButtonNo);

        spinner_Ocupacion = (Spinner) root.findViewById(R.id.spinnerOcupacion);
        spinner_Tiempo_Realizando_Actividad = (Spinner) root.findViewById(R.id.spinnerTiempoRealizandoActividad);
        spinner_Realiza_Actividad_Economica = (Spinner) root.findViewById(R.id.spinnerRealizaActividad);
        spinner_Aportacion_Negocio = (Spinner) root.findViewById(R.id.spinnerAportacion);
        spinner_Otra_Fuente_Ingresos = (Spinner) root.findViewById(R.id.spinnerOtraFuenteIngresos);
        spinner_Tiempo_Negocio_Actual = (Spinner) root.findViewById(R.id.spinnerTiempoNegocioActual);
        spinner_Es_Local = (Spinner) root.findViewById(R.id.spinnerLocalEs);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_applicants, menu);
        menu.findItem(R.id.action_search_simple).setVisible(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_camera) {
            try {
                dbHandler = new DBHandler(getActivity());
                GlobalVariables.complementary = dbHandler.GetComplementary(GlobalVariables.id_Name);
                if(!TextUtils.isEmpty(GlobalVariables.CURP) && GlobalVariables.CURP!=null)
                {
                    GlobalVariables.CURP = GlobalVariables.complementary.getCurp();
                }
            } catch (CursorIndexOutOfBoundsException e) {

            }
            if(!TextUtils.isEmpty(GlobalVariables.CURP) && GlobalVariables.CURP!=null) {
                Intent intent = new Intent(getActivity(), DocumentsActivity.class);
                startActivity(intent);
            } else {
                Toast.makeText(getActivity(), R.string.message_camera, Toast.LENGTH_LONG).show();
            }
        } else if (id == R.id.action_save) {
            //saveAction();
        } else if (id == R.id.action_sync) {
            Toast.makeText(getActivity(), "Sincronizar", Toast.LENGTH_LONG).show();
        } else if (id == R.id.action_info) {
            infoAction();
        }

        return super.onOptionsItemSelected(item);
    }

    void saveAction() {
        // Use the Builder class for convenient dialog construction
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("¿Desea guardar el solicitante?")
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int id) {
                        //Action
                        if (id_check == R.id.radioButtonSi)
                        {
                            typeCheckYes();
                        }
                        else
                        {
                            typeCheckNo();
                        }
                        onEconomic();
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //Action
                    }
                });
        // Create the AlertDialog object and return
        builder.show();
    }

    void infoAction() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Politicas de privacidad")
                .setItems(R.array.politica_privacidad, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // The 'which' argument contains the index position
                        // of the selected item
                    }
                });

        builder.show();
    }

    void setDataComplementary() {

        dbHandler = new DBHandler(getActivity());
        complementary = new Complementary();
        economic = new Economic();
        try
        {
            complementary = dbHandler.GetComplementary(GlobalVariables.id_Name);
            economic = dbHandler.GetEconomic(GlobalVariables.id_Name);

            if(complementary!=null)
            {
                if (!TextUtils.isEmpty(complementary.getPhysicalPerson())) {
                    radio_Group_Options.setSelected(Boolean.parseBoolean(complementary.getPhysicalPerson()));
                    if (Boolean.parseBoolean(complementary.getPhysicalPerson()) == true) {
                        radioButtonSi.setChecked(true);
                    } else {
                        radioButtonNo.setChecked(true);
                    }
                }


                if (!TextUtils.isEmpty(complementary.getFiel())) {
                    editTex_NumeroFiel.setText(complementary.getFiel());
                }

                if (!TextUtils.isEmpty(complementary.getHomoclave())) {
                    editTex_Homoclave.setText(complementary.getHomoclave());
                }

                if (!TextUtils.isEmpty(complementary.getCurp())) {
                    editTex_CURP.setText(complementary.getCurp());
                }

                if (!TextUtils.isEmpty(complementary.getDependentNumber())) {
                    editTex_Dependientes_Economicos.setText(complementary.getDependentNumber());
                }

                if (!TextUtils.isEmpty(complementary.getIdOccupation())) {
                    spinner_Ocupacion.setSelection(Integer.parseInt(complementary.getIdOccupation()));
                }
            }

            if(economic!=null)
            {
                if (!TextUtils.isEmpty(economic.getEmail())) {
                    editTex_Correo_Electronico.setText(economic.getEmail());
                }

                if (!TextUtils.isEmpty(economic.getIdLocalType())) {
                    spinner_Es_Local.setSelection(Integer.parseInt(economic.getIdLocalType()));
                }
                int count = 0;

                if (!TextUtils.isEmpty(economic.getContributionLevel())) {
                    for (String aportacion : list_Aportacion_Negocio) {
                        if (aportacion.equals(economic.getContributionLevel().toString())) {
                            aportacion_negocio = aportacion;
                            count++;
                            break;
                        }
                    }
                    spinner_Aportacion_Negocio.setSelection(count);
                }

                count = 0;

                if (!TextUtils.isEmpty(economic.getAnotherSourceIncome())) {
                    for (String otra : list_Otra_Fuente_Ingresos) {
                        if (otra.equals(economic.getAnotherSourceIncome().toString())) {
                            otra_fuente_ingresos = otra;
                            count++;
                            break;
                        }
                    }
                    spinner_Otra_Fuente_Ingresos.setSelection(count);
                }

                count = 0;
                if (!TextUtils.isEmpty(economic.getCurrentBusinessTime())) {
                    for (String negocio : list_Tiempo_Negocio_Actual) {
                        if (negocio.equals(economic.getCurrentBusinessTime())) {
                            aportacion_negocio = negocio;
                            count++;
                            break;
                        }
                    }
                    spinner_Tiempo_Negocio_Actual.setSelection(count);
                }

                if (!TextUtils.isEmpty(economic.getIdMakesActivity())) {
                    spinner_Realiza_Actividad_Economica.setSelection(Integer.parseInt(economic.getIdMakesActivity()));
                }

                if (!TextUtils.isEmpty(economic.getIdTimeActivity())) {
                    spinner_Tiempo_Realizando_Actividad.setSelection(Integer.parseInt(economic.getIdTimeActivity()));
                }
            }
        }
        catch (CursorIndexOutOfBoundsException e)
        {
            e.getStackTrace();
        }
    }

    void updateDataEconomic()
    {
        dbHandler = new DBHandler(getActivity());
        try
        {
            Economic economicUpdate = new Economic();
            economicUpdate.setIdLocalType(String.valueOf(id_es_local));
            economicUpdate.setContributionLevel(String.valueOf(aportacion_negocio));
            economicUpdate.setCurrentBusinessTime(String.valueOf(tiempo_negocio_actual));
            economicUpdate.setAnotherSourceIncome(String.valueOf(otra_fuente_ingresos));
            economicUpdate.setIdName(String.valueOf(GlobalVariables.id_Name));
            economicUpdate.setEmail(editTex_Correo_Electronico.getText().toString());
            economicUpdate.setIdMakesActivity(String.valueOf(id_realiza_actividad_economica));
            economicUpdate.setIdTimeActivity(String.valueOf(id_tiempo_realizando_activida));
            economicUpdate.setIdEconomicActivity(String.valueOf(GlobalVariables.activity_economic));
            if(Boolean.parseBoolean(economicUpdate.getComplete())==false)
            {
                economicUpdate.setComplete(getString(R.string.value_false).toString());
            }
            else
            {
                economicUpdate.setComplete(getString(R.string.value_true).toString());
            }
            economicUpdate.setIdECONOMIC(GlobalVariables.economic.getIdECONOMIC());
            dbHandler.UpdateEconomic(economic);
            Snackbar.make(root, R.string.msg_save_change, Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();

        }
        catch (Exception ex)
        {
            Snackbar.make(root, "Error", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        }
    }

    void updateDataComplementary() {
        dbHandler = new DBHandler(getActivity());
        Complementary complementaryUpdate = new Complementary();

        complementaryUpdate.setPhysicalPerson(physical_person.toString());
        complementaryUpdate.setCurp(editTex_CURP.getText().toString());
        complementaryUpdate.setDependentNumber(editTex_Dependientes_Economicos.getText().toString());
        complementaryUpdate.setIdOccupation(String.valueOf(id_occupation));
        complementaryUpdate.setEmail(editTex_Correo_Electronico.getText().toString());
        complementaryUpdate.setHomoclave(editTex_Homoclave.getText().toString());
        complementaryUpdate.setFiel(editTex_NumeroFiel.getText().toString());
        complementaryUpdate.setIdName(String.valueOf(GlobalVariables.id_Name)); //IdName
        if(Boolean.parseBoolean(complementary.getComplete())==false)
        {
            complementaryUpdate.setComplete(getString(R.string.value_false).toString());
        }
        else
        {
            complementaryUpdate.setComplete(getString(R.string.value_true).toString());
        }
        complementaryUpdate.setIdCOMPLEMENTARY(GlobalVariables.complementary.getIdCOMPLEMENTARY());

        dbHandler.UpdateComplementary(complementary);
        Snackbar.make(root, R.string.msg_save_change, Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
        //Toast.makeText(getActivity(), R.string.msg_save_change, Toast.LENGTH_LONG).show();
    }

    public void typeCheckYes() {

        boolean cancel = false;
        View focusView = null;
        // Reset errors.
        editTex_CURP.setError(null);
        editTex_Correo_Electronico.setError(null);
        editTex_Homoclave.setError(null);
        editTex_NumeroFiel.setError(null);

        if (!TextUtils.isEmpty(editTex_NumeroFiel.getText().toString())
                || !TextUtils.isEmpty(editTex_Homoclave.getText().toString())
                || !TextUtils.isEmpty(editTex_CURP.getText().toString())
                || !TextUtils.isEmpty(editTex_Dependientes_Economicos.toString())
                || id_occupation > 0)
        {
            if (GlobalVariables.id_Name > 0)
            {
                if(!TextUtils.isEmpty(editTex_CURP.getText().toString()))
                {
                    curp_validate = GlobalVariables.curpValidate(editTex_CURP.getText().toString());
                }
                if(!TextUtils.isEmpty(editTex_Homoclave.getText().toString()))
                {
                    homoClave_validate = GlobalVariables.validateHomoClave(editTex_Homoclave.getText().toString());
                }
                if (!TextUtils.isEmpty(editTex_NumeroFiel.getText().toString()))
                {
                    validateLengthFiel = GlobalVariables.validateLengthFiel(editTex_NumeroFiel.getText().toString());
                }
                if (GlobalVariables.setServiceData == true)
                {
                    try
                    {
                        dbHandler = new DBHandler(getActivity());
                        GlobalVariables.complementary = dbHandler.GetComplementary(GlobalVariables.id_Name);
                    }
                    catch (CursorIndexOutOfBoundsException e)
                    {

                    }
                    if (GlobalVariables.complementary == null)
                    {
                        if(curp_validate==true&&homoClave_validate==true&&validateLengthFiel==true)
                        {
                            insertComplementary();
                        }
                        else
                        {
                            if(curp_validate==false)
                            {
                                editTex_CURP.setError(getString(R.string.error_invalid_curp));
                                return;
                            }
                            else if(homoClave_validate==false)
                            {
                                editTex_Homoclave.setError(getString(R.string.error_invalid_homo_clave));
                                return;
                            }
                            else if(validateLengthFiel==false)
                            {
                                editTex_NumeroFiel.setError(getString(R.string.error_invalid_number_fiel));
                                return;
                            }
                        }
                    }
                    else
                    {
                        if(curp_validate==true&&homoClave_validate==true&&validateLengthFiel==true)
                        {
                            updateDataComplementary();
                        }
                        else
                        {
                            if(curp_validate==false)
                            {
                                editTex_CURP.setError(getString(R.string.error_invalid_curp));
                                return;
                            }
                            else if(homoClave_validate==false)
                            {
                                editTex_Homoclave.setError(getString(R.string.error_invalid_homo_clave));
                                return;
                            }
                            else if(validateLengthFiel==false)
                            {
                                editTex_NumeroFiel.setError(getString(R.string.error_invalid_number_fiel));
                                return;
                            }
                        }
                    }
                }
                else
                {
                    if(curp_validate==true&&homoClave_validate==true&&validateLengthFiel==true)
                    {
                        insertComplementary();
                    }
                    else
                    {
                        if(curp_validate==false)
                        {
                            editTex_CURP.setError(getString(R.string.error_invalid_curp));
                            return;
                        }
                        else if(homoClave_validate==false)
                        {
                            editTex_Homoclave.setError(getString(R.string.error_invalid_homo_clave));
                            return;
                        }
                        else if(validateLengthFiel==false)
                        {
                            editTex_NumeroFiel.setError(getString(R.string.error_invalid_number_fiel));
                            return;
                        }
                    }
                }
            }
            else
            {
                Toast.makeText(getActivity(), R.string.error_name, Toast.LENGTH_LONG).show();
            }
        }
        else
        {
            Toast.makeText(getActivity(), R.string.error_empty_input, Toast.LENGTH_LONG).show();
        }
    }

    public void typeCheckNo() {
        boolean cancel = false;
        View focusView = null;
        // Reset errors.
        editTex_CURP.setError(null);
        editTex_Correo_Electronico.setError(null);
        editTex_Homoclave.setError(null);

        editTex_Homoclave.setText("");
        editTex_NumeroFiel.setText("");

        if (!TextUtils.isEmpty(editTex_CURP.getText().toString()) || !TextUtils.isEmpty(editTex_Dependientes_Economicos.toString()) || id_occupation > 0)
        {
            if (GlobalVariables.id_Name > 0)
            {
                if(!TextUtils.isEmpty(editTex_CURP.getText().toString()))
                {
                    curp_validate = GlobalVariables.curpValidate(editTex_CURP.getText().toString());
                }
                if (GlobalVariables.setServiceData == true)
                {
                    try
                    {
                        dbHandler = new DBHandler(getActivity());
                        GlobalVariables.complementary = dbHandler.GetComplementary(GlobalVariables.id_Name);
                    }
                    catch (CursorIndexOutOfBoundsException e)
                    {
                    }

                    if (GlobalVariables.complementary == null)
                    {
                        if(curp_validate==true)
                        {
                            insertComplementary();
                        }
                        else
                        {
                            editTex_CURP.setError(getString(R.string.error_invalid_curp));
                        }
                    }
                    else
                    {
                        if (curp_validate==true)
                        {
                            updateDataComplementary();
                        }
                        else
                        {
                            editTex_CURP.setError(getString(R.string.error_invalid_curp));
                        }
                    }
                }
                else
                {
                    if(curp_validate==true)
                    {
                        insertComplementary();
                    }
                    else
                    {
                        editTex_CURP.setError(getString(R.string.error_invalid_curp));
                    }
                }

            }
            else
            {
                Toast.makeText(getActivity(), R.string.error_name, Toast.LENGTH_LONG).show();
            }
        }
        else
        {
            Toast.makeText(getActivity(), R.string.error_empty_input, Toast.LENGTH_LONG).show();
        }
    }

    public void onEconomic()
    {
        try
        {
            if (GlobalVariables.id_Name > 0)
            {
                if (GlobalVariables.setServiceData == true) {
                    if (GlobalVariables.economic == null) {
                        insertEconomic();
                    } else {
                        updateDataEconomic();
                    }
                } else {
                    insertEconomic();
                }
            }
            else
            {
                Toast.makeText(getActivity(), R.string.error_name, Toast.LENGTH_LONG).show();
            }
        }
        catch (Exception ex)
        {

        }
    }

    public List listOccupation() {
        // Spinner Drop down elements type Ocupación
        List<String> list_Ocupacion = new ArrayList<String>();
        list_Ocupacion.add("OBRERO");
        list_Ocupacion.add("ABOGADO O ASESOR LEGAL");
        list_Ocupacion.add("ACTOR");
        list_Ocupacion.add("ACTUARIO O MATEMATICO");
        list_Ocupacion.add("ADMINISTRADOR");
        list_Ocupacion.add("AGENTE O CORREDOR");
        list_Ocupacion.add("AJUSTADOR");
        list_Ocupacion.add("ANALISTA FINANCIERO");
        list_Ocupacion.add("ARTESANO");
        list_Ocupacion.add("ASISTENTE TECNICO");
        list_Ocupacion.add("ASISTENTE EJECUTIVO");
        list_Ocupacion.add("ASISTENTE DE EDUCACION");
        list_Ocupacion.add("ATLETA");
        list_Ocupacion.add("AUXILIAR TECNICO");
        list_Ocupacion.add("AUXILIAR MEDICO");
        list_Ocupacion.add("AYUDANTE TECNICO");
        list_Ocupacion.add("AYUDANTES DE SERVICIOS");
        list_Ocupacion.add("AYUDANTE EN LA INDUSTRIA");
        list_Ocupacion.add("BAILARIN");
        list_Ocupacion.add("CIENTIFICO\n");
        list_Ocupacion.add("EMPLEADO BANCARIO\n");
        list_Ocupacion.add("FUNCIONARIO");
        list_Ocupacion.add("CANTANTE");
        list_Ocupacion.add("CAPACITADOR");
        list_Ocupacion.add("DENTISTA");
        list_Ocupacion.add("COCINERO");
        list_Ocupacion.add("CONDUCTOR");
        list_Ocupacion.add("CONSEJERO");
        list_Ocupacion.add("CONSULTOR");
        list_Ocupacion.add("CONTADOR O AUDITOR");
        list_Ocupacion.add("DIRECTOR");
        list_Ocupacion.add("DIRECTOR O GERENTE GENERAL");
        list_Ocupacion.add("DISEÑADOR");
        list_Ocupacion.add("ECONOMISTA");
        list_Ocupacion.add("EMPLEADO");
        list_Ocupacion.add("EMPLEADO PUBLICO");
        list_Ocupacion.add("EMPLEADO EN VENTAS");
        list_Ocupacion.add("ENFERMERA(O)");
        list_Ocupacion.add("ESCRITOR");
        list_Ocupacion.add("ESTILISTA O MASAJISTA");
        list_Ocupacion.add("FABRICANTE");
        list_Ocupacion.add("FOTOGRAFO");
        list_Ocupacion.add("FUNCIONARIO PUBLICO");
        list_Ocupacion.add("GERENTE DE SERVICIOS");
        list_Ocupacion.add("GERENTE OPERATIVO");
        list_Ocupacion.add("GERENTE PRODUCCION");
        list_Ocupacion.add("GERENTE");
        list_Ocupacion.add("GERENTE DE VENTAS");
        list_Ocupacion.add("GEOLOGOS, GEOQUIMICOS, GEOFISICOS Y GEOGRAFOS");
        list_Ocupacion.add("INGENIERO");
        list_Ocupacion.add("INSTRUCTOR");
        list_Ocupacion.add("INTERPRETE");
        list_Ocupacion.add("INVESTIGADOR");
        list_Ocupacion.add("JARDINERO");
        list_Ocupacion.add("JEFE DE SERVICIOS");
        list_Ocupacion.add("JEFE INDUSTRIAL");
        list_Ocupacion.add("JEFE TECNICO");
        list_Ocupacion.add("JEFE COMERCIAL");
        list_Ocupacion.add("JEFE DE SEGURIDAD");
        list_Ocupacion.add("JOYERO");
        list_Ocupacion.add("JUEZ");
        list_Ocupacion.add("LABORATORISTA");
        list_Ocupacion.add("LOCUTOR, COMENTARISTA, O CRONISTA DE RADIO O TV");
        list_Ocupacion.add("MECANICO INDUSTRIAL");
        list_Ocupacion.add("MECANICO");
        list_Ocupacion.add("EMPLEADO DE SERVICIOS");
        list_Ocupacion.add("METEOROLOGO");
        list_Ocupacion.add("MINERO");
        list_Ocupacion.add("MODELO O EDECAN");
        list_Ocupacion.add("EMPLEADO EN LA INDUSTRIA");
        list_Ocupacion.add("MEDICO");
        list_Ocupacion.add("MUSICO");
        list_Ocupacion.add("NOTARIO PUBLICO");
        list_Ocupacion.add("EMPLEADO DE LA CONSTRUCCION");
        list_Ocupacion.add("EMPLEADO DE SEGURIDAD");
        list_Ocupacion.add("MILITAR");
        list_Ocupacion.add("OPERADOR TECNICO");
        list_Ocupacion.add("OPERADOR INDUSTRIAL");
        list_Ocupacion.add("OPERADOR DE SERVICIOS");
        list_Ocupacion.add("PAGADORES Y COBRADORES");
        list_Ocupacion.add("PANADERO");
        list_Ocupacion.add("EMPRESARIO INDUSTRIAL");
        list_Ocupacion.add("EMPRESARIO AGRICOLA");
        list_Ocupacion.add("EMPRESARIO DE SERVICIOS");
        list_Ocupacion.add("EMPRESARIO DE LA CONSTRUCCION");
        list_Ocupacion.add("PERIODISTAS, REPORTERO O REDACTOR");
        list_Ocupacion.add("PESCADOR");
        list_Ocupacion.add("PILOTO");
        list_Ocupacion.add("PROFESOR EDUCACION MEDIA SUPERIOR");
        list_Ocupacion.add("PROFESORES EDUCACION BASICA");
        list_Ocupacion.add("PROGRAMADOR");
        list_Ocupacion.add("PROMOTOR DE SERVICIOS");
        list_Ocupacion.add("PSICOLOGO");
        list_Ocupacion.add("QUIMICO");
        list_Ocupacion.add("SECRETARIA");
        list_Ocupacion.add("SERVIDOR PUBLICO");
        list_Ocupacion.add("SOBRECARGO");
        list_Ocupacion.add("SOCIOLOGOS ANTROPOLOGOS E HISTORIADORES");
        list_Ocupacion.add("SUPERVISOR AVICOLA Y GANADERO");
        list_Ocupacion.add("SUPERVISOR DE LA CONSTRUCCION");
        list_Ocupacion.add("SUPERVISOR DE SERVICIOS");
        list_Ocupacion.add("SUPERVISOR INDUSTRIAL");
        list_Ocupacion.add("SUPERVISOR DE COMERCIO");
        list_Ocupacion.add("SUPERVISOR DE SEGURIDAD");
        list_Ocupacion.add("SUPERVISOR DE PROCESAMIENTO");
        list_Ocupacion.add("TRABAJADOR AGRICOLA");
        list_Ocupacion.add("TRABAJADOR DE LA INDUSTRIA\n");
        list_Ocupacion.add("TRADUCTOR");
        list_Ocupacion.add("TECNICO INDUSTRIAL");
        list_Ocupacion.add("TECNICO EN SERVICIOS");
        list_Ocupacion.add("TECNICO ESPECIALIZADO\n");
        list_Ocupacion.add("VALUADOR");
        list_Ocupacion.add("VENDEDOR");
        list_Ocupacion.add("VETERINARIO");
        list_Ocupacion.add("AMA DE CASA");
        list_Ocupacion.add("ESTUDIANTE");
        list_Ocupacion.add("DESEMPLEADO");
        list_Ocupacion.add("HOGAR");
        list_Ocupacion.add("JUBILADO");
        list_Ocupacion.add("ASALARIADO");
        list_Ocupacion.add("COMERCIANTE");
        list_Ocupacion.add("OTRO");
        list_Ocupacion.add("EXEMPLEADO");
        return list_Ocupacion;
    }

}
