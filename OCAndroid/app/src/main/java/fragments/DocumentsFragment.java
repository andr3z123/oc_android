package fragments;


import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import adapter.CustomRecyclerAdapterDocuments;
import data.GlobalVariables;
import model.Documents;
import nubaj.com.ocandroid.CameraActivity;
import nubaj.com.ocandroid.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class DocumentsFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<Documents> items;

    public DocumentsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_documents, container, false);

        mRecyclerView = (RecyclerView) root.findViewById(R.id.reciclador);

        readDocumentExistencet();

        items = new ArrayList<>();

        Documents documents = new Documents();
        documents.setTitulo_Documento("Identificación Oficial Frente");
        documents.setImagen_Documento(GlobalVariables.url0);

        items.add(documents);

        documents = new Documents();
        documents.setTitulo_Documento("Identificación Oficial Reverso");
        documents.setImagen_Documento(GlobalVariables.url1);

        items.add(documents);

        documents = new Documents();
        documents.setTitulo_Documento("Comprobante de domicilio");
        documents.setImagen_Documento(GlobalVariables.url2);

        items.add(documents);

        documents = new Documents();
        documents.setTitulo_Documento("Autorización buró");
        documents.setImagen_Documento(GlobalVariables.url3);

        items.add(documents);

        documents = new Documents();
        documents.setTitulo_Documento("CUFE");
        documents.setImagen_Documento(GlobalVariables.url4);

        items.add(documents);

        documents = new Documents();
        documents.setTitulo_Documento("Foto de negocio");
        documents.setImagen_Documento(GlobalVariables.url5);

        items.add(documents);

        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new GridLayoutManager(getActivity(), 2);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new CustomRecyclerAdapterDocuments(getActivity(), items);
        mRecyclerView.setAdapter(mAdapter);
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        mRecyclerView.getAdapter().notifyDataSetChanged();
        if (mRecyclerView.getAdapter() != null) {
            ((CustomRecyclerAdapterDocuments) mAdapter).setOnItemClickListener(new CustomRecyclerAdapterDocuments.MyClickListener() {
                @Override
                public void onItemClick(int position, View v) {
                    GlobalVariables.type_Document = position;
                    Intent intent = new Intent(getActivity(), CameraActivity.class);
                    startActivity(intent);
                }
            });
        }
    }

    void readDocumentExistencet() {

        GlobalVariables.url0 = new File(Environment.getExternalStorageDirectory() + GlobalVariables.url_Photo + "/" + GlobalVariables.type_Document0 + "_" + GlobalVariables.CURP + "/" + GlobalVariables.type_Document0 + "_" + GlobalVariables.CURP + ".jpg");
        GlobalVariables.url1 = new File(Environment.getExternalStorageDirectory() + GlobalVariables.url_Photo + "/" + GlobalVariables.type_Document1 + "_" + GlobalVariables.CURP + "/" + GlobalVariables.type_Document1 + "_" + GlobalVariables.CURP + ".jpg");
        GlobalVariables.url2 = new File(Environment.getExternalStorageDirectory() + GlobalVariables.url_Photo + "/" + GlobalVariables.type_Document2 + "_" + GlobalVariables.CURP + "/" + GlobalVariables.type_Document2 + "_" + GlobalVariables.CURP + ".jpg");
        GlobalVariables.url3 = new File(Environment.getExternalStorageDirectory() + GlobalVariables.url_Photo + "/" + GlobalVariables.type_Document3 + "_" + GlobalVariables.CURP + "/" + GlobalVariables.type_Document3 + "_" + GlobalVariables.CURP + ".jpg");
        GlobalVariables.url4 = new File(Environment.getExternalStorageDirectory() + GlobalVariables.url_Photo + "/" + GlobalVariables.type_Document4 + "_" + GlobalVariables.CURP + "/" + GlobalVariables.type_Document4 + "_" + GlobalVariables.CURP + ".jpg");
        GlobalVariables.url5 = new File(Environment.getExternalStorageDirectory() + GlobalVariables.url_Photo + "/" + GlobalVariables.type_Document5 + "_" + GlobalVariables.CURP + "/" + GlobalVariables.type_Document5 + "_" + GlobalVariables.CURP + ".jpg");
    }
}
