package fragments;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import adapter.Area1NothingSelectedSpinnerAdapter;
import data.GlobalVariables;
import implementation.FunctionJson;
import model.RequestName;
import model.RequestNameUser;
import model.ResponseNameUser;
import nubaj.com.ocandroid.ApplicantsActivity;
import nubaj.com.ocandroid.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ApplicantsTABFragment extends Fragment {

    private ApplicantsActivity.SectionsPagerAdapter mSectionsPagerAdapter;
    private int msYear, msMonth, msDay;
    private DatePickerDialog pickerDialog_Date;
    public static TabLayout tabLayout;
    public static ViewPager viewPager;
    private int[] tabIcons = {
            R.mipmap.ic_new_person,
            R.mipmap.ic_renewed,
            R.mipmap.ic_sync
    };

    public ApplicantsTABFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_applicants_tab, container, false);
        //GlobalVariables.load(getActivity());
        //mSectionsPagerAdapter = new ApplicantsActivity.SectionsPagerAdapter(getSupportFragmentManager());
        tabLayout = (TabLayout) root.findViewById(R.id.tabs);
        viewPager = (ViewPager) root.findViewById(R.id.container);
        //Pinta los titulos
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setAdapter(new SectionsPagerAdapter(getChildFragmentManager()));
        setupTabIcons();
        //setHasOptionsMenu(true);
        return root;
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static ApplicantsActivity.PlaceholderFragment newInstance(int sectionNumber) {
            ApplicantsActivity.PlaceholderFragment fragment = new ApplicantsActivity.PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_applicants, container, false);
            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
            return rootView;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).

            switch(position)
            {
                case 0:
                    return new NewsApplicantsFragment();
                case 1:
                    return new RenewsApplicantsFragment();
                case 2:
                    return new SynchronizedFragment();
            }
            return ApplicantsActivity.PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getString(R.string.title_news);
                case 1:
                    return getString(R.string.title_renews);
                case 2:
                    return "X";
            }
            return null;
        }
    }

    private void setupTabIcons() {
        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
        tabLayout.getTabAt(2).setIcon(tabIcons[2]);
    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_add) {
            GlobalVariables.setServiceData = false;
            if (GlobalVariables.phone != null && GlobalVariables.address !=null && GlobalVariables.additional != null){
                GlobalVariables.phone.clear();
                GlobalVariables.address.clear();
                GlobalVariables.additional.clear();
            }
            Intent intent = new Intent(getActivity(), ApplicantsActivity.class);
            startActivity(intent);
        }*/
        /*if (id == R.id.action_search_simple){
            //dialogFilter();
        }*/

        return super.onOptionsItemSelected(item);
    }

    void dialogFilter(){

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View layout = inflater.inflate(R.layout.custom_dialog_filter, null);

        EditText editText_Nombre = (EditText) layout.findViewById(R.id.editTextNombre);
        final TextView textView_Fecha_Alta = (TextView) layout.findViewById(R.id.textViewAltaFecha);
        Spinner spinner_Fuente_Origen = (Spinner) layout.findViewById(R.id.spinnerFuenteOrigen);
        Spinner spinner_Nivel_Atencion = (Spinner) layout.findViewById(R.id.spinnerNivelAtencion);

        builder.setTitle(R.string.prompt_dialog_title_filter);
        builder.setView(layout);

        editText_Nombre.getText();
        textView_Fecha_Alta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                instanceDate(textView_Fecha_Alta);
                pickerDialog_Date.show();
            }
        });

        // Spinner Drop down elements type Fuente/Origen
        List<String> list_Fuente_Origen = new ArrayList<String>();
        list_Fuente_Origen.add("Promoción directa");
        list_Fuente_Origen.add("Call center");
        list_Fuente_Origen.add("Ejecutivo de atención");
        list_Fuente_Origen.add("Portal web");
        list_Fuente_Origen.add("Exclientes");
        list_Fuente_Origen.add("Subsecuente");
        ColorStateList.valueOf(R.color.colorWhite);
        ArrayAdapter<String> adapter_Fuente_Origen = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, list_Fuente_Origen);
        adapter_Fuente_Origen.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_Fuente_Origen.setAdapter(adapter_Fuente_Origen);
        spinner_Fuente_Origen.setAdapter(new Area1NothingSelectedSpinnerAdapter(adapter_Fuente_Origen, R.layout.area_1_spinner_row_nothing_selected, getActivity()));
        spinner_Fuente_Origen.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });


        // Spinner Drop down elements type Producto
        List<String> list_Nivel_Atencion = new ArrayList<String>();
        list_Nivel_Atencion.add("Atención en tiempo");
        list_Nivel_Atencion.add("En limite de tiempo");
        list_Nivel_Atencion.add("Fuera de tiempo");

        ArrayAdapter<String> adapter_Nivel_Atencion = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, list_Nivel_Atencion);
        adapter_Nivel_Atencion.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_Nivel_Atencion.setAdapter(adapter_Nivel_Atencion);
        spinner_Nivel_Atencion.setAdapter(new Area1NothingSelectedSpinnerAdapter(adapter_Nivel_Atencion, R.layout.area_1_spinner_row_nothing_selected, getActivity()));

        spinner_Nivel_Atencion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        builder.setNegativeButton(R.string.prompt_cancel, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.setPositiveButton(R.string.prompt_acept, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    void instanceDate(final TextView textView_Date) {

        Calendar calendar_Date = Calendar.getInstance();

        pickerDialog_Date = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);

                msYear = year;
                msMonth = monthOfYear;
                msDay = dayOfMonth;

                textView_Date.setText(GlobalVariables.getFormatDate(msDay, msMonth, msYear));

            }

        }, calendar_Date.get(Calendar.YEAR), calendar_Date.get(Calendar.MONTH), calendar_Date.get(Calendar.DAY_OF_MONTH));
    }
}
