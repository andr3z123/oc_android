package fragments;


import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.CursorIndexOutOfBoundsException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import adapter.Area1NothingSelectedSpinnerAdapter;
import data.GlobalVariables;
import implementation.DBHandler;
import implementation.FunctionJson;
import model.Name;
import nubaj.com.ocandroid.DocumentsActivity;
import nubaj.com.ocandroid.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class NameFragment extends Fragment
{
    private static final String DPD_FROM    = "Datepickerdialogfrom";
    private View root;
    private int msYear, msMonth, msDay;
    protected DatePickerDialog pickerDialog_Date;
    private TextView textView_Fecha_Alta, textView_id_cliente, textView_id_BP;
    private EditText
            editText_Primer_Nombre,
            editText_Segundo_Nombre,
            editText_Apellido_Paterno,
            editText_Apellido_Materno;
    private int id_status, id_producto;
    private Spinner spinner_Status, spinner_Producto;
    private Gson gson;
    private FunctionJson functionJson;
    private DBHandler dbHandler;
    private Name name;
    private Calendar calendar;

    public NameFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_name, container, false);
        initialize();
        initializeCapitalLetters();

        if (GlobalVariables.setServiceData == true)
        {
            setDataName();
            if(TextUtils.isEmpty(textView_Fecha_Alta.getText().toString()))
            {
                calendar = Calendar.getInstance();
                textView_Fecha_Alta.setText(GlobalVariables.getFormatDate(calendar.get(Calendar.DAY_OF_MONTH),calendar.get(Calendar.MONTH),calendar.get(Calendar.YEAR)));
            }

            if(name!=null)
            {
                if(name.getIdStatus()<=0)
                {
                    spinner_Status.setSelection(1);
                    id_status = 1;
                }
            }
            else
            {
                spinner_Status.setSelection(1);
                id_status = 1;
            }
        }
        else
        {
            calendar = Calendar.getInstance();
            textView_Fecha_Alta.setText(GlobalVariables.getFormatDate(calendar.get(Calendar.DAY_OF_MONTH),calendar.get(Calendar.MONTH),calendar.get(Calendar.YEAR)));
            spinner_Status.setSelection(1);
            id_status = 1;
        }

        FloatingActionButton fab = (FloatingActionButton) root.findViewById(R.id.floatingActionButtonSave);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveAction();
            }
        });
        setHasOptionsMenu(true);
        return root;
    }

    private void initializeCapitalLetters() {
        GlobalVariables.capitalLetters(editText_Apellido_Materno);
        GlobalVariables.capitalLetters(editText_Apellido_Paterno);
        GlobalVariables.capitalLetters(editText_Primer_Nombre);
        GlobalVariables.capitalLetters(editText_Segundo_Nombre);
    }

    void initializeComponents() {

        editText_Primer_Nombre = (EditText) root.findViewById(R.id.editTexFirstName);
        editText_Segundo_Nombre = (EditText) root.findViewById(R.id.editTexSecondName);
        editText_Apellido_Paterno = (EditText) root.findViewById(R.id.editTexApellidoPaterno);
        editText_Apellido_Materno = (EditText) root.findViewById(R.id.editTexApellidoMaterno);

        spinner_Producto = (Spinner) root.findViewById(R.id.spinnerProducto);
        spinner_Status = (Spinner) root.findViewById(R.id.spinnerStatus);

        textView_Fecha_Alta = (TextView) root.findViewById(R.id.textViewFechaAlta);
        textView_id_BP = (TextView) root.findViewById(R.id.textViewIdBP);
        textView_id_cliente = (TextView) root.findViewById(R.id.textViewIdCliente);
    }

    void initialize() {

        initializeComponents();

        if (GlobalVariables.setServiceData == true) {
            textView_id_cliente.setVisibility(View.VISIBLE);
            textView_id_BP.setVisibility(View.VISIBLE);
        } else {
            textView_id_cliente.setVisibility(View.GONE);
            textView_id_BP.setVisibility(View.GONE);
        }
        /*textView_Fecha_Alta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                instanceDate(textView_Fecha_Alta);
                pickerDialog_Date.show();
            }
        });*/

        // Spinner Drop down elements type Producto
        List<String> list_Producto = new ArrayList<String>();
        list_Producto.add("Crédito Comerciante");
        list_Producto.add("Crédito Individual");
        list_Producto.add("Crédito Mujer");

        ArrayAdapter<String> adapter3 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, list_Producto);
        adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_Producto.setAdapter(adapter3);
        spinner_Producto.setAdapter(new Area1NothingSelectedSpinnerAdapter(adapter3, R.layout.area_1_spinner_row_nothing_selected, getContext()));
        spinner_Producto.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
            {
                if(i>0)
                {
                    id_producto = i;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        // Spinner Drop down elements type Status
        List<String> list_Status = new ArrayList<String>();
        list_Status.add("Proceso");
        list_Status.add("Declinado");
        list_Status.add("Acreditado");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, list_Status);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_Status.setAdapter(adapter);
        spinner_Status.setAdapter(new Area1NothingSelectedSpinnerAdapter(adapter, R.layout.area_1_spinner_row_nothing_selected, getContext()));
        spinner_Status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
            {
                if(i>0)
                {
                    id_status = i;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

    }

    void instanceDate(final TextView textView_Date)
    {

        Calendar calendar_Date = Calendar.getInstance();
        final SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        pickerDialog_Date = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener()
        {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
            {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                msYear = year;
                msMonth = monthOfYear;
                msDay = dayOfMonth;

                try
                {
                    Calendar beginDate = Calendar.getInstance();
                    long startMillis = beginDate.getTimeInMillis();
                    String str1 = formatter.format(startMillis).toString();

                    Calendar calendarFormatterDate = Calendar.getInstance();
                    calendarFormatterDate.set(msYear, msMonth, msDay);
                    long endMillis = calendarFormatterDate.getTimeInMillis();
                    String str2 = formatter.format(endMillis).toString();

                    Date dateToday = formatter.parse(str1);
                    Date dateToEnter = formatter.parse(str2);

                    if (dateToday.equals(dateToEnter)){
                        textView_Date.setText(GlobalVariables.getFormatDate(msDay, msMonth, msYear));
                    }else {
                        Toast.makeText(getActivity(), "La fecha debe ser actual", Toast.LENGTH_LONG).show();
                    }

                } catch (ParseException e) {
                    e.getStackTrace();
                }

            }

        }, calendar_Date.get(Calendar.YEAR), calendar_Date.get(Calendar.MONTH), calendar_Date.get(Calendar.DAY_OF_MONTH));
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_applicants, menu);
        menu.findItem(R.id.action_search_simple).setVisible(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_camera) {
            try
            {
                dbHandler = new DBHandler(getActivity());
                GlobalVariables.complementary = dbHandler.GetComplementary(GlobalVariables.id_Name);
                if(!TextUtils.isEmpty(GlobalVariables.CURP) && GlobalVariables.CURP!=null)
                {
                    GlobalVariables.CURP = GlobalVariables.complementary.getCurp();
                }
            } catch (CursorIndexOutOfBoundsException e) {

            }
            if(!TextUtils.isEmpty(GlobalVariables.CURP) && GlobalVariables.CURP!=null)
            {
                Intent intent = new Intent(getActivity(), DocumentsActivity.class);
                startActivity(intent);
            }
            else
            {
                Toast.makeText(getActivity(), R.string.message_camera, Toast.LENGTH_LONG).show();
            }
        }
        else if (id == R.id.action_save)
        {
            //saveAction();
        }
        else if (id == R.id.action_sync)
        {
            Toast.makeText(getActivity(), "Sincronizar", Toast.LENGTH_LONG).show();
        }
        else if (id == R.id.action_info)
        {
            infoAction();
        }

        return super.onOptionsItemSelected(item);
    }

    void saveAction() {
        // Use the Builder class for convenient dialog construction
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("¿Desea guardar el solicitante?")
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (!editText_Primer_Nombre.getText().toString().isEmpty()
                                //&& !editText_Segundo_Nombre.getText().toString().isEmpty()
                                || !editText_Apellido_Paterno.getText().toString().isEmpty()
                                || !editText_Apellido_Materno.getText().toString().isEmpty()
                                || id_status > 0
                            //&& id_producto > 0
                                ) {

                            if (GlobalVariables.setServiceData == true) {
                                updateDataName();
                            } else {
                                insertName();
                            }

                        } else {
                            Toast.makeText(getActivity(), getString(R.string.error_empty_input), Toast.LENGTH_LONG).show();
                        }
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //Action
                    }
                });
        // Create the AlertDialog object and return
        builder.show();
    }

    void infoAction() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Politicas de privacidad")
                .setItems(R.array.politica_privacidad, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // The 'which' argument contains the index position
                        // of the selected item
                    }
                });

        builder.show();
    }

    private void insertName()
    {
        Name name = new Name();
        try {
            name.setName1(editText_Primer_Nombre.getText().toString());
            name.setName2(editText_Segundo_Nombre.getText().toString());
            name.setLastName(editText_Apellido_Paterno.getText().toString());
            name.setmLastName(editText_Apellido_Materno.getText().toString());
            name.setIdUser(GlobalVariables.responseUser.getUser().getIdUSERS());
            name.setComplete(getString(R.string.value_false));
            name.setDateAdmission(textView_Fecha_Alta.getText().toString());
            name.setDayRet("40");
            name.setIdStatus(id_status);
            name.setIdProduct(id_producto);

            dbHandler = new DBHandler(getActivity());
            GlobalVariables.id_Name = dbHandler.AddName(name);
            //setDataName();
            getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(getActivity(), "Alta", Toast.LENGTH_LONG).show();
                    //GlobalVariables.isSaveName=true;
                }
            });


        } catch (Exception e) {
            getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(getActivity(), "Error", Toast.LENGTH_LONG).show();
                }
            });
            e.getStackTrace();
        }
    }

    void setDataName() {
        dbHandler = new DBHandler(getActivity());
        name = new Name();
        name = dbHandler.GetName(GlobalVariables.id_Name);

        editText_Primer_Nombre.setText(name.getName1());
        editText_Segundo_Nombre.setText(name.getName2());
        editText_Apellido_Paterno.setText(name.getLastName());
        editText_Apellido_Materno.setText(name.getmLastName());
        textView_Fecha_Alta.setText(name.getDateAdmission());
        spinner_Producto.setSelection(name.getIdProduct());
        spinner_Status.setSelection(name.getIdStatus());
        if(TextUtils.isEmpty(name.getIdNameServer()))
        {
            textView_id_BP.setText(String.valueOf(GlobalVariables.id_Name));
        }
        else
        {
            textView_id_BP.setText(String.valueOf(name.getIdNameServer()));
        }

    }

    void updateDataName() {
        dbHandler = new DBHandler(getActivity());
        Name name = new Name();
        name.setName1(editText_Primer_Nombre.getText().toString());
        name.setName2(editText_Segundo_Nombre.getText().toString());
        name.setLastName(editText_Apellido_Paterno.getText().toString());
        name.setmLastName(editText_Apellido_Materno.getText().toString());
        name.setIdUser(GlobalVariables.responseUser.getUser().getIdUSERS());
        if(Boolean.parseBoolean(name.getComplete())==false)
        {
            name.setComplete("false");
        }
        else
        {
            name.setComplete("true");
        }
        name.setIdName(String.valueOf(GlobalVariables.id_Name));
        name.setDateAdmission(textView_Fecha_Alta.getText().toString());
        name.setDayRet("40");
        name.setIdProduct(id_producto);
        name.setIdStatus(id_status);

        dbHandler.UpdateName(name);
        Snackbar.make(root, R.string.msg_save_change, Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }

}
