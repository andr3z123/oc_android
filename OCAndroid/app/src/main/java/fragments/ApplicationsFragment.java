package fragments;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import adapter.Area1NothingSelectedSpinnerAdapter;
import adapter.CustomRecyclerAdapterApplications;
import adapter.CustomRecyclerAdapterMember;
import data.GlobalVariables;
import data.InternetConnection;
import implementation.DBHandler;
import implementation.FunctionJson;
import model.Applications;
import model.GroupData;
import model.GroupDetail;
import model.Member;
import model.Members;
import model.Name;
import model.PlaceMeeting;
import model.RequestGroupData;
import model.RequestGroupDetail;
import model.RequestMembers;
import model.RequestPlaceMeeting;
import model.ResponseGroupData;
import model.ResponseGroupDetail;
import model.ResponseMembers;
import model.ResponsePlaceMeeting;
import nubaj.com.ocandroid.GroupRequestActivity;
import nubaj.com.ocandroid.MainActivity;
import nubaj.com.ocandroid.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ApplicationsFragment extends Fragment {

    private Dialog builder;
    private SearchView search;
    private ArrayList<Member> items1;
    private RecyclerView mRecyclerView, mRecyclerView1;
    private RecyclerView.Adapter mAdapter, mAdapter1;
    private RecyclerView.LayoutManager mLayoutManager, mLayoutManager1;
    private ArrayList<Applications> items;
    private View root;
    private int msYear, msMonth, msDay, idFlag=0;
    private DatePickerDialog pickerDialog_Date;
    private MenuItem searchViewItem;
    private RelativeLayout relative_Layout_Items_Zero;
    private DBHandler dbHandler;
    private FloatingActionButton floatingActionButtonSyncUp;
    private RequestGroupDetail requestGroupDetail;
    private boolean group_detail_flag=true, group_data_flag=true, members_flag=true, place_meeting_flag=true;
    private GroupData groupDataDB;
    private ArrayList<Members> membersDB;
    private PlaceMeeting placeMeetingDB;
    private ResponseGroupData responseGroupData;
    Applications applications;
    Gson gson;
    FunctionJson functionJson;

    public ApplicationsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_applications, container, false);
        //search = (SearchView) root.findViewById(R.id.searchViewFilter);
        mRecyclerView = (RecyclerView) root.findViewById(R.id.reciclador);
        relative_Layout_Items_Zero = (RelativeLayout) root.findViewById(R.id.relativeLayoutZeroItems);

        try
        {
            fillList();
        }
        catch (Exception e)
        {
            Toast.makeText(getActivity(), "Error", Toast.LENGTH_LONG).show();
            e.getStackTrace();
        }
        floatingActionButtonSyncUp = (FloatingActionButton) root.findViewById(R.id.floatingActionButtonSyncUp);
        floatingActionButtonSyncUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                if(InternetConnection.checkConnection(getActivity()))
                {
                    new ApplicationsFragment.request_sync_up().execute();
                }
                else
                {
                    Toast.makeText(getActivity(), "No hay conexión a internet", Toast.LENGTH_LONG).show();
                }
            }
        });
        return root;
    }

    void fillList()
    {
        items = new ArrayList<>();
        GlobalVariables.groupDetailArrayList = new ArrayList<GroupDetail>();
        try
        {
            dbHandler = new DBHandler(getActivity());
            GlobalVariables.groupDetailArrayList = dbHandler.GetAllGroupDetail(Integer.parseInt(GlobalVariables.responseUser.getUser().getIdUSERS()));


            if(GlobalVariables.groupDetailArrayList!=null && GlobalVariables.groupDetailArrayList.size()>0)
            {
                for(GroupDetail groupDetail: GlobalVariables.groupDetailArrayList)
                {
                    if(Boolean.parseBoolean(groupDetail.getComplete())==false)
                    {
                        applications = new Applications();
                        applications.setIcon_Tipo_Solicitud(R.mipmap.ic_launcher);
                        applications.setTitulo_Tipo_Solicitud("Solicitud");
                        applications.setTitulo_Grupo(groupDetail.getGroupName().toString());
                        applications.setFecha_Solicitud(groupDetail.getDate().toString());
                        items.add(applications);
                    }
                }
                if (items.size() != 0)
                {
                    mRecyclerView.setHasFixedSize(true);
                    mLayoutManager = new LinearLayoutManager(getActivity());
                    mRecyclerView.setLayoutManager(mLayoutManager);
                    mAdapter = new CustomRecyclerAdapterApplications(getActivity(), items);
                    mRecyclerView.setAdapter(mAdapter);
                    mAdapter.notifyDataSetChanged();
                    relative_Layout_Items_Zero.setVisibility(View.GONE);
                    mRecyclerView.setVisibility(View.VISIBLE);

                }
                else
                {
                    relative_Layout_Items_Zero.setVisibility(View.VISIBLE);
                    mRecyclerView.setVisibility(View.GONE);
                }

            }
            else
            {
                relative_Layout_Items_Zero.setVisibility(View.VISIBLE);
                mRecyclerView.setVisibility(View.GONE);
            }
            setHasOptionsMenu(true);
        }
        catch (Exception ex)
        {
            ex.getStackTrace();
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onResume()
    {
        super.onResume();
        fillList();
        getActivity().runOnUiThread(new Runnable()
        {
            public void run()
            {
                if (mRecyclerView.getAdapter() != null)
                {
                    ((CustomRecyclerAdapterApplications) mAdapter).setOnItemClickListener(new CustomRecyclerAdapterApplications
                            .MyClickListener()
                    {
                        @Override
                        public void onItemClick(int position, View v)
                        {

                            getGroupDetail(items.get(position).getTitulo_Grupo().toString());
                            Intent intent = new Intent(getActivity(), GroupRequestActivity.class);
                            startActivity(intent);
                        }
                    });
                }
            }
        });
    }

    void getGroupDetail(String name)
    {
        GlobalVariables.groupDetail = new GroupDetail();
        try
        {
            for(GroupDetail groupDetail : GlobalVariables.groupDetailArrayList)
            {
                if(groupDetail.getGroupName().equals(name))
                {
                    GlobalVariables.groupDetail = groupDetail;
                    break;
                }
            }
        }
        catch (Exception ex)
        {
            ex.getStackTrace();
        }
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.main, menu);

        menu.findItem(R.id.action_add).setVisible(true);

        searchViewItem = menu.findItem(R.id.action_search_simple);
        final SearchView searchViewAndroidActionBar = (SearchView) MenuItemCompat.getActionView(searchViewItem);
        searchViewAndroidActionBar.setOnQueryTextListener(searchFilterListener); // call the QuerytextListner.
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_add)
        {
            GlobalVariables.groupDetail = new GroupDetail();
            dialogTypeCredit();
        }
        if (id == R.id.action_search_simple) {
            //dialogFilter();
        }
        return super.onOptionsItemSelected(item);
    }

    public void dialogTypeCredit()
    {
            android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.dialog_title_selecionar_tipo_credito)
                .setItems(R.array.seleccionar_tipo_credito, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // The 'which' argument contains the index position
                        // of the selected item
                        switch (which){
                            /*case 0:
                                dialogSelectAplicant();
                                break;*/
                            case 0:
                                Intent intent = new Intent(getActivity(), GroupRequestActivity.class);
                                startActivity(intent);
                                break;
                            case 1:
                                intent = new Intent(getActivity(), GroupRequestActivity.class);
                                startActivity(intent);
                                break;
                        }
                    }
                });

        builder.show();
    }

    SearchView.OnQueryTextListener searchFilterListener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextChange(String query) {
            //query = query.toLowerCase();

            final ArrayList<Applications> filtered_List = new ArrayList<>();

            for (int i = 0; i < items.size(); i++) {

                final String text_Filter = items.get(i).getTitulo_Grupo().toString();
                //final String text = String.valueOf(items.get(i)).toString();
                if (text_Filter.toUpperCase().contains(query) || text_Filter.toLowerCase().contains(query)) {
                    filtered_List.add(items.get(i));
                }
            }

            mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            mAdapter = new CustomRecyclerAdapterApplications(getActivity(), filtered_List);
            mRecyclerView.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();  // data set changed
            return true;

        }
        public boolean onQueryTextSubmit(String query) {
            return false;
        }
    };

    SearchView.OnQueryTextListener searchFilterListener1 = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextChange(String query) {
            //query = query.toLowerCase();

            final ArrayList<Member> filtered_List = new ArrayList<>();

            for (int i = 0; i < items1.size(); i++) {

                final String text_Filter = items1.get(i).getNombre_Solicitante().toString();
                //final String text = String.valueOf(items.get(i)).toString();
                if (text_Filter.toUpperCase().contains(query) || text_Filter.toLowerCase().contains(query)) {
                    filtered_List.add(items1.get(i));
                }
            }

            mRecyclerView1.setLayoutManager(new LinearLayoutManager(getActivity()));
            mAdapter1 = new CustomRecyclerAdapterMember(getActivity(), filtered_List);
            mRecyclerView1.setAdapter(mAdapter1);
            mAdapter1.notifyDataSetChanged();  // data set changed
            return true;

        }
        public boolean onQueryTextSubmit(String query) {
            return false;
        }
    };

    void instanceDate(final TextView textView_Date) {

        Calendar calendar_Date = Calendar.getInstance();

        pickerDialog_Date = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);

                msYear = year;
                msMonth = monthOfYear;
                msDay = dayOfMonth;

                textView_Date.setText(GlobalVariables.getFormatDate(msDay, msMonth, msYear));

            }

        }, calendar_Date.get(Calendar.YEAR), calendar_Date.get(Calendar.MONTH), calendar_Date.get(Calendar.DAY_OF_MONTH));
    }

    public class request_sync_up extends AsyncTask<String, String, String>
    {
        ProgressDialog pDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setTitle(getString(R.string.message_title_applicants));
            pDialog.setMessage(getString(R.string.message_applicants));
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params)
        {
            request_sync_up();
            return "";
        }

        @Override
        protected void onPostExecute(String s)
        {
            //la respuesta del web services despues que se envio la información del movil por el web services
            super.onPostExecute(s);
            pDialog.dismiss();
            fillList();
        }

        private void request_sync_up()
        {
            dbHandler = new DBHandler(getActivity());
            ArrayList<GroupDetail> groupDetailArrayList = new ArrayList<GroupDetail>();
            boolean ok = false;
            gson = new GsonBuilder().create();
            functionJson = new FunctionJson();
            requestGroupDetail = new RequestGroupDetail();
            String json="",response="";
            try
            {
                groupDetailArrayList = dbHandler.GetAllGroupDetail(Integer.parseInt(GlobalVariables.responseUser.getUser().getIdUSERS()));
                if(groupDetailArrayList!=null)
                {
                    for (GroupDetail groupDetail : groupDetailArrayList)
                    {
                        group_detail_flag = true;
                        group_data_flag = true;
                        members_flag = true;
                        place_meeting_flag = true;
                        verifyGroupDetail(groupDetail);
                        if (Boolean.parseBoolean(groupDetail.getComplete()) == false)
                        {
                            if (group_detail_flag == true)
                            {
                                getGroupData(groupDetail.getIdGROUP_DETAIL());
                                getMembers(groupDetail.getIdGROUP_DETAIL());
                                getPlaceMeeting(groupDetail.getIdGROUP_DETAIL());
                                if (group_data_flag == true && members_flag == true && place_meeting_flag == true)
                                {
                                    requestGroupDetail.setId_user(GlobalVariables.responseUser.getUser().getIdUSERS());
                                    requestGroupDetail.setComplete(getString(R.string.value_true));
                                    requestGroupDetail.setGroup_cycle(groupDetail.getGroupCycle());
                                    requestGroupDetail.setGroup_name(groupDetail.getGroupName());
                                    requestGroupDetail.setId_channel_dispersion(groupDetail.getIdChannelDispersion());
                                    requestGroupDetail.setId_dispersion_medium(groupDetail.getIdDispersionMedium());
                                    requestGroupDetail.setId_sponsor(groupDetail.getIdSponsor());

                                    json = gson.toJson(requestGroupDetail);
                                    response = functionJson.RequestHttp(getString(R.string.url_insert_group_detail), FunctionJson.POST, json);
                                    if (!TextUtils.isEmpty(response))
                                    {
                                        gson = new GsonBuilder().create();
                                        GlobalVariables.groupDetailResponse = gson.fromJson(response, ResponseGroupDetail.class);
                                        groupDetail.setComplete(getString(R.string.value_true));
                                        groupDetail.setIdGroupDetailServer(GlobalVariables.groupDetailResponse.getGroup_detail().getIdGROUP_DETAIL());
                                        idFlag = updateGroupDetail(groupDetail);
                                        if (idFlag > 0)
                                        {
                                            functionJson = new FunctionJson();
                                            RequestMembers requestMembers = new RequestMembers();

                                            response = "";
                                            GlobalVariables.responseMembersArrayList = new ArrayList<ResponseMembers>();

                                            for (Members members : membersDB)
                                            {
                                                json = "";
                                                requestMembers.setId_group_detail(groupDetail.getIdGroupDetailServer());
                                                requestMembers.setComplete(getString(R.string.value_true));
                                                requestMembers.setId_name(members.getIdNameServer().toString());

                                                json = gson.toJson(requestMembers);
                                                response = functionJson.RequestHttp(getString(R.string.url_insert_members), FunctionJson.POST, json);
                                                gson = new GsonBuilder().create();
                                                ResponseMembers responseMembers = new ResponseMembers();
                                                responseMembers = gson.fromJson(response, ResponseMembers.class);
                                                GlobalVariables.responseMembersArrayList.add(responseMembers);;
                                                members.setComplete(getString(R.string.value_true));
                                                updateMembers(members);
                                            }
                                            if (GlobalVariables.responseMembersArrayList != null || GlobalVariables.responseMembersArrayList.size() > 0)
                                            {
                                                RequestPlaceMeeting requestPlaceMeeting = new RequestPlaceMeeting();
                                                response = "";
                                                GlobalVariables.responsePlaceMeeting = new ResponsePlaceMeeting();

                                                requestPlaceMeeting.setId_group_detail(groupDetail.getIdGroupDetailServer());
                                                requestPlaceMeeting.setAdministrative_area_level2(placeMeetingDB.getAdministrativeAreaLevel2());
                                                requestPlaceMeeting.setComments(placeMeetingDB.getComments());
                                                requestPlaceMeeting.setId_country(placeMeetingDB.getIdCountry());
                                                requestPlaceMeeting.setId_federal_entity(placeMeetingDB.getIdFederalEntity());
                                                requestPlaceMeeting.setId_phone_types(placeMeetingDB.getIdPhoneTypes());
                                                requestPlaceMeeting.setLocation_reference(placeMeetingDB.getLocationReference());
                                                requestPlaceMeeting.setNeighborhood(placeMeetingDB.getNeighborhood());
                                                requestPlaceMeeting.setPhone(placeMeetingDB.getPhone());
                                                requestPlaceMeeting.setPostal_code(placeMeetingDB.getPostalCode());
                                                requestPlaceMeeting.setRoute(placeMeetingDB.getRoute());
                                                requestPlaceMeeting.setRoute_reference1(placeMeetingDB.getRouteReference1());
                                                requestPlaceMeeting.setRoute_reference2(placeMeetingDB.getRouteReference2());
                                                requestPlaceMeeting.setStreet_number1(placeMeetingDB.getStreetNumber1());
                                                requestPlaceMeeting.setStreet_number2(placeMeetingDB.getStreetNumber2());
                                                json = gson.toJson(requestPlaceMeeting);
                                                response = functionJson.RequestHttp(getString(R.string.url_insert_place_meeting), FunctionJson.POST, json);
                                                gson = new GsonBuilder().create();
                                                ResponsePlaceMeeting responsePlaceMeeting = new ResponsePlaceMeeting();
                                                responsePlaceMeeting = gson.fromJson(response, ResponsePlaceMeeting.class);

                                                if (responsePlaceMeeting != null)
                                                {
                                                    RequestGroupData requestGroupData = new RequestGroupData();
                                                    response = "";
                                                    json = "";
                                                    requestGroupData.setAmount_fine_delay(groupDataDB.getAmountFineDelay());
                                                    requestGroupData.setAmount_missing_fine(groupDataDB.getAmountMissingFine());
                                                    requestGroupData.setComplete(getString(R.string.value_true));
                                                    requestGroupData.setDisbursement_date(groupDataDB.getDisbursementDate());
                                                    requestGroupData.setId_deadlines(groupDataDB.getIdDeadlines());
                                                    requestGroupData.setId_frequencies(groupDataDB.getIdFrequencies());
                                                    requestGroupData.setId_place_meeting(responsePlaceMeeting.getPlace_meeting().getIdPLACEMEETING());
                                                    requestGroupData.setId_product(groupDataDB.getIdProduct());
                                                    requestGroupData.setMeeting_date(groupDataDB.getMeetingDate());
                                                    requestGroupData.setMeeting_schedule(groupDataDB.getMeetingSchedule());
                                                    requestGroupData.setMinium_amount_savings(groupDataDB.getMiniumAmountSavings());
                                                    requestGroupData.setPay_date1(groupDataDB.getPayDate1());
                                                    requestGroupData.setId_group_detail(String.valueOf(groupDetail.getIdGroupDetailServer()));
                                                    for(Members members: membersDB)
                                                    {
                                                        if(members.getIdName().equals(groupDataDB.getIdNameGroupData()))
                                                        {
                                                            requestGroupData.setId_name(members.getIdNameServer());
                                                            break;
                                                        }
                                                    }
                                                    json = gson.toJson(requestGroupData);
                                                    response = functionJson.RequestHttp(getString(R.string.url_insert_group_data), FunctionJson.POST, json);
                                                    gson = new GsonBuilder().create();
                                                    responseGroupData = new ResponseGroupData();
                                                    responseGroupData = gson.fromJson(response, ResponseGroupData.class);
                                                }
                                                else
                                                {

                                                }
                                            }
                                        }
                                        getActivity().runOnUiThread(new Runnable() {
                                            public void run() {
                                                Toast.makeText(getActivity(), GlobalVariables.groupDetailResponse.getMensaje(), Toast.LENGTH_LONG).show();
                                            }
                                        });
                                    }
                                }
                                else
                                {
                                    if(members_flag==false && group_detail_flag==true)
                                    {
                                        getActivity().runOnUiThread(new Runnable() {
                                            public void run() {
                                                Toast.makeText(getActivity(), "Necesitas Sincronizar a los solicitantes antes de sincronizar las solicitudes", Toast.LENGTH_LONG).show();
                                            }
                                        });
                                    }
                                    else
                                    {
                                        getActivity().runOnUiThread(new Runnable() {
                                            public void run() {
                                                Toast.makeText(getActivity(), "Necesita tener todos los datos obligatorios registrados", Toast.LENGTH_LONG).show();
                                            }
                                        });
                                    }
                                }
                            }
                            else
                            {
                                getActivity().runOnUiThread(new Runnable() {
                                    public void run() {
                                        Toast.makeText(getActivity(), "Necesita tener todos los datos obligatorios registrados", Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                        }
                    }
                }
                else
                {
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(getActivity(), "No hay datos para sincronizar", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
            catch (Exception e)
            {
                e.getStackTrace();
            }
        }

        private void verifyGroupDetail(GroupDetail groupDetail)
        {
            try
            {
                if(groupDetail!=null)
                {
                    if(TextUtils.isEmpty(groupDetail.getGroupName()))
                    {
                        group_detail_flag=false;
                        return;
                    }
                    if(TextUtils.isEmpty(groupDetail.getIdChannelDispersion()))
                    {
                        group_detail_flag = false;
                        return;
                    }
                    if(TextUtils.isEmpty(groupDetail.getIdDispersionMedium()))
                    {
                        group_detail_flag=false;
                        return;
                    }
                    if(TextUtils.isEmpty(groupDetail.getGroupCycle()))
                    {
                        group_detail_flag=false;
                        return;
                    }
                }
                else
                {
                    group_detail_flag = false;
                    return;
                }
            }
            catch (Exception ex)
            {
                ex.getStackTrace();
            }
        }

        private GroupData getGroupData(String id)
        {
            groupDataDB = new GroupData();
            ArrayList<Name> nameArrayList = new ArrayList<Name>();
            try
            {
                groupDataDB = dbHandler.GetGroupData(Integer.parseInt(id));
                nameArrayList = dbHandler.GetAllListName(Integer.parseInt(GlobalVariables.responseUser.getUser().getIdUSERS()));
                if(groupDataDB!=null)
                {
                    /*for(Name name:nameArrayList)
                    {
                        if(name.getIdName().equals(groupDataDB.getIdNameGroupData()))
                        {
                            if(!TextUtils.isEmpty(name.getIdNameServer()))
                            {
                                groupDataDB.setIdNameGroupData(name.getIdNameServer());
                                dbHandler.UpdateGroupData(groupDataDB);
                            }
                        }
                    }*/

                    if(TextUtils.isEmpty(groupDataDB.getIdFrequencies()))
                    {
                        group_data_flag=false;
                        return null;
                    }
                    if(TextUtils.isEmpty(groupDataDB.getPayDate1()))
                    {
                        group_data_flag=false;
                        return null;
                    }
                    if(TextUtils.isEmpty(groupDataDB.getDisbursementDate()))
                    {
                        group_data_flag=false;
                        return null;
                    }
                    if(TextUtils.isEmpty(groupDataDB.getIdPlaceMeeting()))
                    {
                        group_data_flag=false;
                        return null;
                    }
                    if(TextUtils.isEmpty(groupDataDB.getAmountFineDelay()))
                    {
                        group_data_flag=false;
                        return null;
                    }
                    if(TextUtils.isEmpty(groupDataDB.getAmountMissingFine()))
                    {
                        group_data_flag=false;
                        return null;
                    }
                    if(TextUtils.isEmpty(groupDataDB.getMeetingDate()))
                    {
                        group_data_flag=false;
                        return null;
                    }
                    if(TextUtils.isEmpty(groupDataDB.getMeetingSchedule()))
                    {
                        group_data_flag=false;
                        return null;
                    }
                    if (TextUtils.isEmpty(groupDataDB.getIdPlaceMeeting()))
                    {
                        group_data_flag=false;
                        return null;
                    }
                    if(TextUtils.isEmpty(groupDataDB.getIdDeadlines()))
                    {
                        group_data_flag=false;
                        return null;
                    }
                }
                else
                {
                    group_data_flag = false;
                    return null;
                }
            }
            catch (Exception ex)
            {
                ex.getStackTrace();
            }
            return groupDataDB;
        }

        private ArrayList<Members> getMembers(String id)
        {
            membersDB = new ArrayList<Members>();
            ArrayList<Name> nameArrayList = new ArrayList<Name>();
            try
            {
                membersDB = dbHandler.GetListMembers(Integer.parseInt(id));
                nameArrayList = dbHandler.GetAllListName(Integer.parseInt(GlobalVariables.responseUser.getUser().getIdUSERS()));
                if(membersDB!=null)
                {
                    for (Members members: membersDB)
                    {
                        for(Name name:nameArrayList)
                        {
                            if(name.getIdName().equals(members.getIdName()))
                            {
                                if(!TextUtils.isEmpty(name.getIdNameServer()))
                                {
                                    members.setIdNameServer(name.getIdNameServer());
                                    dbHandler.UpdateMembers(members);
                                }
                            }
                        }
                    }

                    for (Members members : membersDB)
                    {
                        if (TextUtils.isEmpty(members.getIdGroupDetail())) {
                            members_flag = false;
                            return null;
                        }
                        if (TextUtils.isEmpty(members.getIdNameServer()))
                        {
                            members_flag = false;
                            return null;
                        }
                    }
                }
                else
                {
                    members_flag = false;
                    return null;
                }
            }
            catch (Exception ex)
            {
                ex.getStackTrace();
            }
            return membersDB;
        }

        private PlaceMeeting getPlaceMeeting(String id)
        {
            placeMeetingDB = new PlaceMeeting();
            try
            {
                placeMeetingDB = dbHandler.GetPlaceMeeting(Integer.parseInt(id));
                if(placeMeetingDB!=null)
                {
                    if(TextUtils.isEmpty(placeMeetingDB.getIdGroupDetail()))
                    {
                        place_meeting_flag=false;
                        return null;
                    }
                    if(TextUtils.isEmpty(placeMeetingDB.getAdministrativeAreaLevel2()))
                    {
                        place_meeting_flag=false;
                        return null;
                    }
                    if(TextUtils.isEmpty(placeMeetingDB.getIdCountry()))
                    {
                        place_meeting_flag=false;
                        return null;
                    }
                    if(TextUtils.isEmpty(placeMeetingDB.getIdFederalEntity()))
                    {
                        place_meeting_flag=false;
                        return null;
                    }
                    if(TextUtils.isEmpty(placeMeetingDB.getIdPhoneTypes()))
                    {
                        place_meeting_flag=false;
                        return null;
                    }
                    if(TextUtils.isEmpty(placeMeetingDB.getNeighborhood()))
                    {
                        place_meeting_flag=false;
                        return null;
                    }
                    if(TextUtils.isEmpty(placeMeetingDB.getPhone()))
                    {
                        place_meeting_flag=false;
                        return null;
                    }
                    if(TextUtils.isEmpty(placeMeetingDB.getPostalCode()))
                    {
                        place_meeting_flag=false;
                        return null;
                    }
                    if (TextUtils.isEmpty(placeMeetingDB.getRoute()))
                    {
                        place_meeting_flag=false;
                        return null;
                    }
                    if(TextUtils.isEmpty(placeMeetingDB.getStreetNumber1()))
                    {
                        place_meeting_flag=false;
                        return null;
                    }
                }
                else
                {
                    place_meeting_flag = false;
                    return null;
                }
            }
            catch (Exception ex)
            {
                ex.getStackTrace();
            }
            return placeMeetingDB;
        }

        private int updateMembers(Members members)
        {
            int id =0;
            try
            {
                id = dbHandler.UpdateMembers(members);
            }
            catch (Exception ex)
            {
                ex.getStackTrace();
            }

            return id;
        }

        private int updateGroupDetail(GroupDetail groupDetail)
        {
            int id = 0;
            try
            {
                id = dbHandler.UpdateGroupDetail(groupDetail);
            }
            catch (Exception ex)
            {
                ex.getStackTrace();
            }
            return id;
        }
    }
}
