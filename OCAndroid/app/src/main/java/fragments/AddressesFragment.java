package fragments;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.CursorIndexOutOfBoundsException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;

import adapter.CustomRecyclerAdapterAddresses;
import data.GlobalVariables;
import implementation.DBHandler;
import implementation.FunctionJson;
import model.Address;
import model.Addresses;
import model.RequestAddress;
import model.RequestName;
import model.ResponseName;
import nubaj.com.ocandroid.DocumentsActivity;
import nubaj.com.ocandroid.NewAddressActivity;
import nubaj.com.ocandroid.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddressesFragment extends Fragment {

    private View root;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<Addresses> items;
    public SearchView search;
    private Button button_nueva_direccion;
    private RelativeLayout relativeLayoutItemsZero;
    private RelativeLayout relativeLayoutInternet;
    private MenuItem searchViewItem;
    private TextView textViewInternet;
    private DBHandler dbHandler;
    private boolean is_add_address;

    public AddressesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_addresses, container, false);

        //search = (SearchView) root.findViewById(R.id.searchViewFilter);
        mRecyclerView = (RecyclerView) root.findViewById(R.id.reciclador);
        relativeLayoutItemsZero = (RelativeLayout) root.findViewById(R.id.relativeLayoutZeroItems);
        relativeLayoutInternet = (RelativeLayout) root.findViewById(R.id.relativeLayoutInternet);
        textViewInternet = (TextView) root.findViewById(R.id.textViewInternet);
        try {
            fillListhAddress();
        }catch (Exception e) {
            Toast.makeText(getActivity(), "Error", Toast.LENGTH_LONG).show();
            e.getStackTrace();
        }
        initializeComponents();
        initialize();
        setHasOptionsMenu(true);
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        fillListhAddress();
        if(mRecyclerView.getAdapter()!=null)
        {
            ((CustomRecyclerAdapterAddresses) mAdapter).setOnItemClickListener(new CustomRecyclerAdapterAddresses
                    .MyClickListener()
            {
                @Override
                public void onItemClick(int position, View v)
                {
                    GlobalVariables.state_New_Addrees_Boolean = true;
                    GlobalVariables.position = position;
                    Intent intent = new Intent(getActivity(), NewAddressActivity.class);
                    startActivity(intent);
                }
            });
        }
    }

    void initializeComponents(){
        button_nueva_direccion = (Button) root.findViewById(R.id.buttonAddAddresses);
    }

    void initialize(){
        button_nueva_direccion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                GlobalVariables.state_New_Addrees_Boolean=false;
                Intent intent = new Intent(getActivity(), NewAddressActivity.class);
                startActivity(intent);
            }
        });
    }

    SearchView.OnQueryTextListener searchFilterListener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextChange(String query) {
            //query = query.toLowerCase();

            final ArrayList<Addresses> filtered_List = new ArrayList<>();

            for (int i = 0; i < items.size(); i++) {

                final String text_Filter = items.get(i).getDomicilio().toString();
                //final String text = String.valueOf(items.get(i)).toString();
                if (text_Filter.toUpperCase().contains(query) || text_Filter.toLowerCase().contains(query)) {
                    filtered_List.add(items.get(i));
                }
            }

            mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            mAdapter = new CustomRecyclerAdapterAddresses(getActivity(), filtered_List);
            mRecyclerView.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();  // data set changed
            return true;

        }
        public boolean onQueryTextSubmit(String query) {
            return false;
        }
    };

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_applicants, menu);

        searchViewItem = menu.findItem(R.id.action_search_simple);
        final SearchView searchViewAndroidActionBar = (SearchView) MenuItemCompat.getActionView(searchViewItem);
        searchViewAndroidActionBar.setOnQueryTextListener(searchFilterListener); // call the QuerytextListner.
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_camera){
            try {
                dbHandler = new DBHandler(getActivity());
                GlobalVariables.complementary = dbHandler.GetComplementary(GlobalVariables.id_Name);
                if(!TextUtils.isEmpty(GlobalVariables.CURP) && GlobalVariables.CURP!=null)
                {
                    GlobalVariables.CURP = GlobalVariables.complementary.getCurp();
                }
            } catch (CursorIndexOutOfBoundsException e) {

            }
            if(!TextUtils.isEmpty(GlobalVariables.CURP) && GlobalVariables.CURP!=null){
                Intent intent = new Intent(getActivity(), DocumentsActivity.class);
                startActivity(intent);
            }
            else
            {
                Toast.makeText(getActivity(), R.string.message_camera, Toast.LENGTH_LONG).show();
            }
        }else if (id == R.id.action_save){
            saveAction();
        }else if (id == R.id.action_sync){
            Toast.makeText(getActivity(), "Sincronizar", Toast.LENGTH_LONG).show();
        }else if (id == R.id.action_info){
            infoAction();
        }
        return super.onOptionsItemSelected(item);
    }

    void saveAction(){
        // Use the Builder class for convenient dialog construction
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("¿Desea guardar el solicitante?")
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //Action
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //Action
                    }
                });
        // Create the AlertDialog object and return
        builder.show();
    }

    void infoAction(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Politicas de privacidad")
                .setItems(R.array.politica_privacidad, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // The 'which' argument contains the index position
                        // of the selected item
                    }
                });

        builder.show();
    }

    void fillListhAddress(){

        dbHandler = new DBHandler(getActivity());
        GlobalVariables.address = dbHandler.GetAllAddress(GlobalVariables.id_Name);

        if (GlobalVariables.setServiceData == false){
            if (GlobalVariables.address != null){
                if (GlobalVariables.is_add_address == false ) {
                    GlobalVariables.address.clear();
                }
            }
        }

        if(GlobalVariables.address == null)
        {
            mRecyclerView.setVisibility(View.GONE);
            relativeLayoutInternet.setVisibility(View.VISIBLE);
            //textViewInternet.setText(GlobalVariables.responseNameUser.getMensaje().toString());
            textViewInternet.setText("No se encontró información");
        }
        else
        {
            items = new ArrayList<>();
            if (GlobalVariables.address.size() > 0) {
                for (Address address : GlobalVariables.address) {
                    Addresses addresses = new Addresses();
                    addresses.setDomicilio(address.getAdministrativeAreaLevel2() + " " + address.getStreetNumber1() + " " + address.getNeighborhood());
                    String get_tipo_domicilio= String.valueOf(GlobalVariables.listTypeAddress().get(Integer.parseInt(address.getIdAddressTypes())));
                    addresses.setTipo_Domicilio(get_tipo_domicilio);
                    items.add(addresses);
                }
            }

            if (items.size() != 0) {
                mRecyclerView.setHasFixedSize(true);
                mLayoutManager = new LinearLayoutManager(getActivity());
                mRecyclerView.setLayoutManager(mLayoutManager);
                mAdapter = new CustomRecyclerAdapterAddresses(getActivity(), items);
                mAdapter.notifyDataSetChanged();
                mRecyclerView.setAdapter(mAdapter);
                if (GlobalVariables.isRefresh_addresses_list_boolean == true){
                    GlobalVariables.isRefresh_addresses_list_boolean = false;
                    getFragmentManager().beginTransaction().detach(this).attach(this).commit();
                }
                //setHasOptionsMenu(true);
            } else {
                //Toast.makeText(getActivity(), "No tiene conexión a internet", Toast.LENGTH_SHORT).show();
                mRecyclerView.setVisibility(View.GONE);
                relativeLayoutInternet.setVisibility(View.VISIBLE);
            }
        }
    }
}
