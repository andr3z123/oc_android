package fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import java.util.ArrayList;

import adapter.CustomRecyclerAdapterEconomicActivityCatalog;
import data.GlobalVariables;
import model.EconomicActivityCatalog;
import nubaj.com.ocandroid.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class EconomicActivityCatalogFragment extends Fragment {

    private View root;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<EconomicActivityCatalog> items;
    public SearchView search;

    public EconomicActivityCatalogFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_economic_activity_catalog, container, false);

        search = (SearchView) root.findViewById(R.id.searchViewFilter);
        mRecyclerView = (RecyclerView) root.findViewById(R.id.reciclador);

        // Inicializar
        items = new ArrayList<>();

        items.add(new EconomicActivityCatalog("EXTRACCIÓN Y BENEFICIO DE CARBON MINER", "756397569"));
        items.add(new EconomicActivityCatalog("EXPLORACCIÓN DE PETROLEO POR COMPADIAS", "142431661"));
        items.add(new EconomicActivityCatalog("EXTRACCIÓN DE YESO", "187489488"));
        items.add(new EconomicActivityCatalog("EXTRACCIÓN Y BENEFICIO DE ARENA Y GRAVA", "997375176"));

        GlobalVariables.economicActivityCatalogs = items;

        // Obtener el Recycler
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new CustomRecyclerAdapterEconomicActivityCatalog(getActivity(), items);
        mRecyclerView.setAdapter(mAdapter);
        search.setOnQueryTextListener(searchFilterListener); // call the QuerytextListner.

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(mRecyclerView.getAdapter()!=null)
        {
            ((CustomRecyclerAdapterEconomicActivityCatalog) mAdapter).setOnItemClickListener(new CustomRecyclerAdapterEconomicActivityCatalog
                    .MyClickListener()
            {
                @Override
                public void onItemClick(int position, View v)
                {
                    GlobalVariables.position_activity_economic = position;
                    GlobalVariables.economicActivityCatalogs.get(GlobalVariables.position_activity_economic).getDescripción();
                    GlobalVariables.economicActivityCatalogs.get(GlobalVariables.position_activity_economic).getId_Actividad_Economica();
                    GlobalVariables.activity_economic = position+1;
                    getActivity().finish();
                }
            });
        }
    }

    SearchView.OnQueryTextListener searchFilterListener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextChange(String query) {
            //query = query.toLowerCase();

            final ArrayList<EconomicActivityCatalog> filtered_List = new ArrayList<>();

            for (int i = 0; i < items.size(); i++) {

                final String text_Filter = items.get(i).getDescripción().toString();
                //final String text = String.valueOf(items.get(i)).toString();
                if (text_Filter.toUpperCase().contains(query) || text_Filter.toLowerCase().contains(query)) {
                    filtered_List.add(items.get(i));
                }
            }

            mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            mAdapter = new CustomRecyclerAdapterEconomicActivityCatalog(getActivity(), filtered_List);
            mRecyclerView.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();  // data set changed
            return true;

        }
        public boolean onQueryTextSubmit(String query) {
            return false;
        }
    };
}
