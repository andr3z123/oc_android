package fragments;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import adapter.Area1NothingSelectedSpinnerAdapter;
import data.GlobalVariables;
import implementation.DBHandler;
import model.Name;
import nubaj.com.ocandroid.FirmActivity;
import nubaj.com.ocandroid.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetailMemberFragment extends Fragment {

    private View root;
    private Dialog builder;
    private TextView textViewIdCliente, textViewTitleNombre, textViewTitleApellidoPaterno, textViewTitleApellidoMaterno;
    private DBHandler dbHandler;

    public DetailMemberFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_detail_member, container, false);
        initialize();
        setHasOptionsMenu(true);
        return root;
    }
    void initialize()
    {
        textViewIdCliente = (TextView) root.findViewById(R.id.textViewIdCliente);
        textViewTitleNombre = (TextView) root.findViewById(R.id.textViewNombre);
        textViewTitleApellidoPaterno = (TextView) root.findViewById(R.id.textViewApellidoPaterno);
        textViewTitleApellidoMaterno = (TextView) root.findViewById(R.id.textViewApellidoMaterno);

        textViewIdCliente.setText(GlobalVariables.name.getIdName());
        textViewTitleNombre.setText(GlobalVariables.name.getName1()+ " "+GlobalVariables.name.getName2());
        textViewTitleApellidoPaterno.setText(GlobalVariables.name.getLastName());
        textViewTitleApellidoMaterno.setText(GlobalVariables.name.getmLastName());
        Spinner spinner_Rol = (Spinner) root.findViewById(R.id.spinnerRol);

        // Spinner Drop down elements type Producto
        List<String> list_Rol = new ArrayList<String>();
        list_Rol.add("Representante");
        list_Rol.add("Integrante");
        list_Rol.add("Suplente");

        ArrayAdapter<String> adapter_Rol = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, list_Rol);
        adapter_Rol.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_Rol.setAdapter(adapter_Rol);
        spinner_Rol.setAdapter(new Area1NothingSelectedSpinnerAdapter(adapter_Rol, R.layout.area_1_spinner_row_nothing_selected, getContext()));
        spinner_Rol.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });


        final Spinner spinner_Monto_Credito = (Spinner) root.findViewById(R.id.spinnerMontoCredito);

        // Spinner Drop down elements type Status
        List<String> list_Monto_Credito = new ArrayList<String>();
        list_Monto_Credito.add("7000");
        list_Monto_Credito.add("7500");
        list_Monto_Credito.add("8000");
        list_Monto_Credito.add("8500");
        list_Monto_Credito.add("9000");
        list_Monto_Credito.add("9500");
        list_Monto_Credito.add("10000");
        list_Monto_Credito.add("10500");
        list_Monto_Credito.add("11000");
        list_Monto_Credito.add("11500");
        list_Monto_Credito.add("12000");
        list_Monto_Credito.add("12500");
        list_Monto_Credito.add("13000");
        list_Monto_Credito.add("13500");
        list_Monto_Credito.add("14000");
        list_Monto_Credito.add("14500");
        list_Monto_Credito.add("15000");
        list_Monto_Credito.add("15500");
        list_Monto_Credito.add("16000");

        ArrayAdapter<String> adapter_Monto_Credito = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, list_Monto_Credito);
        adapter_Monto_Credito.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_Monto_Credito.setAdapter(adapter_Monto_Credito);
        spinner_Monto_Credito.setAdapter(new Area1NothingSelectedSpinnerAdapter(adapter_Monto_Credito, R.layout.area_1_spinner_row_nothing_selected, getActivity()));
        spinner_Monto_Credito.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_detail_member, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_firm){
            dialogFirm();
        }else if(id == R.id.action_save){
            saveDialog();
        }
        return super.onOptionsItemSelected(item);
    }

    void saveDialog(){
        // Use the Builder class for convenient dialog construction
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("¿Desea guardar?")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //Action
                        //updateListMembers();
                        builder.setCancelable(false);
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //Action
                        builder.setCancelable(false);
                    }
                });
        // Create the AlertDialog object and return
        builder.show();
    }

    void updateListMembers()
    {
        dbHandler = new DBHandler(getActivity());
        Name name = new Name();
        try
        {
            name.setIdStatus(GlobalVariables.name.getIdStatus());
            name.setComplete(GlobalVariables.name.getComplete());
            name.setDayRet(GlobalVariables.name.getDayRet());
            name.setDateAdmission(GlobalVariables.name.getDateAdmission());
            name.setIdName(GlobalVariables.name.getIdName());
            name.setIdUser(GlobalVariables.name.getIdUser());
            name.setLastName(textViewTitleApellidoPaterno.getText().toString());
            name.setmLastName(textViewTitleApellidoMaterno.getText().toString());
            name.setName1(textViewTitleNombre.getText().toString());
            dbHandler.UpdateName(name);
            Toast.makeText(getActivity(),"Alta",Toast.LENGTH_SHORT).show();
        }
        catch (Exception ex)
        {
            Toast.makeText(getActivity(),"Error",Toast.LENGTH_SHORT).show();
            ex.getStackTrace();
        }
    }


    void dialogFirm(){

        final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View layout = inflater.inflate(R.layout.custom_dialog_firm, null);
        builder.setTitle(R.string.prompt_dialog_title_firm);

        builder.setView(layout);
        final Button button_Firma_Solicitante = (Button)layout.findViewById(R.id.buttonFirmaSolicitante);
        Button button_Visualizador = (Button)layout.findViewById(R.id.buttonVisualizador);
        Button button_Aviso_Privacidad = (Button)layout.findViewById(R.id.buttonAvisoPrivacidad);

        button_Firma_Solicitante.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), FirmActivity.class);
                startActivity(intent);
            }
        });

        builder.setNegativeButton(R.string.prompt_cancel, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.setPositiveButton(R.string.prompt_acept, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }
}
