package fragments;


import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.CursorIndexOutOfBoundsException;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.support.v7.widget.SearchView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import adapter.Area1NothingSelectedSpinnerAdapter;
import adapter.CustomRecyclerAdapterPhones;
import data.GlobalVariables;
import implementation.DBHandler;
import implementation.FunctionJson;
import model.NameUser;
import model.Phone;
import model.Phones;
import model.RequestName;
import model.RequestPhone;
import model.ResponseName;
import model.ResponsePhones;
import nubaj.com.ocandroid.ApplicantsActivity;
import nubaj.com.ocandroid.DocumentsActivity;
import nubaj.com.ocandroid.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class PhonesFragment extends Fragment {

    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE = 123;
    private Dialog builder;
    private Phones phones;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<Phones> items;
    private View root;
    private RelativeLayout relativeLayoutItemsZero;
    private RelativeLayout relativeLayoutInternet;
    private Button button_Agregar_Telefono;
    private MenuItem searchViewItem;
    private int position_Phone, id_phone_type;
    private Gson gson;
    private FunctionJson functionJson;
    private EditText editText_Telefono;
    private Spinner spinner_Tipo_Telefono;
    private View layout;
    private TextView textViewInternet;
    private Phone phone;
    private DBHandler dbHandler;
    private boolean is_add_phone = false;

    public PhonesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_phones, container, false);
        //getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        //search = (SearchView) root.findViewById(R.id.searchViewFilter);
        mRecyclerView = (RecyclerView) root.findViewById(R.id.reciclador);

        relativeLayoutItemsZero = (RelativeLayout) root.findViewById(R.id.relativeLayoutZeroItems);
        relativeLayoutInternet = (RelativeLayout) root.findViewById(R.id.relativeLayoutInternet);
        textViewInternet = (TextView) root.findViewById(R.id.textViewInternet);

        initializeComponents();
        initialize();

        fillListPhones();

        setHasOptionsMenu(true);
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        fillListPhones();
        if (mRecyclerView.getAdapter() != null) {
            ((CustomRecyclerAdapterPhones) mAdapter).setOnItemClickListener(new CustomRecyclerAdapterPhones
                    .MyClickListener() {
                @Override
                public void onItemClick(int position, View v)
                {
                    position_Phone = position;
                    /*int  has_call_phone_permission = getActivity().checkSelfPermission(Manifest.permission.CALL_PHONE);
                    if (has_call_phone_permission != PackageManager.PERMISSION_GRANTED)
                    {
                        requestPermissions(new String[] {Manifest.permission.CALL_PHONE}, MY_PERMISSIONS_REQUEST_CALL_PHONE);
                        return;
                    }
                    else
                    {
                        requestPermissions(new String[] {Manifest.permission.CALL_PHONE}, MY_PERMISSIONS_REQUEST_CALL_PHONE);
                        return;
                    }*/
                    if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED)
                    {
                        Intent sIntent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + items.get(position_Phone).getTelefono().toString()));
                        sIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(sIntent);
                    }
                    else
                    {
                        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE)
                                != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(getActivity(), new String[] { Manifest.permission.CALL_PHONE },
                                    MY_PERMISSIONS_REQUEST_CALL_PHONE);
                        }
                    }
                }
            });
        }
    }

    void initializeComponents() {
        button_Agregar_Telefono = (Button) root.findViewById(R.id.buttonAddPhone);
    }

    void initialize() {
        button_Agregar_Telefono.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogAddPhone();
            }
        });
    }


    void dialogAddPhone() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
        layout = inflater.inflate(R.layout.custom_dialog_add_phone, null);
        builder.setTitle(R.string.prompt_dialog_title_add_phone);

        editText_Telefono = (EditText) layout.findViewById(R.id.editTextTelefono);
        spinner_Tipo_Telefono = (Spinner) layout.findViewById(R.id.spinnerTipoTelefono);

        builder.setView(layout);

        // Spinner Drop down elements type Tipo telefono
        List<String> list_Tipo_Telefono = new ArrayList<String>();
        list_Tipo_Telefono.add("Casa");
        list_Tipo_Telefono.add("Celular");
        list_Tipo_Telefono.add("Negocio");
        list_Tipo_Telefono.add("Recados");
        list_Tipo_Telefono.add("Trabajo");

        ArrayAdapter<String> adapter_Tipo_Telefono = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, list_Tipo_Telefono);
        adapter_Tipo_Telefono.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_Tipo_Telefono.setAdapter(adapter_Tipo_Telefono);
        spinner_Tipo_Telefono.setAdapter(new Area1NothingSelectedSpinnerAdapter(adapter_Tipo_Telefono, R.layout.area_1_spinner_row_nothing_selected, getActivity()));
        spinner_Tipo_Telefono.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                id_phone_type = i;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        builder.setNegativeButton(R.string.prompt_cancel, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.setPositiveButton(R.string.prompt_acept, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (GlobalVariables.id_Name > 0)
                {
                    boolean validate_number_length_phone = GlobalVariables.validateLengthPhone(editText_Telefono.getText().toString());
                    if (validate_number_length_phone == true)
                    {
                        if (!editText_Telefono.getText().toString().isEmpty() && id_phone_type > 0)
                        {
                            insertPhone();
                            GlobalVariables.isRefresh_phones_list_boolean = true;
                            fillListPhones();

                        } else {
                            Toast.makeText(getActivity(), R.string.error_empty_input_phone, Toast.LENGTH_LONG).show();
                        }
                    }
                    else
                    {
                        Toast.makeText(getActivity(), R.string.error_phone_lenght, Toast.LENGTH_LONG).show();
                    }
                    dialog.cancel();
                }
                else
                {
                    Toast.makeText(getActivity(), R.string.error_name, Toast.LENGTH_LONG).show();
                }
            }
        });


        if (GlobalVariables.setServiceData == true)
        {
            setDataPhone();
        }

        builder.show();
    }

    private void insertPhone() {
        Phone phone = new Phone();
        String json = "", response = "";
        try
        {
            phone.setNumber(editText_Telefono.getText().toString());
            phone.setIdPhoneTypes(String.valueOf(id_phone_type));
            phone.setIdName(String.valueOf(GlobalVariables.id_Name)); // Id Name
            phone.setComplete(getString(R.string.value_true));
            dbHandler = new DBHandler(getActivity());
            dbHandler.AddPhones(phone);
            getActivity().runOnUiThread(new Runnable()
            {
                public void run()
                {
                    Toast.makeText(getActivity(), "Alta", Toast.LENGTH_LONG).show();
                    is_add_phone = true;
                }
            });

        }
        catch (Exception e)
        {
            getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(getActivity(), "Error", Toast.LENGTH_LONG).show();
                }
            });
            e.getStackTrace();
        }
    }

    SearchView.OnQueryTextListener searchFilterListener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextChange(String query) {
            //query = query.toLowerCase();

            final ArrayList<Phones> filtered_List = new ArrayList<>();

            for (int i = 0; i < items.size(); i++) {

                final String text_Filter = items.get(i).getTelefono().toString();
                //final String text = String.valueOf(items.get(i)).toString();
                if (text_Filter.toUpperCase().contains(query) || text_Filter.toLowerCase().contains(query)) {
                    filtered_List.add(items.get(i));
                }
            }

            mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            mAdapter = new CustomRecyclerAdapterPhones(getActivity(), filtered_List);
            mRecyclerView.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();  // data set changed
            return true;

        }

        public boolean onQueryTextSubmit(String query) {
            return false;
        }
    };

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_applicants, menu);

        searchViewItem = menu.findItem(R.id.action_search_simple);
        final SearchView searchViewAndroidActionBar = (SearchView) MenuItemCompat.getActionView(searchViewItem);
        searchViewAndroidActionBar.setOnQueryTextListener(searchFilterListener); // call the QuerytextListner.
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_camera)
        {
            try
            {
                dbHandler = new DBHandler(getActivity());
                GlobalVariables.complementary = dbHandler.GetComplementary(GlobalVariables.id_Name);
                if(!TextUtils.isEmpty(GlobalVariables.CURP) && GlobalVariables.CURP!=null)
                {
                    GlobalVariables.CURP = GlobalVariables.complementary.getCurp();
                }
            }
            catch (CursorIndexOutOfBoundsException e) {

            }
            if(!TextUtils.isEmpty(GlobalVariables.CURP) && GlobalVariables.CURP!=null)
            {
                Intent intent = new Intent(getActivity(), DocumentsActivity.class);
                startActivity(intent);
            }
            else
            {
                Toast.makeText(getActivity(), R.string.message_camera, Toast.LENGTH_LONG).show();
            }
        }
        else if (id == R.id.action_save)
        {
            saveAction();
        }
        else if (id == R.id.action_sync)
        {
            Toast.makeText(getActivity(), "Sincronizar", Toast.LENGTH_LONG).show();
        }
        else if (id == R.id.action_info)
        {
            infoAction();
        }
        return super.onOptionsItemSelected(item);
    }

    void saveAction() {
        // Use the Builder class for convenient dialog construction
        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity());
        builder.setMessage("¿Desea guardar el solicitante?")
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //Action
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //Action
                    }
                });
        // Create the AlertDialog object and return
        builder.show();
    }

    void infoAction() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity());
        builder.setTitle("Politicas de privacidad")
                .setItems(R.array.politica_privacidad, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // The 'which' argument contains the index position
                        // of the selected item
                    }
                });

        builder.show();
    }

    void setDataPhone()
    {
        if(GlobalVariables.phone!=null)
        {
            for (Phone phone : GlobalVariables.phone) {
                editText_Telefono.setText(GlobalVariables.phone.get(position_Phone).getNumber());
                spinner_Tipo_Telefono.setSelection(Integer.parseInt(GlobalVariables.phone.get(position_Phone).getIdPHONES()));
            }
        }

    }

    void updateDataPhone() {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL_PHONE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // All good!
                    Intent sIntent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + items.get(position_Phone).getTelefono().toString()));
                    sIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(sIntent);
                } else {
                    Toast.makeText(getActivity(), R.string.permission_call_phone, Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    void fillListPhones() {

        dbHandler = new DBHandler(getActivity());
        ArrayList<Phone> phone;
        phone = dbHandler.GetAllPhones(GlobalVariables.id_Name);

        if (GlobalVariables.setServiceData == false) {
            if (phone != null) {
                if (is_add_phone == false)
                {
                    if(GlobalVariables.phone!=null)
                    {
                        GlobalVariables.phone.clear();
                    }
                }
            }
        }

        if (phone == null) {
            mRecyclerView.setVisibility(View.GONE);
            relativeLayoutInternet.setVisibility(View.VISIBLE);
            //textViewInternet.setText(GlobalVariables.responseNameUser.getMensaje().toString());
            textViewInternet.setText("No se encontró información");
        } else {
            items = new ArrayList<>();
            if (phone.size() > 0) {
                for (Phone phone1 : phone)
                {
                    phones = new Phones();
                    //phones.setTipo("EMPRESA");
                    phones.setTelefono(phone1.getNumber());
                    phones.setIcon_telefono(R.mipmap.ic_phone_blue);
                    switch (Integer.parseInt(phone1.getIdPhoneTypes()))
                    {
                        case 1:
                            phones.setIcon_Contact(R.mipmap.ic_home_blue);
                            break;
                        case 2:
                            phones.setIcon_Contact(R.mipmap.ic_mobile_blue);
                            break;
                        case 3:
                            phones.setIcon_Contact(R.mipmap.ic_office_blue);
                            break;
                        case 4:
                            phones.setIcon_Contact(R.mipmap.ic_mobile_blue);
                            break;
                        case 5:
                            phones.setIcon_Contact(R.mipmap.ic_work_blue);
                            break;
                    }
                    items.add(phones);
                }
            }

            if (items.size() != 0)
            {
                mRecyclerView.setHasFixedSize(true);
                mLayoutManager = new LinearLayoutManager(getActivity());
                mRecyclerView.setLayoutManager(mLayoutManager);
                mAdapter = new CustomRecyclerAdapterPhones(getActivity(), items);
                mAdapter.notifyDataSetChanged();
                mRecyclerView.setAdapter(mAdapter);
                if (GlobalVariables.isRefresh_phones_list_boolean == true) {
                    GlobalVariables.isRefresh_phones_list_boolean = false;
                    getFragmentManager().beginTransaction().detach(this).attach(this).commit();
                }
                //setHasOptionsMenu(true);
            }
            else
            {
                //Toast.makeText(getActivity(), "No tiene conexión a internet", Toast.LENGTH_SHORT).show();
                mRecyclerView.setVisibility(View.GONE);
                relativeLayoutInternet.setVisibility(View.VISIBLE);
            }
        }
    }
}
