package fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.util.Size;
import android.util.SparseIntArray;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
import com.squareup.picasso.Picasso;

import data.GlobalVariables;
import implementation.ImageSurfaceView;
import nubaj.com.ocandroid.PreviewPhotoActivity;
import nubaj.com.ocandroid.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class CameraFragment extends Fragment {

    private View root;
    private String type_Document;
    private static final String TAG = "AndroidCameraApi";
    private Button takePictureButton;
    private TextureView textureView;
    private static final SparseIntArray ORIENTATIONS = new SparseIntArray();
    static {
        ORIENTATIONS.append(Surface.ROTATION_0, 90);
        ORIENTATIONS.append(Surface.ROTATION_90, 0);
        ORIENTATIONS.append(Surface.ROTATION_180, 270);
        ORIENTATIONS.append(Surface.ROTATION_270, 180);
    }
    private String cameraId;
    protected CameraDevice cameraDevice;
    protected CameraCaptureSession cameraCaptureSessions;
    protected CaptureRequest captureRequest;
    protected CaptureRequest.Builder captureRequestBuilder;
    private Size imageDimension;
    private ImageReader imageReader;
    private File file;
    private static final int REQUEST_CAMERA_PERMISSION = 200;
    private boolean mFlashSupported;
    private Handler mBackgroundHandler;
    private HandlerThread mBackgroundThread;

    public CameraFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_camera, container, false);

        textureView = (TextureView) root.findViewById(R.id.texture);
        assert textureView != null;
        textureView.setSurfaceTextureListener(textureListener);
        takePictureButton = (Button) root.findViewById(R.id.btn_takepicture);
        assert takePictureButton != null;
        //relativeLayoutPreview.setVisibility(View.GONE);
        /*takePictureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePicture();
            }
        });*/
        setHasOptionsMenu(true);
        return root;
    }

    TextureView.SurfaceTextureListener textureListener = new TextureView.SurfaceTextureListener() {
        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
            //open your camera here
            openCamera();
        }
        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
            // Transform you image captured size according to the surface width and height
        }
        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
            return false;
        }
        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surface) {
        }
    };
    private final CameraDevice.StateCallback stateCallback = new CameraDevice.StateCallback() {
        @Override
        public void onOpened(CameraDevice camera) {
            //This is called when the camera is open
            Log.e(TAG, "onOpened");
            cameraDevice = camera;
            createCameraPreview();
        }
        @Override
        public void onDisconnected(CameraDevice camera) {
            cameraDevice.close();
        }
        @Override
        public void onError(CameraDevice camera, int error) {
            cameraDevice.close();
            cameraDevice = null;
        }
    };
    final CameraCaptureSession.CaptureCallback captureCallbackListener = new CameraCaptureSession.CaptureCallback() {
        @Override
        public void onCaptureCompleted(CameraCaptureSession session, CaptureRequest request, TotalCaptureResult result) {
            super.onCaptureCompleted(session, request, result);
            Toast.makeText(getActivity(), "Saved:" + file, Toast.LENGTH_SHORT).show();
            createCameraPreview();
        }
    };
    protected void startBackgroundThread() {
        mBackgroundThread = new HandlerThread("Camera Background");
        mBackgroundThread.start();
        mBackgroundHandler = new Handler(mBackgroundThread.getLooper());
    }
    protected void stopBackgroundThread() {
        mBackgroundThread.quitSafely();
        try {
            mBackgroundThread.join();
            mBackgroundThread = null;
            mBackgroundHandler = null;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    protected void takePicture()
    {
        if(null == cameraDevice) {
            Log.e(TAG, "cameraDevice is null");
            return;
        }
        CameraManager manager = (CameraManager) getActivity().getSystemService(Context.CAMERA_SERVICE);
        try {
            CameraCharacteristics characteristics = manager.getCameraCharacteristics(cameraDevice.getId());
            Size[] jpegSizes = null;
            if (characteristics != null) {
                jpegSizes = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP).getOutputSizes(ImageFormat.JPEG);
            }
            int width = 480;
            int height = 320;
            if (jpegSizes != null && 0 < jpegSizes.length) {
                width = jpegSizes[0].getWidth();
                height = jpegSizes[0].getHeight();
            }
            ImageReader reader = ImageReader.newInstance(width, height, ImageFormat.JPEG, 1);
            List<Surface> outputSurfaces = new ArrayList<Surface>(2);
            outputSurfaces.add(reader.getSurface());
            outputSurfaces.add(new Surface(textureView.getSurfaceTexture()));
            final CaptureRequest.Builder captureBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);
            captureBuilder.addTarget(reader.getSurface());
            captureBuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);
            // Orientation
            String currentDateTime = generateTimestamp();
            int rotation = getActivity().getWindowManager().getDefaultDisplay().getRotation();
            captureBuilder.set(CaptureRequest.JPEG_ORIENTATION, ORIENTATIONS.get(rotation));

            switch (GlobalVariables.type_Document){
                case 0:
                    type_Document = "0";
                    break;
                case 1:
                    type_Document = "1";
                    break;
                case 2:
                    type_Document = "2";
                    break;
                case 3:
                    type_Document = "3";
                    break;
                case 4:
                    type_Document = "4";
                    break;
                case 5:
                    type_Document = "5";
                    break;
            }

            File fileDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + GlobalVariables.url_Photo + "/" + type_Document + "_" + GlobalVariables.CURP);
            if (!fileDir.exists())
            {
                if (fileDir.mkdirs())
                {
                    System.out.println("Directory is created!");
                }
                else
                {
                    System.out.println("Failed to create directory!");
                }
            }

            file = new File(Environment.getExternalStorageDirectory() + GlobalVariables.url_Photo + "/" + type_Document + "_" + GlobalVariables.CURP + "/" + type_Document + "_" + GlobalVariables.CURP + ".jpg");
            GlobalVariables.file = file;
            ImageReader.OnImageAvailableListener readerListener = new ImageReader.OnImageAvailableListener() {
                @Override
                public void onImageAvailable(ImageReader reader) {
                    Image image = null;
                    try {
                        image = reader.acquireLatestImage();
                        ByteBuffer buffer = image.getPlanes()[0].getBuffer();
                        byte[] bytes = new byte[buffer.capacity()];
                        buffer.get(bytes);
                        GlobalVariables.bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                        dialogPhoto(bytes);
                        //save(bytes);
                    } finally {
                        if (image != null) {
                            image.close();
                        }
                    }
                }
                /*private void save(byte[] bytes) throws IOException {
                    FileOutputStream output = null;
                    try {
                        output = new FileOutputStream(file);
                        output.write(bytes);
                    } finally {
                        if (null != output) {
                            output.close();
                        }
                    }
                }*/
            };
            reader.setOnImageAvailableListener(readerListener, mBackgroundHandler);
            final CameraCaptureSession.CaptureCallback captureListener = new CameraCaptureSession.CaptureCallback() {
                @Override
                public void onCaptureCompleted(CameraCaptureSession session, CaptureRequest request, TotalCaptureResult result) {
                    super.onCaptureCompleted(session, request, result);
                    //Toast.makeText(getActivity(), "Saved:" + file, Toast.LENGTH_SHORT).show();
                    //Toast.makeText(getActivity(), "Documento capturado", Toast.LENGTH_SHORT).show();
                    createCameraPreview();
                }
            };
            cameraDevice.createCaptureSession(outputSurfaces, new CameraCaptureSession.StateCallback() {
                @Override
                public void onConfigured(CameraCaptureSession session) {
                    try
                    {
                        session.capture(captureBuilder.build(), captureListener, mBackgroundHandler);
                        cameraDevice.close();
                    } catch (CameraAccessException e) {
                        e.printStackTrace();
                    }
                }
                @Override
                public void onConfigureFailed(CameraCaptureSession session) {
                }
            }, mBackgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }
    protected void createCameraPreview() {
        try {
            SurfaceTexture texture = textureView.getSurfaceTexture();
            assert texture != null;
            texture.setDefaultBufferSize(imageDimension.getWidth(), imageDimension.getHeight());
            Surface surface = new Surface(texture);
            captureRequestBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            captureRequestBuilder.addTarget(surface);
            cameraDevice.createCaptureSession(Arrays.asList(surface), new CameraCaptureSession.StateCallback(){
                @Override
                public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
                    //The camera is already closed
                    if (null == cameraDevice) {
                        return;
                    }
                    // When the session is ready, we start displaying the preview.
                    cameraCaptureSessions = cameraCaptureSession;
                    updatePreview();
                }
                @Override
                public void onConfigureFailed(@NonNull CameraCaptureSession cameraCaptureSession) {
                    Toast.makeText(getActivity(), "Configuration change", Toast.LENGTH_SHORT).show();
                }
            }, null);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }
    private void openCamera() {
        CameraManager manager = (CameraManager) getActivity().getSystemService(Context.CAMERA_SERVICE);
        Log.e(TAG, "is camera open");
        try {
            cameraId = manager.getCameraIdList()[0];
            CameraCharacteristics characteristics = manager.getCameraCharacteristics(cameraId);
            StreamConfigurationMap map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
            assert map != null;
            imageDimension = map.getOutputSizes(SurfaceTexture.class)[0];
            // Add permission for camera and let user grant the permission
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CAMERA_PERMISSION);
                return;
            }
            manager.openCamera(cameraId, stateCallback, null);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
        Log.e(TAG, "openCamera X");
    }
    protected void updatePreview() {
        if(null == cameraDevice) {
            Log.e(TAG, "updatePreview error, return");
        }
        captureRequestBuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);
        try {
            cameraCaptureSessions.setRepeatingRequest(captureRequestBuilder.build(), null, mBackgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }
    private void closeCamera() {
        if (null != cameraDevice) {
            cameraDevice.close();
            cameraDevice = null;
        }
        if (null != imageReader) {
            imageReader.close();
            imageReader = null;
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CAMERA_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                // close the app
                Toast.makeText(getActivity(), "Sorry!!!, you can't use this app without granting permission", Toast.LENGTH_LONG).show();
                getActivity().finish();
            }
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        Log.e(TAG, "onResume");
        startBackgroundThread();
        if (textureView.isAvailable()) {
            openCamera();
        } else {
            textureView.setSurfaceTextureListener(textureListener);
        }
    }
    @Override
    public void onPause() {
        Log.e(TAG, "onPause");
        //closeCamera();
        stopBackgroundThread();
        super.onPause();
    }

    private static String generateTimestamp() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss_SSS", Locale.US);
        return sdf.format(new Date());
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_camera, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_take_picture){
            takePicture();
            //dialogPhoto();
        }
        return super.onOptionsItemSelected(item);
    }

    void dialogPhoto(final byte[] bytes) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View layout = inflater.inflate(R.layout.custom_dialog_photo, null);
        builder.setTitle(R.string.prompt_dialog_title_photo);

        ImageView imageView_Preview_Photo = (ImageView) layout.findViewById(R.id.imageViewPhoto);

        imageView_Preview_Photo.setImageBitmap(GlobalVariables.bitmap);

        builder.setView(layout);

        builder.setNegativeButton(R.string.prompt_cancel, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.setPositiveButton(R.string.prompt_acept, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    save(bytes);
                    readDocumentExistencet();
                    getActivity().finish();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                //dialog.cancel();
            }
        });

        builder.show();
    }

    private void save(byte[] bytes) throws IOException {
        FileOutputStream output = null;
        try {
            output = new FileOutputStream(file);
            output.write(bytes);
        } finally {
            if (null != output) {
                output.close();
            }
        }
    }

    void converImageToPDF(String type_Document) {

        Document document = new Document();
        try {
            //PdfWriter.getInstance(document, new FileOutputStream(Environment.getExternalStorageDirectory() + GlobalVariables.url_Photo + "/" + type_Document + ".pdf")); //  Change pdf's name.
            PdfWriter.getInstance(document, new FileOutputStream(Environment.getExternalStorageDirectory() + GlobalVariables.url_Photo + "/" + type_Document + "_" + GlobalVariables.CURP + "/"  +  type_Document + "_" + GlobalVariables.CURP + ".pdf")); //  Change pdf's name.
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        document.open();
        com.itextpdf.text.Image img = null;  // Change image's name and extension.
        try {
            img = com.itextpdf.text.Image.getInstance(Environment.getExternalStorageDirectory() + GlobalVariables.url_Photo + "/" + type_Document + "_" + GlobalVariables.CURP + "/"  +  type_Document + "_" + GlobalVariables.CURP + ".jpg");
        } catch (BadElementException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        float scaler = ((document.getPageSize().getWidth() - document.leftMargin() - document.rightMargin() - 0) / img.getWidth()) * 100; // 0 means you have no indentation. If you have any, change it.
        img.scalePercent(scaler);
        img.setAlignment(com.itextpdf.text.Image.ALIGN_CENTER | com.itextpdf.text.Image.ALIGN_TOP);


        //img.setAlignment(Image.LEFT| Image.TEXTWRAP);

 /* float width = document.getPageSize().width() - document.leftMargin() - document.rightMargin();
 float height = document.getPageSize().height() - document.topMargin() - document.bottomMargin();
 img.scaleToFit(width, height)*/
        try {
            document.add(img);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        document.close();

    }


    void readDocumentExistencet()
    {
        GlobalVariables.url0 = new File(Environment.getExternalStorageDirectory() + GlobalVariables.url_Photo + "/" + GlobalVariables.type_Document0 + "_" + GlobalVariables.CURP + "/" + GlobalVariables.type_Document0 + "_" + GlobalVariables.CURP + ".jpg");
        GlobalVariables.url1 = new File(Environment.getExternalStorageDirectory() + GlobalVariables.url_Photo + "/" + GlobalVariables.type_Document1 + "_" + GlobalVariables.CURP + "/" + GlobalVariables.type_Document1 + "_" + GlobalVariables.CURP + ".jpg");
        GlobalVariables.url2 = new File(Environment.getExternalStorageDirectory() + GlobalVariables.url_Photo + "/" + GlobalVariables.type_Document2 + "_" + GlobalVariables.CURP + "/" + GlobalVariables.type_Document2 + "_" + GlobalVariables.CURP + ".jpg");
        GlobalVariables.url3 = new File(Environment.getExternalStorageDirectory() + GlobalVariables.url_Photo + "/" + GlobalVariables.type_Document3 + "_" + GlobalVariables.CURP + "/" + GlobalVariables.type_Document3 + "_" + GlobalVariables.CURP + ".jpg");
        GlobalVariables.url4 = new File(Environment.getExternalStorageDirectory() + GlobalVariables.url_Photo + "/" + GlobalVariables.type_Document4 + "_" + GlobalVariables.CURP + "/" + GlobalVariables.type_Document4 + "_" + GlobalVariables.CURP + ".jpg");
        GlobalVariables.url5 = new File(Environment.getExternalStorageDirectory() + GlobalVariables.url_Photo + "/" + GlobalVariables.type_Document5 + "_" + GlobalVariables.CURP + "/" + GlobalVariables.type_Document5 + "_" + GlobalVariables.CURP + ".jpg");

        if (GlobalVariables.url0.exists()) {
            converImageToPDF(GlobalVariables.type_Document0);
        }
        if (GlobalVariables.url1.exists()) {
            converImageToPDF(GlobalVariables.type_Document1);
        }
        if (GlobalVariables.url2.exists()) {
            converImageToPDF(GlobalVariables.type_Document2);
        }
        if (GlobalVariables.url3.exists()) {
            converImageToPDF(GlobalVariables.type_Document3);
        }
        if (GlobalVariables.url4.exists()) {
            converImageToPDF(GlobalVariables.type_Document4);
        }
        if (GlobalVariables.url5.exists()) {
            converImageToPDF(GlobalVariables.type_Document5);
        }
    }
}
