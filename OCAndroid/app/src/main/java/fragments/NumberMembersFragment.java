package fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.ArrayList;

import adapter.CustomRecyclerAdapterMember;
import adapter.CustomRecyclerAdapterNumberMembers;
import data.GlobalVariables;
import implementation.DBHandler;
import model.ListMembers;
import model.Member;
import model.Members;
import model.Name;
import model.NumberMembers;
import nubaj.com.ocandroid.InsuranceDetailActivity;
import nubaj.com.ocandroid.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class NumberMembersFragment extends Fragment {

    private ListMembers listMembersObject;
    private ArrayList<Members> membersArrayList;

    private View root;
    public SearchView search;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<ListMembers> items;
    private RelativeLayout relativeLayoutItemsZero;
    private RelativeLayout relativeLayoutInternet;
    private MenuItem searchViewItem;
    private DBHandler dbHandler;

    public NumberMembersFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_number_members, container, false);
        //search = (SearchView) root.findViewById(R.id.searchViewFilter);
        mRecyclerView = (RecyclerView) root.findViewById(R.id.reciclador);
        relativeLayoutItemsZero = (RelativeLayout) root.findViewById(R.id.relativeLayoutZeroItems);
        relativeLayoutInternet = (RelativeLayout) root.findViewById(R.id.relativeLayoutInternet);

        fillListMEmbers();

        setHasOptionsMenu(true);
        return root;
    }

    public void fillListMEmbers(){

        dbHandler = new DBHandler(getActivity());
        membersArrayList = new ArrayList<Members>();
        items = new ArrayList<ListMembers>();
        if(GlobalVariables.groupDetail != null) {
            if (GlobalVariables.getAllNameList == null || GlobalVariables.getAllNameList.size() == 0) {
                GlobalVariables.getAllNameList = new ArrayList<Name>();
                GlobalVariables.getAllNameList = dbHandler.GetAllListName(Integer.parseInt(GlobalVariables.responseUser.getUser().getIdUSERS()));
            }
            try {
                membersArrayList = dbHandler.GetListMembers(Integer.parseInt(GlobalVariables.groupDetail.getIdGROUP_DETAIL()));
                for (Members members : membersArrayList) {
                    for (Name name : GlobalVariables.getAllNameList) {
                        if (name.getIdName().equals(members.getIdName())) {
                            listMembersObject = new ListMembers();
                            listMembersObject.setNombre_Integrante_Grupo(name.getName1() + " " + name.getName2() + " " + name.getLastName() + " " + name.getmLastName());
                            items.add(listMembersObject);
                        }
                    }
                }
            } catch (Exception ex) {
                ex.getStackTrace();
            }
        }
        if (items.size() != 0)
        {
            mRecyclerView.setHasFixedSize(true);
            mLayoutManager = new LinearLayoutManager(getActivity());
            mRecyclerView.setLayoutManager(mLayoutManager);
            mAdapter = new CustomRecyclerAdapterNumberMembers(getActivity(), items);
            mRecyclerView.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();
            mRecyclerView.setVisibility(View.VISIBLE);
            relativeLayoutItemsZero.setVisibility(View.GONE);

        }
        else
        {
            mRecyclerView.setVisibility(View.GONE);
            //search.setVisibility(View.GONE);
            relativeLayoutItemsZero.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        fillListMEmbers();
        if(mRecyclerView.getAdapter()!=null)
        {
            ((CustomRecyclerAdapterNumberMembers) mAdapter).setOnItemClickListener(new CustomRecyclerAdapterNumberMembers
                    .MyClickListener()
            {
                @Override
                public void onItemClick(int position, View v)
                {
                    GlobalVariables.id_Name = Integer.parseInt(GlobalVariables.getAllNameList.get(position).getIdName());
                    Intent intent = new Intent(getActivity(), InsuranceDetailActivity.class);
                    startActivity(intent);
                }
            });
        }
    }

    SearchView.OnQueryTextListener searchFilterListener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextChange(String query) {
            //query = query.toLowerCase();

            final ArrayList<ListMembers> filtered_List = new ArrayList<>();

            for (int i = 0; i < items.size(); i++) {

                final String text_Filter = items.get(i).getNombre_Integrante_Grupo().toString();
                //final String text = String.valueOf(items.get(i)).toString();
                if (text_Filter.toUpperCase().contains(query) || text_Filter.toLowerCase().contains(query)) {
                    filtered_List.add(items.get(i));
                }
            }

            mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            mAdapter = new CustomRecyclerAdapterNumberMembers(getActivity(), filtered_List);
            mRecyclerView.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();  // data set changed
            return true;

        }
        public boolean onQueryTextSubmit(String query) {
            return false;
        }
    };


    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_search, menu);
        searchViewItem = menu.findItem(R.id.action_search_simple);
        final SearchView searchViewAndroidActionBar = (SearchView) MenuItemCompat.getActionView(searchViewItem);
        searchViewAndroidActionBar.setOnQueryTextListener(searchFilterListener); // call the QuerytextListner.
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        return super.onOptionsItemSelected(item);
    }

}
