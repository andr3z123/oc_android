package fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.support.v7.widget.SearchView;
import android.widget.RelativeLayout;
import java.util.ArrayList;
import adapter.CustomRecyclerAdapterInsurance;
import data.GlobalVariables;
import implementation.DBHandler;
import model.Applications;
import model.GroupDetail;
import model.Insurence;
import nubaj.com.ocandroid.NumberMembersActivity;
import nubaj.com.ocandroid.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class InsuranceFragment extends Fragment {

    private View root;
    public SearchView search;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<Insurence> items;
    private RelativeLayout relativeLayoutItemsZero;
    private RelativeLayout relativeLayoutInternet;
    private MenuItem searchViewItem;
    private DBHandler dbHandler;

    public InsuranceFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_insurance, container, false);
        //search = (SearchView) root.findViewById(R.id.searchViewFilter);
        mRecyclerView = (RecyclerView) root.findViewById(R.id.reciclador);
        relativeLayoutItemsZero = (RelativeLayout) root.findViewById(R.id.relativeLayoutZeroItems);
        relativeLayoutInternet = (RelativeLayout) root.findViewById(R.id.relativeLayoutInternet);

        fillListInsurence();
        setHasOptionsMenu(true);
        initialize();

        return root;
    }

    void fillListInsurence(){

        items = new ArrayList<>();
        GlobalVariables.groupDetailArrayList = new ArrayList<GroupDetail>();
        try
        {
            dbHandler = new DBHandler(getActivity());
            GlobalVariables.groupDetailArrayList = dbHandler.GetAllGroupDetail(Integer.parseInt(GlobalVariables.responseUser.getUser().getIdUSERS()));
            Insurence insurence;

            if(GlobalVariables.groupDetailArrayList!=null && GlobalVariables.groupDetailArrayList.size()>0)
            {
                for(GroupDetail groupDetail: GlobalVariables.groupDetailArrayList)
                {
                    insurence = new Insurence();
                    insurence.setIcon_Person_Status(R.mipmap.ic_launcher);
                    insurence.setTipo_Status("Solicitud");
                    insurence.setNombre_Grupo(groupDetail.getGroupName().toString());
                    insurence.setFecha("20/03/2016");
                    items.add(insurence);
                }
                if (items.size() != 0)
                {
                    mRecyclerView.setHasFixedSize(true);
                    mLayoutManager = new LinearLayoutManager(getActivity());

                    mRecyclerView.setLayoutManager(mLayoutManager);
                    mAdapter = new CustomRecyclerAdapterInsurance(getActivity(), items);
                    mRecyclerView.setAdapter(mAdapter);
                }
                else
                {
                    //relative_Layout_Items_Zero.setVisibility(View.VISIBLE);
                    mRecyclerView.setVisibility(View.GONE);
                }

            }
            else
            {
                //relative_Layout_Items_Zero.setVisibility(View.VISIBLE);
                mRecyclerView.setVisibility(View.GONE);
            }
            initialize();
            setHasOptionsMenu(true);
        }
        catch (Exception ex)
        {
            ex.getStackTrace();
        }
        /*dbHandler = new DBHandler(getActivity());
        GlobalVariables.secures = dbHandler.GetSecure(GlobalVariables.id_Name);

        items = new ArrayList<>();

        Insurence insurence = new Insurence();
        insurence.setIcon_Person_Status(R.mipmap.ic_launcher);
        insurence.setTipo_Status("Solicitud");
        insurence.setNombre_Grupo("Escuadron de la muerte");
        insurence.setFecha("12/04/2016");

        items.add(insurence);

        if (items.size() != 0) {
            mRecyclerView.setHasFixedSize(true);
            mLayoutManager = new LinearLayoutManager(getActivity());
            mRecyclerView.setLayoutManager(mLayoutManager);
            mAdapter = new CustomRecyclerAdapterInsurance(getActivity(), items);
            mRecyclerView.setAdapter(mAdapter);
            //search.setOnQueryTextListener(searchFilterListener); // call the QuerytextListner.

        } else {
            mRecyclerView.setVisibility(View.GONE);
            //search.setVisibility(View.GONE);
            relativeLayoutItemsZero.setVisibility(View.VISIBLE);
        }*/
    }

    @Override
    public void onResume() {
        super.onResume();
        fillListInsurence();
        if (mRecyclerView.getAdapter() != null) {
            ((CustomRecyclerAdapterInsurance) mAdapter).setOnItemClickListener(new CustomRecyclerAdapterInsurance.MyClickListener() {
                @Override
                public void onItemClick(int position, View v) {
                    getGroupDetail(items.get(position).getNombre_Grupo().toString());
                    Intent intent = new Intent(getActivity(), NumberMembersActivity.class);
                    startActivity(intent);
                }
            });
        }
    }

    void initialize() {
        /*Spinner spinner_Filtro = (Spinner) root.findViewById(R.id.spinnerFiltro);

        // Spinner Drop down elements type Producto
        List<String> list_Filtro = new ArrayList<String>();
        list_Filtro.add("Todas");
        list_Filtro.add("Solicitudes");
        list_Filtro.add("Renovaciones");
        list_Filtro.add("Busqueda");

        ArrayAdapter<String> adapter_Filtro = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, list_Filtro);
        adapter_Filtro.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_Filtro.setAdapter(adapter_Filtro);
        spinner_Filtro.setAdapter(new Area1NothingSelectedSpinnerAdapter(adapter_Filtro, R.layout.area_1_spinner_row_nothing_selected, getContext()));
        spinner_Filtro.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });*/
    }

    SearchView.OnQueryTextListener searchFilterListener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextChange(String query) {
            //query = query.toLowerCase();

            final ArrayList<Insurence> filtered_List = new ArrayList<>();

            for (int i = 0; i < items.size(); i++) {

                final String text_Filter = items.get(i).getNombre_Grupo().toString();
                //final String text = String.valueOf(items.get(i)).toString();
                if (text_Filter.toUpperCase().contains(query) || text_Filter.toLowerCase().contains(query)) {
                    filtered_List.add(items.get(i));
                }
            }

            mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            mAdapter = new CustomRecyclerAdapterInsurance(getActivity(), filtered_List);
            mRecyclerView.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();  // data set changed
            return true;

        }

        public boolean onQueryTextSubmit(String query) {
            return false;
        }
    };

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.main, menu);

        menu.findItem(R.id.action_add).setVisible(false);

        searchViewItem = menu.findItem(R.id.action_search_simple);
        final SearchView searchViewAndroidActionBar = (SearchView) MenuItemCompat.getActionView(searchViewItem);
        searchViewAndroidActionBar.setOnQueryTextListener(searchFilterListener); // call the QuerytextListner.
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search_simple) {
            // MenuItem searchViewItem = .findItem(R.id.action_search);
        }
        return super.onOptionsItemSelected(item);
    }

    void getGroupDetail(String name)
    {
        GlobalVariables.groupDetail = new GroupDetail();
        try
        {
            for(GroupDetail groupDetail : GlobalVariables.groupDetailArrayList)
            {
                if(groupDetail.getGroupName().equals(name))
                {
                    GlobalVariables.groupDetail = groupDetail;
                    break;
                }
            }
        }
        catch (Exception ex)
        {
            ex.getStackTrace();
        }
    }
}
