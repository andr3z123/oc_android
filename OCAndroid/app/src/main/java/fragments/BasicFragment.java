package fragments;


import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.CursorIndexOutOfBoundsException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import adapter.Area1NothingSelectedSpinnerAdapter;
import data.GlobalVariables;
import implementation.DBHandler;
import implementation.FunctionJson;
import model.Basics;
import model.RequestBasics;
import model.ResponseBasics;
import nubaj.com.ocandroid.DocumentsActivity;
import nubaj.com.ocandroid.EconomicActivityCatalogActivity;
import nubaj.com.ocandroid.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class BasicFragment extends Fragment {

    private Spinner
            spinner_Genero,
            spinner_Pais_Nacimiento,
            spinner_Entidad_Federativa,
            spinner_Nacionalidad,
            spinner_Estado_Civil,
            spinner_Tipo_Vivienda,
            spinner_Nivel_Escolar,
            spinner_Tipo_Local,
            spinner_Numero_Hijos;

    private EditText editText_Clave_Credencial_Elector, editText_Numero_Registro_Electoral;
    private TextView textView_Fecha_Nacimiento, textView_result;
    private Button button_Catalogo_Actividad_Economica;
    private int msYear, msMonth, msDay;
    private DatePickerDialog pickerDialog_Date;
    private View root;
    private Gson gson;
    private FunctionJson functionJson;
    static Basics basics;
    boolean claveElector_validate=true,validate_number_electoral=true, actividad_economica=false;

    private int id_civil_status,
            id_living_place,
            id_level_education,
            id_country_birth,
            id_federal_entity,
            id_nacionality,
            id_gender,
            sons,
            pep;
    DBHandler dbHandler;

    public BasicFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_basic, container, false);
        initialize();
        initializeCapitalLetters();

        FloatingActionButton fab = (FloatingActionButton) root.findViewById(R.id.floatingActionButtonSave);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveAction();
            }
        });
        setHasOptionsMenu(true);
        return root;
    }

    @Override
    public void onResume()
    {
        super.onResume();
        if(actividad_economica==true)
        {
            actividad_economica = false;
            fill_economic_catalog();
        }
    }

    private void initializeCapitalLetters() {
        GlobalVariables.capitalLetters(editText_Clave_Credencial_Elector);
    }


    public class insert_basics extends AsyncTask<String, String, String> {
        ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setTitle(getString(R.string.message_title_basics));
            pDialog.setMessage(getString(R.string.message_basics));
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            insertBasics();
            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            //la respuesta del web services despues que se envio la información del movil por el web services
            super.onPostExecute(s);
            pDialog.dismiss();

        }

        private void insertBasics() {
            Basics basics = new Basics();
            try
            {
                basics.setBirthdate(textView_Fecha_Nacimiento.getText().toString());
                basics.setVoterKey(editText_Clave_Credencial_Elector.getText().toString());
                basics.setNumRegVoter(editText_Numero_Registro_Electoral.getText().toString());
                basics.setIdCivilStatus(String.valueOf(id_civil_status));
                basics.setIdGender(String.valueOf(id_gender));
                basics.setIdCountryBirth(String.valueOf(id_country_birth));
                basics.setIdFederalEntity(String.valueOf(id_federal_entity));
                basics.setIdLevelEducation(String.valueOf(id_level_education));
                basics.setIdLivingPlace(String.valueOf(id_living_place));
                basics.setIdNationality(String.valueOf(id_nacionality));
                basics.setIdKindLocal(String.valueOf(pep));
                basics.setSons(String.valueOf(sons));
                basics.setIdName(String.valueOf(GlobalVariables.id_Name)); //Id Name
                basics.setComplete(getString(R.string.value_false));
                dbHandler = new DBHandler(getActivity());
                dbHandler.AddBasics(basics);

                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(getActivity(), "Alta", Toast.LENGTH_LONG).show();
                    }
                });


            } catch (Exception e) {
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(getActivity(), "Error", Toast.LENGTH_LONG).show();
                    }
                });
                e.getStackTrace();
            }
        }
    }

    void fill_economic_catalog()
    {
        try
        {
            if (GlobalVariables.economicActivityCatalogs != null)
            {
                String description = GlobalVariables.economicActivityCatalogs.get(GlobalVariables.position_activity_economic).getDescripción().toString();
                String id = GlobalVariables.economicActivityCatalogs.get(GlobalVariables.position_activity_economic).getId_Actividad_Economica().toString();
                textView_result.setText(id + "\n" + description);
            }
        }
        catch (Exception ex)
        {

        }
    }

    void initializeComponents() {

        editText_Clave_Credencial_Elector = (EditText) root.findViewById(R.id.editTexClaveCredencialElector);
        editText_Numero_Registro_Electoral = (EditText) root.findViewById(R.id.editTexNumeroRegistroElectoral);
        spinner_Genero = (Spinner) root.findViewById(R.id.spinnerGenero);
        spinner_Pais_Nacimiento = (Spinner) root.findViewById(R.id.spinnerPaisNacimiento);
        spinner_Entidad_Federativa = (Spinner) root.findViewById(R.id.spinnerEntidadFederativa);
        spinner_Nacionalidad = (Spinner) root.findViewById(R.id.spinnerNacionalidad);
        spinner_Estado_Civil = (Spinner) root.findViewById(R.id.spinnerEstadoCivil);
        spinner_Tipo_Vivienda = (Spinner) root.findViewById(R.id.spinnerTipoVivienda);
        spinner_Nivel_Escolar = (Spinner) root.findViewById(R.id.spinnerNivelEscolar);
        spinner_Tipo_Local = (Spinner) root.findViewById(R.id.spinnerTipoLocal);
        spinner_Numero_Hijos = (Spinner) root.findViewById(R.id.spinnerNumeroHijos);

        textView_Fecha_Nacimiento = (TextView) root.findViewById(R.id.textViewFechaNacimiento);
        textView_result = (TextView) root.findViewById(R.id.textViewResult);

        button_Catalogo_Actividad_Economica = (Button) root.findViewById(R.id.buttonCatalogoActividadEconomica);

    }

    void initialize() {

        initializeComponents();

        textView_Fecha_Nacimiento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                instanceDate(textView_Fecha_Nacimiento);
                pickerDialog_Date.show();
            }
        });
        if (GlobalVariables.economicActivityCatalogs != null)
        {
            String description = GlobalVariables.economicActivityCatalogs.get(GlobalVariables.position_activity_economic).getDescripción().toString();
            String id = GlobalVariables.economicActivityCatalogs.get(GlobalVariables.position_activity_economic).getId_Actividad_Economica().toString();
            textView_result.setText(id + "\n" + description);
        }
        button_Catalogo_Actividad_Economica.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                actividad_economica = true;
                Intent intent = new Intent(getActivity(), EconomicActivityCatalogActivity.class);
                startActivity(intent);
            }
        });

        // Spinner Drop down elements type Genero
        List<String> list_Genero = new ArrayList<String>();
        list_Genero.add("Femenino");
        list_Genero.add("Masculino");

        //spinnerInstance(list_Genero, spinner_Genero);
        ArrayAdapter<String> adapter_Genero = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, list_Genero);
        adapter_Genero.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_Genero.setAdapter(adapter_Genero);
        spinner_Genero.setAdapter(new Area1NothingSelectedSpinnerAdapter(adapter_Genero, R.layout.area_1_spinner_row_nothing_selected, getContext()));
        spinner_Genero.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
            {
                if(i>0)
                {
                    id_gender = i;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        //spinnerInstance(list_Genero, spinner_Genero);
        ArrayAdapter<String> adapter_Pais_Nacimiento = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, GlobalVariables.listCountry());
        adapter_Pais_Nacimiento.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_Pais_Nacimiento.setAdapter(adapter_Pais_Nacimiento);
        spinner_Pais_Nacimiento.setAdapter(new Area1NothingSelectedSpinnerAdapter(adapter_Pais_Nacimiento, R.layout.area_1_spinner_row_nothing_selected, getContext()));
        spinner_Pais_Nacimiento.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
            {
                if(i>0)
                {
                    id_country_birth = i;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        // Spinner Drop down elements type Entidad Federativa
        // Spinner Drop down elements Estado
        List<String> list_Entidad_Federativa = new ArrayList<String>();
        list_Entidad_Federativa.add("Aguascalientes");
        list_Entidad_Federativa.add("Baja California Norte");
        list_Entidad_Federativa.add("Baja California Sur");
        list_Entidad_Federativa.add("Campeche");
        list_Entidad_Federativa.add("Coahuila");
        list_Entidad_Federativa.add("Colima");
        list_Entidad_Federativa.add("Chiapas");
        list_Entidad_Federativa.add("Chihuahua");
        list_Entidad_Federativa.add("CD MX");
        list_Entidad_Federativa.add("Durango");
        list_Entidad_Federativa.add("Guanajuato");
        list_Entidad_Federativa.add("Guerrero");
        list_Entidad_Federativa.add("Hidalgo");
        list_Entidad_Federativa.add("Jalisco");
        list_Entidad_Federativa.add("México");
        list_Entidad_Federativa.add("Michoacán");
        list_Entidad_Federativa.add("Morelos");
        list_Entidad_Federativa.add("Nayarit");
        list_Entidad_Federativa.add("Nuevo León");
        list_Entidad_Federativa.add("Oaxaca");
        list_Entidad_Federativa.add("Puebla");
        list_Entidad_Federativa.add("Querétaro");
        list_Entidad_Federativa.add("Quintana Roo");
        list_Entidad_Federativa.add("San Luis Potosí");
        list_Entidad_Federativa.add("Sinaloa");
        list_Entidad_Federativa.add("Sonora");
        list_Entidad_Federativa.add("Tabasco");
        list_Entidad_Federativa.add("Tamaulipas");
        list_Entidad_Federativa.add("Tlaxcala");
        list_Entidad_Federativa.add("Veracruz");
        list_Entidad_Federativa.add("Yucatán");
        list_Entidad_Federativa.add("Zacatecas");

        //spinnerInstance(list_Entidad_Federativa, spinner_Entidad_Federativa);
        ArrayAdapter<String> adapter_Entidad_Federativa = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, list_Entidad_Federativa);
        adapter_Entidad_Federativa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_Entidad_Federativa.setAdapter(adapter_Entidad_Federativa);
        spinner_Entidad_Federativa.setAdapter(new Area1NothingSelectedSpinnerAdapter(adapter_Entidad_Federativa, R.layout.area_1_spinner_row_nothing_selected, getContext()));
        spinner_Entidad_Federativa.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
            {
                if(i>0)
                {
                    id_federal_entity = i;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        // Spinner Drop down elements type Genero
        List<String> list_Nacionalidad = new ArrayList<String>();
        list_Nacionalidad.add("Mexicana");

        ArrayAdapter<String> adapter_Nacionalidad = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, list_Nacionalidad);
        adapter_Nacionalidad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_Nacionalidad.setAdapter(adapter_Nacionalidad);
        spinner_Nacionalidad.setAdapter(new Area1NothingSelectedSpinnerAdapter(adapter_Nacionalidad, R.layout.area_1_spinner_row_nothing_selected, getContext()));
        spinner_Nacionalidad.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
            {
                if(i>0)
                {
                    id_nacionality = i;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        // Spinner Drop down elements type Estado Civil
        List<String> list_Estado_Civil = new ArrayList<String>();
        list_Estado_Civil.add("Soltero");
        list_Estado_Civil.add("Casado");
        list_Estado_Civil.add("Viudo");
        list_Estado_Civil.add("Divorciado");
        list_Estado_Civil.add("Union Libre");
        list_Estado_Civil.add("Padre Soltero");

        ArrayAdapter<String> adapter_Estado_Civil = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, list_Estado_Civil);
        adapter_Estado_Civil.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_Estado_Civil.setAdapter(adapter_Estado_Civil);
        spinner_Estado_Civil.setAdapter(new Area1NothingSelectedSpinnerAdapter(adapter_Estado_Civil, R.layout.area_1_spinner_row_nothing_selected, getContext()));
        spinner_Estado_Civil.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
            {
                if(i>0)
                {
                    id_civil_status = i;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        // Spinner Drop down elements type Estado Civil
        List<String> list_Nivel_Escolar = new ArrayList<String>();
        list_Nivel_Escolar.add("Sin Estudios");
        list_Nivel_Escolar.add("Primaria");
        list_Nivel_Escolar.add("Secundaria");
        list_Nivel_Escolar.add("Preparatoria");
        list_Nivel_Escolar.add("Tecnica");
        list_Nivel_Escolar.add("Profesional");
        list_Nivel_Escolar.add("Maestria");
        list_Nivel_Escolar.add("Doctorado");
        list_Nivel_Escolar.add("Otro");

        ArrayAdapter<String> adapter_Nivel_Escolar = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, list_Nivel_Escolar);
        adapter_Nivel_Escolar.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_Nivel_Escolar.setAdapter(adapter_Nivel_Escolar);
        spinner_Nivel_Escolar.setAdapter(new Area1NothingSelectedSpinnerAdapter(adapter_Nivel_Escolar, R.layout.area_1_spinner_row_nothing_selected, getContext()));
        spinner_Nivel_Escolar.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
            {
                if(i>0)
                {
                    id_level_education = i;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        // Spinner Drop down elements type Estado Civil
        List<String> list_Tipo_Vivienda = new ArrayList<String>();
        list_Tipo_Vivienda.add("Propia");
        list_Tipo_Vivienda.add("Rentada");
        list_Tipo_Vivienda.add("Familiar");
        list_Tipo_Vivienda.add("Pagando");

        ArrayAdapter<String> adapter_Tipo_Vivienda = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, list_Tipo_Vivienda);
        adapter_Tipo_Vivienda.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_Tipo_Vivienda.setAdapter(adapter_Tipo_Vivienda);
        spinner_Tipo_Vivienda.setAdapter(new Area1NothingSelectedSpinnerAdapter(adapter_Tipo_Vivienda, R.layout.area_1_spinner_row_nothing_selected, getContext()));
        spinner_Tipo_Vivienda.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
            {
                if(i>0)
                {
                    id_living_place = i;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        // Spinner Drop down elements type Estado Civil
        List<String> list_Tipo_Local = new ArrayList<String>();
        list_Tipo_Local.add("Establecido");
        list_Tipo_Local.add("Semiestablecido");
        list_Tipo_Local.add("No establecido");

        ArrayAdapter<String> adapter_Tipo_Local = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, list_Tipo_Local);
        adapter_Tipo_Local.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_Tipo_Local.setAdapter(adapter_Tipo_Local);
        spinner_Tipo_Local.setAdapter(new Area1NothingSelectedSpinnerAdapter(adapter_Tipo_Local, R.layout.area_1_spinner_row_nothing_selected, getContext()));
        spinner_Tipo_Local.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
            {

                pep = i;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        // Spinner Drop down elements type Estado Civil
        List<String> list_Numero_Hijos = new ArrayList<String>();
        list_Numero_Hijos.add("0");
        list_Numero_Hijos.add("1");
        list_Numero_Hijos.add("3");
        list_Numero_Hijos.add("4");
        list_Numero_Hijos.add("5");
        list_Numero_Hijos.add("6");
        list_Numero_Hijos.add("7");
        list_Numero_Hijos.add("8");
        list_Numero_Hijos.add("9");
        list_Numero_Hijos.add("10");
        list_Numero_Hijos.add("11");
        list_Numero_Hijos.add("12");
        list_Numero_Hijos.add("13");
        list_Numero_Hijos.add("14");
        list_Numero_Hijos.add("15");
        list_Numero_Hijos.add("16");
        list_Numero_Hijos.add("17");
        list_Numero_Hijos.add("18");
        list_Numero_Hijos.add("19");
        list_Numero_Hijos.add("20");

        ArrayAdapter<String> adapter_Numero_Hijos = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, list_Numero_Hijos);
        adapter_Numero_Hijos.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_Numero_Hijos.setAdapter(adapter_Numero_Hijos);
        spinner_Numero_Hijos.setAdapter(new Area1NothingSelectedSpinnerAdapter(adapter_Numero_Hijos, R.layout.area_1_spinner_row_nothing_selected, getContext()));
        spinner_Numero_Hijos.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
            {
                if(i>0)
                {
                    sons = i;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        if (GlobalVariables.setServiceData == true)
        {
            setDataBasic();
        }
    }

    void instanceDate(final TextView textView_Date) {

        Calendar calendar_Date = Calendar.getInstance();

        pickerDialog_Date = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);

                msYear = year;
                msMonth = monthOfYear;
                msDay = dayOfMonth;
                SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                try {

                    Calendar todayDate = Calendar.getInstance();
                    long todayMillis = todayDate.getTimeInMillis();
                    String str2 = formatter.format(todayMillis).toString();

                    Calendar birth_Dat = Calendar.getInstance();
                    birth_Dat.set(msYear, msMonth, msDay);
                    long enterMillis = birth_Dat.getTimeInMillis();
                    String str1 = formatter.format(enterMillis).toString();

                    Date current_Date = formatter.parse(str2);
                    Date birth_Date = formatter.parse(str1);

                    int year_Age = todayDate.get(Calendar.YEAR) - birth_Dat.get(Calendar.YEAR);
                    int month_Age = todayDate.get(Calendar.MONTH) - birth_Dat.get(Calendar.MONTH);
                    int day_Age = todayDate.get(Calendar.DAY_OF_MONTH) - birth_Dat.get(Calendar.DAY_OF_MONTH);

                    if (month_Age < 0 || (month_Age == 0 && day_Age < 0)) {
                        year_Age--;
                    }

                    //int años = calcularEdad(str1);

                    if (current_Date.after(birth_Date) && year_Age > 18) {
                        textView_Date.setText(GlobalVariables.getFormatDate(msDay, msMonth, msYear));
                    } else {
                        Toast.makeText(getActivity(), "Seleccione una fecha de nacimiento valida", Toast.LENGTH_LONG).show();
                    }

                } catch (ParseException e) {
                    e.getStackTrace();
                }
            }

        }, calendar_Date.get(Calendar.YEAR), calendar_Date.get(Calendar.MONTH), calendar_Date.get(Calendar.DAY_OF_MONTH));
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_applicants, menu);
        menu.findItem(R.id.action_search_simple).setVisible(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_camera) {
            try {
                dbHandler = new DBHandler(getActivity());
                GlobalVariables.complementary = dbHandler.GetComplementary(GlobalVariables.id_Name);
                if(!TextUtils.isEmpty(GlobalVariables.CURP) && GlobalVariables.CURP!=null)
                {
                    GlobalVariables.CURP = GlobalVariables.complementary.getCurp();
                }
            } catch (CursorIndexOutOfBoundsException e) {

            }
            if(!TextUtils.isEmpty(GlobalVariables.CURP) && GlobalVariables.CURP!=null) {
                Intent intent = new Intent(getActivity(), DocumentsActivity.class);
                startActivity(intent);
            } else {
                Toast.makeText(getActivity(), R.string.message_camera, Toast.LENGTH_LONG).show();
            }
        } else if (id == R.id.action_save) {
            //saveAction();
        } else if (id == R.id.action_sync) {
            Toast.makeText(getActivity(), "Sincronizar", Toast.LENGTH_LONG).show();
        } else if (id == R.id.action_info) {
            infoAction();
        }

        return super.onOptionsItemSelected(item);
    }

    void saveAction() {
        // Use the Builder class for convenient dialog construction
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("¿Desea guardar el solicitante?")
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //Action
                        boolean cancel = false;
                        View focusView = null;
                        // Reset errors.
                        textView_Fecha_Nacimiento = (TextView) root.findViewById(R.id.textViewFechaNacimiento);
                        editText_Numero_Registro_Electoral.setError(null);
                        editText_Clave_Credencial_Elector.setError(null);
                        if(!TextUtils.isEmpty(editText_Clave_Credencial_Elector.getText().toString()))
                        {
                            claveElector_validate = GlobalVariables.claveElectorValidate(editText_Clave_Credencial_Elector.getText().toString());
                        }
                        if(!TextUtils.isEmpty(editText_Numero_Registro_Electoral.getText().toString()))
                        {
                            validate_number_electoral = GlobalVariables.validateLengthNumberElectoral(editText_Numero_Registro_Electoral.getText().toString());
                        }

                        if (id_gender > 0
                                || !TextUtils.isEmpty(textView_Fecha_Nacimiento.getText().toString())
                                || !TextUtils.isEmpty(editText_Clave_Credencial_Elector.getText().toString())
                                || !TextUtils.isEmpty(editText_Numero_Registro_Electoral.getText().toString())
                                || id_federal_entity > 0
                                || id_nacionality > 0
                                || id_civil_status > 0
                                || sons > 0
                                || id_level_education > 0
                                || id_living_place > 0
                                || pep > 0
                                )
                        {

                            if (GlobalVariables.id_Name > 0)
                            {
                                if (GlobalVariables.setServiceData == true)
                                {
                                    try
                                    {
                                        dbHandler = new DBHandler(getActivity());
                                        GlobalVariables.basics = dbHandler.GetBasics(GlobalVariables.id_Name);
                                    }
                                    catch (CursorIndexOutOfBoundsException e)
                                    {

                                    }
                                    if (GlobalVariables.basics == null)
                                    {
                                        if(claveElector_validate==true && validate_number_electoral==true)
                                        {
                                            new BasicFragment.insert_basics().execute();
                                        }
                                        else
                                        {
                                            if(claveElector_validate==false)
                                            {
                                                editText_Clave_Credencial_Elector.setError(getString(R.string.error_invalid_key_ine));
                                                focusView = editText_Clave_Credencial_Elector;
                                                cancel = true;
                                            }
                                            else if(validate_number_electoral==false)
                                            {
                                                Toast.makeText(getActivity(), R.string.error_registro_electoral_lenght, Toast.LENGTH_LONG).show();
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if(claveElector_validate==true && validate_number_electoral==true)
                                        {
                                            updateDataBasic();
                                        }
                                        else
                                        {
                                            if(claveElector_validate==false)
                                            {
                                                editText_Clave_Credencial_Elector.setError(getString(R.string.error_invalid_key_ine));
                                                focusView = editText_Clave_Credencial_Elector;
                                                cancel = true;
                                            }
                                            else if(validate_number_electoral==false)
                                            {
                                                Toast.makeText(getActivity(), R.string.error_registro_electoral_lenght, Toast.LENGTH_LONG).show();
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if(claveElector_validate==true && validate_number_electoral==true)
                                    {
                                        new BasicFragment.insert_basics().execute();
                                    }
                                    else
                                    {
                                        if(claveElector_validate==false)
                                        {
                                            editText_Clave_Credencial_Elector.setError(getString(R.string.error_invalid_key_ine));
                                            focusView = editText_Clave_Credencial_Elector;
                                            cancel = true;
                                        }
                                        else if(validate_number_electoral==false)
                                        {
                                            Toast.makeText(getActivity(), R.string.error_registro_electoral_lenght, Toast.LENGTH_LONG).show();
                                        }
                                    }
                                }
                            }
                            else
                            {
                                Toast.makeText(getActivity(), R.string.error_name, Toast.LENGTH_LONG).show();
                            }
                            claveElector_validate = true;
                            validate_number_electoral = true;
                        }
                        else
                        {
                            Toast.makeText(getActivity(), R.string.error_empty_input, Toast.LENGTH_LONG).show();
                        }
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //Action
                    }
                });
        // Create the AlertDialog object and return
        builder.show();
    }

    void infoAction() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Politicas de privacidad")
                .setItems(R.array.politica_privacidad, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // The 'which' argument contains the index position
                        // of the selected item
                    }
                });

        builder.show();
    }

    void setDataBasic()
    {
        dbHandler = new DBHandler(getActivity());
        basics = new Basics();
        try
        {
            basics = dbHandler.GetBasics(GlobalVariables.id_Name);
            textView_Fecha_Nacimiento.setText(basics.getBirthdate());
            editText_Clave_Credencial_Elector.setText(basics.getVoterKey());
            editText_Numero_Registro_Electoral.setText(basics.getNumRegVoter());
            spinner_Genero.post(new Runnable() {
                @Override
                public void run()
                {
                    spinner_Genero.setSelection(Integer.parseInt(basics.getIdGender()));
                }
            });
            spinner_Pais_Nacimiento.post(new Runnable() {
                @Override
                public void run()
                {
                    spinner_Pais_Nacimiento.setSelection(Integer.parseInt(basics.getIdCountryBirth()));
                }
            });
            spinner_Entidad_Federativa.post(new Runnable() {
                @Override
                public void run()
                {
                    spinner_Entidad_Federativa.setSelection(Integer.parseInt(basics.getIdFederalEntity()));
                }
            });
            spinner_Nacionalidad.post(new Runnable() {
                @Override
                public void run()
                {
                    spinner_Nacionalidad.setSelection(Integer.parseInt(basics.getIdNationality()));
                }
            });
            spinner_Estado_Civil.post(new Runnable() {
                @Override
                public void run()
                {
                    spinner_Estado_Civil.setSelection(Integer.parseInt(basics.getIdCivilStatus()));
                }
            });
            spinner_Numero_Hijos.post(new Runnable() {
                @Override
                public void run()
                {
                    spinner_Numero_Hijos.setSelection(Integer.parseInt(basics.getSons()));
                }
            });
            spinner_Nivel_Escolar.post(new Runnable() {
                @Override
                public void run()
                {
                    spinner_Nivel_Escolar.setSelection(Integer.parseInt(basics.getIdLevelEducation()));
                }
            });
            spinner_Tipo_Vivienda.post(new Runnable() {
                @Override
                public void run()
                {
                    spinner_Tipo_Vivienda.setSelection(Integer.parseInt(basics.getIdLivingPlace()));
                }
            });
            spinner_Tipo_Local.post(new Runnable() {
                @Override
                public void run()
                {
                    spinner_Tipo_Local.setSelection(Integer.parseInt(basics.getIdKindLocal()));
                }
            });
        } catch (CursorIndexOutOfBoundsException e) {

        }
    }

    void updateDataBasic() {
        dbHandler = new DBHandler(getActivity());
        Basics basicsUpdate = new Basics();
        basicsUpdate.setIdBASICS(basics.getIdBASICS());
        basicsUpdate.setBirthdate(textView_Fecha_Nacimiento.getText().toString());
        basicsUpdate.setVoterKey(editText_Clave_Credencial_Elector.getText().toString());
        basicsUpdate.setNumRegVoter(editText_Numero_Registro_Electoral.getText().toString());
        basicsUpdate.setIdCivilStatus(String.valueOf(id_civil_status));
        basicsUpdate.setIdGender(String.valueOf(id_gender));
        basicsUpdate.setIdCountryBirth(String.valueOf(id_country_birth));
        basicsUpdate.setIdFederalEntity(String.valueOf(id_federal_entity));
        basicsUpdate.setIdLevelEducation(String.valueOf(id_level_education));
        basicsUpdate.setIdLivingPlace(String.valueOf(id_living_place));
        basicsUpdate.setIdNationality(String.valueOf(id_nacionality));
        basicsUpdate.setIdKindLocal(String.valueOf(pep));
        basicsUpdate.setSons(String.valueOf(sons));
        basicsUpdate.setIdName(String.valueOf(GlobalVariables.id_Name)); //Id Name
        if(Boolean.parseBoolean(basics.getComplete())==false)
        {
            basicsUpdate.setComplete(getString(R.string.value_false).toString());
        }
        else
        {
            basicsUpdate.setComplete(getString(R.string.value_true).toString());
        }

        dbHandler.UpdateBasics(basicsUpdate);
        Snackbar.make(root, R.string.msg_save_change, Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
        //Toast.makeText(getActivity(), R.string.msg_save_change, Toast.LENGTH_LONG).show();
    }
}