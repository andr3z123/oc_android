package fragments;


import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;

import adapter.CustomRecyclerAdapterApplications;
import adapter.CustomRecyclerAdapterMember;
import adapter.CustomRecyclerAdapterSynchronizedRequest;
import data.GlobalVariables;
import implementation.DBHandler;
import model.Applications;
import model.GroupDetail;
import model.Member;
import nubaj.com.ocandroid.GroupRequestActivity;
import nubaj.com.ocandroid.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class SynchronizedRequestFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<Applications> items;
    private MenuItem searchViewItem;
    private RelativeLayout relative_Layout_Items_Zero;
    private DBHandler dbHandler;

    public SynchronizedRequestFragment()
    {
        /*if(GlobalVariables.is_loading==true)
        {
            fillList();
        }*/
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_synchronized_request, container, false);
        mRecyclerView = (RecyclerView) root.findViewById(R.id.reciclador);
        relative_Layout_Items_Zero = (RelativeLayout) root.findViewById(R.id.relativeLayoutZeroItems);


        //if (InternetConnection.checkConnection(getActivity())){
        try
        {
            GlobalVariables.is_loading = true;
            fillList();
        }
        catch (Exception e)
        {
            Toast.makeText(getActivity(), "Error", Toast.LENGTH_LONG).show();
            e.getStackTrace();
        }
        return root;
    }

    void fillList()
    {
        items = new ArrayList<>();
        GlobalVariables.groupDetailArrayList = new ArrayList<GroupDetail>();
        try
        {
            dbHandler = new DBHandler(getActivity());
            GlobalVariables.groupDetailArrayList = dbHandler.GetAllGroupDetail(Integer.parseInt(GlobalVariables.responseUser.getUser().getIdUSERS()));
            Applications applications;

            if(GlobalVariables.groupDetailArrayList!=null && GlobalVariables.groupDetailArrayList.size()>0)
            {
                for(GroupDetail groupDetail: GlobalVariables.groupDetailArrayList)
                {
                    if(Boolean.parseBoolean(groupDetail.getComplete())==true)
                    {
                        applications = new Applications();
                        applications.setIcon_Tipo_Solicitud(R.mipmap.ic_launcher);
                        applications.setTitulo_Tipo_Solicitud("Solicitud");
                        applications.setTitulo_Grupo(groupDetail.getGroupName().toString());
                        applications.setFecha_Solicitud(groupDetail.getDate().toString());
                        items.add(applications);
                    }
                }
                if (items.size() != 0)
                {
                    mRecyclerView.setHasFixedSize(true);
                    mLayoutManager = new LinearLayoutManager(getActivity());
                    mRecyclerView.setLayoutManager(mLayoutManager);
                    mAdapter = new CustomRecyclerAdapterSynchronizedRequest(getActivity(), items);
                    mRecyclerView.setAdapter(mAdapter);
                    mAdapter.notifyDataSetChanged();
                    relative_Layout_Items_Zero.setVisibility(View.GONE);
                    mRecyclerView.setVisibility(View.VISIBLE);

                }
                else
                {
                    relative_Layout_Items_Zero.setVisibility(View.VISIBLE);
                    mRecyclerView.setVisibility(View.GONE);
                }

            }
            else
            {
                relative_Layout_Items_Zero.setVisibility(View.VISIBLE);
                mRecyclerView.setVisibility(View.GONE);
            }
            setHasOptionsMenu(true);
        }
        catch (Exception ex)
        {
            ex.getStackTrace();
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();
        fillList();
        getActivity().runOnUiThread(new Runnable()
        {
            public void run()
            {
                if (mRecyclerView.getAdapter() != null)
                {
                    ((CustomRecyclerAdapterSynchronizedRequest) mAdapter).setOnItemClickListener(new CustomRecyclerAdapterSynchronizedRequest
                            .MyClickListener()
                    {
                        @Override
                        public void onItemClick(int position, View v)
                        {

                            getGroupDetail(items.get(position).getTitulo_Grupo().toString());
                            Intent intent = new Intent(getActivity(), GroupRequestActivity.class);
                            startActivity(intent);
                        }
                    });
                }
            }
        });
    }

    void getGroupDetail(String name)
    {
        GlobalVariables.groupDetail = new GroupDetail();
        try
        {
            for(GroupDetail groupDetail : GlobalVariables.groupDetailArrayList)
            {
                if(groupDetail.getGroupName().equals(name))
                {
                    GlobalVariables.groupDetail = groupDetail;
                    break;
                }
            }
        }
        catch (Exception ex)
        {
            ex.getStackTrace();
        }
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.main, menu);

        menu.findItem(R.id.action_add).setVisible(true);

        searchViewItem = menu.findItem(R.id.action_search_simple);
        final SearchView searchViewAndroidActionBar = (SearchView) MenuItemCompat.getActionView(searchViewItem);
        searchViewAndroidActionBar.setOnQueryTextListener(searchFilterListener); // call the QuerytextListner.
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_add)
        {
            GlobalVariables.groupDetail = new GroupDetail();
            dialogTypeCredit();
        }
        if (id == R.id.action_search_simple) {
            //dialogFilter();
        }
        return super.onOptionsItemSelected(item);
    }

    public void dialogTypeCredit()
    {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.dialog_title_selecionar_tipo_credito)
                .setItems(R.array.seleccionar_tipo_credito, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // The 'which' argument contains the index position
                        // of the selected item
                        switch (which){
                            /*case 0:
                                dialogSelectAplicant();
                                break;*/
                            case 0:
                                Intent intent = new Intent(getActivity(), GroupRequestActivity.class);
                                startActivity(intent);
                                break;
                            case 1:
                                intent = new Intent(getActivity(), GroupRequestActivity.class);
                                startActivity(intent);
                                break;
                        }
                    }
                });

        builder.show();
    }

    SearchView.OnQueryTextListener searchFilterListener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextChange(String query) {
            //query = query.toLowerCase();

            final ArrayList<Applications> filtered_List = new ArrayList<>();

            for (int i = 0; i < items.size(); i++) {

                final String text_Filter = items.get(i).getTitulo_Grupo().toString();
                //final String text = String.valueOf(items.get(i)).toString();
                if (text_Filter.toUpperCase().contains(query) || text_Filter.toLowerCase().contains(query)) {
                    filtered_List.add(items.get(i));
                }
            }

            mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            mAdapter = new CustomRecyclerAdapterApplications(getActivity(), filtered_List);
            mRecyclerView.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();  // data set changed
            return true;

        }
        public boolean onQueryTextSubmit(String query) {
            return false;
        }
    };

}
