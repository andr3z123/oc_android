package fragments;


import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import adapter.Area1NothingSelectedSpinnerAdapter;
import data.GlobalVariables;
import implementation.DBHandler;
import model.GroupDetail;
import model.Phone;
import nubaj.com.ocandroid.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class PrincipalesFragment extends Fragment implements View.OnClickListener
{

    private View root;
    private EditText editText_Nombre_Grupo;
    private TextView textViewIdSponsor, textViewCycleGroup, textViewIdContrato;
    private Spinner spinner_Canal_Dispersor, spinner_Medio_Dispersion_Grupo;
    private DBHandler dbHandler;
    private ArrayAdapter<String> adapter_Canal_Dispersor;
    private int id_group_detail=0, channel_dispersion=0, medium_dispersion=0;

    public PrincipalesFragment()
    {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_principales, container, false);
        initialize();
        setHasOptionsMenu(true);
        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        if(GlobalVariables.groupDetail!=null)
        {
            fillObjects();
        }
    }

    void initialize()
    {
        textViewIdContrato = (TextView) root.findViewById(R.id.textViewIdContrato);
        editText_Nombre_Grupo = (EditText) root.findViewById(R.id.editTextNombreGrupo);
        textViewCycleGroup = (TextView) root.findViewById(R.id.textViewCicloGrupo);

        textViewIdSponsor = (TextView) root.findViewById(R.id.textViewIdOportunidad);
        spinner_Canal_Dispersor = (Spinner) root.findViewById(R.id.spinnerCanalDispersor);
        FloatingActionButton fab = (FloatingActionButton) root.findViewById(R.id.floatingActionButtonSave);
        fab.setOnClickListener(this);
        initializeCapitalLetters();
        // Spinner Drop down elements type Producto
        List<String> list_Canal_Dispersor = new ArrayList<String>();
        list_Canal_Dispersor.add("Bancomer");
        list_Canal_Dispersor.add("Banamex");
        list_Canal_Dispersor.add("Santander");
        list_Canal_Dispersor.add("Scotiabank");
        list_Canal_Dispersor.add("Banorte");

        adapter_Canal_Dispersor = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, list_Canal_Dispersor);
        adapter_Canal_Dispersor.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_Canal_Dispersor.setAdapter(adapter_Canal_Dispersor);
        spinner_Canal_Dispersor.setAdapter(new Area1NothingSelectedSpinnerAdapter(adapter_Canal_Dispersor, R.layout.area_1_spinner_row_nothing_selected, getContext()));
        spinner_Canal_Dispersor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
            {
                if(i>0)
                {
                    channel_dispersion = i;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });


        spinner_Medio_Dispersion_Grupo = (Spinner) root.findViewById(R.id.spinnerMedioDispersionGrupo);

        // Spinner Drop down elements type Status
        List<String> list_Medio_Dispersion_Grupo = new ArrayList<String>();
        list_Medio_Dispersion_Grupo.add("ODP");
        list_Medio_Dispersion_Grupo.add("ODP2");
        list_Medio_Dispersion_Grupo.add("ODP3");
        list_Medio_Dispersion_Grupo.add("ODP4");

        ArrayAdapter<String> adapter_Medio_Dispersion_Grupo = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, list_Medio_Dispersion_Grupo);
        adapter_Medio_Dispersion_Grupo.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_Medio_Dispersion_Grupo.setAdapter(adapter_Medio_Dispersion_Grupo);
        spinner_Medio_Dispersion_Grupo.setAdapter(new Area1NothingSelectedSpinnerAdapter(adapter_Medio_Dispersion_Grupo, R.layout.area_1_spinner_row_nothing_selected, getContext()));
        spinner_Medio_Dispersion_Grupo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
            {
                if(i>0)
                {
                    medium_dispersion = i;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView)
            {

            }
        });
    }

    void fillObjects()
    {
        try
        {
            if(GlobalVariables.groupDetail!=null)
            {
                if(GlobalVariables.groupDetail.getIdGROUP_DETAIL()!=null)
                {
                    textViewIdContrato.setText(GlobalVariables.groupDetail.getIdGROUP_DETAIL().toString());
                }
                else
                {
                    int id_group_detail = 0;
                    dbHandler = new DBHandler(getActivity());
                    id_group_detail =  dbHandler.GetLastIdGroupDetail()+1;
                    textViewIdSponsor.setText(String.valueOf(id_group_detail));
                }
            }
            editText_Nombre_Grupo.setText(GlobalVariables.groupDetail.getGroupName());
            textViewCycleGroup.setText(GlobalVariables.groupDetail.getGroupCycle().toString());
            if(GlobalVariables.groupDetail!=null)
            {
                if(!TextUtils.isEmpty(GlobalVariables.groupDetail.getIdGroupDetailServer()))
                {
                    textViewIdSponsor.setText(GlobalVariables.groupDetail.getIdGROUP_DETAIL());
                }
                else
                {
                    textViewIdSponsor.setText(GlobalVariables.groupDetail.getIdGROUP_DETAIL());
                }
            }
            else
            {
                textViewIdSponsor.setText(GlobalVariables.groupDetail.getIdGROUP_DETAIL());
            }
            spinner_Canal_Dispersor.setSelection(Integer.parseInt(GlobalVariables.groupDetail.getIdChannelDispersion()));
            spinner_Medio_Dispersion_Grupo.setSelection(Integer.parseInt(GlobalVariables.groupDetail.getIdDispersionMedium()));
        }
        catch (Exception ex)
        {
            ex.getStackTrace();
        }
    }

    private void initializeCapitalLetters() {
        GlobalVariables.capitalLetters(editText_Nombre_Grupo);
    }

    public void onClick(View view)
    {
        try
        {
            if(view.getId()==R.id.floatingActionButtonSave)
            {
                if(!TextUtils.isEmpty(editText_Nombre_Grupo.getText().toString()))
                {
                    insertMain();
                }
                else
                {
                    Snackbar.make(getView(),"Necesita ingresar el nombre del grupo", Snackbar.LENGTH_SHORT).setAction("Action",null).show();
                }
            }
        }
        catch (Exception ex)
        {
            ex.getStackTrace();
        }
    }

    private void insertMain()
    {
        GroupDetail groupDetail = new GroupDetail();
        try
        {

            groupDetail.setGroupName(editText_Nombre_Grupo.getText().toString());
            groupDetail.setGroupCycle(textViewCycleGroup.getText().toString());
            groupDetail.setIdSponsor("1");
            groupDetail.setIdChannelDispersion(String.valueOf(channel_dispersion));
            groupDetail.setIdDispersionMedium(String.valueOf(medium_dispersion));
            groupDetail.setIdUser(GlobalVariables.responseUser.getUser().getIdUSERS());
            Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
            groupDetail.setDate(GlobalVariables.getFormatDate(calendar.get(Calendar.DAY_OF_MONTH), calendar.get(Calendar.MONTH), calendar.get(Calendar.YEAR)));
            groupDetail.setComplete(getString(R.string.value_false));
            dbHandler = new DBHandler(getActivity());
            if(GlobalVariables.groupDetail.getIdGROUP_DETAIL()==null )
            {
                id_group_detail = dbHandler.AddGroupDetail(groupDetail);
                GlobalVariables.groupDetail = new GroupDetail();
                GlobalVariables.groupDetail.setIdGROUP_DETAIL(String.valueOf(id_group_detail));
            }
            else
            {
                groupDetail.setIdGROUP_DETAIL(GlobalVariables.groupDetail.getIdGROUP_DETAIL());
                id_group_detail = dbHandler.UpdateGroupDetail(groupDetail);
                GlobalVariables.groupDetail = new GroupDetail();
                GlobalVariables.groupDetail = groupDetail;
            }
            getActivity().runOnUiThread(new Runnable()
            {
                public void run()
                {
                    Toast.makeText(getActivity(), "Alta", Toast.LENGTH_LONG).show();
                }
            });


        }
        catch (Exception e)
        {
            getActivity().runOnUiThread(new Runnable()
            {
                public void run()
                {
                    Toast.makeText(getActivity(), "Error", Toast.LENGTH_LONG).show();
                }
            });
            e.getStackTrace();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        return super.onOptionsItemSelected(item);
    }

}
