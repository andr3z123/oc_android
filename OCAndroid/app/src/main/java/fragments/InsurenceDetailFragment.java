package fragments;


import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.CursorIndexOutOfBoundsException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import adapter.Area1NothingSelectedSpinnerAdapter;
import data.GlobalVariables;
import implementation.DBHandler;
import model.Beneficiary;
import model.Secure;
import nubaj.com.ocandroid.FirmActivity;
import nubaj.com.ocandroid.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class InsurenceDetailFragment extends Fragment {

    private View root;
    private CheckBox checkBox_Beneficiario;
    private LinearLayout linearLayout_Familiar;
    private RelativeLayout linearLayout_Normal_Detail, linearLayout_Beneficiario;
    private boolean id_Selection;
    private Spinner spinner_Forma_Pago, spinner_Modalidad, spinner_Parentesco, spinner_Genero, spinner_Parentesco_Familiar, spinner_Genero_Familiar;
    private TextView textView_Fecha_Inicio_Vigencia, textView_Fecha_Naciemiento, textView_Fecha_Naciemiento_Familiar, textView_Plazo;
    private int count=0, msYear, msMonth, msDay, get_position_gender, get_position_relationship, get_position_gender_family, get_position_relationship_family, get_id_modality, id_secure;
    private DatePickerDialog pickerDialog_Date;
    private EditText editText_Name1, editText_Name2, editText_Last_Name, editText_mLast_Name, editText_Name1_Family, editText_Name2_Family, editText_Last_Name_Family, editText_mLast_Name_Family;
    private DBHandler dbHandler;
    private List<Beneficiary> getAllBeneficiaryList;
    private Secure secure;

    public InsurenceDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_insurence_detail, container, false);
        initializeComponents();

        initialize();

        dbHandler = new DBHandler(getActivity());
        try {
            secure = dbHandler.GetSecure(GlobalVariables.id_Name);
            if (secure == null){

            }else {
                getDataBeneficiary();
                GlobalVariables.get_id_secure = id_secure;
            }
        }catch (CursorIndexOutOfBoundsException e){
            e.printStackTrace();
        }

        setHasOptionsMenu(true);
        return root;
    }

    void initialize() {

        textView_Fecha_Inicio_Vigencia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                instanceDate(textView_Fecha_Inicio_Vigencia);
                pickerDialog_Date.show();
            }
        });

        textView_Fecha_Naciemiento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                instanceDate(textView_Fecha_Naciemiento);
                pickerDialog_Date.show();
            }
        });

        textView_Fecha_Naciemiento_Familiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                instanceDate(textView_Fecha_Naciemiento_Familiar);
                pickerDialog_Date.show();
            }
        });

        checkBox_Beneficiario.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    linearLayout_Normal_Detail.setVisibility(View.VISIBLE);
                    linearLayout_Beneficiario.setVisibility(View.VISIBLE);
                    linearLayout_Familiar.setVisibility(View.GONE);
                    spinner_Modalidad.setEnabled(true);
                    spinner_Modalidad.setClickable(true);
                    spinner_Modalidad.setSelection(1);

                } else {
                    linearLayout_Normal_Detail.setVisibility(View.VISIBLE);
                    linearLayout_Beneficiario.setVisibility(View.GONE);
                    linearLayout_Familiar.setVisibility(View.GONE);
                    spinner_Modalidad.setEnabled(false);
                    spinner_Modalidad.setClickable(false);
                    spinner_Modalidad.setSelection(0);
                }
            }
        });

        // Spinner Drop down elements type Forma de pago
        List<String> list_Forma_Pago = new ArrayList<String>();
        list_Forma_Pago.add("Pago diferido");

        ArrayAdapter<String> adapter_Forma_Pago = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, list_Forma_Pago);
        adapter_Forma_Pago.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_Forma_Pago.setAdapter(adapter_Forma_Pago);
        spinner_Forma_Pago.setAdapter(new Area1NothingSelectedSpinnerAdapter(adapter_Forma_Pago, R.layout.area_1_spinner_row_nothing_selected, getContext()));
        spinner_Forma_Pago.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        // Spinner Drop down elements type Modalidad
        List<String> list_Modalidad = new ArrayList<String>();
        list_Modalidad.add("Individual");
        list_Modalidad.add("Familiar");

        ArrayAdapter<String> adapter_Modalidad = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, list_Modalidad);
        adapter_Modalidad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_Modalidad.setAdapter(adapter_Modalidad);
        spinner_Modalidad.setAdapter(new Area1NothingSelectedSpinnerAdapter(adapter_Modalidad, R.layout.area_1_spinner_row_nothing_selected, getContext()));
        spinner_Modalidad.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                get_id_modality = i;
                if (i == 1) {
                    linearLayout_Normal_Detail.setVisibility(View.VISIBLE);
                    linearLayout_Beneficiario.setVisibility(View.VISIBLE);
                    linearLayout_Familiar.setVisibility(View.GONE);
                } else if (i == 2) {
                    linearLayout_Normal_Detail.setVisibility(View.VISIBLE);
                    linearLayout_Beneficiario.setVisibility(View.VISIBLE);
                    linearLayout_Familiar.setVisibility(View.VISIBLE);
                    GlobalVariables.isFamily_boolean = true;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        // Spinner Drop down elements type Parentesco
        List<String> list_Parentesco = new ArrayList<String>();
        list_Parentesco.add("Conyuge");
        list_Parentesco.add("Padre");
        list_Parentesco.add("Madre");
        list_Parentesco.add("Hijo(a)");
        list_Parentesco.add("Hermano(a)");
        list_Parentesco.add("Abuelo(a)");
        list_Parentesco.add("Primo(a)");
        list_Parentesco.add("Tio(a)");
        list_Parentesco.add("Sobrino(a)");
        list_Parentesco.add("Yerno");
        list_Parentesco.add("Nuera");
        list_Parentesco.add("Cuñado(a)");
        list_Parentesco.add("Suegro(a)");
        list_Parentesco.add("Compañero(a)");
        list_Parentesco.add("Amigo(a)");
        list_Parentesco.add("Padrino");
        list_Parentesco.add("Madrina");
        list_Parentesco.add("Compadre");
        list_Parentesco.add("Comadre");
        list_Parentesco.add("Concubino(a)");
        list_Parentesco.add("Compañero(a) de grupo");
        list_Parentesco.add("Otros");

        ArrayAdapter<String> adapter_Parentesco = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, list_Parentesco);
        adapter_Parentesco.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_Parentesco.setAdapter(adapter_Parentesco);
        spinner_Parentesco.setAdapter(new Area1NothingSelectedSpinnerAdapter(adapter_Parentesco, R.layout.area_1_spinner_row_nothing_selected, getContext()));
        spinner_Parentesco.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                get_position_relationship = i;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        ArrayAdapter<String> adapter_Parentesco_Familiar = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, list_Parentesco);
        adapter_Parentesco.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_Parentesco_Familiar.setAdapter(adapter_Parentesco_Familiar);
        spinner_Parentesco_Familiar.setAdapter(new Area1NothingSelectedSpinnerAdapter(adapter_Parentesco_Familiar, R.layout.area_1_spinner_row_nothing_selected, getContext()));
        spinner_Parentesco_Familiar.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                get_position_relationship_family = i;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        // Spinner Drop down elements type Genero
        List<String> list_Genero = new ArrayList<String>();
        list_Genero.add("Femenino");
        list_Genero.add("Masculino");

        ArrayAdapter<String> adapter_Genero = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, list_Genero);
        adapter_Genero.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_Genero.setAdapter(adapter_Genero);
        spinner_Genero.setAdapter(new Area1NothingSelectedSpinnerAdapter(adapter_Genero, R.layout.area_1_spinner_row_nothing_selected, getContext()));
        spinner_Genero.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                get_position_gender = i;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        ArrayAdapter<String> adapter_Genero_Familiar = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, list_Genero);
        adapter_Genero.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_Genero_Familiar.setAdapter(adapter_Genero_Familiar);
        spinner_Genero_Familiar.setAdapter(new Area1NothingSelectedSpinnerAdapter(adapter_Genero_Familiar, R.layout.area_1_spinner_row_nothing_selected, getContext()));
        spinner_Genero_Familiar.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                get_position_gender_family = i;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });


    }

    void initializeComponents() {

        editText_Name1 = (EditText) root.findViewById(R.id.editTextPrimerNombre);
        editText_Name2 = (EditText) root.findViewById(R.id.editTextSegundoNombre);
        editText_Last_Name = (EditText) root.findViewById(R.id.editTextApellidoPaterno);
        editText_mLast_Name = (EditText) root.findViewById(R.id.editTextApellidoMaterno);

        editText_Name1_Family = (EditText) root.findViewById(R.id.editTextPrimerNombreFamiliar);
        editText_Name2_Family = (EditText) root.findViewById(R.id.editTextSegundoNombreFamiliar);
        editText_Last_Name_Family = (EditText) root.findViewById(R.id.editTextApellidoPaternoFamilira);
        editText_mLast_Name_Family = (EditText) root.findViewById(R.id.editTextApellidoMaternoFamiliar);

        spinner_Forma_Pago = (Spinner) root.findViewById(R.id.spinnerFormaPago);
        spinner_Modalidad = (Spinner) root.findViewById(R.id.spinnerModalidad);
        spinner_Parentesco = (Spinner) root.findViewById(R.id.spinnerParentesco);
        spinner_Genero = (Spinner) root.findViewById(R.id.spinnerGenero);
        spinner_Parentesco_Familiar = (Spinner) root.findViewById(R.id.spinnerParentescoFamiliar);
        spinner_Genero_Familiar = (Spinner) root.findViewById(R.id.spinnerGeneroFamiliar);

        checkBox_Beneficiario = (CheckBox) root.findViewById(R.id.checkBoxCapturaSeguros);
        id_Selection = checkBox_Beneficiario.isChecked();
        linearLayout_Normal_Detail = (RelativeLayout) root.findViewById(R.id.linearLayout_NormalDetail);
        linearLayout_Beneficiario = (RelativeLayout) root.findViewById(R.id.linearLayout_Beneficiario);
        linearLayout_Familiar = (LinearLayout) root.findViewById(R.id.linearLayout_Familiar);

        textView_Fecha_Inicio_Vigencia = (TextView) root.findViewById(R.id.textViewFechaInicioVigencia);
        textView_Fecha_Naciemiento = (TextView) root.findViewById(R.id.textViewFechaNacimiento);
        textView_Fecha_Naciemiento_Familiar = (TextView) root.findViewById(R.id.textViewFechaNacimientoFamiliar);
        textView_Plazo = (TextView) root.findViewById(R.id.textViewPlazo);

        if (id_Selection == false) {
            linearLayout_Normal_Detail.setVisibility(View.VISIBLE);
            linearLayout_Beneficiario.setVisibility(View.GONE);
            linearLayout_Familiar.setVisibility(View.GONE);
            spinner_Forma_Pago.setEnabled(false);
            spinner_Forma_Pago.setClickable(false);
            spinner_Modalidad.setEnabled(false);
            spinner_Modalidad.setClickable(false);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_insurence_detail, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_save) {
            saveAction();
        } else if (id == R.id.action_firm) {
                Intent intent = new Intent(getActivity(), FirmActivity.class);
                startActivity(intent);
        } else if (id == R.id.action_undo) {
            Toast.makeText(getActivity(), "Undo", Toast.LENGTH_LONG).show();
        }
        /*switch (id)
        {
            case android.R.id.home:
                backDialog();
        }*/
        return super.onOptionsItemSelected(item);
    }


    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            backDialog();
            return true;
        }
        return super.getActivity().onKeyDown(keyCode, event);
    }

    void backDialog() {
        // Use the Builder class for convenient dialog construction
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Los cambios no guardados se perderan ¿Desea salir?")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //Action
                        //getActivity().finish();
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //Action
                        builder.setCancelable(false);
                    }
                });
        // Create the AlertDialog object and return
        builder.show();
    }

    void saveAction() {
        // Use the Builder class for convenient dialog construction
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("¿Desea guardar?")
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //Action
                        new InsurenceDetailFragment.insert_main().execute();
                        //getDataBeneficiary();
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //Action
                    }
                });
        // Create the AlertDialog object and return
        builder.show();
    }

    void instanceDate(final TextView textView_Date) {

        Calendar calendar_Date = Calendar.getInstance();

        pickerDialog_Date = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);

                msYear = year;
                msMonth = monthOfYear;
                msDay = dayOfMonth;

                textView_Date.setText(GlobalVariables.getFormatDate(msDay, msMonth, msYear));

            }

        }, calendar_Date.get(Calendar.YEAR), calendar_Date.get(Calendar.MONTH), calendar_Date.get(Calendar.DAY_OF_MONTH));
    }

    public class insert_main extends AsyncTask<String, String, String> {
        ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setTitle(getString(R.string.message_title_applicants));
            pDialog.setMessage(getString(R.string.message_applicants));
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            insertMain();
            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            //la respuesta del web services despues que se envio la información del movil por el web services
            super.onPostExecute(s);
            pDialog.dismiss();

        }

        private void insertMain() {
            Beneficiary beneficiary = new Beneficiary();
            Beneficiary beneficiaryF = new Beneficiary();
            Secure secure = new Secure();
            try {

                secure.setIdName(String.valueOf(GlobalVariables.id_Name));
                secure.setStartDate(textView_Fecha_Inicio_Vigencia.getText().toString());
                secure.setLimitTime("X");
                secure.setModality(String.valueOf(get_id_modality));
                secure.setComplete("true");

                dbHandler = new DBHandler(getActivity());
                id_secure = dbHandler.AddSecure(secure);
                GlobalVariables.get_id_secure = id_secure;

                if (GlobalVariables.isFamily_boolean == true) {

                    beneficiary.setName1(editText_Name1.getText().toString());
                    beneficiary.setName2(editText_Name2.getText().toString());
                    beneficiary.setLastName(editText_Last_Name.getText().toString());
                    beneficiary.setmLastName(editText_mLast_Name.getText().toString());
                    beneficiary.setIdGender(String.valueOf(get_position_gender));
                    beneficiary.setIdRelationship(String.valueOf(get_position_relationship));
                    beneficiary.setBirthDate(textView_Fecha_Naciemiento.getText().toString());
                    beneficiary.setIdSecure(String.valueOf(id_secure));
                    beneficiary.setIdSigning(String.valueOf(GlobalVariables.id_signing));

                    beneficiaryF.setName1(editText_Name1_Family.getText().toString());
                    beneficiaryF.setName2(editText_Name2_Family.getText().toString());
                    beneficiaryF.setLastName(editText_Last_Name_Family.getText().toString());
                    beneficiaryF.setmLastName(editText_mLast_Name_Family.getText().toString());
                    beneficiaryF.setIdGender(String.valueOf(get_position_gender_family));
                    beneficiaryF.setIdRelationship(String.valueOf(get_position_relationship_family));
                    beneficiaryF.setBirthDate(textView_Fecha_Naciemiento_Familiar.getText().toString());
                    beneficiaryF.setIdSecure(String.valueOf(id_secure));
                    beneficiaryF.setIdSigning(String.valueOf(GlobalVariables.id_signing));

                    dbHandler = new DBHandler(getActivity());
                    dbHandler.AddBeneficiary(beneficiary);
                    dbHandler.AddBeneficiary(beneficiaryF);
                    GlobalVariables.isFamily_boolean = false;

                }else {
                    beneficiary.setName1(editText_Name1.getText().toString());
                    beneficiary.setName2(editText_Name2.getText().toString());
                    beneficiary.setLastName(editText_Last_Name.getText().toString());
                    beneficiary.setmLastName(editText_mLast_Name.getText().toString());
                    beneficiary.setIdGender(String.valueOf(get_position_gender));
                    beneficiary.setIdRelationship(String.valueOf(get_position_relationship));
                    beneficiary.setBirthDate(textView_Fecha_Naciemiento.getText().toString());
                    beneficiary.setIdSecure(String.valueOf(id_secure));
                    beneficiary.setIdSigning(String.valueOf(GlobalVariables.id_signing));

                    dbHandler = new DBHandler(getActivity());
                    dbHandler.AddBeneficiary(beneficiary);
                }
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(getActivity(), "Alta", Toast.LENGTH_LONG).show();
                    }
                });
            } catch (Exception e) {
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(getActivity(), "Error", Toast.LENGTH_LONG).show();
                    }
                });
                e.getStackTrace();
            }
        }
    }


    public void getDataBeneficiary() {

        dbHandler = new DBHandler(getActivity());
        secure = dbHandler.GetSecure(GlobalVariables.id_Name);
        checkBox_Beneficiario.setChecked(true);
        textView_Fecha_Inicio_Vigencia.setText(secure.getStartDate());
        spinner_Modalidad.setSelection(Integer.parseInt(secure.getModality()));

        getAllBeneficiaryList = dbHandler.GetAllBeneficiary(secure.getIdSECURE());

        if (getAllBeneficiaryList != null) {

            if (getAllBeneficiaryList.size() > 0) {

                for (Beneficiary beneficiary : getAllBeneficiaryList) {

                    if (count == 0){
                        editText_Name1.setText(beneficiary.getName1());
                        editText_Name2.setText(beneficiary.getName2());
                        editText_Last_Name.setText(beneficiary.getLastName());
                        editText_mLast_Name.setText(beneficiary.getmLastName());

                        spinner_Genero.setSelection(Integer.parseInt(beneficiary.getIdGender()));
                        spinner_Parentesco.setSelection(Integer.parseInt(beneficiary.getIdRelationship()));

                        textView_Fecha_Naciemiento.setText(beneficiary.getBirthDate());
                    }else {
                        editText_Name1_Family.setText(beneficiary.getName1());
                        editText_Name2_Family.setText(beneficiary.getName2());
                        editText_Last_Name_Family.setText(beneficiary.getLastName());
                        editText_mLast_Name_Family.setText(beneficiary.getmLastName());

                        spinner_Genero_Familiar.setSelection(Integer.parseInt(beneficiary.getIdGender()));
                        spinner_Parentesco_Familiar.setSelection(Integer.parseInt(beneficiary.getIdRelationship()));

                        textView_Fecha_Naciemiento_Familiar.setText(beneficiary.getBirthDate());
                    }
                    count++;
                }
            }
        }

        initializeComponentsSetEnableFalse();
    }

    void initializeComponentsSetEnableFalse() {

        editText_Name1.setEnabled(false);
        editText_Name2.setEnabled(false);
        editText_Last_Name.setEnabled(false);
        editText_mLast_Name.setEnabled(false);

        editText_Name1_Family.setEnabled(false);
        editText_Name2_Family.setEnabled(false);
        editText_Last_Name_Family.setEnabled(false);
        editText_mLast_Name_Family.setEnabled(false);

        spinner_Forma_Pago.setEnabled(false);
        spinner_Modalidad.setEnabled(false);
        spinner_Parentesco.setEnabled(false);
        spinner_Genero.setEnabled(false);
        spinner_Parentesco_Familiar.setEnabled(false);
        spinner_Genero_Familiar.setEnabled(false);

        //checkBox_Beneficiario.setChecked(true);
        checkBox_Beneficiario.setEnabled(false);

        textView_Fecha_Inicio_Vigencia.setEnabled(false);
        textView_Fecha_Naciemiento.setEnabled(false);
        textView_Fecha_Naciemiento_Familiar.setEnabled(false);
        textView_Plazo.setEnabled(false);
    }

}