package fragments;


import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfNumber;
import com.itextpdf.text.pdf.PdfPage;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import data.GlobalVariables;
import implementation.DBHandler;
import implementation.Signature;
import model.Signing;
import nubaj.com.ocandroid.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class FirmFragment extends Fragment {

    private View root;
    //Dialog dialog;
    private LinearLayout mContent;
    private Signature signature;
    private PdfNumber orientation;
    private DBHandler dbHandler;

    private File file = new File(Environment.getExternalStorageDirectory().getPath() + GlobalVariables.url_Photo);
    //private String pic_name = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
    private String pic_name = "seguro_firma_";
    private String StoredPath = file + "/" + pic_name + GlobalVariables.get_id_secure + ".png";
    private String StoredPathPDF = file + "/" + pic_name + GlobalVariables.get_id_secure + ".pdf";
    private String name_pdf = pic_name + GlobalVariables.get_id_secure;

    public FirmFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_firm, container, false);

        signature = new Signature(getContext(), null);
        signature.setBackgroundColor(Color.WHITE);
        mContent = (LinearLayout) root.findViewById(R.id.linearLayoutFirm);
        mContent.addView(signature, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);


        setHasOptionsMenu(true);
        return root;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_firm, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_save) {
            saveAction();
        } else if (id == R.id.action_clean) {
            signature.clear();
        }
        return super.onOptionsItemSelected(item);
    }

    void saveAction() {
        // Use the Builder class for convenient dialog construction
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("¿Desea guardar?")
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int id)
                    {
                        //Action
                        Log.v("log_tag", "Panel Saved");
                        root.setDrawingCacheEnabled(true);
                        signature.save(root, StoredPath, mContent);
                        converImageToPDF(StoredPath, StoredPathPDF);
                        //new FirmFragment.insert_signing().execute();
                        // Calling the same class
                        //getActivity().recreate();
                        insert_signing();
                        getActivity().finish();
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //Action
                    }
                });
        // Create the AlertDialog object and return
        builder.show();
    }

    void converImageToPDF(String storedPath, String storedPathPDF) {

        Document document = new Document();
        try {
            PdfWriter.getInstance(document, new FileOutputStream(storedPathPDF)); //  Change pdf's name.
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        document.open();
        Image img = null;  // Change image's name and extension.
        try {
            img = Image.getInstance(storedPath);
        } catch (BadElementException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        float scaler = ((document.getPageSize().getWidth() - document.leftMargin() - document.rightMargin() - 0) / img.getWidth()) * 150; // 0 means you have no indentation. If you have any, change it.
        img.scalePercent(scaler);
        //img.setRotation(1.5708f); //90°
        //img.getImageRotation();
        img.setAlignment(Image.ALIGN_CENTER | Image.ALIGN_TOP);

        /*img.setAlignment(Image.LEFT | Image.TEXTWRAP);

        float width = document.getPageSize().getWidth() - document.leftMargin() - document.rightMargin();
        float height = document.getPageSize().getHeight() - document.topMargin() - document.bottomMargin();
        img.scaleToFit(width, height);*/

        try {
            document.add(img);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        document.close();

    }
    public void setOrientation(PdfNumber orientation) {
        this.orientation = orientation;
    }

    private void insert_signing() {
        Signing signing = new Signing();
        try {
            signing.setDescriptionSigning(name_pdf);
            dbHandler = new DBHandler(getActivity());
            dbHandler.AddSigning(signing);
            getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(getActivity(), "Alta", Toast.LENGTH_LONG).show();
                }
            });

        } catch (Exception e) {
            getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(getActivity(), "Error", Toast.LENGTH_LONG).show();
                }
            });
            e.getStackTrace();
        }
    }
}
