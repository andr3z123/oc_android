package fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import adapter.CustomRecyclerAdapterApplicantsNews;
import adapter.CustomRecyclerAdapterApplicantsRenews;
import data.GlobalVariables;
import model.ApplicantsNews;
import model.ApplicantsRenews;
import model.NameUser;
import nubaj.com.ocandroid.ApplicantsActivity;
import nubaj.com.ocandroid.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class RenewsApplicantsFragment extends Fragment {

    private View root;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<ApplicantsRenews> items;
    private RelativeLayout relativeLayoutInternet;
    private TextView textViewInternet;
    private List<ApplicantsRenews> applicantsNewsList;
    private ListView listViewApplicantsRenews;
    private CustomRecyclerAdapterApplicantsRenews customArrayAdapterApplicantsNews;
    private MenuItem searchViewItem;

    public RenewsApplicantsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_renews_applicants, container, false);

        mRecyclerView = (RecyclerView) root.findViewById(R.id.reciclador);
        relativeLayoutInternet = (RelativeLayout) root.findViewById(R.id.relativeLayoutInternet);
        textViewInternet = (TextView) root.findViewById(R.id.textViewInternet);

        if(GlobalVariables.nameUserArrayList==null)
        {
            mRecyclerView.setVisibility(View.GONE);
            relativeLayoutInternet.setVisibility(View.VISIBLE);
            //textViewInternet.setText(GlobalVariables.responseNameUser.getMensaje().toString());
            textViewInternet.setText("No se encontró información");
        }
        else
        {
            mRecyclerView.setVisibility(View.GONE);
            relativeLayoutInternet.setVisibility(View.VISIBLE);
            //textViewInternet.setText(GlobalVariables.responseNameUser.getMensaje().toString());
            textViewInternet.setText("No se encontró información");
            /*items = new ArrayList<>();

            for(NameUser nameUser : GlobalVariables.nameUserArrayList)
            {
                ApplicantsRenews applicantsNews = new ApplicantsRenews();
                applicantsNews.setIcon_Status(R.mipmap.ic_launcher);
                applicantsNews.setNombre_Solicitante(nameUser.getName1()+" "+nameUser.getName2()+" "+nameUser.getLastName()+" "+nameUser.getmLastName());
                applicantsNews.setFecha_Ingreso(nameUser.getDateAdmission());
                applicantsNews.setId_Cliente(String.valueOf(nameUser.getIdNAME()));
                applicantsNews.setDias_Restantes(nameUser.getDayRet());
                //applicantsNews.setColonia(nameUser.getAdministrativeAreaLevel2());

                items.add(applicantsNews);
            }

            mRecyclerView.setHasFixedSize(true);
            mLayoutManager = new LinearLayoutManager(getActivity());
            mRecyclerView.setLayoutManager(mLayoutManager);
            mAdapter = new CustomRecyclerAdapterApplicantsRenews(getActivity(), items);
            mRecyclerView.setAdapter(mAdapter);*/
            //setHasOptionsMenu(true);
        }
        setHasOptionsMenu(true);
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(mRecyclerView.getAdapter()!=null)
        {
            ((CustomRecyclerAdapterApplicantsRenews) mAdapter).setOnItemClickListener(new CustomRecyclerAdapterApplicantsRenews
                    .MyClickListener()
            {
                @Override
                public void onItemClick(int position, View v)
                {
                    Intent intent = new Intent(getActivity(), ApplicantsActivity.class);
                    startActivity(intent);
                }
            });
        }
    }

    SearchView.OnQueryTextListener searchFilterListener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextChange(String query) {
            //query = query.toLowerCase();

            final ArrayList<ApplicantsRenews> filtered_List = new ArrayList<>();

            for (int i = 0; i < items.size(); i++) {

                final String text_Filter = items.get(i).getNombre_Solicitante().toString();
                //final String text = String.valueOf(items.get(i)).toString();
                if (text_Filter.toUpperCase().contains(query) || text_Filter.toLowerCase().contains(query)) {
                    filtered_List.add(items.get(i));
                }
            }

            mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            mAdapter = new CustomRecyclerAdapterApplicantsRenews(getActivity(), filtered_List);
            mRecyclerView.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();  // data set changed
            return true;

        }

        public boolean onQueryTextSubmit(String query) {
            return false;
        }
    };

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.main, menu);

        searchViewItem = menu.findItem(R.id.action_search_simple);
        final SearchView searchViewAndroidActionBar = (SearchView) MenuItemCompat.getActionView(searchViewItem);
        searchViewAndroidActionBar.setOnQueryTextListener(searchFilterListener); // call the QuerytextListner.
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_add) {
            GlobalVariables.setServiceData = false;
            if (GlobalVariables.phone != null && GlobalVariables.address !=null && GlobalVariables.additional != null){
                GlobalVariables.phone.clear();
                GlobalVariables.address.clear();
                GlobalVariables.additional.clear();
            }
            Intent intent = new Intent(getActivity(), ApplicantsActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

}
