package fragments;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import adapter.CustomRecyclerAdapterListMembers;
import adapter.CustomRecyclerAdapterMember;
import data.GlobalVariables;
import implementation.DBHandler;
import model.ListMembers;
import model.Member;
import model.Members;
import model.Name;
import nubaj.com.ocandroid.DetailMemberActivity;
import nubaj.com.ocandroid.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListMembersFragment extends Fragment {

    private View root;
    private RecyclerView mRecyclerView;
    private RecyclerView mRecyclerView1;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.Adapter mAdapter1;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.LayoutManager mLayoutManager1;
    private ArrayList<ListMembers> items;
    private ArrayList<Member> items1;
    private Button button_Add_Integrante;
    private SearchView search;
    private RelativeLayout relativeLayoutItemsZero;
    private RelativeLayout relativeLayoutInternet;
    private DBHandler dbHandler;
    private ArrayList<Name> nameArrayList;
    private Member listMembers;
    private ListMembers listMembersObject;
    private int id=0;
    private Name name;
    private ArrayList<Members> membersArrayList;

    public ListMembersFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_list_members, container, false);
        mRecyclerView = (RecyclerView) root.findViewById(R.id.reciclador);
        button_Add_Integrante = (Button) root.findViewById(R.id.buttonAddIntegrante);
        relativeLayoutItemsZero = (RelativeLayout) root.findViewById(R.id.relativeLayoutZeroItems);
        relativeLayoutInternet = (RelativeLayout) root.findViewById(R.id.relativeLayoutInternet);
        fillMembers();
        button_Add_Integrante.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(!TextUtils.isEmpty(GlobalVariables.groupDetail.getIdGROUP_DETAIL()))
                {
                    dialogNewMember();
                }
                else
                {
                    Snackbar.make(getView(),"Necesita guardar el nombre del grupo", Snackbar.LENGTH_SHORT).setAction("Action",null).show();
                }
            }
        });


        return root;
    }

    @Override
    public void onResume()
    {
        super.onResume();
        fillMembers();
        if(mRecyclerView.getAdapter()!=null)
        {
            ((CustomRecyclerAdapterListMembers) mAdapter).setOnItemClickListener(new CustomRecyclerAdapterListMembers
                    .MyClickListener()
            {
                @Override
                public void onItemClick(int position, View v)
                {
                    getName(position);
                    Intent intent = new Intent(getActivity(), DetailMemberActivity.class);
                    startActivity(intent);
                }
            });
        }
    }

    void getName(int position)
    {
        try
        {
            GlobalVariables.name = new Name();
            fillName();
            dbHandler = new DBHandler(getActivity());
            GlobalVariables.getAllNameList = dbHandler.GetAllListName(Integer.parseInt(GlobalVariables.responseUser.getUser().getIdUSERS()));
            for(Name name: GlobalVariables.getAllNameList)
            {
                if(name.getIdName().equals(String.valueOf(items.get(position).getId_name())))
                {
                    GlobalVariables.name = name;
                    break;
                }
            }
        }
        catch (Exception ex)
        {
            ex.getStackTrace();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_group_request, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_save){

        }else if (id == R.id.action_sync){

        }
        return super.onOptionsItemSelected(item);
    }

    void dialogNewMember(){

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.prompt_dialog_title_new_member);

        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View layout = inflater.inflate(R.layout.custom_dialog_select_applicant, null);
        search = (SearchView) layout.findViewById(R.id.searchViewFilter);
        mRecyclerView1 = (RecyclerView) layout.findViewById(R.id.reciclador);

        items1 = new ArrayList<>();

        fillName();

        if (items1.size() != 0)
        {
            mRecyclerView1.setHasFixedSize(true);
            mLayoutManager1 = new LinearLayoutManager(getActivity());
            mRecyclerView1.setLayoutManager(mLayoutManager1);
            mAdapter1 = new CustomRecyclerAdapterMember(getActivity(), items1);
            mRecyclerView1.setAdapter(mAdapter1);
            search.setOnQueryTextListener(searchFilterListener);
        } else {
            mRecyclerView1.setVisibility(View.GONE);
            //search.setVisibility(View.GONE);
            relativeLayoutItemsZero.setVisibility(View.VISIBLE);
        }

        if(mRecyclerView1.getAdapter()!=null)
        {
            ((CustomRecyclerAdapterMember) mAdapter1).setOnItemClickListener(new CustomRecyclerAdapterMember
                    .MyClickListener()
            {
                @Override
                public void onItemClick(int position, View v)
                {
                    id = items1.get(position).getId_name();
                }
            });
        }

        builder.setView(layout);

        builder.setNegativeButton(R.string.prompt_cancel, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.setPositiveButton(R.string.prompt_acept, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                addMembers();
                fillMembers();
                dialog.cancel();
            }
        });

        builder.show();
    }

    void fillMembers()
    {
        try
        {
            items = new ArrayList<>();

            if(GlobalVariables.groupDetail!=null)
            {
                fillListMembers();
            }
            else
            {
                //fillListMembersAdd();
            }
            if (items.size() != 0)
            {
                mRecyclerView.setHasFixedSize(true);
                mLayoutManager = new LinearLayoutManager(getActivity());
                mRecyclerView.setLayoutManager(mLayoutManager);
                mAdapter = new CustomRecyclerAdapterListMembers(getActivity(), items);
                mRecyclerView.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();
                mRecyclerView.setVisibility(View.VISIBLE);
                relativeLayoutItemsZero.setVisibility(View.GONE);

            }
            else
            {
                mRecyclerView.setVisibility(View.GONE);
                //search.setVisibility(View.GONE);
                relativeLayoutItemsZero.setVisibility(View.VISIBLE);
            }
            setHasOptionsMenu(true);
        }
        catch (Exception ex)
        {
            ex.getStackTrace();
        }
    }

    void addMembers()
    {
        try
        {
            int id_members=0;
            dbHandler = new DBHandler(getActivity());
            Members members = new Members();
            members.setIdName(String.valueOf(id));
            members.setIdGroupDetail(GlobalVariables.groupDetail.getIdGROUP_DETAIL());
            members.setComplete(getString(R.string.value_true).toString());
            id = dbHandler.AddMembers(members);
            name = dbHandler.GetName(Integer.parseInt(members.getIdName()));
        }
        catch (Exception ex)
        {
            ex.getStackTrace();
        }
    }

    void  fillListMembers()
    {
        dbHandler = new DBHandler(getActivity());
        membersArrayList = new ArrayList<Members>();
        items = new ArrayList<ListMembers>();
        if(GlobalVariables.getAllNameList==null || GlobalVariables.getAllNameList.size()==0)
        {
            GlobalVariables.getAllNameList = new ArrayList<Name>();
            GlobalVariables.getAllNameList = dbHandler.GetAllListName(Integer.parseInt(GlobalVariables.responseUser.getUser().getIdUSERS()));
        }
        try
        {
            membersArrayList = dbHandler.GetListMembers(Integer.parseInt(GlobalVariables.groupDetail.getIdGROUP_DETAIL()));
            for(Members members: membersArrayList)
            {
                for (Name name: GlobalVariables.getAllNameList)
                {
                    if(name.getIdName().equals(members.getIdName()))
                    {
                        listMembersObject = new ListMembers();
                        listMembersObject.setId_name(Integer.parseInt(name.getIdName()));
                        listMembersObject.setNombre_Integrante_Grupo(name.getName1()+" "+name.getName2()+" "+name.getLastName()+" "+name.getmLastName());
                        listMembersObject.setRol("");
                        listMembersObject.setLista_Control("Aprobado");
                        listMembersObject.setNivel_Riesgo("");
                        listMembersObject.setIcon_Persona(R.mipmap.ic_launcher);
                        listMembersObject.setIcon_Status(R.mipmap.ic_launcher);

                        items.add(listMembersObject);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ex.getStackTrace();
        }
        GlobalVariables.membersArrayList = new ArrayList<Members>();
        GlobalVariables.membersArrayList = membersArrayList;
    }

    void fillName()
    {
        dbHandler = new DBHandler(getActivity());
        GlobalVariables.getAllNameList = new ArrayList<Name>();
        try
        {
            GlobalVariables.getAllNameList = dbHandler.GetAllListName(Integer.parseInt(GlobalVariables.responseUser.getUser().getIdUSERS()));
            for (Name name : GlobalVariables.getAllNameList)
            {
                listMembers = new Member();
                if(!TextUtils.isEmpty(name.getIdNameServer()))
                {
                    listMembers.setId_name(Integer.parseInt(name.getIdName()));
                    listMembers.setNombre_Solicitante(name.getName1().toString() + " " + name.getName2().toString() + " " + name.getLastName().toString() + " " + name.getmLastName().toString());
                    listMembers.setFecha_Ingreso(name.getDateAdmission());
                    listMembers.setIcon_Status(R.mipmap.ic_launcher);

                    items1.add(listMembers);
                }
            }

        }
        catch (Exception ex)
        {
            ex.getStackTrace();
        }
    }

    SearchView.OnQueryTextListener searchFilterListener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextChange(String query) {
            //query = query.toLowerCase();

            final ArrayList<Member> filtered_List = new ArrayList<>();

            for (int i = 0; i < items1.size(); i++) {

                final String text_Filter = items1.get(i).getNombre_Solicitante().toString();
                //final String text = String.valueOf(items.get(i)).toString();
                if (text_Filter.toUpperCase().contains(query) || text_Filter.toLowerCase().contains(query)) {
                    filtered_List.add(items1.get(i));
                }
            }

            mRecyclerView1.setLayoutManager(new LinearLayoutManager(getActivity()));
            mAdapter1 = new CustomRecyclerAdapterMember(getActivity(), filtered_List);
            mRecyclerView1.setAdapter(mAdapter1);
            mAdapter1.notifyDataSetChanged();  // data set changed
            return true;

        }
        public boolean onQueryTextSubmit(String query) {
            return false;
        }
    };
}
