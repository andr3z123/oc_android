package fragments;


import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.ArrayList;

import adapter.CustomRecyclerAdapterNotices;
import model.Notices;
import nubaj.com.ocandroid.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class NoticesFragment extends Fragment {

    private View root;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<Notices> items;
    private RelativeLayout relativeLayoutItemsZero;
    private RelativeLayout relativeLayoutInternet;

    public NoticesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_notices, container, false);

        mRecyclerView = (RecyclerView) root.findViewById(R.id.reciclador);
        relativeLayoutItemsZero = (RelativeLayout) root.findViewById(R.id.relativeLayoutZeroItems);
        relativeLayoutInternet = (RelativeLayout) root.findViewById(R.id.relativeLayoutInternet);

        items = new ArrayList<>();

        Notices notices = new Notices();
        notices.setNombre_Aviso("Alerta");
        notices.setContenido_Aviso("Algo de info");
        notices.setIcon_Aviso(R.mipmap.ic_edit);

        items.add(notices);

        if (items.size() != 0) {
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new CustomRecyclerAdapterNotices(getActivity(), items);
        mRecyclerView.setAdapter(mAdapter);
        } else {
            mRecyclerView.setVisibility(View.GONE);
            //search.setVisibility(View.GONE);
            relativeLayoutItemsZero.setVisibility(View.VISIBLE);
        }

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(mRecyclerView.getAdapter()!=null)
        {
            ((CustomRecyclerAdapterNotices) mAdapter).setOnItemClickListener(new CustomRecyclerAdapterNotices
                    .MyClickListener()
            {
                @Override
                public void onItemClick(int position, View v)
                {

                    Toast.makeText(getActivity(), "No hay información", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

}
