package fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.support.v7.widget.SearchView;
import android.widget.Toast;

import java.util.ArrayList;
import adapter.CustomRecyclerAdapterNextRenovations;
import model.NextRenovations;
import nubaj.com.ocandroid.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class NextRenovationsFragment extends Fragment {

    private View root;
    public SearchView search;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<NextRenovations> items;
    private RelativeLayout relativeLayoutItemsZero;
    private RelativeLayout relativeLayoutInternet;
    private MenuItem searchViewItem;

    public NextRenovationsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_next_renovations, container, false);
        //search = (SearchView) root.findViewById(R.id.searchViewFilter);
        mRecyclerView = (RecyclerView) root.findViewById(R.id.reciclador);
        relativeLayoutItemsZero = (RelativeLayout) root.findViewById(R.id.relativeLayoutZeroItems);
        relativeLayoutInternet = (RelativeLayout) root.findViewById(R.id.relativeLayoutInternet);

        items = new ArrayList<>();

        NextRenovations nextRenovations = new NextRenovations();
        nextRenovations.setIcon_Status(R.mipmap.ic_launcher);
        nextRenovations.setNombre_Solicitante("ANDRADE ESPAÑA GUTIERRITOS");
        nextRenovations.setFecha_Ingreso("12/09/2016");
        nextRenovations.setId_Cliente("7236976392");
        nextRenovations.setDias_Restantes("0");
        nextRenovations.setColonia("LOS ALPES");

        items.add(nextRenovations);

        nextRenovations = new NextRenovations();
        nextRenovations.setIcon_Status(R.mipmap.ic_launcher);
        nextRenovations.setNombre_Solicitante("MANUEL ESPAÑA GUTIERRITOS");
        nextRenovations.setFecha_Ingreso("12/09/2016");
        nextRenovations.setId_Cliente("7236976392");
        nextRenovations.setDias_Restantes("0");
        nextRenovations.setColonia("LOS ALPES");

        items.add(nextRenovations);

        nextRenovations = new NextRenovations();
        nextRenovations.setIcon_Status(R.mipmap.ic_launcher);
        nextRenovations.setNombre_Solicitante("MIGUEL ESPAÑA GUTIERRITOS");
        nextRenovations.setFecha_Ingreso("12/09/2016");
        nextRenovations.setId_Cliente("7236976392");
        nextRenovations.setDias_Restantes("0");
        nextRenovations.setColonia("LOS ALPES");

        items.add(nextRenovations);

        //if (InternetConnection.checkConnection(getActivity())) {
            if (items.size() != 0) {
                mRecyclerView.setHasFixedSize(true);
                mLayoutManager = new LinearLayoutManager(getActivity());
                mRecyclerView.setLayoutManager(mLayoutManager);
                mAdapter = new CustomRecyclerAdapterNextRenovations(getActivity(), items);
                mRecyclerView.setAdapter(mAdapter);
                //search.setOnQueryTextListener(searchFilterListener); // call the QuerytextListner.
            } else {
                mRecyclerView.setVisibility(View.GONE);
                //search.setVisibility(View.GONE);
                relativeLayoutItemsZero.setVisibility(View.VISIBLE);
            }
        /*} else {
            mRecyclerView.setVisibility(View.GONE);
            search.setVisibility(View.GONE);
            relativeLayoutInternet.setVisibility(View.VISIBLE);
        }*/
        setHasOptionsMenu(true);
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mRecyclerView.getAdapter() != null) {
            ((CustomRecyclerAdapterNextRenovations) mAdapter).setOnItemClickListener(new CustomRecyclerAdapterNextRenovations
                    .MyClickListener() {
                @Override
                public void onItemClick(int position, View v) {
                    Toast.makeText(getActivity(), "No hay información", Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    SearchView.OnQueryTextListener searchFilterListener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextChange(String query) {
            //query = query.toLowerCase();

            final ArrayList<NextRenovations> filtered_List = new ArrayList<>();

            for (int i = 0; i < items.size(); i++) {

                final String text_Filter = items.get(i).getNombre_Solicitante().toString();
                //final String text = String.valueOf(items.get(i)).toString();
                if (text_Filter.toUpperCase().contains(query) || text_Filter.toLowerCase().contains(query)) {
                    filtered_List.add(items.get(i));
                }
            }

            mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            mAdapter = new CustomRecyclerAdapterNextRenovations(getActivity(), filtered_List);
            mRecyclerView.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();  // data set changed
            return true;

        }

        public boolean onQueryTextSubmit(String query) {
            return false;
        }
    };

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.main, menu);

        menu.findItem(R.id.action_add).setVisible(false);

        searchViewItem = menu.findItem(R.id.action_search_simple);
        final SearchView searchViewAndroidActionBar = (SearchView) MenuItemCompat.getActionView(searchViewItem);
        searchViewAndroidActionBar.setOnQueryTextListener(searchFilterListener); // call the QuerytextListner.
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search_simple) {
           // MenuItem searchViewItem = .findItem(R.id.action_search);
        }
        return super.onOptionsItemSelected(item);
    }
}
