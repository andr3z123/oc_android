package fragments;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import adapter.Area1NothingSelectedSpinnerAdapter;
import data.GlobalVariables;
import implementation.DBHandler;
import implementation.FunctionJson;
import model.Address;
import model.Addresses;
import model.RequestAddress;
import model.ResponseName;
import nubaj.com.ocandroid.NewAddressActivity;
import nubaj.com.ocandroid.R;

import static data.GlobalVariables.getAddressesList;
import static data.GlobalVariables.id_occupation;
import static data.GlobalVariables.position;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewAddressFragment extends Fragment {

    private View root;
    private Spinner spinner_Tipo_Domicilio, spinner_Pais, spinner_Entidad_Federativa;
    private Gson gson;
    private FunctionJson functionJson;
    private EditText editTextPostalCode, editTextAdministrativeAreaLevel2, editTextNeighborhood, editTextLocality, editTextRoute, editTextStreetNumber1, editTextStreetNumber2, editTextReference1, editTextReference2, editTextReferenceUbication;
    DBHandler dbHandler;
    private int id_address_type, id_country, id_federal_entity;

    public NewAddressFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_new_address, container, false);

        initializeComponents();
        initializeCapitalLetters();
        initialize();

        if (GlobalVariables.state_New_Addrees_Boolean != false) {
            setDataAddress();
            //GlobalVariables.state_New_Addrees_Boolean = false;
        }

        setHasOptionsMenu(true);
        return root;
    }

    private void initializeCapitalLetters() {
        GlobalVariables.capitalLetters(editTextAdministrativeAreaLevel2);
        GlobalVariables.capitalLetters(editTextLocality);
        GlobalVariables.capitalLetters(editTextNeighborhood);
        GlobalVariables.capitalLetters(editTextRoute);
        GlobalVariables.capitalLetters(editTextStreetNumber1);
        GlobalVariables.capitalLetters(editTextStreetNumber2);
        GlobalVariables.capitalLetters(editTextReference1);
        GlobalVariables.capitalLetters(editTextReference2);
        GlobalVariables.capitalLetters(editTextReferenceUbication);
    }

    private void insertAddress() {
        Address address = new Address();
        try {
            address.setRoute(editTextRoute.getText().toString());
            address.setStreetNumber1(editTextStreetNumber1.getText().toString());
            address.setStreetNumber2(editTextStreetNumber2.getText().toString());
            address.setNeighborhood(editTextNeighborhood.getText().toString());
            address.setAdministrativeAreaLevel2(editTextAdministrativeAreaLevel2.getText().toString());
            address.setIdFederalEntity(String.valueOf(id_federal_entity));
            address.setPostalCode(editTextPostalCode.getText().toString());
            address.setIdCountry(String.valueOf(id_country));
            address.setRouteReference1(editTextReference1.getText().toString());
            address.setRouteReference2(editTextReference2.getText().toString());
            address.setIdName(String.valueOf(GlobalVariables.id_Name)); //Id Name
            address.setComplete(getString(R.string.value_false));
            address.setIdAddressTypes(String.valueOf(id_address_type));
            dbHandler = new DBHandler(getActivity());
            dbHandler.AddAddress(address);

            getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(getActivity(), "Alta", Toast.LENGTH_LONG).show();
                    GlobalVariables.is_add_address = true;
                }
            });


        } catch (Exception e) {
            getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(getActivity(), "Error", Toast.LENGTH_LONG).show();
                }
            });
            e.getStackTrace();
        }
    }

    void initializeComponents() {
        spinner_Tipo_Domicilio = (Spinner) root.findViewById(R.id.spinnerTipoDomicilio);
        spinner_Pais = (Spinner) root.findViewById(R.id.spinnerPais);
        spinner_Entidad_Federativa = (Spinner) root.findViewById(R.id.spinnerEntidadFederativa);
        editTextPostalCode = (EditText) root.findViewById(R.id.editTextCodigoPostal);
        editTextAdministrativeAreaLevel2 = (EditText) root.findViewById(R.id.editTextDelegacionMunicipio);
        editTextLocality = (EditText) root.findViewById(R.id.editTextCiudadLocalidad);
        editTextNeighborhood = (EditText) root.findViewById(R.id.editTextColoniaBarrio);
        editTextRoute = (EditText) root.findViewById(R.id.editTextCalle);
        editTextStreetNumber1 = (EditText) root.findViewById(R.id.editTextNumeroExterior);
        editTextStreetNumber2 = (EditText) root.findViewById(R.id.editTextNumeroInterior);
        editTextReference1 = (EditText) root.findViewById(R.id.editTextEntreCalles1);
        editTextReference2 = (EditText) root.findViewById(R.id.editTextEntreCalles2);
        editTextReferenceUbication = (EditText) root.findViewById(R.id.editTextReferenciaUbicacion);
    }

    void initialize() {
        // Spinner Drop down elements type Genero

        List<String> list_Tipo_Domicilio = new ArrayList<String>();
        list_Tipo_Domicilio.add("Casa");
        list_Tipo_Domicilio.add("Familiar");
        list_Tipo_Domicilio.add("Negocio");
        list_Tipo_Domicilio.add("Domicilio fiscal");
        list_Tipo_Domicilio.add("Empresa/Trabajo");
        list_Tipo_Domicilio.add("Dirección anterior");
        list_Tipo_Domicilio.add("Domicilio correspondencia");
        list_Tipo_Domicilio.add("Principal");
        list_Tipo_Domicilio.add("Otro");

        ArrayAdapter<String> adapter_Tipo_Domicilio = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, list_Tipo_Domicilio);
        adapter_Tipo_Domicilio.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_Tipo_Domicilio.setAdapter(adapter_Tipo_Domicilio);
        spinner_Tipo_Domicilio.setAdapter(new Area1NothingSelectedSpinnerAdapter(adapter_Tipo_Domicilio, R.layout.area_1_spinner_row_nothing_selected, getContext()));
        spinner_Tipo_Domicilio.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                id_address_type = i;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        List<String> list_Entidad_Federativa = new ArrayList<String>();
        list_Entidad_Federativa.add("Aguascalientes");
        list_Entidad_Federativa.add("Baja California Norte");
        list_Entidad_Federativa.add("Baja California Sur");
        list_Entidad_Federativa.add("Campeche");
        list_Entidad_Federativa.add("Coahuila");
        list_Entidad_Federativa.add("Colima");
        list_Entidad_Federativa.add("Chiapas");
        list_Entidad_Federativa.add("Chihuahua");
        list_Entidad_Federativa.add("CD MX");
        list_Entidad_Federativa.add("Durango");
        list_Entidad_Federativa.add("Guanajuato");
        list_Entidad_Federativa.add("Guerrero");
        list_Entidad_Federativa.add("Hidalgo");
        list_Entidad_Federativa.add("Jalisco");
        list_Entidad_Federativa.add("México");
        list_Entidad_Federativa.add("Michoacán");
        list_Entidad_Federativa.add("Morelos");
        list_Entidad_Federativa.add("Nayarit");
        list_Entidad_Federativa.add("Nuevo León");
        list_Entidad_Federativa.add("Oaxaca");
        list_Entidad_Federativa.add("Puebla");
        list_Entidad_Federativa.add("Querétaro");
        list_Entidad_Federativa.add("Quintana Roo");
        list_Entidad_Federativa.add("San Luis Potosí");
        list_Entidad_Federativa.add("Sinaloa");
        list_Entidad_Federativa.add("Sonora");
        list_Entidad_Federativa.add("Tabasco");
        list_Entidad_Federativa.add("Tamaulipas");
        list_Entidad_Federativa.add("Tlaxcala");
        list_Entidad_Federativa.add("Veracruz");
        list_Entidad_Federativa.add("Yucatán");
        list_Entidad_Federativa.add("Zacatecas");

        //spinnerInstance(list_Entidad_Federativa, spinner_Entidad_Federativa);
        ArrayAdapter<String> adapter_Entidad_Federativa = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, list_Entidad_Federativa);
        adapter_Entidad_Federativa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_Entidad_Federativa.setAdapter(adapter_Entidad_Federativa);
        spinner_Entidad_Federativa.setAdapter(new Area1NothingSelectedSpinnerAdapter(adapter_Entidad_Federativa, R.layout.area_1_spinner_row_nothing_selected, getContext()));
        spinner_Entidad_Federativa.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                id_federal_entity = i;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        ArrayAdapter<String> adapter_Nacionalidad = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, GlobalVariables.listCountry());
        adapter_Nacionalidad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_Pais.setAdapter(adapter_Nacionalidad);
        spinner_Pais.setAdapter(new Area1NothingSelectedSpinnerAdapter(adapter_Nacionalidad, R.layout.area_1_spinner_row_nothing_selected, getContext()));
        spinner_Pais.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                id_country = i;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_meeting_place, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_save) {
            saveAction();
        }

        return super.onOptionsItemSelected(item);
    }


    void saveAction() {
        // Use the Builder class for convenient dialog construction
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("¿Desea guardar?")
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        spinner_Tipo_Domicilio = (Spinner) root.findViewById(R.id.spinnerTipoDomicilio);
                        spinner_Pais = (Spinner) root.findViewById(R.id.spinnerPais);
                        spinner_Entidad_Federativa = (Spinner) root.findViewById(R.id.spinnerEntidadFederativa);
                        editTextPostalCode = (EditText) root.findViewById(R.id.editTextCodigoPostal);
                        editTextAdministrativeAreaLevel2 = (EditText) root.findViewById(R.id.editTextDelegacionMunicipio);
                        editTextLocality = (EditText) root.findViewById(R.id.editTextCiudadLocalidad);
                        editTextNeighborhood = (EditText) root.findViewById(R.id.editTextColoniaBarrio);
                        editTextRoute = (EditText) root.findViewById(R.id.editTextCalle);
                        editTextStreetNumber1 = (EditText) root.findViewById(R.id.editTextNumeroExterior);
                        editTextStreetNumber2 = (EditText) root.findViewById(R.id.editTextNumeroInterior);
                        editTextReference1 = (EditText) root.findViewById(R.id.editTextEntreCalles1);
                        editTextReference2 = (EditText) root.findViewById(R.id.editTextEntreCalles2);
                        editTextReferenceUbication = (EditText) root.findViewById(R.id.editTextReferenciaUbicacion);
                        if (id_address_type > 0
                                || id_country > 0
                                || id_federal_entity > 0
                                || !editTextPostalCode.getText().toString().isEmpty()
                                || !editTextAdministrativeAreaLevel2.getText().toString().isEmpty()
                                || !editTextLocality.getText().toString().isEmpty()
                                || !editTextNeighborhood.getText().toString().isEmpty()
                                || !editTextRoute.getText().toString().isEmpty()
                                || !editTextStreetNumber1.getText().toString().isEmpty()
                                || !editTextStreetNumber2.getText().toString().isEmpty()
                                ) {

                            if (GlobalVariables.state_New_Addrees_Boolean == true)
                            {
                                updateDataAddress();
                                GlobalVariables.isRefresh_addresses_list_boolean = true;
                                getActivity().finish();
                            }
                            else
                            {
                                if (GlobalVariables.id_Name > 0)
                                {
                                    insertAddress();
                                    GlobalVariables.isRefresh_addresses_list_boolean = true;
                                    getActivity().finish();
                                }
                                else
                                {
                                    Toast.makeText(getActivity(), R.string.error_name, Toast.LENGTH_LONG).show();
                                }
                            }
                        }
                        else
                        {
                            Toast.makeText(getActivity(), R.string.error_empty_input, Toast.LENGTH_LONG).show();
                        }
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //Action
                    }
                });
        // Create the AlertDialog object and return
        builder.show();
    }

    void setDataAddress() {

        spinner_Tipo_Domicilio.setSelection(Integer.parseInt(GlobalVariables.address.get(GlobalVariables.position).getIdAddressTypes()));
        editTextPostalCode.setText(GlobalVariables.address.get(GlobalVariables.position).getPostalCode());
        spinner_Pais.setSelection(Integer.parseInt(GlobalVariables.address.get(GlobalVariables.position).getIdCountry()));
        spinner_Entidad_Federativa.setSelection(Integer.parseInt(GlobalVariables.address.get(GlobalVariables.position).getIdFederalEntity()));
        editTextAdministrativeAreaLevel2.setText(GlobalVariables.address.get(GlobalVariables.position).getAdministrativeAreaLevel2());
        editTextLocality.setText(GlobalVariables.address.get(GlobalVariables.position).getNeighborhood());
        editTextNeighborhood.setText(GlobalVariables.address.get(GlobalVariables.position).getNeighborhood());
        editTextRoute.setText(GlobalVariables.address.get(GlobalVariables.position).getRoute());
        editTextStreetNumber1.setText(GlobalVariables.address.get(GlobalVariables.position).getStreetNumber1());
        editTextStreetNumber2.setText(GlobalVariables.address.get(GlobalVariables.position).getStreetNumber2());
        editTextReference1.setText(GlobalVariables.address.get(GlobalVariables.position).getRouteReference1());
        editTextReference2.setText(GlobalVariables.address.get(GlobalVariables.position).getRouteReference2());
        editTextReferenceUbication.setText(GlobalVariables.address.get(GlobalVariables.position).getRoute());

    }

    void updateDataAddress() {
        dbHandler = new DBHandler(getActivity());
        Address address = new Address();
        address.setRoute(editTextRoute.getText().toString());
        address.setStreetNumber1(editTextStreetNumber1.getText().toString());
        address.setStreetNumber2(editTextStreetNumber2.getText().toString());
        address.setNeighborhood(editTextNeighborhood.getText().toString());
        address.setAdministrativeAreaLevel2(editTextAdministrativeAreaLevel2.getText().toString());
        address.setIdFederalEntity(String.valueOf(id_federal_entity));
        address.setPostalCode(editTextPostalCode.getText().toString());
        address.setIdCountry(String.valueOf(id_country));
        address.setRouteReference1(editTextReference1.getText().toString());
        address.setRouteReference2(editTextReference2.getText().toString());
        address.setIdName(String.valueOf(GlobalVariables.id_Name)); //Id Name
        if(Boolean.parseBoolean(GlobalVariables.address.get(GlobalVariables.position).getComplete())==false)
        {
            address.setComplete("false");
        }
        else
        {
            address.setComplete("true");
        }
        address.setIdAddressTypes(String.valueOf(id_address_type));
        address.setIdADDRESS(GlobalVariables.address.get(GlobalVariables.position).getIdADDRESS());
        dbHandler.UpdateAddress(address);
        Toast.makeText(getActivity(), R.string.msg_save_change, Toast.LENGTH_LONG).show();
    }
}
