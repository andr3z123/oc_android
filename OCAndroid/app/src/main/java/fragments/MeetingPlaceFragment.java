package fragments;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import adapter.Area1NothingSelectedSpinnerAdapter;
import data.GlobalVariables;
import implementation.DBHandler;
import model.Address;
import model.Phone;
import model.Phones;
import model.PlaceMeeting;
import nubaj.com.ocandroid.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class MeetingPlaceFragment extends Fragment {

    private View root;
    private Spinner spinner_Pais, spinner_Entidad_Federativa, spinner_Tipo_Telefono;
    private EditText editTextNumeroExterior, editTextNumeroInterior, editText_Postal_Code, editText_Delacion_Municipio, editText_Ciudad_Localidad, editText_Colonia_Barrio, editText_Calle, editText_Entre_Calles_1, editText_Entre_Calles_2, editText_Referencia_Ubicacion, editText_Telefono, editText_Comentarios;
    private CheckBox checkBoxPlaceMeeting;
    private TextInputLayout textInputLayoutCodigoPostal, textInputLayout, textInputLayoutCiudadLocalidad, textInputLayoutColoniaBarrio, textInputLayoutCalle, textInputLayoutNumeroExterior, textInputLayoutNumeroInterior, textInputLayoutEntreCalle1, textInputLayoutEntreCalle2, textInputLayoutReferenciaUbicacion, textInputLayoutTelefono, textInputLayoutComentarios;
    private LinearLayout linearLayoutPais, linearLayoutEntidadFederativa, linearLayoutTipoTelefono;
    private DBHandler dbHandler;
    private int id=0, entidad_federativa=0,pais=0,tipo_telefono=0;
    private PlaceMeeting placeMeeting;

    public MeetingPlaceFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_meeting_place, container, false);
        setHasOptionsMenu(true);
        initialize();
        fillPlaceMeeting();
        return root;
    }

    void initializeComponents()
    {
        textInputLayoutCodigoPostal = (TextInputLayout) root.findViewById(R.id.textInputCodigoPostal);
        textInputLayout = (TextInputLayout) root.findViewById(R.id.textInputLayout);
        textInputLayoutCiudadLocalidad = (TextInputLayout) root.findViewById(R.id.textInputLayoutCiudadLocalidad);
        textInputLayoutColoniaBarrio = (TextInputLayout) root.findViewById(R.id.textInputLayoutColoniaBarrio);
        textInputLayoutCalle = (TextInputLayout) root.findViewById(R.id.textInputLayoutCalle);
        textInputLayoutNumeroExterior = (TextInputLayout) root.findViewById(R.id.textInputLayoutNumeroExterior);
        textInputLayoutNumeroInterior = (TextInputLayout) root.findViewById(R.id.textInputLayoutNumeroInterior);
        textInputLayoutEntreCalle1 = (TextInputLayout) root.findViewById(R.id.textInputLayoutEntreQueCalles1);
        textInputLayoutEntreCalle2 = (TextInputLayout) root.findViewById(R.id.textInputLayoutEntreQueCalles2);
        textInputLayoutReferenciaUbicacion = (TextInputLayout) root.findViewById(R.id.textInputLayoutReferenciaUbicacion);
        textInputLayoutTelefono = (TextInputLayout) root.findViewById(R.id.textInputLayoutTelefono);
        textInputLayoutComentarios = (TextInputLayout) root.findViewById(R.id.textInputLayoutComentarios);
        linearLayoutPais = (LinearLayout) root.findViewById(R.id.linearLayoutPais);
        linearLayoutEntidadFederativa = (LinearLayout) root.findViewById(R.id.linearLayoutEntidadFederativa);
        linearLayoutTipoTelefono = (LinearLayout) root.findViewById(R.id.linearLayoutTipoTelefono);

        editText_Postal_Code = (EditText) root.findViewById(R.id.editTextCodigoPostal);
        editText_Calle = (EditText) root.findViewById(R.id.editTextCalle);
        editText_Delacion_Municipio = (EditText) root.findViewById(R.id.editTexDelegacionMunicipio);
        editText_Ciudad_Localidad = (EditText) root.findViewById(R.id.editTextCiudadLocalidad);
        editText_Colonia_Barrio = (EditText) root.findViewById(R.id.editTextColoniaBarrio);
        editText_Entre_Calles_1 = (EditText) root.findViewById(R.id.editTextEntreQueCalles1);
        editText_Entre_Calles_2 = (EditText) root.findViewById(R.id.editTextEntreQueCalles2);
        editText_Referencia_Ubicacion = (EditText) root.findViewById(R.id.editTextReferenciaUbicacion);
        editText_Telefono = (EditText) root.findViewById(R.id.editTextTelefono);
        editText_Comentarios = (EditText) root.findViewById(R.id.editTextComentarios);
        editTextNumeroExterior = (EditText) root.findViewById(R.id.editTextNumeroExterior);
        editTextNumeroInterior = (EditText) root.findViewById(R.id.editTextNumeroInterior);
        checkBoxPlaceMeeting = (CheckBox) root.findViewById(R.id.checkBoxPreguntaClientePrestaCasa);

        spinner_Pais = (Spinner) root.findViewById(R.id.spinnerPais);
        spinner_Entidad_Federativa = (Spinner) root.findViewById(R.id.spinnerEntidadFederativa);
        spinner_Tipo_Telefono = (Spinner) root.findViewById(R.id.spinnerTipoTelefono);
    }

    private void initializeCapitalLetters() {
        GlobalVariables.capitalLetters(editText_Calle);
        GlobalVariables.capitalLetters(editText_Delacion_Municipio);
        GlobalVariables.capitalLetters(editText_Ciudad_Localidad);
        GlobalVariables.capitalLetters(editTextNumeroExterior);
        GlobalVariables.capitalLetters(editTextNumeroInterior);
        GlobalVariables.capitalLetters(editText_Colonia_Barrio);
        GlobalVariables.capitalLetters(editText_Entre_Calles_1);
        GlobalVariables.capitalLetters(editText_Entre_Calles_2);
        GlobalVariables.capitalLetters(editText_Referencia_Ubicacion);
        GlobalVariables.capitalLetters(editText_Comentarios);
    }
    void initialize(){
        initializeComponents();
        initializeCapitalLetters();
        // Spinner Drop down elements type Entidad Federativa
        // Spinner Drop down elements Estado
        List<String> list_Entidad_Federativa = new ArrayList<String>();
        list_Entidad_Federativa.add("Aguascalientes");
        list_Entidad_Federativa.add("Baja California Norte");
        list_Entidad_Federativa.add("Baja California Sur");
        list_Entidad_Federativa.add("Campeche");
        list_Entidad_Federativa.add("Coahuila");
        list_Entidad_Federativa.add("Colima");
        list_Entidad_Federativa.add("Chiapas");
        list_Entidad_Federativa.add("Chihuahua");
        list_Entidad_Federativa.add("CD MX");
        list_Entidad_Federativa.add("Durango");
        list_Entidad_Federativa.add("Guanajuato");
        list_Entidad_Federativa.add("Guerrero");
        list_Entidad_Federativa.add("Hidalgo");
        list_Entidad_Federativa.add("Jalisco");
        list_Entidad_Federativa.add("México");
        list_Entidad_Federativa.add("Michoacán");
        list_Entidad_Federativa.add("Morelos");
        list_Entidad_Federativa.add("Nayarit");
        list_Entidad_Federativa.add("Nuevo León");
        list_Entidad_Federativa.add("Oaxaca");
        list_Entidad_Federativa.add("Puebla");
        list_Entidad_Federativa.add("Querétaro");
        list_Entidad_Federativa.add("Quintana Roo");
        list_Entidad_Federativa.add("San Luis Potosí");
        list_Entidad_Federativa.add("Sinaloa");
        list_Entidad_Federativa.add("Sonora");
        list_Entidad_Federativa.add("Tabasco");
        list_Entidad_Federativa.add("Tamaulipas");
        list_Entidad_Federativa.add("Tlaxcala");
        list_Entidad_Federativa.add("Veracruz");
        list_Entidad_Federativa.add("Yucatán");
        list_Entidad_Federativa.add("Zacatecas");

        //spinnerInstance(list_Entidad_Federativa, spinner_Entidad_Federativa);
        ArrayAdapter<String> adapter_Entidad_Federativa = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, list_Entidad_Federativa);
        adapter_Entidad_Federativa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_Entidad_Federativa.setAdapter(adapter_Entidad_Federativa);
        spinner_Entidad_Federativa.setAdapter(new Area1NothingSelectedSpinnerAdapter(adapter_Entidad_Federativa, R.layout.area_1_spinner_row_nothing_selected, getContext()));
        spinner_Entidad_Federativa.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
            {
                if(i>0)
                {
                    entidad_federativa = i;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        // Spinner Drop down elements type Genero
        List<String> list_Pais = new ArrayList<String>();
        list_Pais.add("México");

        ArrayAdapter<String> adapter_Pais = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, list_Pais);
        adapter_Pais.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_Pais.setAdapter(adapter_Pais);
        spinner_Pais.setAdapter(new Area1NothingSelectedSpinnerAdapter(adapter_Pais, R.layout.area_1_spinner_row_nothing_selected, getContext()));
        spinner_Pais.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
            {
                if(i>0)
                {
                    pais = i;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        // Spinner Drop down elements type Genero
        List<String> list_Tipo_Telefono = new ArrayList<String>();
        list_Tipo_Telefono.add("Casa");
        list_Tipo_Telefono.add("Celular");
        list_Tipo_Telefono.add("Negocio");
        list_Tipo_Telefono.add("Recados");
        list_Tipo_Telefono.add("Trabajo");

        ArrayAdapter<String> adapter_Tipo_Telefono = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, list_Tipo_Telefono);
        adapter_Tipo_Telefono.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_Tipo_Telefono.setAdapter(adapter_Tipo_Telefono);
        spinner_Tipo_Telefono.setAdapter(new Area1NothingSelectedSpinnerAdapter(adapter_Tipo_Telefono, R.layout.area_1_spinner_row_nothing_selected, getContext()));
        spinner_Tipo_Telefono.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
            {
                if(i>0)
                {
                    tipo_telefono = i;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        checkBoxPlaceMeeting.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b)
            {
                if(b==true)
                {
                    loadAddress();
                    //gone();
                }
                else
                {
                    clearAddress();
                    //visible();
                }
                GlobalVariables.is_check_place_meeting = b;
            }
        });

        if(GlobalVariables.is_check_place_meeting==true)
        {
            checkBoxPlaceMeeting.setChecked(true);
        }
        else
        {
            checkBoxPlaceMeeting.setChecked(false);
        }
    }

    void loadAddress()
    {
        Address address = new Address();
        ArrayList<Phone> phoneArrayList = new ArrayList<Phone>();
        dbHandler = new DBHandler(getActivity());
        try
        {
            address = dbHandler.GetAddress(GlobalVariables.id_name_group_data);
            if(address!=null)
            {
                editText_Postal_Code.setText(address.getPostalCode().toString());
                spinner_Pais.setSelection(Integer.parseInt(address.getIdCountry()));
                spinner_Entidad_Federativa.setSelection(Integer.parseInt(address.getIdFederalEntity()));
                editText_Delacion_Municipio.setText(address.getAdministrativeAreaLevel2().toString());
                editText_Colonia_Barrio.setText(address.getNeighborhood().toString());
                editText_Calle.setText(address.getRoute().toString());
                editTextNumeroExterior.setText(address.getStreetNumber1().toString());
                editTextNumeroInterior.setText(address.getStreetNumber2().toString());
                editText_Entre_Calles_1.setText(address.getRouteReference1().toString());
                editText_Entre_Calles_2.setText(address.getRouteReference2().toString());
            }

            phoneArrayList = dbHandler.GetAllPhones(GlobalVariables.id_name_group_data);
            if(phoneArrayList.size()>0)
            {
                for (Phone phone : phoneArrayList)
                {
                    editText_Telefono.setText(phone.getNumber().toString());
                    spinner_Tipo_Telefono.setSelection(Integer.parseInt(phone.getIdPhoneTypes().toString()));
                    break;
                }
            }
        }
        catch (Exception ex)
        {
            ex.getStackTrace();
        }
    }

    void clearAddress()
    {
        try
        {
            editText_Postal_Code.setText("");
            spinner_Pais.setSelection(0);
            spinner_Entidad_Federativa.setSelection(0);
            editText_Delacion_Municipio.setText("");
            editText_Colonia_Barrio.setText("");
            editText_Calle.setText("");
            editTextNumeroExterior.setText("");
            editTextNumeroInterior.setText("");
            editText_Entre_Calles_1.setText("");
            editText_Entre_Calles_2.setText("");

            editText_Telefono.setText("");
            spinner_Tipo_Telefono.setSelection(0);
        }
        catch (Exception ex)
        {
            ex.getStackTrace();
        }
    }

    void visible()
    {
        try
        {
            textInputLayoutCodigoPostal.setVisibility(View.VISIBLE);
            textInputLayout.setVisibility(View.VISIBLE);
            textInputLayoutCiudadLocalidad.setVisibility(View.VISIBLE);
            textInputLayoutColoniaBarrio.setVisibility(View.VISIBLE);
            textInputLayoutCalle.setVisibility(View.VISIBLE);
            textInputLayoutNumeroExterior.setVisibility(View.VISIBLE);
            textInputLayoutNumeroInterior.setVisibility(View.VISIBLE);
            textInputLayoutEntreCalle1.setVisibility(View.VISIBLE);
            textInputLayoutEntreCalle2.setVisibility(View.VISIBLE);
            textInputLayoutReferenciaUbicacion.setVisibility(View.VISIBLE);
            textInputLayoutTelefono.setVisibility(View.VISIBLE);
            textInputLayoutComentarios.setVisibility(View.VISIBLE);
            linearLayoutPais.setVisibility(View.VISIBLE);
            linearLayoutEntidadFederativa.setVisibility(View.VISIBLE);
            linearLayoutTipoTelefono.setVisibility(View.VISIBLE);

            /*editText_Postal_Code.setVisibility(View.VISIBLE);
            editText_Calle.setVisibility(View.VISIBLE);
            editText_Delacion_Municipio.setVisibility(View.VISIBLE);
            editText_Ciudad_Localidad.setVisibility(View.VISIBLE);
            editText_Colonia_Barrio.setVisibility(View.VISIBLE);
            editText_Entre_Calles_1.setVisibility(View.VISIBLE);
            editText_Entre_Calles_2.setVisibility(View.VISIBLE);
            editText_Referencia_Ubicacion.setVisibility(View.VISIBLE);
            editText_Telefono.setVisibility(View.VISIBLE);
            editText_Comentarios.setVisibility(View.VISIBLE);
            editTextNumeroExterior.setVisibility(View.VISIBLE);
            editTextNumeroInterior.setVisibility(View.VISIBLE);
            spinner_Pais.setVisibility(View.VISIBLE);
            spinner_Entidad_Federativa.setVisibility(View.VISIBLE);
            spinner_Tipo_Telefono.setVisibility(View.VISIBLE);*/
        }
        catch (Exception ex)
        {
            ex.getStackTrace();
        }
    }

    void gone()
    {
        try
        {
            textInputLayoutCodigoPostal.setVisibility(View.GONE);
            textInputLayout.setVisibility(View.GONE);
            textInputLayoutCiudadLocalidad.setVisibility(View.GONE);
            textInputLayoutColoniaBarrio.setVisibility(View.GONE);
            textInputLayoutCalle.setVisibility(View.GONE);
            textInputLayoutNumeroExterior.setVisibility(View.GONE);
            textInputLayoutNumeroInterior.setVisibility(View.GONE);
            textInputLayoutEntreCalle1.setVisibility(View.GONE);
            textInputLayoutEntreCalle2.setVisibility(View.GONE);
            textInputLayoutReferenciaUbicacion.setVisibility(View.GONE);
            textInputLayoutTelefono.setVisibility(View.GONE);
            textInputLayoutComentarios.setVisibility(View.GONE);
            linearLayoutPais.setVisibility(View.GONE);
            linearLayoutEntidadFederativa.setVisibility(View.GONE);
            linearLayoutTipoTelefono.setVisibility(View.GONE);

            /*editText_Postal_Code.setVisibility(View.GONE);
            editText_Calle.setVisibility(View.GONE);
            editText_Delacion_Municipio.setVisibility(View.GONE);
            editText_Ciudad_Localidad.setVisibility(View.GONE);
            editText_Colonia_Barrio.setVisibility(View.GONE);
            editText_Entre_Calles_1.setVisibility(View.GONE);
            editText_Entre_Calles_2.setVisibility(View.GONE);
            editText_Referencia_Ubicacion.setVisibility(View.GONE);
            editText_Telefono.setVisibility(View.GONE);
            editText_Comentarios.setVisibility(View.GONE);
            editTextNumeroExterior.setVisibility(View.GONE);
            editTextNumeroInterior.setVisibility(View.GONE);
            spinner_Pais.setVisibility(View.GONE);
            spinner_Entidad_Federativa.setVisibility(View.GONE);
            spinner_Tipo_Telefono.setVisibility(View.GONE);*/
        }
        catch (Exception ex)
        {
            ex.getStackTrace();
        }
    }

    void fillPlaceMeeting()
    {
        dbHandler = new DBHandler(getActivity());
        placeMeeting = new PlaceMeeting();
        try
        {
            placeMeeting = dbHandler.GetPlaceMeeting(Integer.parseInt(GlobalVariables.groupDetail.getIdGROUP_DETAIL()));
            if(placeMeeting!=null || placeMeeting.getIdPLACEMEETING()!=null)
            {
                id = Integer.parseInt(placeMeeting.getIdPLACEMEETING());
                editText_Calle.setText(placeMeeting.getRoute());
                editTextNumeroExterior.setText(placeMeeting.getStreetNumber1());
                editTextNumeroInterior.setText(placeMeeting.getStreetNumber2());
                editText_Colonia_Barrio.setText(placeMeeting.getNeighborhood());
                editText_Delacion_Municipio.setText(placeMeeting.getAdministrativeAreaLevel2());
                spinner_Entidad_Federativa.setSelection(Integer.parseInt(placeMeeting.getIdFederalEntity()));
                editText_Postal_Code.setText(placeMeeting.getPostalCode());
                spinner_Pais.setSelection(Integer.parseInt(placeMeeting.getIdCountry()));
                editText_Entre_Calles_1.setText(placeMeeting.getRouteReference1());
                editText_Entre_Calles_2.setText(placeMeeting.getRouteReference2());
                editText_Telefono.setText(placeMeeting.getPhone());
                spinner_Tipo_Telefono.setSelection(Integer.parseInt(placeMeeting.getIdPhoneTypes()));
                editText_Referencia_Ubicacion.setText(placeMeeting.getLocationReference());
                editText_Comentarios.setText(placeMeeting.getComments());
            }
        }
        catch (Exception ex)
        {
            ex.getStackTrace();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_meeting_place, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_save){
            saveDialog();
        }
        return super.onOptionsItemSelected(item);
    }

    void saveDialog(){
        // Use the Builder class for convenient dialog construction
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("¿Desea guardar?")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //Action
                        if(GlobalVariables.groupData==null)
                        {
                            insertPlaceMeeting();
                        }
                        else
                        {
                            if(TextUtils.isEmpty(GlobalVariables.groupData.getIdGROUPDATA()))
                            {
                                insertPlaceMeeting();
                            }
                            else
                            {
                                updatePlaceMeeting();
                            }
                        }
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //Action
                        builder.setCancelable(false);
                    }
                });
        // Create the AlertDialog object and return
        builder.show();
    }

    void insertPlaceMeeting()
    {
        placeMeeting = new PlaceMeeting();
        try
        {
            placeMeeting.setRoute(editText_Calle.getText().toString());
            placeMeeting.setStreetNumber1(editTextNumeroExterior.getText().toString());
            placeMeeting.setStreetNumber2(editTextNumeroInterior.getText().toString());
            placeMeeting.setNeighborhood(editText_Colonia_Barrio.getText().toString());
            placeMeeting.setAdministrativeAreaLevel2(editText_Delacion_Municipio.getText().toString());
            placeMeeting.setIdFederalEntity(String.valueOf(entidad_federativa));
            placeMeeting.setPostalCode(editText_Postal_Code.getText().toString());
            placeMeeting.setIdCountry(String.valueOf(pais));
            placeMeeting.setRouteReference1(editText_Entre_Calles_1.getText().toString());
            placeMeeting.setRouteReference2(editText_Entre_Calles_2.getText().toString());
            placeMeeting.setIdGroupDetail(GlobalVariables.groupDetail.getIdGROUP_DETAIL());
            placeMeeting.setPhone(editText_Telefono.getText().toString());
            placeMeeting.setIdPhoneTypes(String.valueOf(tipo_telefono));
            placeMeeting.setLocationReference(editText_Referencia_Ubicacion.getText().toString());
            placeMeeting.setComments(editText_Comentarios.getText().toString());
            dbHandler = new DBHandler(getActivity());
            GlobalVariables.id_place_meeting = dbHandler.AddPlaceMeeting(placeMeeting);
            getActivity().finish();
        }
        catch (Exception e)
        {
            e.getStackTrace();
        }
    }

    void updatePlaceMeeting()
    {
        placeMeeting = new PlaceMeeting();
        try
        {
            placeMeeting.setIdPLACEMEETING(String.valueOf(id));
            placeMeeting.setRoute(editText_Calle.getText().toString());
            placeMeeting.setStreetNumber1(editTextNumeroExterior.getText().toString());
            placeMeeting.setStreetNumber2(editTextNumeroInterior.getText().toString());
            placeMeeting.setNeighborhood(editText_Colonia_Barrio.getText().toString());
            placeMeeting.setAdministrativeAreaLevel2(editText_Delacion_Municipio.getText().toString());
            placeMeeting.setIdFederalEntity(String.valueOf(entidad_federativa));
            placeMeeting.setPostalCode(editText_Postal_Code.getText().toString());
            placeMeeting.setIdCountry(String.valueOf(pais));
            placeMeeting.setRouteReference1(editText_Entre_Calles_1.getText().toString());
            placeMeeting.setRouteReference2(editText_Entre_Calles_2.getText().toString());
            placeMeeting.setIdGroupDetail(GlobalVariables.groupDetail.getIdGROUP_DETAIL());
            placeMeeting.setPhone(editText_Telefono.getText().toString());
            placeMeeting.setIdPhoneTypes(String.valueOf(tipo_telefono));
            placeMeeting.setLocationReference(editText_Referencia_Ubicacion.getText().toString());
            placeMeeting.setComments(editText_Comentarios.getText().toString());
            dbHandler = new DBHandler(getActivity());
            GlobalVariables.id_place_meeting = dbHandler.UpdatePlaceMeeting(placeMeeting);
        }
        catch (Exception ex)
        {
            ex.getStackTrace();
        }
    }

    void inserPlaceMeeting()
    {
        PlaceMeeting placeMeeting = new PlaceMeeting();
        try
        {
            placeMeeting.setRoute(editText_Calle.getText().toString());
            placeMeeting.setStreetNumber1(editTextNumeroExterior.getText().toString());
            placeMeeting.setStreetNumber2(editTextNumeroInterior.getText().toString());
            placeMeeting.setNeighborhood(editText_Colonia_Barrio.getText().toString());
            placeMeeting.setAdministrativeAreaLevel2(editText_Delacion_Municipio.getText().toString());
            placeMeeting.setIdFederalEntity(String.valueOf(entidad_federativa));
            placeMeeting.setPostalCode(editText_Postal_Code.getText().toString());
            placeMeeting.setIdCountry(String.valueOf(pais));
            placeMeeting.setRouteReference1(editText_Entre_Calles_1.getText().toString());
            placeMeeting.setRouteReference2(editText_Entre_Calles_2.getText().toString());
            placeMeeting.setIdGroupDetail(GlobalVariables.groupDetail.getIdGROUP_DETAIL());
            placeMeeting.setPhone(editText_Telefono.getText().toString());
            placeMeeting.setIdPhoneTypes(String.valueOf(tipo_telefono));
            placeMeeting.setLocationReference(editText_Referencia_Ubicacion.getText().toString());
            placeMeeting.setComments(editText_Comentarios.getText().toString());
            dbHandler = new DBHandler(getActivity());
            id = dbHandler.AddPlaceMeeting(placeMeeting);
            getActivity().runOnUiThread(new Runnable()
            {
                public void run()
                {
                    Toast.makeText(getActivity(), "Alta", Toast.LENGTH_LONG).show();
                }
            });


        }
        catch (Exception e)
        {
            getActivity().runOnUiThread(new Runnable()
            {
                public void run()
                {
                    Toast.makeText(getActivity(), "Error", Toast.LENGTH_LONG).show();
                }
            });
            e.getStackTrace();
        }
    }
}
