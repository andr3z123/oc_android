package fragments;


import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import adapter.Area1NothingSelectedSpinnerAdapter;
import data.GlobalVariables;
import implementation.DBHandler;
import model.GroupData;
import model.ListMembers;
import model.Member;
import model.Members;
import model.Name;
import model.PlaceMeeting;
import model.User;
import nubaj.com.ocandroid.MeetingPlaceActivity;
import nubaj.com.ocandroid.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class GroupFactsFragment extends Fragment implements View.OnClickListener
{
    private View root;

    private Button buttonLugarReunion;

    private TextView
            textViewPromotorResponsable,
            textView_Fecha_Primer_Pago,
            textView_Fecha_Desembolso,
            textView_Horario_Reunion;

    private int
            msYear,
            msMonth,
            msDay,
            msHour,
            msMinute,
            id_persona=0,
            frecuencia=0,
            dia_reunion=0;

    private Spinner spinner_Frecuencia, spinner_Dia_Reunion, spinner_Cliente_Presta_Casa;
    private DatePickerDialog pickerDialog_Date;
    private TimePickerDialog timePickerDialog;
    private DBHandler dbHandler;
    private EditText editTextFail, editTextDelay;
    private Calendar calendar_Date;
    private GroupData groupData;
    private ArrayList<String> list_Cliente_Presta_Casa;
    private ArrayAdapter<String> adapter_Cliente_Presta_Casa;
    private FloatingActionButton fab;
    private ArrayList<Members> membersArrayList;

    public GroupFactsFragment()
    {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_group_facts, container, false);
        initialize();
        setHasOptionsMenu(true);
        return root;
    }

    void initializeComponents()
    {
        buttonLugarReunion = (Button)root.findViewById(R.id.buttonLugarReunion);

        textViewPromotorResponsable = (TextView) root.findViewById(R.id.textViewPromotorResponsable);
        textView_Fecha_Primer_Pago = (TextView) root.findViewById(R.id.textViewFechaPrimerPago);
        textView_Fecha_Desembolso = (TextView) root.findViewById(R.id.textViewFechaDesembolso);
        textView_Horario_Reunion = (TextView) root.findViewById(R.id.textViewHorarioReunion);

        spinner_Frecuencia = (Spinner) root.findViewById(R.id.spinnerFrecuencia);
        spinner_Dia_Reunion = (Spinner) root.findViewById(R.id.spinnerDiaReunion);
        spinner_Cliente_Presta_Casa = (Spinner) root.findViewById(R.id.spinnerClientePrestaCasa);

        editTextFail = (EditText) root.findViewById(R.id.editTextFalta);
        editTextDelay = (EditText) root.findViewById(R.id.editTextRetardo);
        fab = (FloatingActionButton) root.findViewById(R.id.floatingActionButtonSave);
        fab.setOnClickListener(this);
    }

    void initialize()
    {
        initializeComponents();

        buttonLugarReunion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MeetingPlaceActivity.class);
                startActivity(intent);
            }
        });

        textView_Fecha_Primer_Pago.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                instanceDateAndTime(textView_Fecha_Primer_Pago);
                pickerDialog_Date.show();
            }
        });

        textView_Fecha_Desembolso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                instanceDateAndTime(textView_Fecha_Desembolso);
                pickerDialog_Date.show();
            }
        });

        textView_Horario_Reunion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                instanceDateAndTime(textView_Horario_Reunion);
                timePickerDialog.show();
            }
        });

        // Spinner Drop down elements type Genero
        List<String> list_Frecuencia = new ArrayList<String>();
        list_Frecuencia.add("Bi-Semanal - 8 PAGOS");
        list_Frecuencia.add("Bi-Semanal - 10 PAGOS");
        list_Frecuencia.add("Bi-Semanal - 12 PAGOS");
        list_Frecuencia.add("Bi-Semanal - 14 PAGOS");
        list_Frecuencia.add("Bi-Semanal - 16 PAGOS");

        //spinnerInstance(list_Genero, spinner_Genero);
        ArrayAdapter<String> adapter_Frecuencia = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, list_Frecuencia);
        adapter_Frecuencia.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_Frecuencia.setAdapter(adapter_Frecuencia);
        spinner_Frecuencia.setAdapter(new Area1NothingSelectedSpinnerAdapter(adapter_Frecuencia, R.layout.area_1_spinner_row_nothing_selected, getContext()));
        spinner_Frecuencia.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
            {
                if(i>0)
                {
                    frecuencia = i;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        // Spinner Drop down elements type Genero
        List<String> list_Dia_Reunion = new ArrayList<String>();
        list_Dia_Reunion.add("Lunes");
        list_Dia_Reunion.add("Martes");
        list_Dia_Reunion.add("Miercoles");
        list_Dia_Reunion.add("Jueves");
        list_Dia_Reunion.add("Viernes");
        list_Dia_Reunion.add("Sabado");

        ArrayAdapter<String> adapter_Dia_Reunion = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, list_Dia_Reunion);
        adapter_Dia_Reunion.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_Dia_Reunion.setAdapter(adapter_Dia_Reunion);
        spinner_Dia_Reunion.setAdapter(new Area1NothingSelectedSpinnerAdapter(adapter_Dia_Reunion, R.layout.area_1_spinner_row_nothing_selected, getContext()));
        spinner_Dia_Reunion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
            {
                if(i>0)
                {
                    dia_reunion = i;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        // Spinner Drop down elements type Genero
        fillListMembers();


        spinner_Cliente_Presta_Casa.setAdapter(new Area1NothingSelectedSpinnerAdapter(adapter_Cliente_Presta_Casa, R.layout.area_1_spinner_row_nothing_selected, getContext()));
        spinner_Cliente_Presta_Casa.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent)
            {
                try
                {
                    if (list_Cliente_Presta_Casa.size() < GlobalVariables.membersArrayList.size()) {
                        fillListMembers();
                    }
                }
                catch (Exception ex)
                {
                    ex.getStackTrace();
                }
                return false;
            }
        });

        spinner_Cliente_Presta_Casa.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                id_persona = i;
                searchIdName(id_persona);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        fillGroupData();
    }

    void searchIdName(int id)
    {
        int idName = 0;
        try
        {
            if(membersArrayList!=null)
            {
                if(membersArrayList.size()>0)
                {
                    GlobalVariables.id_name_group_data = Integer.parseInt(membersArrayList.get(id).getIdName());
                    id_persona = Integer.parseInt(membersArrayList.get(id).getIdName());
                }
            }
        }
        catch (Exception ex)
        {
            ex.getStackTrace();
        }
    }

    void fillListMembers()
    {
        list_Cliente_Presta_Casa = new ArrayList<String>();
        dbHandler = new DBHandler(getActivity());
        membersArrayList = new ArrayList<Members>();
        try
        {
            membersArrayList = dbHandler.GetListMembers(Integer.parseInt(GlobalVariables.groupDetail.getIdGROUP_DETAIL()));
            for(Members members: membersArrayList)
            {
                for (Name name: GlobalVariables.getAllNameList)
                {
                    if(name.getIdName().equals(members.getIdName()))
                    {
                        list_Cliente_Presta_Casa.add(name.getName1()+" "+name.getName2()+" "+name.getLastName()+" "+name.getmLastName());
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ex.getStackTrace();
        }
        adapter_Cliente_Presta_Casa = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, list_Cliente_Presta_Casa);
        adapter_Cliente_Presta_Casa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_Cliente_Presta_Casa.setAdapter(adapter_Cliente_Presta_Casa);
    }

    void fillGroupData()
    {
        dbHandler = new DBHandler(getActivity());
        try
        {
            if(GlobalVariables.groupDetail.getIdGROUP_DETAIL()!=null)
            {
                GlobalVariables.groupData = dbHandler.GetGroupData(Integer.parseInt(GlobalVariables.groupDetail.getIdGROUP_DETAIL()));
                if (GlobalVariables.groupData != null)
                {
                    editTextFail.setText(GlobalVariables.groupData.getAmountMissingFine());
                    editTextDelay.setText(GlobalVariables.groupData.getAmountFineDelay());
                    textView_Horario_Reunion.setText(GlobalVariables.groupData.getMeetingSchedule());
                    textView_Fecha_Primer_Pago.setText(GlobalVariables.groupData.getMeetingDate());
                    textView_Fecha_Desembolso.setText(GlobalVariables.groupData.getDisbursementDate());
                    textView_Fecha_Primer_Pago.setText(GlobalVariables.groupData.getPayDate1());
                    spinner_Frecuencia.setSelection(Integer.parseInt(GlobalVariables.groupData.getIdFrequencies()));
                    spinner_Dia_Reunion.setSelection(Integer.parseInt(GlobalVariables.groupData.getIdDeadlines()));
                    spinner_Cliente_Presta_Casa.setSelection(Integer.parseInt(GlobalVariables.groupData.getIdNameGroupData()));
                    if(GlobalVariables.groupDetail!=null)
                    {
                        if(!TextUtils.isEmpty(GlobalVariables.groupDetail.getIdUser()))
                        {
                            User user = new User();
                            user = dbHandler.GetUser(Integer.parseInt(GlobalVariables.groupDetail.getIdUser()));
                            textViewPromotorResponsable.setText(user.getUserName().toString());
                        }
                        else
                        {
                            textViewPromotorResponsable.setText(GlobalVariables.userArrayList.get(0).getUserName());
                        }
                    }
                    else
                    {
                        textViewPromotorResponsable.setText(GlobalVariables.userArrayList.get(0).getUserName());
                    }

                }
                else
                {
                    textViewPromotorResponsable.setText(GlobalVariables.userArrayList.get(0).getUserName());
                }
            }
            else
            {
                textViewPromotorResponsable.setText(GlobalVariables.userArrayList.get(0).getUserName());
            }

            spinner_Cliente_Presta_Casa.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    id_persona = i;
                    searchIdName(id_persona);
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        }
        catch (Exception ex)
        {
            ex.getStackTrace();
        }
    }

    public void onClick(View view)
    {
        try
        {
            if(view.getId()==R.id.floatingActionButtonSave)
            {
                if(!TextUtils.isEmpty(GlobalVariables.groupDetail.getIdGROUP_DETAIL()))
                {
                    if(!TextUtils.isEmpty(editTextFail.getText().toString())||!TextUtils.isEmpty(editTextDelay.getText().toString())||!textView_Horario_Reunion.getText().equals("hh:mm") || msDay!=0 &&msMonth!=0||msYear!=0||!textView_Fecha_Desembolso.getText().equals("aaaa-mm-dd")||!textView_Fecha_Primer_Pago.getText().equals("aaaa-mm-dd")||frecuencia!=0||dia_reunion!=0)
                    {
                        if (GlobalVariables.groupData != null)
                        {
                            if (!TextUtils.isEmpty(GlobalVariables.groupData.getIdGROUPDATA()))
                            {
                                updateGroupData();
                            }
                        }
                        else
                        {
                            if (!TextUtils.isEmpty(editTextFail.getText().toString()) || !TextUtils.isEmpty(editTextDelay.getText().toString()) || !textView_Horario_Reunion.getText().equals("hh:mm") || !TextUtils.isEmpty(String.valueOf(msDay)) && !TextUtils.isEmpty(String.valueOf(msMonth)) || !TextUtils.isEmpty(String.valueOf(msYear)) || !textView_Fecha_Desembolso.getText().equals("aaaa-mm-dd") || !textView_Fecha_Primer_Pago.getText().equals("aaaa-mm-dd") || frecuencia != 0 || dia_reunion != 0) {
                                insertGroupData();
                            }
                            else
                            {
                                Snackbar.make(getView(), "Necesita ingresar datos al grupo", Snackbar.LENGTH_SHORT).setAction("Action", null).show();
                            }
                        }
                    }
                    else
                    {
                        Snackbar.make(getView(), "Necesita ingresar datos al grupo", Snackbar.LENGTH_SHORT).setAction("Action", null).show();
                    }
                }
                else
                {
                    Snackbar.make(getView(),"Necesita guardar el nombre del grupo", Snackbar.LENGTH_SHORT).setAction("Action",null).show();
                }
            }
        }
        catch (Exception ex)
        {
            ex.getStackTrace();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        return super.onOptionsItemSelected(item);
    }

    void instanceDateAndTime(final TextView textView_Date) {

        calendar_Date = Calendar.getInstance();

        pickerDialog_Date = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);

                msYear = year;
                msMonth = monthOfYear;
                msDay = dayOfMonth;

                textView_Date.setText(GlobalVariables.getFormatDate(msDay, msMonth, msYear));

            }

        }, calendar_Date.get(Calendar.YEAR), calendar_Date.get(Calendar.MONTH), calendar_Date.get(Calendar.DAY_OF_MONTH));


        final Calendar c = Calendar.getInstance();
        msHour = c.get(Calendar.HOUR_OF_DAY);
        msMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        timePickerDialog = new TimePickerDialog(getContext(),
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {
                        msHour = hourOfDay;
                        msMinute = minute;

                        textView_Date.setText(String.format("%02d:%02d", msHour, msMinute));
                    }
                }, msHour, msMinute, false);
    }

    private void insertGroupData()
    {
        groupData = new GroupData();
        try
        {
            groupData.setMiniumAmountSavings("0.0");
            groupData.setAmountMissingFine(editTextFail.getText().toString());
            groupData.setAmountFineDelay(editTextDelay.getText().toString());
            groupData.setMeetingSchedule(textView_Horario_Reunion.getText().toString());
            groupData.setMeetingDate(msYear+"-"+msMonth+"-"+msDay);
            groupData.setDisbursementDate(textView_Fecha_Desembolso.getText().toString());
            groupData.setPayDate1(textView_Fecha_Primer_Pago.getText().toString());
            groupData.setIdProduct("1");
            groupData.setIdFrequencies(String.valueOf(frecuencia));
            groupData.setIdDeadlines(String.valueOf(dia_reunion));
            groupData.setIdPlaceMeeting(String.valueOf(GlobalVariables.id_place_meeting));
            groupData.setIdGroupDetail(GlobalVariables.groupDetail.getIdGROUP_DETAIL().toString());
            groupData.setIdNameGroupData(String.valueOf(id_persona));
            groupData.setComplete(getString(R.string.value_true));
            dbHandler = new DBHandler(getActivity());
            dbHandler.AddGroupData(groupData);
            Toast.makeText(getActivity(), "Alta", Toast.LENGTH_LONG).show();


        }
        catch (Exception e)
        {
            Toast.makeText(getActivity(), "Error", Toast.LENGTH_LONG).show();
            e.getStackTrace();
        }
    }

    private void updateGroupData()
    {
        groupData = new GroupData();
        try
        {
            groupData.setIdGROUPDATA(GlobalVariables.groupData.getIdGROUPDATA());
            groupData.setMiniumAmountSavings("0.0");
            groupData.setAmountMissingFine(editTextFail.getText().toString());
            groupData.setAmountFineDelay(editTextDelay.getText().toString());
            groupData.setMeetingSchedule(textView_Horario_Reunion.getText().toString());
            groupData.setMeetingDate(msYear+"-"+msMonth+"-"+msDay);
            groupData.setDisbursementDate(textView_Fecha_Desembolso.getText().toString());
            groupData.setPayDate1(textView_Fecha_Primer_Pago.getText().toString());
            groupData.setIdProduct("1");
            groupData.setIdFrequencies(String.valueOf(frecuencia));
            groupData.setIdDeadlines(String.valueOf(dia_reunion));
            groupData.setIdPlaceMeeting(String.valueOf(GlobalVariables.id_place_meeting));
            groupData.setIdGroupDetail(GlobalVariables.groupDetail.getIdGROUP_DETAIL().toString());
            groupData.setIdNameGroupData(String.valueOf(id_persona));
            groupData.setComplete(getString(R.string.value_true));
            dbHandler = new DBHandler(getActivity());
            dbHandler.UpdateGroupData(groupData);
            Toast.makeText(getActivity(), "Alta", Toast.LENGTH_LONG).show();
        }
        catch (Exception ex)
        {
            Toast.makeText(getActivity(), "Error", Toast.LENGTH_LONG).show();
            ex.getStackTrace();
        }
    }
}
