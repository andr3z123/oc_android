package fragments;


import android.content.Intent;
import android.database.CursorIndexOutOfBoundsException;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import adapter.CustomRecyclerAdapterApplicantsNews;
import data.GlobalVariables;
import implementation.DBHandler;
import model.ApplicantsNews;
import model.Economic;
import model.Name;
import model.NameUser;
import nubaj.com.ocandroid.ApplicantsActivity;
import nubaj.com.ocandroid.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class SynchronizedFragment extends Fragment
{
    static DBHandler dbHandler;
    static Name name;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<ApplicantsNews> items;
    private RelativeLayout relativeLayoutInternet;
    private TextView textViewInternet;
    private boolean entry = false;
    private MenuItem searchViewItem;

    public SynchronizedFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_synchronized, container, false);
        mRecyclerView = (RecyclerView) root.findViewById(R.id.reciclador);
        relativeLayoutInternet = (RelativeLayout) root.findViewById(R.id.relativeLayoutInternet);
        textViewInternet = (TextView) root.findViewById(R.id.textViewInternet);

        fillList();
        setHasOptionsMenu(true);
        return root;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.main, menu);

        searchViewItem = menu.findItem(R.id.action_search_simple);
        final SearchView searchViewAndroidActionBar = (SearchView) MenuItemCompat.getActionView(searchViewItem);
        searchViewAndroidActionBar.setOnQueryTextListener(searchFilterListener); // call the QuerytextListner.
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_add) {
            GlobalVariables.setServiceData = false;
            if (GlobalVariables.phone != null && GlobalVariables.address !=null && GlobalVariables.additional != null){
                GlobalVariables.phone.clear();
                GlobalVariables.address.clear();
                GlobalVariables.additional.clear();
            }
            Intent intent = new Intent(getActivity(), ApplicantsActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    SearchView.OnQueryTextListener searchFilterListener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextChange(String query) {
            //query = query.toLowerCase();

            final ArrayList<ApplicantsNews> filtered_List = new ArrayList<>();

            for (int i = 0; i < items.size(); i++) {

                final String text_Filter = items.get(i).getNombre_Solicitante().toString();
                //final String text = String.valueOf(items.get(i)).toString();
                if (text_Filter.toUpperCase().contains(query) || text_Filter.toLowerCase().contains(query)) {
                    filtered_List.add(items.get(i));
                }
            }

            mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            mAdapter = new CustomRecyclerAdapterApplicantsNews(getActivity(), filtered_List);
            mRecyclerView.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();  // data set changed
            return true;

        }

        public boolean onQueryTextSubmit(String query) {
            return false;
        }
    };

    @Override
    public void onResume()
    {
        super.onResume();
        fillList();
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                if (mRecyclerView.getAdapter() != null) {
                    ((CustomRecyclerAdapterApplicantsNews) mAdapter).setOnItemClickListener(new CustomRecyclerAdapterApplicantsNews
                            .MyClickListener() {
                        @Override
                        public void onItemClick(int position, View v)
                        {
                            for(NameUser nameUser: GlobalVariables.nameUserArrayList)
                            {
                                if(items.get(position).getIdName().equals(String.valueOf(nameUser.getIdNameServer())))
                                {
                                    GlobalVariables.position = nameUser.getIdNAME();
                                    GlobalVariables.id_Name = nameUser.getIdNAME();
                                    break;
                                }
                            }
                            //GlobalVariables.id_Name = GlobalVariables.nameUserArrayList.get(position).getIdNAME();
                            GlobalVariables.setServiceData = true;
                            getDataPhone();
                            getDataAddress();
                            getDataBasics();
                            getDataEconomic();
                            getDataComplementary();
                            getDataAddional();
                            Intent intent = new Intent(getActivity(), ApplicantsActivity.class);
                            startActivity(intent);
                        }
                    });
                }
            }
        });
    }

    void getDataPhone() {
        dbHandler = new DBHandler(getActivity());
        try {
            GlobalVariables.phone = dbHandler.GetAllPhones(GlobalVariables.id_Name);
            String x = "";
        } catch (CursorIndexOutOfBoundsException e) {

        }
    }

    void getDataAddress() {
        dbHandler = new DBHandler(getActivity());
        try {
            GlobalVariables.address = dbHandler.GetAllAddress(GlobalVariables.id_Name);
        } catch (CursorIndexOutOfBoundsException e) {

        }
    }

    void getDataAddional() {
        dbHandler = new DBHandler(getActivity());
        try {
            GlobalVariables.additional = dbHandler.GetAdditional(GlobalVariables.id_Name);

        } catch (CursorIndexOutOfBoundsException e) {

        }
    }

    void getDataBasics() {
        try {
            dbHandler = new DBHandler(getActivity());
            GlobalVariables.basics = dbHandler.GetBasics(GlobalVariables.id_Name);
        } catch (CursorIndexOutOfBoundsException e) {

        }
    }

    void getDataEconomic()
    {
        try
        {
            dbHandler = new DBHandler(getActivity());
            GlobalVariables.economic = new Economic();
            GlobalVariables.economic = dbHandler.GetEconomic(GlobalVariables.id_Name);
            if(GlobalVariables.economic!=null)
            {
                if(TextUtils.isEmpty(GlobalVariables.economic.getIdEconomicActivity()))
                {
                    GlobalVariables.activity_economic = Integer.parseInt(GlobalVariables.economic.getIdEconomicActivity());
                }
            }
        }
        catch (CursorIndexOutOfBoundsException e)
        {

        }
    }

    void getDataComplementary() {
        try {
            dbHandler = new DBHandler(getActivity());
            GlobalVariables.complementary = dbHandler.GetComplementary(GlobalVariables.id_Name);
        } catch (CursorIndexOutOfBoundsException e) {

        }
    }

    void fillList() {

        items = new ArrayList<>();

        try
        {
            dbHandler = new DBHandler(getActivity());
            GlobalVariables.nameUserArrayList = dbHandler.GetAllAplicants(Integer.parseInt(GlobalVariables.responseUser.getUser().getIdUSERS()));
            ApplicantsNews applicantsNews;

            if (GlobalVariables.nameUserArrayList != null && GlobalVariables.nameUserArrayList.size() >0)
            {
                for (NameUser nameUser : GlobalVariables.nameUserArrayList)
                {
                    if(nameUser.isComplete()==true)
                    {
                        applicantsNews = new ApplicantsNews();
                        applicantsNews.setIcon_Status(R.mipmap.ic_launcher);
                        applicantsNews.setNombre_Solicitante(nameUser.getName1() + " " + nameUser.getName2() + " " + nameUser.getLastName() + " " + nameUser.getmLastName());
                        applicantsNews.setFecha_Ingreso(nameUser.getDateAdmission());
                        applicantsNews.setId_Cliente(String.valueOf(nameUser.getIdNAME()));
                        applicantsNews.setDias_Restantes(nameUser.getDayRet());
                        applicantsNews.setFecha_Ingreso(nameUser.getDateAdmission());
                        applicantsNews.setIdName(String.valueOf(nameUser.getIdNameServer()));
                        applicantsNews.setIdNameServer(nameUser.getIdNAME());
                        items.add(applicantsNews);
                    }
                }

                if (items.size() != 0)
                {
                    mRecyclerView.setHasFixedSize(true);
                    mLayoutManager = new LinearLayoutManager(getActivity());

                    mRecyclerView.setLayoutManager(mLayoutManager);
                    mAdapter = new CustomRecyclerAdapterApplicantsNews(getActivity(), items);
                    mAdapter.notifyDataSetChanged();
                    mRecyclerView.setAdapter(mAdapter);
                    if (GlobalVariables.isRefresh_news_applicants_boolean == true)
                    {
                        getFragmentManager().beginTransaction().detach(this).attach(this).commit();
                        GlobalVariables.isRefresh_news_applicants_boolean = false;
                    }
                    //setHasOptionsMenu(true);
                }
                else
                {
                    //Toast.makeText(getActivity(), "No tiene conexión a internet", Toast.LENGTH_SHORT).show();
                    mRecyclerView.setVisibility(View.GONE);
                    relativeLayoutInternet.setVisibility(View.VISIBLE);
                    textViewInternet.setText("No se encontró información");
                }
            }
            else
            {
                mRecyclerView.setVisibility(View.GONE);
                relativeLayoutInternet.setVisibility(View.VISIBLE);
                //textViewInternet.setText(GlobalVariables.responseNameUser.getMensaje().toString());
                textViewInternet.setText("No se encontró información");
            }
        }
        catch (Exception ex)
        {
            ex.getStackTrace();
        }
    }

}
