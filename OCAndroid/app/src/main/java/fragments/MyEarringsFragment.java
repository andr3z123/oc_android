package fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.ArrayList;

import adapter.CustomRecyclerAdapterMyEarrings;
import model.MyEarrings;
import nubaj.com.ocandroid.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyEarringsFragment extends Fragment {

    private View root;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<MyEarrings> items;
    private RelativeLayout relativeLayoutItemsZero;
    private RelativeLayout relativeLayoutInternet;

    public MyEarringsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_my_earrings, container, false);
        mRecyclerView = (RecyclerView) root.findViewById(R.id.reciclador);
        relativeLayoutItemsZero = (RelativeLayout) root.findViewById(R.id.relativeLayoutZeroItems);
        relativeLayoutInternet = (RelativeLayout) root.findViewById(R.id.relativeLayoutInternet);

        items = new ArrayList<>();

        MyEarrings additional = new MyEarrings();
        additional.setId_Oportunidad("753179");
        additional.setNombre_Pendiente("Los linces");
        additional.setFecha("12/05/2016");
        additional.setIcon_Editar(R.mipmap.ic_my_earrings_1);

        items.add(additional);

        additional = new MyEarrings();
        additional.setId_Oportunidad("753179");
        additional.setNombre_Pendiente("Los linces");
        additional.setFecha("12/05/2016");
        additional.setIcon_Editar(R.mipmap.ic_my_earrings_1);

        items.add(additional);

        additional = new MyEarrings();
        additional.setId_Oportunidad("753179");
        additional.setNombre_Pendiente("Los linces");
        additional.setFecha("12/05/2016");
        additional.setIcon_Editar(R.mipmap.ic_my_earrings_1);

        items.add(additional);

        if (items.size() != 0) {
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new CustomRecyclerAdapterMyEarrings(getActivity(), items);
        mRecyclerView.setAdapter(mAdapter);
        } else {
            mRecyclerView.setVisibility(View.GONE);
            //search.setVisibility(View.GONE);
            relativeLayoutItemsZero.setVisibility(View.VISIBLE);
        }
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(mRecyclerView.getAdapter()!=null)
        {
            ((CustomRecyclerAdapterMyEarrings) mAdapter).setOnItemClickListener(new CustomRecyclerAdapterMyEarrings
                    .MyClickListener()
            {
                @Override
                public void onItemClick(int position, View v)
                {

                    Toast.makeText(getActivity(), "No hay información", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}
