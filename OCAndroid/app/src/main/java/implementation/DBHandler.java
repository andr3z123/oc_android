package implementation;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import model.Additional;
import model.Additionals;
import model.Address;
import model.Basics;
import model.Beneficiary;
import model.Complementary;
import model.Document;
import model.Economic;
import model.GroupData;
import model.GroupDetail;
import model.Members;
import model.Name;
import model.NameUser;
import model.Phone;
import model.PlaceMeeting;
import model.Prospect;
import model.Secure;
import model.Signing;
import model.User;

/**
 * Created by andresaleman on 2/14/17.
 */

public class DBHandler extends SQLiteOpenHelper
{
    //Variables
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "oc";

    //Tables
    private static final String TABLE_USER="User";
    private static final String TABLE_NAME="Name";
    private static final String TABLE_SIGNING="SIGNING";
    private static final String TABLE_PROSPECT="Prospect";
    private static final String TABLE_ADDRESS="Address";
    private static final String TABLE_PHONES="Phones";
    private static final String TABLE_BASICS="Basics";
    private static final String TABLE_ECONOMIC="Economic";
    private static final String TABLE_COMPLEMENTARY="Complementary";
    private static final String TABLE_ADDITIONAL="Additional";
    private static final String TABLE_GROUP_DETAIL="GroupDetail";
    private static final String TABLE_MEMBERS="Members";
    private static final String TABLE_GROUP_DATA="GroupData";
    private static final String TABLE_PLACE_MEETING="PlaceMeeting";
    private static final String TABLE_DOCUMENTS="Documents";
    private static final String TABLE_SECURE="Secure";
    private static final String TABLE_BENEFICIARY="Beneficiary";

    //Columns
    private static final String KEY_ID_USER="Id";
    private static final String KEY_USER_NAME="UserName";
    private static final String KEY_PASSWORD="Password";

    private static final String KEY_ID_NAME="Id";
    private static final String KEY_NAME1="Name1";
    private static final String KEY_NAME2="Name2";
    private static final String KEY_LAST_NAME="LastName";
    private static final String KEY_M_LAST_NAME="MLastName";
    private static final String KEY_ID_USER_NAME="IdUser";
    private static final String KEY_COMPLETE="Complete";
    private static final String KEY_DATE_ADMISSION="DateAdmission";
    private static final String KEY_DAY_RET="DayRet";
    private static final String KEY_ID_STATUS_NAME="IdStatus";
    private static final String KEY_ID_PRODUCT_NAME="IdProduct";
    private static final String KEY_ID_NAME_SERVER="IdNameServer";

    private static final String KEY_ID_SIGNING="Id";
    private static final String KEY_DESCRIPTION_SIGNING="DescriptionSigning";

    private static final String KEY_ID_PROSPECT="Id";
    private static final String KEY_ID_ROL_PROSPECT="IdRol";
    private static final String KEY_ID_STATUS_PROSPECT="IdStatus";
    private static final String KEY_ID_PRODUCT_PROSPECT="IdProduct";
    private static final String KEY_COMPLETE_PROSPECT="Complete";
    private static final String KEY_ID_NAME_PROSPECT="IdName";

    private static final String KEY_ID_ADDRESS="Id";
    private static final String KEY_ROUTE="Route";
    private static final String KEY_STREET_NUMBER1="StreetNumber1";
    private static final String KEY_STREET_NUMBER2="StreetNumber2";
    private static final String KEY_NEIGHBORHOOD = "Neighborhood";
    private static final String KEY_ADMINISTRATIVEAREALEVEL2="AdministrativeAreaLevel2";
    private static final String KEY_ID_FEDERAL_ENTITY_ADDRESS="IdFederalEntity";
    private static final String KEY_POSTAL_CODE="PostalCode";
    private static final String KEY_ID_COUNTRY_ADDRESS="IdCountry";
    private static final String KEY_ROUTE_REFERENCE1="RouteReference1";
    private static final String KEY_ROUTE_REFERENCE2="RouteReference2";
    private static final String KEY_ID_NAME_ADDRESS="IdName";
    private static final String KEY_COMPLETE_ADDRESS="Complete";
    private static final String KEY_ID_ADDRESS_TYPE="IdAddressType";

    private static final String KEY_ID_PHONES="Id";
    private static final String KEY_NUMBER_PHONES="Number";
    private static final String KEY_ID_PHONE_TYPES_PHONES="IdPhoneTypes";
    private static final String KEY_ID_NAME_PHONES="IdName";
    private static final String KEY_COMPLETE_PHONES="Complete";

    private static final String KEY_ID_BASICS="Id";
    private static final String KEY_BIRTH_DATE="BirthDate";
    private static final String KEY_VOTER_KEY="VoterKey";
    private static final String KEY_NUM_REG_VOTER="NumRegVoter";
    private static final String KEY_ID_GENDER_BASICS="IdGender";
    private static final String KEY_ID_COUNTRY_BIRTH_BASICS="IdCountryBirth";
    private static final String KEY_ID_FEDERAL_ENTITY_BASICS="IdFederalEntity";
    private static final String KEY_ID_NATIONALITY_BASICS="IdNationality";
    private static final String KEY_ID_CIVIL_STATUS_BASICS="IdCivilStatus";
    private static final String KEY_SONS="Sons";
    private static final String KEY_ID_LIVING_PLACE_BASICS="IdLivingPlace";
    private static final String KEY_ID_LEVEL_EDUCATION_BASICS="IdLevelEducation";
    private static final String KEY_ID_KIND_LOCAL="IdKindLocal";
    private static final String KEY_ID_NAME_BASICS="IdName";
    private static final String KEY_COMPLETE_BASICS="Complete";

    private static final String KEY_ID_ECONOMIC="Id";
    private static final String KEY_ID_ECONOMIC_ACITIVTY_ECONOMIC="IdEconomicActivity";
    private static final String KEY_CONTRIBUTION_LEVEL="ContributionLevel";
    private static final String KEY_ANOTHER_SOURCE_NAME="AnotherSourceName";
    private static final String KEY_CURRENT_BUSINESS_TIME="CurrentBusinessTime";
    private static final String KEY_ID_LOCAL_TYPE_ECONOMIC="IdLocalType";
    private static final String KEY_ID_MAKES_ACTIVITY_ECONOMIC="IdMakesActivity";
    private static final String KEY_ID_TIME_ACTIVITY_ECONOMIC="IdTimeActivity";
    private static final String KEY_EMAIL_ECONOMIC="Email";
    private static final String KEY_ID_NAME_ECONOMIC="IdName";
    private static final String KEY_COMPLETE_ECONOMIC="Complete";

    private static final String KEY_ID_COMPLEMENTARY="Id";
    private static final String KEY_PHYSICAL_PERSON="PhysicalPerson";
    private static final String KEY_CURP="Curp";
    private static final String KEY_DEPENDENT_NUMBER="DependentNumber";
    private static final String KEY_ID_OCCUPATION_COMPLEMENTARY="IdOccupation";
    private static final String KEY_EMAIL_COMPLEMENTARY="Email";
    private static final String KEY_HOMOCLAVE="Homoclave";
    private static final String KEY_FIEL="Fiel";
    private static final String KEY_ID_NAME_COMPLEMENTARY="IdName";
    private static final String KEY_COMPLETE_COMPLEMENTARY="Complete";

    private static final String KEY_ID_ADDITIONAL="Id";
    private static final String KEY_NAME1_ADDITIONAL="Name1";
    private static final String KEY_NAME2_ADDITIONAL="Name2";
    private static final String KEY_LAST_NAME_ADDITIONAL="LastName";
    private static final String KEY_M_LAST_NAME_ADDITIONAL="MLastName";
    private static final String KEY_ID_OCCUPATION_ADDITIONAL="IdOccupation";
    private static final String KEY_PHONE_ADDITIONAL="Phone";
    private static final String KEY_ID_NAME_ADDITIONAL="IdName";
    private static final String KEY_ID_ADDITIONAL_TYPES_ADDITIONAL="IdAdditionalTypes";
    private static final String KEY_COMPLETE_ADDITIONAL="Complete";

    private static final String KEY_ID_GROUP_DETAIL="Id";
    private static final String KEY_GROUP_NAME="GroupName";
    private static final String KEY_GROUP_CYCLE="GroupCycle";
    private static final String KEY_ID_SPONSOR_GROUP_DETAIL="IdSponsor";
    private static final String KEY_ID_CHANNEL_DISPERSION="IdChannelDispersion";
    private static final String KEY_ID_DISPERSION_MEDIUM="IdDispersionMedium";
    private static final String KEY_ID_USER_GROUP_DETAIL="IdUser";
    private static final String KEY_DATE_CREATE_GROUP="Date";
    private static final String KEY_ID_GROUP_DETAIL_SERVER="IdGroupDetailServer";
    private static final String KEY_COMPLETE_GROUP_DETAIL="Complete";

    private static final String KEY_ID_MEMBERS="Id";
    private static final String KEY_ID_GROUP_DETAIL_MEMBERS="IdGroupDetail";
    private static final String KEY_ID_NAME_MEMBERS="IdName";
    private static final String KEY_ID_NAME_MEMBERS_SERVER="IdMembersServer";
    private static final String KEY_COMPLETE_MEMBERS="Complete";

    private static final String KEY_ID_GROUP_DATA="Id";
    private static final String KEY_MINIUM_AMOUNT_SAVINGS="MiniumAmountSavings";
    private static final String KEY_AMOUNT_FINE_DELAY="AmountFineDelay";
    private static final String KEY_AMOUNT_MISSING_FINE="AmountMissingFine";
    private static final String KEY_MEETING_SCHEDULE="MeetingSchedule";
    private static final String KEY_MEETING_DATE="MeetingDate";
    private static final String KEY_DISBURSEMENT_DATE="DisbursementDate";
    private static final String KEY_PAY_DATE1="PayDate1";
    private static final String KEY_ID_PRODUCT_GROUP_DATA="IdProduct";
    private static final String KEY_ID_FREQUENCIES_GROUP_DATA="IdFrequencies";
    private static final String KEY_ID_DEADLINES_GROUP_DATA="IdDeadlines";
    private static final String KEY_ID_PLACE_MEETING_GROUP_DATA="IdPlaceMeeting";
    private static final String KEY_ID_GROUP_DETAIL_GROUP_DATA="IdGroupDetail";
    private static final String KEY_ID_NAME_GROUP_DATA="IdName";
    private static final String KEY_COMPLETE_GROUP_DATA="Complete";

    private static final String KEY_ID_PLACE_MEETING="Id";
    private static final String KEY_ROUTE_PLACE_MEETING="Route";
    private static final String KEY_STREET_NUMBER1_PLACE_MEETING="StreetNumber1";
    private static final String KEY_STREET_NUMBER2_PLACE_MEETING="StreetNumber2";
    private static final String KEY_NEIGHBORHOOD_PLACE_MEETING = "Neighborhood";
    private static final String KEY_ADMINISTRATIVEAREALEVEL2_PLACE_MEETING="AdministrativeAreaLevel2";
    private static final String KEY_ID_FEDERAL_ENTITY_PLACE_MEETING="IdFederalEntity";
    private static final String KEY_POSTAL_CODE_PLACE_MEETING="PostalCode";
    private static final String KEY_ID_COUNTRY_PLACE_MEETING="IdCountry";
    private static final String KEY_ROUTE_REFERENCE1_PLACE_MEETING="RouteReference1";
    private static final String KEY_ROUTE_REFERENCE2_PLACE_MEETING="RouteReference2";
    private static final String KEY_LOCATION_REFERENCE="LocationReference";
    private static final String KEY_PHONE_PLACE_MEETING="Phone";
    private static final String KEY_ID_PHONE_TYPES_PLACE_MEETING="IdPhoneTypes";
    private static final String KEY_COMMENTS_PLACE_MEETING="Comments";
    private static final String KEY_ID_GROUP_DETAIL_PLACE_MEETING="IdGroupDetail";

    private static final String KEY_ID_DOCUMENTS="IdDocuments";
    private static final String KEY_DESCRIPTION_DOCUMENTS="DescriptionDocuments";
    private static final String KEY_ID_NAME_DOCUMENTS="IdName";

    private static final String KEY_ID_SECURE="IdSecure";
    private static final String KEY_START_DATE="StartDate";
    private static final String KEY_LIMIT_TIME="LimitTime";
    private static final String KEY_MODALITY="Modality";
    private static final String KEY_ID_NAME_SECURE="IdName";
    private static final String KEY_COMPLETE_SECURE="Complete";

    private static final String KEY_ID_BENEFICIARY="Id";
    private static final String KEY_NAME1_BENEFICIARY="Name1";
    private static final String KEY_NAME2_BENEFICIARY="Name2";
    private static final String KEY_LAST_NAME_BENEFICIARY="LastName";
    private static final String KEY_M_LAST_NAME_BENEFICIARY="MLastName";
    private static final String KEY_ID_RELATIONSHIP="IdRelationship";
    private static final String KEY_BIRTH_DATE_BENEFICIARY="BirthDate";
    private static final String KEY_ID_GENDER_BENEFICIARY="IdGender";
    private static final String KEY_ID_SIGNING_BENEFICIARY="IdSigning";
    private static final String KEY_ID_SECURE_BENEFICIARY="IdSecure";


    //Constructor
    public DBHandler(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    //Override Create Database
    @Override
    public void onCreate(SQLiteDatabase db)
    {
        /*Assign Tables*/
        String CREATE_TABLE_USER = "CREATE TABLE IF NOT EXISTS " + TABLE_USER + "("
                + KEY_ID_USER + " INTEGER PRIMARY KEY,"
                + KEY_USER_NAME + " TEXT NOT NULL,"
                + KEY_PASSWORD + " TEXT NOT NULL);";

        String CREATE_TABLE_NAME = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "("
                + KEY_ID_NAME + " INTEGER PRIMARY KEY,"
                + KEY_NAME1 + " TEXT NULL,"
                + KEY_NAME2 + " TEXT NULL,"
                + KEY_LAST_NAME + " TEXT NULL,"
                + KEY_M_LAST_NAME + " TEXT NULL,"
                + KEY_ID_USER_NAME + " TEXT NULL,"
                + KEY_COMPLETE + " TEXT NULL,"
                + KEY_DATE_ADMISSION + " TEXT NULL,"
                + KEY_DAY_RET + " TEXT NULL,"
                + KEY_ID_STATUS_NAME + " TEXT NULL,"
                + KEY_ID_PRODUCT_NAME + " TEXT NULL,"
                + KEY_ID_NAME_SERVER + " TEXT NULL);";

        String CREATE_TABLE_SIGNING = "CREATE TABLE IF NOT EXISTS " + TABLE_SIGNING + "("
                + KEY_ID_SIGNING + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_DESCRIPTION_SIGNING + " TEXT NOT NULL);";

        String CREATE_TABLE_PROSPECT = "CREATE TABLE IF NOT EXISTS " + TABLE_PROSPECT + "("
                + KEY_ID_PROSPECT + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_ID_ROL_PROSPECT + " TEXT NOT NULL,"
                + KEY_ID_STATUS_PROSPECT + " TEXT NOT NULL,"
                + KEY_ID_PRODUCT_PROSPECT + " TEXT NOT NULL,"
                + KEY_COMPLETE_PROSPECT + " TEXT NOT NULL,"
                + KEY_ID_NAME_PROSPECT + " TEXT NOT NULL);";

        String CREATE_TABLE_ADDRESS = "CREATE TABLE IF NOT EXISTS " + TABLE_ADDRESS + "("
                + KEY_ID_ADDRESS + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_ROUTE + " TEXT NOT NULL,"
                + KEY_STREET_NUMBER1 + " TEXT NOT NULL,"
                + KEY_STREET_NUMBER2 + " TEXT NULL,"
                + KEY_NEIGHBORHOOD + " TEXT NOT NULL,"
                + KEY_ADMINISTRATIVEAREALEVEL2 + " TEXT NOT NULL,"
                + KEY_ID_FEDERAL_ENTITY_ADDRESS + " TEXT NOT NULL,"
                + KEY_POSTAL_CODE + " TEXT NOT NULL,"
                + KEY_ID_COUNTRY_ADDRESS + " TEXT NOT NULL,"
                + KEY_ROUTE_REFERENCE1 + " TEXT NULL,"
                + KEY_ROUTE_REFERENCE2 + " TEXT NULL,"
                + KEY_ID_NAME_ADDRESS + " INTEGER NOT NULL,"
                + KEY_COMPLETE_ADDRESS + " TEXT NOT NULL,"
                + KEY_ID_ADDRESS_TYPE + " TEXT NULL, FOREIGN KEY ("
                + KEY_ID_NAME_ADDRESS+") REFERENCES "+TABLE_NAME+" ("+KEY_ID_NAME+"));";

        String CREATE_TABLE_PHONES = "CREATE TABLE IF NOT EXISTS " + TABLE_PHONES + "("
                + KEY_ID_PHONES + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_NUMBER_PHONES + " TEXT NULL,"
                + KEY_ID_PHONE_TYPES_PHONES + " TEXT NULL,"
                + KEY_ID_NAME_PHONES + " TEXT NULL,"
                + KEY_COMPLETE_PHONES + " TEXT NULL);";

        String CREATE_TABLE_BASICS = "CREATE TABLE IF NOT EXISTS " + TABLE_BASICS + "("
                + KEY_ID_BASICS + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_BIRTH_DATE + " TEXT NULL,"
                + KEY_VOTER_KEY + " TEXT NULL,"
                + KEY_NUM_REG_VOTER + " TEXT NULL,"
                + KEY_ID_GENDER_BASICS + " TEXT NULL,"
                + KEY_ID_COUNTRY_BIRTH_BASICS + " TEXT NULL,"
                + KEY_ID_FEDERAL_ENTITY_BASICS + " TEXT NULL,"
                + KEY_ID_NATIONALITY_BASICS + " TEXT NULL,"
                + KEY_ID_CIVIL_STATUS_BASICS + " TEXT NULL,"
                + KEY_SONS + " TEXT NULL,"
                + KEY_ID_LIVING_PLACE_BASICS + " TEXT NULL,"
                + KEY_ID_LEVEL_EDUCATION_BASICS + " TEXT NULL,"
                + KEY_ID_KIND_LOCAL + " TEXT NULL,"
                + KEY_ID_NAME_BASICS + " TEXT NULL,"
                + KEY_COMPLETE_BASICS + " TEXT NULL);";

        String CREATE_TABLE_ECONOMIC = "CREATE TABLE IF NOT EXISTS " + TABLE_ECONOMIC + "("
                + KEY_ID_ECONOMIC + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_ID_ECONOMIC_ACITIVTY_ECONOMIC + " TEXT NULL,"
                + KEY_CONTRIBUTION_LEVEL + " TEXT NULL,"
                + KEY_ANOTHER_SOURCE_NAME + " TEXT NULL,"
                + KEY_CURRENT_BUSINESS_TIME + " TEXT NULL,"
                + KEY_ID_LOCAL_TYPE_ECONOMIC + " TEXT NULL,"
                + KEY_ID_MAKES_ACTIVITY_ECONOMIC + " TEXT NULL,"
                + KEY_ID_TIME_ACTIVITY_ECONOMIC + " TEXT NULL,"
                + KEY_EMAIL_ECONOMIC + " TEXT NULL,"
                + KEY_ID_NAME_ECONOMIC + " TEXT NULL,"
                + KEY_COMPLETE_ECONOMIC + " TEXT NULL);";

        String CREATE_TABLE_COMPLEMENTARY = "CREATE TABLE IF NOT EXISTS " + TABLE_COMPLEMENTARY + "("
                + KEY_ID_COMPLEMENTARY + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_PHYSICAL_PERSON + " TEXT NULL,"
                + KEY_CURP + " TEXT NULL,"
                + KEY_DEPENDENT_NUMBER + " TEXT NULL,"
                + KEY_ID_OCCUPATION_COMPLEMENTARY + " TEXT NULL,"
                + KEY_EMAIL_COMPLEMENTARY + " TEXT NULL,"
                + KEY_HOMOCLAVE + " TEXT NULL,"
                + KEY_FIEL + " TEXT NULL,"
                + KEY_ID_NAME_COMPLEMENTARY + " TEXT NULL,"
                + KEY_COMPLETE_COMPLEMENTARY + " TEXT NULL);";

        String CREATE_TABLE_ADDITIONAL = "CREATE TABLE IF NOT EXISTS " + TABLE_ADDITIONAL + "("
                + KEY_ID_ADDITIONAL + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_NAME1_ADDITIONAL + " TEXT NULL,"
                + KEY_NAME2_ADDITIONAL + " TEXT NULL,"
                + KEY_LAST_NAME_ADDITIONAL + " TEXT NULL,"
                + KEY_M_LAST_NAME_ADDITIONAL + " TEXT NULL,"
                + KEY_ID_OCCUPATION_ADDITIONAL + " TEXT NULL,"
                + KEY_PHONE_ADDITIONAL + " TEXT NULL,"
                + KEY_ID_NAME_ADDITIONAL + " TEXT NULL,"
                + KEY_ID_ADDITIONAL_TYPES_ADDITIONAL + " TEXT NULL,"
                + KEY_COMPLETE_ADDITIONAL + " TEXT NULL);";

        String CREATE_TABLE_GROUP_DETAIL = "CREATE TABLE IF NOT EXISTS " + TABLE_GROUP_DETAIL + "("
                + KEY_ID_GROUP_DETAIL + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_GROUP_NAME + " TEXT NULL,"
                + KEY_GROUP_CYCLE + " TEXT NULL,"
                + KEY_ID_SPONSOR_GROUP_DETAIL + " TEXT NULL,"
                + KEY_ID_CHANNEL_DISPERSION + " TEXT NULL,"
                + KEY_ID_DISPERSION_MEDIUM + " TEXT NULL,"
                + KEY_ID_USER_GROUP_DETAIL + " TEXT NULL,"
                + KEY_DATE_CREATE_GROUP + " TEXT NULL,"
                + KEY_ID_GROUP_DETAIL_SERVER + " TEXT NULL,"
                + KEY_COMPLETE_GROUP_DETAIL + " TEXT NULL);";

        String CREATE_TABLE_MEMBERS = "CREATE TABLE IF NOT EXISTS " + TABLE_MEMBERS + "("
                + KEY_ID_MEMBERS + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_ID_GROUP_DETAIL_MEMBERS + " TEXT NULL,"
                + KEY_ID_NAME_MEMBERS + " TEXT NULL,"
                + KEY_ID_NAME_MEMBERS_SERVER + " TEXT NULL,"
                + KEY_COMPLETE_MEMBERS + " TEXT NULL);";

        String CREATE_TABLE_GROUP_DATA = "CREATE TABLE IF NOT EXISTS " + TABLE_GROUP_DATA + "("
                + KEY_ID_GROUP_DATA + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_MINIUM_AMOUNT_SAVINGS + " TEXT NULL,"
                + KEY_AMOUNT_FINE_DELAY + " TEXT NULL,"
                + KEY_AMOUNT_MISSING_FINE + " TEXT NULL,"
                + KEY_MEETING_SCHEDULE + " TEXT NULL,"
                + KEY_MEETING_DATE + " TEXT NULL,"
                + KEY_DISBURSEMENT_DATE + " TEXT NULL,"
                + KEY_PAY_DATE1 + " TEXT NULL,"
                + KEY_ID_PRODUCT_GROUP_DATA + " TEXT NULL,"
                + KEY_ID_FREQUENCIES_GROUP_DATA + " TEXT NULL,"
                + KEY_ID_DEADLINES_GROUP_DATA + " TEXT NULL,"
                + KEY_ID_PLACE_MEETING_GROUP_DATA + " TEXT NULL,"
                + KEY_ID_GROUP_DETAIL_GROUP_DATA + " TEXT NULL,"
                + KEY_ID_NAME_GROUP_DATA + " TEXT NULL,"
                + KEY_COMPLETE_GROUP_DATA + " TEXT NULL, FOREIGN KEY ("
                + KEY_ID_GROUP_DETAIL_GROUP_DATA +") REFERENCES "+TABLE_GROUP_DETAIL+" ("+KEY_ID_GROUP_DETAIL+"));";

        String CREATE_TABLE_PLACE_MEETING = "CREATE TABLE IF NOT EXISTS " + TABLE_PLACE_MEETING + "("
                + KEY_ID_PLACE_MEETING + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_ROUTE_PLACE_MEETING + " TEXT NULL,"
                + KEY_STREET_NUMBER1_PLACE_MEETING + " TEXT NULL,"
                + KEY_STREET_NUMBER2_PLACE_MEETING + " TEXT NULL,"
                + KEY_NEIGHBORHOOD_PLACE_MEETING + " TEXT NULL,"
                + KEY_ADMINISTRATIVEAREALEVEL2_PLACE_MEETING + " TEXT NULL,"
                + KEY_ID_FEDERAL_ENTITY_PLACE_MEETING + " TEXT NULL,"
                + KEY_POSTAL_CODE_PLACE_MEETING + " TEXT NULL,"
                + KEY_ID_COUNTRY_PLACE_MEETING + " TEXT NULL,"
                + KEY_ROUTE_REFERENCE1_PLACE_MEETING + " TEXT NULL,"
                + KEY_ROUTE_REFERENCE2_PLACE_MEETING + " TEXT NULL,"
                + KEY_PHONE_PLACE_MEETING + " TEXT NULL,"
                + KEY_ID_PHONE_TYPES_PHONES + " TEXT NULL,"
                + KEY_COMMENTS_PLACE_MEETING + " TEXT NULL,"
                + KEY_LOCATION_REFERENCE + " TEXT NULL,"
                + KEY_ID_GROUP_DETAIL_PLACE_MEETING + " INTEGER NOT NULL, FOREIGN KEY ("
                + KEY_ID_GROUP_DETAIL_PLACE_MEETING +") REFERENCES "+TABLE_GROUP_DETAIL+" ("+KEY_ID_GROUP_DETAIL+"));";

        String CREATE_TABLE_DOCUMENTS = "CREATE TABLE IF NOT EXISTS " + TABLE_DOCUMENTS + "("
                + KEY_ID_DOCUMENTS + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_DESCRIPTION_DOCUMENTS + " TEXT NOT NULL,"
                + KEY_ID_NAME_DOCUMENTS + " TEXT NOT NULL);";

        String CREATE_TABLE_SECURE = "CREATE TABLE IF NOT EXISTS " + TABLE_SECURE + "("
                + KEY_ID_SECURE + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_START_DATE + " TEXT NOT NULL,"
                + KEY_LIMIT_TIME + " TEXT NOT NULL,"
                + KEY_MODALITY + " TEXT NOT NULL,"
                + KEY_ID_NAME_SECURE + " TEXT NOT NULL,"
                + KEY_COMPLETE_SECURE + " TEXT NOT NULL);";

        String CREATE_TABLE_BENEFICIARY = "CREATE TABLE IF NOT EXISTS " + TABLE_BENEFICIARY + "("
                + KEY_ID_BENEFICIARY + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_NAME1_BENEFICIARY + " TEXT NOT NULL,"
                + KEY_NAME2_BENEFICIARY + " TEXT NOT NULL,"
                + KEY_LAST_NAME_BENEFICIARY + " TEXT NOT NULL,"
                + KEY_M_LAST_NAME_BENEFICIARY + " TEXT NOT NULL,"
                + KEY_ID_RELATIONSHIP + " TEXT NOT NULL,"
                + KEY_BIRTH_DATE_BENEFICIARY + " TEXT NOT NULL,"
                + KEY_ID_GENDER_BENEFICIARY + " TEXT NOT NULL,"
                + KEY_ID_SIGNING_BENEFICIARY + " TEXT NOT NULL,"
                + KEY_ID_SECURE_BENEFICIARY + " TEXT NOT NULL);";

        /*Create Tables*/
        db.execSQL(CREATE_TABLE_USER);
        db.execSQL(CREATE_TABLE_NAME);
        db.execSQL(CREATE_TABLE_SIGNING);
        db.execSQL(CREATE_TABLE_PROSPECT);
        db.execSQL(CREATE_TABLE_ADDRESS);
        db.execSQL(CREATE_TABLE_PHONES);
        db.execSQL(CREATE_TABLE_BASICS);
        db.execSQL(CREATE_TABLE_ECONOMIC);
        db.execSQL(CREATE_TABLE_COMPLEMENTARY);
        db.execSQL(CREATE_TABLE_ADDITIONAL);
        db.execSQL(CREATE_TABLE_GROUP_DETAIL);
        db.execSQL(CREATE_TABLE_MEMBERS);
        db.execSQL(CREATE_TABLE_GROUP_DATA);
        db.execSQL(CREATE_TABLE_PLACE_MEETING);
        db.execSQL(CREATE_TABLE_DOCUMENTS);
        db.execSQL(CREATE_TABLE_SECURE);
        db.execSQL(CREATE_TABLE_BENEFICIARY);
    }

    //Override Upgrade Database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {

    }

    /*Insert Table*/
    public int AddUser(User user)
    {
        long id = 0;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        try
        {
            values.put(KEY_ID_USER, user.getIdUSERS());
            values.put(KEY_USER_NAME, user.getUserName()); // Name
            values.put(KEY_PASSWORD, user.getPassword()); // Password
            id = db.insert(TABLE_USER, null, values); // Inserting Row
            db.close(); // Closing database connection
        }
        catch (Exception ex)
        {

        }
        String value = String.valueOf(id);
        return Integer.parseInt(value);
    }

    public int AddName(Name name)
    {
        long id=0;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        try
        {
            values.put(KEY_NAME1, name.getName1()); // Name1
            values.put(KEY_NAME2, name.getName2()); // Name2
            values.put(KEY_LAST_NAME, name.getLastName()); // Last Name
            values.put(KEY_M_LAST_NAME, name.getmLastName()); // M Last Namet
            values.put(KEY_ID_USER_NAME, name.getIdUser()); // Id User
            values.put(KEY_COMPLETE, name.getComplete()); // Complete
            values.put(KEY_DATE_ADMISSION, name.getDateAdmission()); //DateAdmission
            values.put(KEY_DAY_RET, name.getDayRet()); //DayRet
            values.put(KEY_ID_STATUS_NAME, name.getIdStatus()); // IdStatus
            values.put(KEY_ID_PRODUCT_NAME, name.getIdProduct()); //IdProduct
            values.put(KEY_ID_NAME_SERVER, name.getIdNameServer()); //IdNameServer

            id = db.insert(TABLE_NAME, null, values); // Inserting Row
            db.close(); // Closing database connection
        }
        catch (Exception ex)
        {

        }
        String value = String.valueOf(id);
        return Integer.parseInt(value);
    }

    public void AddSigning(Signing signing)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        try
        {
            values.put(KEY_DESCRIPTION_SIGNING, signing.getDescriptionSigning()); // Description Signing

            db.insert(TABLE_SIGNING, null, values); // Inserting Row
            db.close(); // Closing database connection
        }
        catch (Exception ex)
        {

        }
    }

    public void AddProspect(Prospect prospect)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        try
        {
            values.put(KEY_ID_ROL_PROSPECT, prospect.getId_rol()); // Id Rol
            values.put(KEY_ID_STATUS_PROSPECT, prospect.getId_status()); // Id Status
            values.put(KEY_ID_PRODUCT_PROSPECT, prospect.getId_product()); // Id Product
            values.put(KEY_COMPLETE_PROSPECT, prospect.getComplete()); // Complete
            values.put(KEY_ID_NAME_PROSPECT, prospect.getId_name()); // Id Name

            db.insert(TABLE_PROSPECT, null, values); // Inserting Row
            db.close(); // Closing database connection
        }
        catch (Exception ex)
        {

        }
    }

    public int AddAddress(Address address)
    {
        long id =0;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        try
        {
            values.put(KEY_ROUTE, address.getRoute()); // Route
            values.put(KEY_STREET_NUMBER1, address.getStreetNumber1()); // Street Number1
            values.put(KEY_STREET_NUMBER2, address.getStreetNumber2()); // Street Number2
            values.put(KEY_NEIGHBORHOOD, address.getNeighborhood()); // Neigborhood
            values.put(KEY_ADMINISTRATIVEAREALEVEL2, address.getAdministrativeAreaLevel2()); // AdministrativeLevel2
            values.put(KEY_ID_FEDERAL_ENTITY_ADDRESS, address.getIdFederalEntity()); // Id Federal Entity
            values.put(KEY_POSTAL_CODE, address.getPostalCode()); // Postal Code
            values.put(KEY_ID_COUNTRY_ADDRESS, address.getIdCountry()); // Id Country
            values.put(KEY_ROUTE_REFERENCE1, address.getRouteReference1()); // Route Reference 1
            values.put(KEY_ROUTE_REFERENCE2, address.getRouteReference2()); // Route Reference 2
            values.put(KEY_ID_NAME_ADDRESS, address.getIdName()); // Id Name
            values.put(KEY_COMPLETE_ADDRESS, address.getComplete()); // Complete
            values.put(KEY_ID_ADDRESS_TYPE, address.getIdAddressTypes()); //Id Address Type

            id = db.insert(TABLE_ADDRESS, null, values); // Inserting Row
            db.close(); // Closing database connection
        }
        catch (Exception ex)
        {

        }
        String value = String.valueOf(id);
        return Integer.parseInt(value);
    }

    public int AddPhones(Phone phones)
    {
        long id = 0;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        try
        {
            values.put(KEY_NUMBER_PHONES, phones.getNumber()); // Number
            values.put(KEY_ID_PHONE_TYPES_PHONES, phones.getIdPhoneTypes()); // Id Phone Types
            values.put(KEY_ID_NAME_PHONES, phones.getIdName()); // Id Name
            values.put(KEY_COMPLETE_PHONES, phones.getComplete()); // Complete

            id = db.insert(TABLE_PHONES, null, values); // Inserting Row
            db.close(); // Closing database connection
        }
        catch (Exception ex)
        {

        }
        String value = String.valueOf(id);
        return Integer.parseInt(value);
    }

    public int AddBasics(Basics basics)
    {
        long id = 0;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        try
        {
            values.put(KEY_BIRTH_DATE, basics.getBirthdate()); // Birth Date
            values.put(KEY_VOTER_KEY, basics.getVoterKey()); // Voter Key
            values.put(KEY_NUM_REG_VOTER, basics.getNumRegVoter()); // Num Reg Voter
            values.put(KEY_ID_GENDER_BASICS, basics.getIdGender()); // Id gender
            values.put(KEY_ID_COUNTRY_BIRTH_BASICS, basics.getIdCountryBirth()); // Id Country Birth
            values.put(KEY_ID_FEDERAL_ENTITY_BASICS, basics.getIdFederalEntity()); // Id Federal Entity
            values.put(KEY_ID_NATIONALITY_BASICS, basics.getIdNationality()); // Id Nationality
            values.put(KEY_ID_CIVIL_STATUS_BASICS, basics.getIdCivilStatus()); // Id Civil Status
            values.put(KEY_SONS, basics.getSons()); // Sons
            values.put(KEY_ID_LIVING_PLACE_BASICS, basics.getIdLivingPlace()); // Id Living Place
            values.put(KEY_ID_LEVEL_EDUCATION_BASICS, basics.getIdLevelEducation()); // Id Level Education
            values.put(KEY_ID_KIND_LOCAL, basics.getIdKindLocal()); // Pep
            values.put(KEY_ID_NAME_BASICS, basics.getIdName()); // Id Name
            values.put(KEY_COMPLETE_BASICS, basics.getComplete()); // Complete

            id = db.insert(TABLE_BASICS, null, values); // Inserting Row
            db.close(); // Closing database connection
        }
        catch (Exception ex)
        {

        }

        String value = String.valueOf(id);
        return Integer.parseInt(value);
    }

    public int AddEconomic(Economic economic)
    {
        long id =0;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        try
        {
            values.put(KEY_ID_ECONOMIC_ACITIVTY_ECONOMIC, economic.getIdEconomicActivity()); // Id Economic Activity
            values.put(KEY_CONTRIBUTION_LEVEL, economic.getContributionLevel()); // Contribution Level
            values.put(KEY_ANOTHER_SOURCE_NAME, economic.getAnotherSourceIncome()); // Another Source Income
            values.put(KEY_CURRENT_BUSINESS_TIME, economic.getCurrentBusinessTime()); // Currente Business Time
            values.put(KEY_ID_LOCAL_TYPE_ECONOMIC, economic.getIdLocalType()); // Id Local Type
            values.put(KEY_ID_MAKES_ACTIVITY_ECONOMIC, economic.getIdMakesActivity()); //Id Makes Activity
            values.put(KEY_ID_TIME_ACTIVITY_ECONOMIC, economic.getIdTimeActivity()); //Id Time Activity
            values.put(KEY_EMAIL_ECONOMIC, economic.getEmail()); //Email
            values.put(KEY_ID_NAME_ECONOMIC, economic.getIdName()); // Id Name
            values.put(KEY_COMPLETE_ECONOMIC, economic.getComplete()); // Complete

            id = db.insert(TABLE_ECONOMIC, null, values); // Inserting Row
            db.close(); // Closing database connection
        }
        catch (Exception ex)
        {

        }
        String value = String.valueOf(id);
        return Integer.parseInt(value);
    }

    public int AddComplementary(Complementary complementary)
    {
        long id=0;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        try
        {
            values.put(KEY_PHYSICAL_PERSON, complementary.getPhysicalPerson()); // Physcal Person
            values.put(KEY_CURP, complementary.getCurp()); // Curp
            values.put(KEY_DEPENDENT_NUMBER, complementary.getDependentNumber()); // Dependente Number
            values.put(KEY_ID_OCCUPATION_COMPLEMENTARY, complementary.getIdOccupation()); // Id Occupation
            values.put(KEY_EMAIL_COMPLEMENTARY, complementary.getEmail()); // Email
            values.put(KEY_HOMOCLAVE, complementary.getHomoclave()); // Homoclave
            values.put(KEY_FIEL, complementary.getFiel()); // Fiel
            values.put(KEY_ID_NAME_COMPLEMENTARY, complementary.getIdName()); // Id Name
            values.put(KEY_COMPLETE_COMPLEMENTARY, complementary.getComplete()); // Complete

            id = db.insert(TABLE_COMPLEMENTARY, null, values); // Inserting Row
            db.close(); // Closing database connection
        }
        catch (Exception ex)
        {

        }
        String value = String.valueOf(id);
        return Integer.parseInt(value);
    }

    public int AddAdditional(Additionals additionals)
    {
        long id=0;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        try
        {
            values.put(KEY_NAME1_ADDITIONAL, additionals.getName1()); // Name1
            values.put(KEY_NAME2_ADDITIONAL, additionals.getName2()); // Name2
            values.put(KEY_LAST_NAME_ADDITIONAL, additionals.getLastName()); // Last Name
            values.put(KEY_M_LAST_NAME_ADDITIONAL, additionals.getmLastName()); // M Last Name
            values.put(KEY_ID_OCCUPATION_ADDITIONAL, additionals.getIdOccupation()); // Email
            values.put(KEY_PHONE_ADDITIONAL, additionals.getPhone()); // Phone
            values.put(KEY_ID_NAME_ADDITIONAL, additionals.getIdName()); // Id Name
            values.put(KEY_ID_ADDITIONAL_TYPES_ADDITIONAL, additionals.getIdAdditionalTypes()); // Id Additional Types
            values.put(KEY_COMPLETE_ADDITIONAL, additionals.getComplete()); // Complete

            id = db.insert(TABLE_ADDITIONAL, null, values); // Inserting Row
            db.close(); // Closing database connection
        }
        catch (Exception ex)
        {

        }
        String value = String.valueOf(id);
        return Integer.parseInt(value);
    }

    public int AddGroupDetail(GroupDetail groupDetail)
    {
        long id=0;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        try
        {
            values.put(KEY_GROUP_NAME, groupDetail.getGroupName()); // Group Name
            values.put(KEY_GROUP_CYCLE, groupDetail.getGroupCycle()); // Group Cycle
            values.put(KEY_ID_SPONSOR_GROUP_DETAIL, groupDetail.getIdSponsor()); // Id Sponsor
            values.put(KEY_ID_CHANNEL_DISPERSION, groupDetail.getIdChannelDispersion()); // Id Channel Dispersion
            values.put(KEY_ID_DISPERSION_MEDIUM, groupDetail.getIdDispersionMedium()); // Id DispersionMedium
            values.put(KEY_ID_USER_GROUP_DETAIL, groupDetail.getIdUser()); // Id User
            values.put(KEY_DATE_CREATE_GROUP, groupDetail.getDate()); // Date
            values.put(KEY_ID_GROUP_DETAIL_SERVER, groupDetail.getIdGroupDetailServer()); // Id Group Detail Server
            values.put(KEY_COMPLETE_GROUP_DETAIL, groupDetail.getComplete()); // Complete

            id = db.insert(TABLE_GROUP_DETAIL, null, values); // Inserting Row
            db.close(); // Closing database connection
        }
        catch (Exception ex)
        {

        }

        String value = String.valueOf(id);
        return Integer.parseInt(value);
    }

    public int AddMembers(Members members)
    {
        long id = 0;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        try
        {
            values.put(KEY_ID_GROUP_DETAIL_MEMBERS, members.getIdGroupDetail()); // Group Detail
            values.put(KEY_ID_NAME_MEMBERS, members.getIdName()); // Id Name
            values.put(KEY_ID_NAME_MEMBERS_SERVER, members.getIdNameServer()); // Id Name Server
            values.put(KEY_COMPLETE_MEMBERS, members.getComplete()); // Complete

            id = db.insert(TABLE_MEMBERS, null, values); // Inserting Row
            db.close(); // Closing database connection
        }
        catch (Exception ex)
        {

        }
        String value = String.valueOf(id);
        return Integer.parseInt(value);
    }

    public int AddGroupData(GroupData groupData)
    {
        long id = 0;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        try
        {
            values.put(KEY_MINIUM_AMOUNT_SAVINGS, groupData.getMiniumAmountSavings()); // Minium Amount Savings
            values.put(KEY_AMOUNT_FINE_DELAY, groupData.getAmountFineDelay()); // Amount Fine Delay
            values.put(KEY_AMOUNT_MISSING_FINE, groupData.getAmountMissingFine()); // Amount Missing Fine
            values.put(KEY_MEETING_SCHEDULE, groupData.getMeetingSchedule()); // Meeting Schedule
            values.put(KEY_MEETING_DATE, groupData.getMeetingDate()); // Meeting Date
            values.put(KEY_DISBURSEMENT_DATE, groupData.getDisbursementDate()); // Disbursement Date
            values.put(KEY_PAY_DATE1, groupData.getPayDate1()); // Pay Date1
            values.put(KEY_ID_PRODUCT_GROUP_DATA, groupData.getIdProduct()); // Id Product
            values.put(KEY_ID_FREQUENCIES_GROUP_DATA, groupData.getIdFrequencies()); // Id Frequencies
            values.put(KEY_ID_DEADLINES_GROUP_DATA, groupData.getIdDeadlines()); // Id Deadlines
            values.put(KEY_ID_PLACE_MEETING_GROUP_DATA, groupData.getIdPlaceMeeting()); // Id Place Meeting
            values.put(KEY_ID_GROUP_DETAIL_GROUP_DATA, groupData.getIdGroupDetail()); // Group Detail
            values.put(KEY_ID_NAME_GROUP_DATA, groupData.getIdNameGroupData()); // Id Name
            values.put(KEY_COMPLETE_GROUP_DATA, groupData.getComplete()); // Complete

            id = db.insert(TABLE_GROUP_DATA, null, values); // Inserting Row
            db.close(); // Closing database connection
        }
        catch (Exception ex)
        {

        }
        String value = String.valueOf(id);
        return Integer.parseInt(value);
    }

    public int AddPlaceMeeting(PlaceMeeting placeMeeting)
    {
        long id=0;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        try
        {
            values.put(KEY_ROUTE_PLACE_MEETING, placeMeeting.getRoute()); // Route
            values.put(KEY_STREET_NUMBER1_PLACE_MEETING, placeMeeting.getStreetNumber1()); // Street Number1
            values.put(KEY_STREET_NUMBER2_PLACE_MEETING, placeMeeting.getStreetNumber2()); // Street Number2
            values.put(KEY_NEIGHBORHOOD_PLACE_MEETING, placeMeeting.getNeighborhood()); // Neigborhood
            values.put(KEY_ADMINISTRATIVEAREALEVEL2_PLACE_MEETING, placeMeeting.getAdministrativeAreaLevel2()); // AdministrativeLevel2
            values.put(KEY_ID_FEDERAL_ENTITY_PLACE_MEETING, placeMeeting.getIdFederalEntity()); // Id Federal Entity
            values.put(KEY_POSTAL_CODE_PLACE_MEETING, placeMeeting.getPostalCode()); // Postal Code
            values.put(KEY_ID_COUNTRY_PLACE_MEETING, placeMeeting.getIdCountry()); // Id Country
            values.put(KEY_ROUTE_REFERENCE1_PLACE_MEETING, placeMeeting.getRouteReference1()); // Route Reference 1
            values.put(KEY_ROUTE_REFERENCE2_PLACE_MEETING, placeMeeting.getRouteReference2()); // Route Reference 2
            values.put(KEY_LOCATION_REFERENCE, placeMeeting.getLocationReference()); // Location References
            values.put(KEY_PHONE_PLACE_MEETING, placeMeeting.getPhone()); // Phone
            values.put(KEY_ID_PHONE_TYPES_PLACE_MEETING, placeMeeting.getIdPhoneTypes()); // Phone Types
            values.put(KEY_COMMENTS_PLACE_MEETING, placeMeeting.getComments()); // Comments
            values.put(KEY_ID_GROUP_DETAIL_PLACE_MEETING, Integer.parseInt(placeMeeting.getIdGroupDetail())); // Id Group Detail

            id = db.insert(TABLE_PLACE_MEETING, null, values); // Inserting Row
            db.close(); // Closing database connection
        }
        catch (Exception ex)
        {

        }

        String value = String.valueOf(id);
        return Integer.parseInt(value);
    }

    public void AddDocument(Document document)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        try
        {
            values.put(KEY_DESCRIPTION_DOCUMENTS, document.getDescriptionDocuments()); // Description Documents
            values.put(KEY_ID_NAME_DOCUMENTS, document.getIdName()); // Id Name
            db.insert(TABLE_DOCUMENTS, null, values); // Inserting Row
            db.close(); // Closing database connection
        }
        catch (Exception ex)
        {

        }
    }

    public int AddSecure(Secure secure)
    {
        long id=0;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        try
        {
            values.put(KEY_START_DATE, secure.getStartDate()); // Start Date
            values.put(KEY_LIMIT_TIME, secure.getLimitTime()); // Limit Time
            values.put(KEY_MODALITY, secure.getModality()); // Modality
            values.put(KEY_ID_NAME_SECURE, secure.getIdName()); // Id Name
            values.put(KEY_COMPLETE_SECURE, secure.getComplete()); // Complete
            id = db.insert(TABLE_SECURE, null, values); // Inserting Row
            db.close(); // Closing database connection
        }
        catch (Exception ex)
        {

        }
        String value = String.valueOf(id);
        return Integer.parseInt(value);
    }

    public int AddBeneficiary(Beneficiary beneficiary)
    {
        long id =0;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        try
        {
            values.put(KEY_NAME1_BENEFICIARY, beneficiary.getName1()); // Name1
            values.put(KEY_NAME2_BENEFICIARY, beneficiary.getName2()); // Name2
            values.put(KEY_LAST_NAME_BENEFICIARY, beneficiary.getLastName()); // Last Name
            values.put(KEY_M_LAST_NAME_BENEFICIARY, beneficiary.getmLastName()); // M Last Name
            values.put(KEY_ID_RELATIONSHIP, beneficiary.getIdRelationship()); // Id Relationship
            values.put(KEY_BIRTH_DATE_BENEFICIARY, beneficiary.getBirthDate()); // Birth Date
            values.put(KEY_ID_GENDER_BENEFICIARY, beneficiary.getIdGender()); // Id Gender
            values.put(KEY_ID_SIGNING_BENEFICIARY, beneficiary.getIdSigning()); // Id Signing
            values.put(KEY_ID_SECURE_BENEFICIARY, beneficiary.getIdSecure()); // Id Secure
            id = db.insert(TABLE_BENEFICIARY, null, values); // Inserting Row
            db.close(); // Closing database connection
        }
        catch (Exception ex)
        {

        }
        String value = String.valueOf(id);
        return Integer.parseInt(value);
    }

    /*Get Table*/
    public ArrayList<User> GetAllUser()
    {
        ArrayList<User> userList = new ArrayList<User>();
        // Select All Query
        String selectQuery = "SELECT * FROM " + TABLE_USER;
        SQLiteDatabase db = this.getReadableDatabase();
        try
        {
            Cursor cursor = db.rawQuery(selectQuery, null);
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do
                {
                    User user = new User();
                    user.setIdUSERS(cursor.getString(0));
                    user.setUserName(cursor.getString(1));
                    user.setPassword(cursor.getString(2));
                    // Adding Name List
                    userList.add(user);
                } while (cursor.moveToNext());
            }
        }
        catch (SQLiteException sql)
        {
            sql.getStackTrace();
        }
        // return Name List
        return userList;
    }

    public User GetUser(int id)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        User user =null;
        try
        {
            Cursor cursor = db.query(TABLE_USER, new String[]{KEY_ID_USER,
                            KEY_USER_NAME}, KEY_ID_USER + "=?",
                    new String[]{String.valueOf(id)}, null, null, null, null);
            if (cursor != null)
                cursor.moveToFirst();

            user = new User(cursor.getString(0), cursor.getString(1));
        }
        catch (SQLiteException sql)
        {
            sql.getStackTrace();
        }
        // return object signing
        return user;
    }

    public List<Name> GetAllName(int id)
    {
        List<Name> nameList = new ArrayList<Name>();
        // Select All Query
        String selectQuery = "SELECT * FROM " + TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        try
        {
            Cursor cursor = db.rawQuery(selectQuery, null);
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    Name name = new Name();
                    name.setIdName(cursor.getString(0));
                    name.setName1(cursor.getString(1));
                    name.setName2(cursor.getString(2));
                    name.setLastName(cursor.getString(3));
                    name.setmLastName(cursor.getString(4));
                    name.setIdUser(cursor.getString(5));
                    name.setComplete(cursor.getString(6));
                    name.setDateAdmission(cursor.getString(7));
                    name.setDayRet(cursor.getString(8));
                    name.setIdStatus(Integer.parseInt(cursor.getString(9)));
                    name.setIdProduct(Integer.parseInt(cursor.getString(10)));

                    // Adding Name List
                    nameList.add(name);
                } while (cursor.moveToNext());
            }
        }
        catch (SQLiteException sql)
        {
            sql.getStackTrace();
        }
        // return Name List
        return nameList;
    }

    public Name GetName(int id)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        Name name=null;
        try
        {
            Cursor cursor = db.query(TABLE_NAME, new String[]{KEY_ID_NAME,
                            KEY_NAME1, KEY_NAME2, KEY_LAST_NAME, KEY_M_LAST_NAME,
                            KEY_ID_USER_NAME, KEY_COMPLETE, KEY_DATE_ADMISSION, KEY_DAY_RET,
                            KEY_ID_STATUS_NAME, KEY_ID_PRODUCT_NAME, KEY_ID_NAME_SERVER}, KEY_ID_NAME + "=?",
                    new String[]{String.valueOf(id)}, null, null, null, null);
            if (cursor != null)
            {
                if(cursor.moveToFirst())
                {
                    name = new Name();
                    name.setIdName(cursor.getString(0));
                    name.setName1(cursor.getString(1));
                    name.setName2(cursor.getString(2));
                    name.setLastName(cursor.getString(3));
                    name.setmLastName(cursor.getString(4));
                    name.setIdUser(cursor.getString(5));
                    name.setComplete(cursor.getString(6));
                    name.setDateAdmission(cursor.getString(7));
                    name.setDayRet(cursor.getString(8));
                    name.setIdStatus(Integer.parseInt(cursor.getString(9)));
                    name.setIdProduct(Integer.parseInt(cursor.getString(10)));
                    name.setIdNameServer(cursor.getString(11));
                }
            }
        }
        catch (SQLiteException sql)
        {
            sql.getStackTrace();
        }
        // return object signing
        return name;
    }

    public ArrayList<Name> GetListName(int id)
    {
        ArrayList<Name> nameArrayList = new ArrayList<Name>();
        // Select All Query
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_NAME+ " WHERE IdName="+id;
        try
        {
            Cursor cursor = db.query(TABLE_NAME, new String[]{KEY_ID_NAME,
                            KEY_NAME1, KEY_NAME2, KEY_LAST_NAME, KEY_M_LAST_NAME,
                            KEY_ID_USER_NAME, KEY_COMPLETE, KEY_DATE_ADMISSION, KEY_DAY_RET,
                            KEY_ID_STATUS_NAME}, KEY_ID_NAME + "=?",
                    new String[]{String.valueOf(id)}, null, null, null, null);
            // looping through all rows and adding to list
            if (cursor!=null)
            {
                cursor.moveToFirst();
                if (cursor.moveToFirst() == true)
                {
                    do
                    {
                        Name name = new Name();
                        name.setIdName(cursor.getString(0));
                        name.setName1(cursor.getString(1));
                        name.setName2(cursor.getString(2));
                        name.setLastName(cursor.getString(3));
                        name.setmLastName(cursor.getString(4));
                        name.setIdUser(cursor.getString(9));
                        name.setComplete(cursor.getString(10));
                        name.setDateAdmission(cursor.getString(11));
                        name.setDayRet(cursor.getString(12));
                        name.setIdStatus(Integer.parseInt(cursor.getString(13)));

                        // Adding Name List
                        nameArrayList.add(name);
                    } while (cursor.moveToNext());
                }
                else
                {
                    return  null;
                }
            }
        }
        catch (SQLiteException sql)
        {
            sql.getStackTrace();
        }
        // return Name List
        return nameArrayList;
    }

    public ArrayList<Name> GetAllListName(int id)
    {
        ArrayList<Name> nameArrayList = new ArrayList<Name>();
        // Select All Query
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_NAME+ " WHERE IdUser="+id;
        try
        {
            Cursor cursor = db.query(TABLE_NAME, new String[]{KEY_ID_NAME,
                            KEY_NAME1, KEY_NAME2, KEY_LAST_NAME, KEY_M_LAST_NAME,
                            KEY_ID_USER_NAME, KEY_COMPLETE, KEY_DATE_ADMISSION, KEY_DAY_RET,
                            KEY_ID_STATUS_NAME, KEY_ID_PRODUCT_NAME, KEY_ID_NAME_SERVER}, KEY_ID_USER_NAME + "=?",
                    new String[]{String.valueOf(id)}, null, null, null, null);
            // looping through all rows and adding to list
            if (cursor!=null)
            {
                cursor.moveToFirst();
                if (cursor.moveToFirst() == true)
                {
                    do
                    {
                        Name name = new Name();
                        name.setIdName(cursor.getString(0));
                        name.setName1(cursor.getString(1));
                        name.setName2(cursor.getString(2));
                        name.setLastName(cursor.getString(3));
                        name.setmLastName(cursor.getString(4));
                        name.setIdUser(cursor.getString(5));
                        name.setComplete(cursor.getString(6));
                        name.setDateAdmission(cursor.getString(7));
                        name.setDayRet(cursor.getString(8));
                        name.setIdStatus(Integer.parseInt(cursor.getString(9)));
                        name.setIdProduct(Integer.parseInt(cursor.getString(10)));
                        name.setIdNameServer(cursor.getString(11));

                        // Adding Name List
                        nameArrayList.add(name);
                    } while (cursor.moveToNext());
                }
                else
                {
                    return  null;
                }
            }
        }
        catch (SQLiteException sql)
        {
            sql.getStackTrace();
        }
        // return Name List
        return nameArrayList;
    }

    public ArrayList<NameUser> GetAllAplicants(int id)
    {
        ArrayList<NameUser> responseNameUserArrayList = new ArrayList<NameUser>();
        // Select All Query
        //String selectQuery = "SELECT * FROM " + TABLE_ADDRESS+" AS address INNER JOIN "+ TABLE_NAME+" AS n ON address.IdName = n.Id INNER JOIN "+ TABLE_USER +" user ON n.IdUser = user.Id WHERE n.IdUser="+id;
        String selectQuery = "SELECT * FROM " + TABLE_NAME+" AS n INNER JOIN "+ TABLE_USER+" AS u ON n.IdUser = u.Id WHERE u.Id="+id;
        SQLiteDatabase db = this.getReadableDatabase();
        try
        {
            Cursor cursor = db.rawQuery(selectQuery, null);
            // looping through all rows and adding to list
            if (cursor.moveToFirst())
            {
                do
                {
                    NameUser nameUser = new NameUser();
                    nameUser.setIdNAME(Integer.parseInt(cursor.getString(0)));
                    nameUser.setName1(cursor.getString(1));
                    nameUser.setName2(cursor.getString(2));
                    nameUser.setLastName(cursor.getString(3));
                    nameUser.setmLastName(cursor.getString(4));;
                    nameUser.setComplete(Boolean.parseBoolean(cursor.getString(6)));
                    nameUser.setDateAdmission(cursor.getString(7));
                    nameUser.setIdStatus(Integer.parseInt(cursor.getString(9)));
                    nameUser.setDayRet(cursor.getString(8));
                    nameUser.setIdNameServer(cursor.getString(11));


                    // Adding Name List
                    responseNameUserArrayList.add(nameUser);
                } while (cursor.moveToNext());
            }
        }
        catch (SQLiteException sql)
        {
            sql.getStackTrace();
        }
        // return Name List
        return responseNameUserArrayList;
    }

    public Signing GetSigning(int id)
    {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_SIGNING, new String[]{KEY_ID_SIGNING,
                        KEY_DESCRIPTION_SIGNING}, KEY_ID_SIGNING + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Signing signing = new Signing(cursor.getString(0), cursor.getString(1));
        // return object signing
        return signing;
    }

    public Prospect GetProspect(int id)
    {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_PROSPECT, new String[]{KEY_ID_PROSPECT, KEY_ID_ROL_PROSPECT,
                        KEY_ID_STATUS_PROSPECT, KEY_ID_PRODUCT_PROSPECT,KEY_COMPLETE_PROSPECT, KEY_ID_NAME_PROSPECT}, KEY_ID_NAME_PROSPECT + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Prospect prospect = new Prospect(cursor.getString(0), cursor.getString(1), cursor.getString(2),cursor.getString(3),cursor.getString(4),cursor.getString(5));
        // return object signing
        return prospect;
    }

    public ArrayList<Address> GetAllAddress(int id)
    {
        ArrayList<Address> addressArrayList = new ArrayList<Address>();
        // Select All Query
        SQLiteDatabase db = this.getReadableDatabase();
        try
        {
            Cursor cursor = db.query(TABLE_ADDRESS, new String[]{KEY_ID_ADDRESS, KEY_ROUTE,
                            KEY_STREET_NUMBER1, KEY_STREET_NUMBER2, KEY_NEIGHBORHOOD, KEY_ADMINISTRATIVEAREALEVEL2,
                            KEY_ID_FEDERAL_ENTITY_ADDRESS, KEY_POSTAL_CODE, KEY_ID_COUNTRY_ADDRESS, KEY_ROUTE_REFERENCE1,
                            KEY_ROUTE_REFERENCE2, KEY_ID_NAME_ADDRESS, KEY_COMPLETE, KEY_ID_ADDRESS_TYPE}, KEY_ID_NAME_ADDRESS + "=?",
                    new String[]{String.valueOf(id)}, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                if (cursor.moveToFirst() == true) {
                    do {
                        Address address = new Address();
                        address.setIdADDRESS(cursor.getString(0));
                        address.setRoute(cursor.getString(1));
                        address.setStreetNumber1(cursor.getString(2));
                        address.setStreetNumber2(cursor.getString(3));
                        address.setNeighborhood(cursor.getString(4));
                        address.setAdministrativeAreaLevel2(cursor.getString(5));
                        address.setIdFederalEntity(cursor.getString(6));
                        address.setPostalCode(cursor.getString(7));
                        address.setIdCountry(cursor.getString(8));
                        address.setRouteReference1(cursor.getString(9));
                        address.setRouteReference2(cursor.getString(10));
                        address.setIdName(cursor.getString(11));
                        address.setComplete(cursor.getString(12));
                        address.setIdAddressTypes(cursor.getString(13));
                        addressArrayList.add(address);
                    }
                    while (cursor.moveToNext());
                    return addressArrayList;
                } else {
                    return null;
                }
            }
        }
        catch (SQLException sql)
        {
            sql.getStackTrace();
        }
        return addressArrayList;
    }

    public Address GetAddress(int id)
    {
        Address address = new Address();
        // Select All Query
        SQLiteDatabase db = this.getReadableDatabase();
        try
        {
            Cursor cursor = db.query(TABLE_ADDRESS, new String[]{KEY_ID_ADDRESS, KEY_ROUTE,
                            KEY_STREET_NUMBER1, KEY_STREET_NUMBER2, KEY_NEIGHBORHOOD, KEY_ADMINISTRATIVEAREALEVEL2,
                            KEY_ID_FEDERAL_ENTITY_ADDRESS, KEY_POSTAL_CODE, KEY_ID_COUNTRY_ADDRESS, KEY_ROUTE_REFERENCE1,
                            KEY_ROUTE_REFERENCE2, KEY_ID_NAME_ADDRESS, KEY_COMPLETE, KEY_ID_ADDRESS_TYPE}, KEY_ID_NAME_ADDRESS + "=?",
                    new String[]{String.valueOf(id)}, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                if (cursor.moveToFirst() == true) {

                    address.setIdADDRESS(cursor.getString(0));
                    address.setRoute(cursor.getString(1));
                    address.setStreetNumber1(cursor.getString(2));
                    address.setStreetNumber2(cursor.getString(3));
                    address.setNeighborhood(cursor.getString(4));
                    address.setAdministrativeAreaLevel2(cursor.getString(5));
                    address.setIdFederalEntity(cursor.getString(6));
                    address.setPostalCode(cursor.getString(7));
                    address.setIdCountry(cursor.getString(8));
                    address.setRouteReference1(cursor.getString(9));
                    address.setRouteReference2(cursor.getString(10));
                    address.setIdName(cursor.getString(11));
                    address.setComplete(cursor.getString(12));
                    address.setIdAddressTypes(cursor.getString(13));
                    return address;
                } else {
                    return null;
                }
            }
        }
        catch (SQLException sql)
        {
            sql.getStackTrace();
        }
        return address;
    }

    public ArrayList<Phone> GetAllPhones(int id)
    {
        ArrayList<Phone> phoneArrayList = new ArrayList<Phone>();
        SQLiteDatabase db = this.getReadableDatabase();

        try
        {
            Cursor cursor = db.query(TABLE_PHONES, new String[]{KEY_ID_PHONES, KEY_NUMBER_PHONES,
                            KEY_ID_PHONE_TYPES_PHONES, KEY_ID_NAME_PHONES, KEY_COMPLETE}, KEY_ID_NAME_PHONES + "=?",
                    new String[]{String.valueOf(id)}, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                if (cursor.moveToFirst() == true) {
                    do {
                        Phone phone = new Phone();
                        phone.setIdPHONES(String.valueOf(cursor.getString(0)));
                        phone.setNumber(cursor.getString(1));
                        phone.setIdPhoneTypes(cursor.getString(2));
                        phone.setIdName(cursor.getString(3));
                        phone.setComplete(cursor.getString(4));

                        // Adding Name List
                        phoneArrayList.add(phone);
                    }
                    while (cursor.moveToNext());
                    return phoneArrayList;
                }
                else
                {
                    return null;
                }
            }
        }
        catch (SQLException sql)
        {
            sql.getStackTrace();
        }
        return phoneArrayList;
    }

    public Phone GetPhone(int id)
    {
        Phone phone = new Phone();
        SQLiteDatabase db = this.getReadableDatabase();

        try
        {
            Cursor cursor = db.query(TABLE_PHONES, new String[]{KEY_ID_PHONES, KEY_NUMBER_PHONES,
                            KEY_ID_PHONE_TYPES_PHONES, KEY_ID_NAME_PHONES, KEY_COMPLETE}, KEY_ID_NAME_PHONES + "=?",
                    new String[]{String.valueOf(id)}, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                if (cursor.moveToFirst() == true) {

                    phone.setIdPHONES(String.valueOf(cursor.getString(0)));
                    phone.setNumber(cursor.getString(1));
                    phone.setIdPhoneTypes(cursor.getString(2));
                    phone.setIdName(cursor.getString(3));
                    phone.setComplete(cursor.getString(4));

                    // Adding Name List
                    return phone;
                }
                else
                {
                    return null;
                }
            }
        }
        catch (SQLException sql)
        {
            sql.getStackTrace();
        }
        return phone;
    }

    public Basics GetBasics(int id)
    {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_BASICS, new String[]{KEY_ID_ADDRESS, KEY_BIRTH_DATE,
                        KEY_VOTER_KEY, KEY_NUM_REG_VOTER,KEY_ID_GENDER_BASICS, KEY_ID_COUNTRY_BIRTH_BASICS,
                        KEY_ID_FEDERAL_ENTITY_BASICS,KEY_ID_NATIONALITY_BASICS,KEY_ID_CIVIL_STATUS_BASICS,KEY_SONS,
                        KEY_ID_LIVING_PLACE_BASICS,KEY_ID_LEVEL_EDUCATION_BASICS,KEY_ID_KIND_LOCAL,KEY_ID_NAME_BASICS,KEY_COMPLETE_BASICS}, KEY_ID_NAME_BASICS + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Basics basics = new Basics(cursor.getString(0), cursor.getString(1), cursor.getString(2),cursor.getString(3),cursor.getString(4),cursor.getString(5),cursor.getString(6), cursor.getString(7), cursor.getString(8),cursor.getString(9),cursor.getString(10),cursor.getString(11),cursor.getString(12),cursor.getString(13), cursor.getString(14));
        // return object signing
        return basics;
    }

    public Economic GetEconomic(int id)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        Economic economic = null;

        Cursor cursor = db.query(TABLE_ECONOMIC, new String[]{KEY_ID_ECONOMIC, KEY_ID_ECONOMIC_ACITIVTY_ECONOMIC,
                        KEY_CONTRIBUTION_LEVEL, KEY_ANOTHER_SOURCE_NAME,KEY_CURRENT_BUSINESS_TIME, KEY_ID_LOCAL_TYPE_ECONOMIC,
                        KEY_ID_MAKES_ACTIVITY_ECONOMIC, KEY_ID_TIME_ACTIVITY_ECONOMIC, KEY_EMAIL_ECONOMIC,
                        KEY_ID_NAME_ECONOMIC,KEY_COMPLETE_ECONOMIC}, KEY_ID_NAME_ECONOMIC + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null)
        {
            if(cursor.moveToFirst()==true)
            {
                economic = new Economic();
                economic.setIdECONOMIC(cursor.getString(0));
                economic.setIdEconomicActivity(cursor.getString(1));
                economic.setContributionLevel(cursor.getString(2));
                economic.setAnotherSourceIncome(cursor.getString(3));
                economic.setCurrentBusinessTime(cursor.getString(4));
                economic.setIdLocalType(cursor.getString(5));
                economic.setIdMakesActivity(cursor.getString(6));
                economic.setIdTimeActivity(cursor.getString(7));
                economic.setEmail(cursor.getString(8));
                economic.setIdName(cursor.getString(9));
                economic.setComplete(cursor.getString(10));
            }
        }
        // return object signing
        return economic;
    }

    public Complementary GetComplementary(int id)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        Complementary complementary=null;

        try
        {
            Cursor cursor = db.query(TABLE_COMPLEMENTARY, new String[]{KEY_ID_COMPLEMENTARY, KEY_PHYSICAL_PERSON,
                            KEY_CURP, KEY_DEPENDENT_NUMBER, KEY_ID_OCCUPATION_COMPLEMENTARY, KEY_EMAIL_COMPLEMENTARY,
                            KEY_HOMOCLAVE, KEY_FIEL, KEY_ID_NAME_COMPLEMENTARY, KEY_COMPLETE_COMPLEMENTARY}, KEY_ID_NAME_COMPLEMENTARY + "=?",
                    new String[]{String.valueOf(id)}, null, null, null, null);
            if (cursor != null)
            {
                if(cursor.moveToFirst()==true)
                {
                    complementary = new Complementary();
                    complementary.setIdCOMPLEMENTARY(cursor.getString(0));
                    complementary.setPhysicalPerson(cursor.getString(1));
                    complementary.setCurp(cursor.getString(2));
                    complementary.setDependentNumber(cursor.getString(3));
                    complementary.setIdOccupation(cursor.getString(4));
                    complementary.setEmail(cursor.getString(5));
                    complementary.setHomoclave(cursor.getString(6));
                    complementary.setFiel(cursor.getString(7));
                    complementary.setIdName(cursor.getString(8));
                    complementary.setComplete(cursor.getString(9));
                }
            }
        }
        catch (SQLiteException sql)
        {
            sql.getStackTrace();
        }
        // return object signing
        return complementary;
    }

    public ArrayList<Additionals> GetAdditional(int id)
    {
        ArrayList<Additionals> additionalses = new ArrayList<Additionals>();
        // Select All Query
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_ADDITIONAL, new String[]{KEY_ID_ADDITIONAL, KEY_NAME1_ADDITIONAL,
                        KEY_NAME2_ADDITIONAL, KEY_LAST_NAME_ADDITIONAL,KEY_M_LAST_NAME_ADDITIONAL, KEY_ID_OCCUPATION_ADDITIONAL,
                        KEY_PHONE_ADDITIONAL,KEY_ID_NAME_ADDITIONAL, KEY_ID_ADDITIONAL_TYPES_ADDITIONAL, KEY_COMPLETE_ADDITIONAL}, KEY_ID_NAME_ADDITIONAL + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null)
        {
            cursor.moveToFirst();
            if (cursor.moveToFirst() == true)
            {
                do
                {
                    Additionals additionals = new Additionals(cursor.getString(0), cursor.getString(1), cursor.getString(2),cursor.getString(3),cursor.getString(4),cursor.getString(5),cursor.getString(6), cursor.getString(7), cursor.getString(8), cursor.getString(9));
                    //Adding income to list
                    additionalses.add(additionals);
                }
                while (cursor.moveToNext());
                return additionalses;
            }
            else
            {
                return null;
            }
        }
        return additionalses;
    }

    public ArrayList<GroupDetail> GetAllGroupDetail(int id)
    {
        ArrayList<GroupDetail> groupDetails = new ArrayList<GroupDetail>();
        // Select All Query
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_GROUP_DETAIL, new String[]{KEY_ID_GROUP_DETAIL, KEY_GROUP_NAME, KEY_GROUP_CYCLE, KEY_ID_SPONSOR_GROUP_DETAIL,KEY_ID_CHANNEL_DISPERSION,KEY_ID_DISPERSION_MEDIUM, KEY_ID_USER_GROUP_DETAIL, KEY_DATE_CREATE_GROUP, KEY_ID_GROUP_DETAIL_SERVER, KEY_COMPLETE_GROUP_DETAIL}, KEY_ID_USER_GROUP_DETAIL + "=?",
                new String[]{String.valueOf(id)}, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
            if (cursor.moveToFirst() == true) {
                do
                {
                    GroupDetail groupDetail = new GroupDetail();
                    groupDetail.setIdGROUP_DETAIL(cursor.getString(0));
                    groupDetail.setGroupName(cursor.getString(1));
                    groupDetail.setGroupCycle(cursor.getString(2));
                    groupDetail.setIdSponsor(cursor.getString(3));
                    groupDetail.setIdChannelDispersion(cursor.getString(4));
                    groupDetail.setIdDispersionMedium(cursor.getString(5));
                    groupDetail.setIdUser(cursor.getString(6));
                    groupDetail.setDate(cursor.getString(7));
                    groupDetail.setIdGroupDetailServer(cursor.getString(8));
                    groupDetail.setComplete(cursor.getString(9));
                    //Adding income to list
                    groupDetails.add(groupDetail);
                }
                while (cursor.moveToNext());
                return groupDetails;
            } else {
                return null;
            }
        }
        return null;
    }

    public int GetLastIdGroupDetail()
    {
        int lastId = 0;
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT id from GroupDetail order by id DESC limit 1";
        Cursor c = db.rawQuery(query, null);
        if (c != null && c.moveToFirst())
        {
            lastId = (int) c.getLong(0); //The 0 is the column index, we only have 1 column, so the index is 0
        }

        return lastId;
    }

    public GroupDetail GetGroupDetail(int id)
    {
    SQLiteDatabase db = this.getReadableDatabase();

    Cursor cursor = db.query(TABLE_GROUP_DETAIL, new String[]{KEY_ID_GROUP_DETAIL, KEY_GROUP_NAME,
                    KEY_GROUP_CYCLE, KEY_ID_SPONSOR_GROUP_DETAIL,KEY_ID_CHANNEL_DISPERSION, KEY_ID_DISPERSION_MEDIUM,
                    KEY_ID_USER_GROUP_DETAIL, KEY_DATE_CREATE_GROUP, KEY_ID_GROUP_DETAIL_SERVER, KEY_COMPLETE_GROUP_DETAIL}, KEY_ID_USER_GROUP_DETAIL + "=?",
            new String[]{String.valueOf(id)}, null, null, null, null);
    if (cursor != null)
        cursor.moveToFirst();

    GroupDetail groupDetail = new GroupDetail(cursor.getString(0), cursor.getString(1), cursor.getString(2),cursor.getString(3),cursor.getString(4),cursor.getString(5),cursor.getString(6), cursor.getString(7),  cursor.getString(8),  cursor.getString(9));
    // return object signing
    return groupDetail;
}

    public ArrayList<Members> GetListMembers(int id)
    {
        ArrayList<Members> membersArrayList = new ArrayList<Members>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_MEMBERS, new String[]{KEY_ID_MEMBERS, KEY_ID_GROUP_DETAIL_MEMBERS,
                KEY_ID_NAME_MEMBERS, KEY_ID_NAME_MEMBERS_SERVER, KEY_COMPLETE_MEMBERS}, KEY_ID_GROUP_DETAIL_MEMBERS + "=?",
                new String[]{String.valueOf(id)},null, null, null, null);
        if (cursor != null)
        {
            cursor.moveToFirst();
            if (cursor.moveToFirst() == true)
            {
                do
                {
                    Members members = new Members();
                    members.setIdMEMBERS(cursor.getString(0));
                    members.setIdGroupDetail(cursor.getString(1));
                    members.setIdName(cursor.getString(2));
                    members.setIdNameServer(cursor.getString(3));
                    members.setComplete(cursor.getString(4));
                    //Adding income to list
                    membersArrayList.add(members);
                }
                while (cursor.moveToNext());
                return membersArrayList;
            }
            else
            {
                return null;
            }
        }
        return null;
    }

    public GroupData GetGroupData(int id)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        GroupData groupData = null;
        Cursor cursor = db.query(TABLE_GROUP_DATA, new String[]{KEY_ID_GROUP_DATA, KEY_MINIUM_AMOUNT_SAVINGS,
                        KEY_AMOUNT_FINE_DELAY, KEY_AMOUNT_MISSING_FINE,KEY_MEETING_SCHEDULE, KEY_MEETING_DATE,
                        KEY_DISBURSEMENT_DATE,KEY_PAY_DATE1, KEY_ID_PRODUCT_GROUP_DATA, KEY_ID_FREQUENCIES_GROUP_DATA,
                        KEY_ID_DEADLINES_GROUP_DATA, KEY_ID_PLACE_MEETING_GROUP_DATA, KEY_ID_GROUP_DETAIL_GROUP_DATA,
                        KEY_ID_NAME_GROUP_DATA, KEY_COMPLETE_GROUP_DATA}, KEY_ID_GROUP_DETAIL_GROUP_DATA + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        try
        {
            if (cursor != null)
                cursor.moveToFirst();
            if(cursor.moveToFirst() == true)
            {
                groupData = new GroupData(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getString(6), cursor.getString(7), cursor.getString(8), cursor.getString(9), cursor.getString(10), cursor.getString(11), cursor.getString(12), cursor.getString(13), cursor.getString(14));
            }
        }
        catch (SQLException sql)
        {
            sql.getStackTrace();
        }
        // return object signing
        return groupData;
    }

    public PlaceMeeting GetPlaceMeeting(int id)
    {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_PLACE_MEETING, new String[]{KEY_ID_PLACE_MEETING, KEY_ROUTE_PLACE_MEETING,
                        KEY_STREET_NUMBER1_PLACE_MEETING, KEY_STREET_NUMBER2_PLACE_MEETING,KEY_NEIGHBORHOOD_PLACE_MEETING, KEY_ADMINISTRATIVEAREALEVEL2_PLACE_MEETING,
                        KEY_ID_FEDERAL_ENTITY_PLACE_MEETING,KEY_POSTAL_CODE_PLACE_MEETING,KEY_ID_COUNTRY_PLACE_MEETING,KEY_ROUTE_REFERENCE1_PLACE_MEETING,
                        KEY_ROUTE_REFERENCE2_PLACE_MEETING,KEY_LOCATION_REFERENCE,KEY_PHONE_PLACE_MEETING, KEY_ID_PHONE_TYPES_PLACE_MEETING,
                        KEY_COMMENTS_PLACE_MEETING, KEY_ID_GROUP_DETAIL_PLACE_MEETING}, KEY_ID_GROUP_DETAIL_PLACE_MEETING + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        PlaceMeeting placeMeeting = new PlaceMeeting(cursor.getString(0), cursor.getString(1), cursor.getString(2),cursor.getString(3),cursor.getString(4),cursor.getString(5),cursor.getString(6), cursor.getString(7), cursor.getString(8),cursor.getString(9),cursor.getString(10),cursor.getString(11),cursor.getString(12),cursor.getString(13),cursor.getString(14),cursor.getString(15));
        // return object signing
        return placeMeeting;
    }

    public List<Document> GetAllDocument(String id)
    {
        List<Document> documentList = new ArrayList<Document>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_DOCUMENTS, new String[]{KEY_ID_DOCUMENTS, KEY_DESCRIPTION_DOCUMENTS, KEY_ID_NAME_DOCUMENTS}, KEY_ID_NAME_DOCUMENTS + "=?",
                new String[]{id}, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
            if (cursor.moveToFirst() == true) {
                do {
                    Document document = new Document();
                    document.setIdDOCUMENTS(cursor.getString(0));
                    document.setDescriptionDocuments(cursor.getString(1));
                    document.setIdName(cursor.getString(2));
                    //Adding income to list
                    documentList.add(document);
                }
                while (cursor.moveToNext());
                return documentList;
            } else {
                return null;
            }
        }
        return null;
    }

    public Secure GetSecure(int id)
    {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_SECURE, new String[]{KEY_ID_SECURE, KEY_START_DATE, KEY_LIMIT_TIME,
                        KEY_MODALITY, KEY_ID_NAME_SECURE,KEY_COMPLETE_SECURE}, KEY_ID_NAME_SECURE + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Secure secure = new Secure(cursor.getString(0), cursor.getString(1), cursor.getString(2),cursor.getString(3),cursor.getString(4),cursor.getString(5));
        // return object signing
        return secure;
    }

    public List<Beneficiary> GetAllBeneficiary(String id)
    {
        List<Beneficiary> beneficiaryList = new ArrayList<Beneficiary>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_BENEFICIARY, new String[]{KEY_ID_BENEFICIARY, KEY_NAME1_BENEFICIARY, KEY_NAME2_BENEFICIARY,
                                                                 KEY_LAST_NAME_BENEFICIARY, KEY_M_LAST_NAME_BENEFICIARY,KEY_ID_RELATIONSHIP,
                                                                 KEY_BIRTH_DATE_BENEFICIARY, KEY_ID_GENDER_BENEFICIARY, KEY_ID_SIGNING_BENEFICIARY,
                                                                 KEY_ID_SECURE_BENEFICIARY}, KEY_ID_SECURE_BENEFICIARY + "=?",
                new String[]{id}, null, null, null);
        if (cursor != null)
        {
            cursor.moveToFirst();
            if (cursor.moveToFirst() == true) {
                do {
                    Beneficiary beneficiary = new Beneficiary();
                    beneficiary.setIdBENEFICIARY(cursor.getString(0));
                    beneficiary.setName1(cursor.getString(1));
                    beneficiary.setName2(cursor.getString(2));
                    beneficiary.setLastName(cursor.getString(3));
                    beneficiary.setmLastName(cursor.getString(4));
                    beneficiary.setIdRelationship(cursor.getString(5));
                    beneficiary.setBirthDate(cursor.getString(6));
                    beneficiary.setIdGender(cursor.getString(7));
                    beneficiary.setIdSigning(cursor.getString(8));
                    beneficiary.setIdSecure(cursor.getString(9));
                    //Adding income to list
                    beneficiaryList.add(beneficiary);
                }
                while (cursor.moveToNext());
                return beneficiaryList;
            }
            else
            {
                return null;
            }
        }
        return null;
    }

    public int GetLastId()
    {
        int lastId = 0;
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT id from Name order by id DESC limit 1";
        Cursor c = db.rawQuery(query, null);
        if (c != null && c.moveToFirst())
        {
            lastId = (int) c.getLong(0); //The 0 is the column index, we only have 1 column, so the index is 0
        }

        return lastId;
    }

    /*Update Table*/

    public int UpdateIdName(Name name, int idName)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_NAME1, name.getName1()); // Name1
        values.put(KEY_NAME2, name.getName2()); // Name2
        values.put(KEY_LAST_NAME, name.getLastName()); // Last Name
        values.put(KEY_M_LAST_NAME, name.getmLastName()); // M Last Name
        values.put(KEY_ID_USER_NAME, name.getIdUser()); // Id User
        values.put(KEY_COMPLETE, name.getComplete()); // Complete
        values.put(KEY_DATE_ADMISSION, name.getDateAdmission()); //DateAdmission
        values.put(KEY_DAY_RET, name.getDayRet()); //DayRet
        values.put(KEY_ID_STATUS_NAME, name.getIdStatus()); // IdStatus
        values.put(KEY_ID_PRODUCT_NAME, name.getIdProduct()); // IdProduct
        values.put(KEY_ID_NAME_SERVER,name.getIdNameServer()); //IdNameServer
        // updating row
        return db.update(TABLE_NAME, values, KEY_ID_NAME + " = ?",
                new String[]{String.valueOf(idName)});
    }

    public int UpdateName(Name name)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_NAME1, name.getName1()); // Name1
        values.put(KEY_NAME2, name.getName2()); // Name2
        values.put(KEY_LAST_NAME, name.getLastName()); // Last Name
        values.put(KEY_M_LAST_NAME, name.getmLastName()); // M Last Name
        values.put(KEY_ID_USER_NAME, name.getIdUser()); // Id User
        values.put(KEY_COMPLETE, name.getComplete()); // Complete
        values.put(KEY_DATE_ADMISSION, name.getDateAdmission()); //DateAdmission
        values.put(KEY_DAY_RET, name.getDayRet()); //DayRet
        values.put(KEY_ID_STATUS_NAME, name.getIdStatus()); // IdStatus
        values.put(KEY_ID_PRODUCT_NAME, name.getIdProduct()); // IdProduct
        values.put(KEY_ID_NAME_SERVER, name.getIdNameServer()); //IdNameServer
        // updating row
        return db.update(TABLE_NAME, values, KEY_ID_NAME + " = ?",
                new String[]{String.valueOf(name.getIdName())});
    }

    public int UpdateIdPhone(Phone phones, int idPhone)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        try
        {
            values.put(KEY_NUMBER_PHONES, phones.getNumber()); // Number
            values.put(KEY_ID_PHONE_TYPES_PHONES, phones.getIdPhoneTypes()); // Id Phone Types
            values.put(KEY_ID_NAME_PHONES, phones.getIdName()); // Id Name
            values.put(KEY_COMPLETE_PHONES, phones.getComplete()); // Complete
            //values.put(KEY_ID_PHONES, idPhone);
        }
        catch (SQLException sql)
        {
            sql.getStackTrace();
        }
        return db.update(TABLE_PHONES, values, KEY_ID_PHONES + " = ?",
                new String[]{String.valueOf(phones.getIdPHONES())});
    }

    public int UpdateIdPhoneName(Phone phones, int idName)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        try
        {
            values.put(KEY_NUMBER_PHONES, phones.getNumber()); // Number
            values.put(KEY_ID_PHONE_TYPES_PHONES, phones.getIdPhoneTypes()); // Id Phone Types
            values.put(KEY_ID_NAME_PHONES, idName); // Id Name
            values.put(KEY_COMPLETE_PHONES, phones.getComplete()); // Complete
        }
        catch (SQLException sql)
        {
            sql.getStackTrace();
        }
        return db.update(TABLE_PHONES, values, KEY_ID_PHONES + " = ?",
                new String[]{String.valueOf(phones.getIdPHONES())});
    }

    public int UpdatePhones(Phone phones)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_NUMBER_PHONES, phones.getNumber()); // Number
        values.put(KEY_ID_PHONE_TYPES_PHONES, phones.getIdPhoneTypes()); // Id Phone Types
        values.put(KEY_ID_NAME_PHONES, phones.getIdName()); // Id Name
        values.put(KEY_COMPLETE_PHONES, phones.getComplete()); // Complete
        // updating row
        return db.update(TABLE_PHONES, values, KEY_ID_PHONES + " = ?",
                new String[]{String.valueOf(phones.getIdPHONES())});
    }

    public int UpdateIdAddress(Address address, int idAddress)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        try
        {
            values.put(KEY_ROUTE, address.getRoute()); // Route
            values.put(KEY_STREET_NUMBER1, address.getStreetNumber1()); // Street Number1
            values.put(KEY_STREET_NUMBER2, address.getStreetNumber2()); // Street Number2
            values.put(KEY_NEIGHBORHOOD, address.getNeighborhood()); // Neigborhood
            values.put(KEY_ADMINISTRATIVEAREALEVEL2, address.getAdministrativeAreaLevel2()); // AdministrativeLevel2
            values.put(KEY_ID_FEDERAL_ENTITY_ADDRESS, address.getIdFederalEntity()); // Id Federal Entity
            values.put(KEY_POSTAL_CODE, address.getPostalCode()); // Postal Code
            values.put(KEY_ID_COUNTRY_ADDRESS, address.getIdCountry()); // Id Country
            values.put(KEY_ROUTE_REFERENCE1, address.getRouteReference1()); // Route Reference 1
            values.put(KEY_ROUTE_REFERENCE2, address.getRouteReference2()); // Route Reference 2
            values.put(KEY_ID_NAME_ADDRESS, address.getIdName()); // Id Name
            values.put(KEY_COMPLETE_ADDRESS, address.getComplete()); // Complete
            values.put(KEY_ID_ADDRESS_TYPE, address.getIdAddressTypes()); //Id Address Type
            //values.put(KEY_ID_ADDRESS, idAddress);
        }
        catch (SQLException sql)
        {
            sql.getStackTrace();
        }
        return db.update(TABLE_ADDRESS, values, KEY_ID_ADDRESS + " = ?",
                new String[]{String.valueOf(address.getIdADDRESS())});
    }

    public int UpdateAddress(Address address)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ROUTE, address.getRoute()); // Route
        values.put(KEY_STREET_NUMBER1, address.getStreetNumber1()); // Street Number1
        values.put(KEY_STREET_NUMBER2, address.getStreetNumber2()); // Street Number2
        values.put(KEY_NEIGHBORHOOD, address.getNeighborhood()); // Neigborhood
        values.put(KEY_ADMINISTRATIVEAREALEVEL2, address.getAdministrativeAreaLevel2()); // AdministrativeLevel2
        values.put(KEY_ID_FEDERAL_ENTITY_ADDRESS, address.getIdFederalEntity()); // Id Federal Entity
        values.put(KEY_POSTAL_CODE, address.getPostalCode()); // Postal Code
        values.put(KEY_ID_COUNTRY_ADDRESS, address.getIdCountry()); // Id Country
        values.put(KEY_ROUTE_REFERENCE1, address.getRouteReference1()); // Route Reference 1
        values.put(KEY_ROUTE_REFERENCE2, address.getRouteReference2()); // Route Reference 2
        values.put(KEY_ID_NAME_ADDRESS, address.getIdName()); // Id Name
        values.put(KEY_COMPLETE_ADDRESS, address.getComplete()); // Complete
        values.put(KEY_ID_ADDRESS_TYPE, address.getIdAddressTypes()); //Id Address Type
        // updating row
        return db.update(TABLE_ADDRESS, values, KEY_ID_ADDRESS + " = ?",
                new String[]{String.valueOf(address.getIdADDRESS())});
    }

    public int UpdateEconomic(Economic economic)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ID_NAME_ECONOMIC, economic.getIdName()); // Id Name
        values.put(KEY_ANOTHER_SOURCE_NAME, economic.getAnotherSourceIncome()); // Another Source Name
        values.put(KEY_COMPLETE_ECONOMIC, economic.getComplete()); // Complete
        values.put(KEY_CONTRIBUTION_LEVEL, economic.getContributionLevel()); // Contribution Level
        values.put(KEY_CURRENT_BUSINESS_TIME, economic.getCurrentBusinessTime()); // Current Business Time
        values.put(KEY_ID_ECONOMIC_ACITIVTY_ECONOMIC, economic.getIdEconomicActivity()); // Id Economic Activity
        values.put(KEY_ID_LOCAL_TYPE_ECONOMIC, economic.getIdLocalType()); // Id Local Type
        values.put(KEY_ID_MAKES_ACTIVITY_ECONOMIC, economic.getIdMakesActivity()); //Makes Activity
        values.put(KEY_ID_TIME_ACTIVITY_ECONOMIC, economic.getIdTimeActivity()); //Time Activity
        values.put(KEY_EMAIL_ECONOMIC, economic.getEmail()); //Email
        // updating row
        return db.update(TABLE_ECONOMIC, values, KEY_ID_ECONOMIC + " = ?",
                new String[]{String.valueOf(economic.getIdECONOMIC())});
    }

    public int UpdateIdEconomic(Economic economic, int idEconomic)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        try
        {
            values.put(KEY_ID_NAME_ECONOMIC, economic.getIdName()); // Id Name
            values.put(KEY_ANOTHER_SOURCE_NAME, economic.getAnotherSourceIncome()); // Another Source Name
            values.put(KEY_COMPLETE_ECONOMIC, economic.getComplete()); // Complete
            values.put(KEY_CONTRIBUTION_LEVEL, economic.getContributionLevel()); // Contribution Level
            values.put(KEY_CURRENT_BUSINESS_TIME, economic.getCurrentBusinessTime()); // Current Business Time
            values.put(KEY_ID_ECONOMIC_ACITIVTY_ECONOMIC, economic.getIdEconomicActivity()); // Id Economic Activity
            values.put(KEY_ID_LOCAL_TYPE_ECONOMIC, economic.getIdLocalType()); // Id Local Type
            values.put(KEY_ID_MAKES_ACTIVITY_ECONOMIC, economic.getIdMakesActivity()); //Makes Activity
            values.put(KEY_ID_TIME_ACTIVITY_ECONOMIC, economic.getIdTimeActivity()); //Time Activity
            values.put(KEY_EMAIL_ECONOMIC, economic.getEmail()); //Email
            //values.put(KEY_ID_ECONOMIC,idEconomic);
            // updating row

        }
        catch (SQLException sql)
        {
            sql.getStackTrace();
        }
        return db.update(TABLE_ECONOMIC, values, KEY_ID_ECONOMIC + " = ?",
                new String[]{String.valueOf(economic.getIdECONOMIC())});
    }

    public int UpdateIdBasics(Basics basics, int idBasics)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        try
        {
            values.put(KEY_BIRTH_DATE, basics.getBirthdate()); // Birth Date
            values.put(KEY_VOTER_KEY, basics.getVoterKey()); // Voter Key
            values.put(KEY_NUM_REG_VOTER, basics.getNumRegVoter()); // Num Reg Voter
            values.put(KEY_ID_GENDER_BASICS, basics.getIdGender()); // Id gender
            values.put(KEY_ID_COUNTRY_BIRTH_BASICS, basics.getIdCountryBirth()); // Id Country Birth
            values.put(KEY_ID_FEDERAL_ENTITY_BASICS, basics.getIdFederalEntity()); // Id Federal Entity
            values.put(KEY_ID_NATIONALITY_BASICS, basics.getIdNationality()); // Id Nationality
            values.put(KEY_ID_CIVIL_STATUS_BASICS, basics.getIdCivilStatus()); // Id Civil Status
            values.put(KEY_SONS, basics.getSons()); // Sons
            values.put(KEY_ID_LIVING_PLACE_BASICS, basics.getIdLivingPlace()); // Id Living Place
            values.put(KEY_ID_LEVEL_EDUCATION_BASICS, basics.getIdLevelEducation()); // Id Level Education
            values.put(KEY_ID_KIND_LOCAL, basics.getIdKindLocal()); // IdKindLocal
            values.put(KEY_ID_NAME_BASICS, basics.getIdName()); // Id Name
            values.put(KEY_COMPLETE_BASICS, basics.getComplete()); // Complete
            //values.put(KEY_ID_BASICS, idBasics);
        }
        catch (SQLException sql)
        {
            sql.getStackTrace();
        }
        return db.update(TABLE_BASICS, values, KEY_ID_BASICS + " = ?",
                new String[]{String.valueOf(basics.getIdBASICS())});
    }

    public int  UpdateBasics(Basics basics)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_BIRTH_DATE, basics.getBirthdate()); // Birth Date
        values.put(KEY_VOTER_KEY, basics.getVoterKey()); // Voter Key
        values.put(KEY_NUM_REG_VOTER, basics.getNumRegVoter()); // Num Reg Voter
        values.put(KEY_ID_GENDER_BASICS, basics.getIdGender()); // Id gender
        values.put(KEY_ID_COUNTRY_BIRTH_BASICS, basics.getIdCountryBirth()); // Id Country Birth
        values.put(KEY_ID_FEDERAL_ENTITY_BASICS, basics.getIdFederalEntity()); // Id Federal Entity
        values.put(KEY_ID_NATIONALITY_BASICS, basics.getIdNationality()); // Id Nationality
        values.put(KEY_ID_CIVIL_STATUS_BASICS, basics.getIdCivilStatus()); // Id Civil Status
        values.put(KEY_SONS, basics.getSons()); // Sons
        values.put(KEY_ID_LIVING_PLACE_BASICS, basics.getIdLivingPlace()); // Id Living Place
        values.put(KEY_ID_LEVEL_EDUCATION_BASICS, basics.getIdLevelEducation()); // Id Level Education
        values.put(KEY_ID_KIND_LOCAL, basics.getIdKindLocal()); // IdKindLocal
        values.put(KEY_ID_NAME_BASICS, basics.getIdName()); // Id Name
        values.put(KEY_COMPLETE_BASICS, basics.getComplete()); // Complete
        // updating row
        return db.update(TABLE_BASICS, values, KEY_ID_BASICS + " = ?",
                new String[]{String.valueOf(basics.getIdBASICS())});
    }

    public int UpdateIdComplementary(Complementary complementary, int idComplementary)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        try
        {
            values.put(KEY_PHYSICAL_PERSON, complementary.getPhysicalPerson()); // Physcal Person
            values.put(KEY_CURP, complementary.getCurp()); // Curp
            values.put(KEY_DEPENDENT_NUMBER, complementary.getDependentNumber()); // Dependente Number
            values.put(KEY_ID_OCCUPATION_COMPLEMENTARY, complementary.getIdOccupation()); // Id Occupation
            values.put(KEY_EMAIL_COMPLEMENTARY, complementary.getEmail()); // Email
            values.put(KEY_HOMOCLAVE, complementary.getHomoclave()); // Homoclave
            values.put(KEY_FIEL, complementary.getFiel()); // Fiel
            values.put(KEY_ID_NAME_COMPLEMENTARY, complementary.getIdName()); // Id Name
            values.put(KEY_COMPLETE_COMPLEMENTARY, complementary.getComplete()); // Complete
            //values.put(KEY_ID_COMPLEMENTARY, idComplementary);
        }
        catch (SQLException sql)
        {
            sql.getStackTrace();
        }
        return db.update(TABLE_COMPLEMENTARY, values, KEY_ID_COMPLEMENTARY + " = ?",
                new String[]{String.valueOf(complementary.getIdCOMPLEMENTARY())});
    }

    public int UpdateComplementary(Complementary complementary)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_PHYSICAL_PERSON, complementary.getPhysicalPerson()); // Physcal Person
        values.put(KEY_CURP, complementary.getCurp()); // Curp
        values.put(KEY_DEPENDENT_NUMBER, complementary.getDependentNumber()); // Dependente Number
        values.put(KEY_ID_OCCUPATION_COMPLEMENTARY, complementary.getIdOccupation()); // Id Occupation
        values.put(KEY_EMAIL_COMPLEMENTARY, complementary.getEmail()); // Email
        values.put(KEY_HOMOCLAVE, complementary.getHomoclave()); // Homoclave
        values.put(KEY_FIEL, complementary.getFiel()); // Fiel
        values.put(KEY_ID_NAME_COMPLEMENTARY, complementary.getIdName()); // Id Name
        values.put(KEY_COMPLETE_COMPLEMENTARY, complementary.getComplete()); // Complete
        // updating row
        return db.update(TABLE_COMPLEMENTARY, values, KEY_ID_COMPLEMENTARY + " = ?",
                new String[]{String.valueOf(complementary.getIdCOMPLEMENTARY())});
    }

    public int UpdateIdAdditionals(Additionals additionals, int idAdditional)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        try
        {
            values.put(KEY_NAME1_ADDITIONAL, additionals.getName1()); // Name1
            values.put(KEY_NAME2_ADDITIONAL, additionals.getName2()); // Name2
            values.put(KEY_LAST_NAME_ADDITIONAL, additionals.getLastName()); // Last Name
            values.put(KEY_M_LAST_NAME_ADDITIONAL, additionals.getmLastName()); // M Last Name
            values.put(KEY_ID_OCCUPATION_ADDITIONAL, additionals.getIdOccupation()); // Email
            values.put(KEY_PHONE_ADDITIONAL, additionals.getPhone()); // Phone
            values.put(KEY_ID_NAME_ADDITIONAL, additionals.getIdName()); // Id Name
            values.put(KEY_ID_ADDITIONAL_TYPES_ADDITIONAL, additionals.getIdAdditionalTypes()); // Id Additional Types
            values.put(KEY_COMPLETE_ADDITIONAL, additionals.getComplete()); // Complete
            //values.put(KEY_ID_ADDITIONAL, idAdditional);
        }
        catch (SQLException sql)
        {
            sql.getStackTrace();
        }
        return db.update(TABLE_ADDITIONAL, values, KEY_ID_ADDITIONAL + " = ?",
                new String[]{String.valueOf(additionals.getIdADDITIONAL())});
    }

    public int UpdateAdditionals(Additionals additionals)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_NAME1_ADDITIONAL, additionals.getName1()); // Name1
        values.put(KEY_NAME2_ADDITIONAL, additionals.getName2()); // Name2
        values.put(KEY_LAST_NAME_ADDITIONAL, additionals.getLastName()); // Last Name
        values.put(KEY_M_LAST_NAME_ADDITIONAL, additionals.getmLastName()); // M Last Name
        values.put(KEY_ID_OCCUPATION_ADDITIONAL, additionals.getIdOccupation()); // Email
        values.put(KEY_PHONE_ADDITIONAL, additionals.getPhone()); // Phone
        values.put(KEY_ID_NAME_ADDITIONAL, additionals.getIdName()); // Id Name
        values.put(KEY_ID_ADDITIONAL_TYPES_ADDITIONAL, additionals.getIdAdditionalTypes()); // Id Additional Types
        values.put(KEY_COMPLETE_ADDITIONAL, additionals.getComplete()); // Complete
        // updating row
        return db.update(TABLE_ADDITIONAL, values, KEY_ID_ADDITIONAL + " = ?",
                new String[]{String.valueOf(additionals.getIdADDITIONAL())});
    }

    public int UpdateGroupDetail(GroupDetail groupDetail)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_GROUP_NAME, groupDetail.getGroupName()); // Group Name
        values.put(KEY_GROUP_CYCLE, groupDetail.getGroupCycle()); // Group Cycle
        values.put(KEY_ID_SPONSOR_GROUP_DETAIL, groupDetail.getIdSponsor()); // Id Sponsor
        values.put(KEY_ID_CHANNEL_DISPERSION, groupDetail.getIdChannelDispersion()); // Id Channel Dispersion
        values.put(KEY_ID_DISPERSION_MEDIUM, groupDetail.getIdDispersionMedium()); // Id DispersionMedium
        values.put(KEY_ID_USER_GROUP_DETAIL, groupDetail.getIdUser()); // Id User
        values.put(KEY_ID_GROUP_DETAIL_SERVER, groupDetail.getIdGroupDetailServer());
        values.put(KEY_COMPLETE_GROUP_DETAIL, groupDetail.getComplete()); // Complete
        // updating row
        return db.update(TABLE_GROUP_DETAIL, values, KEY_ID_GROUP_DETAIL + " = ?",
                new String[]{String.valueOf(groupDetail.getIdGROUP_DETAIL())});
    }

    public int UpdateMembers(Members members)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ID_GROUP_DETAIL_MEMBERS, members.getIdGroupDetail()); // Group Detail
        values.put(KEY_ID_NAME_MEMBERS, members.getIdName()); // Id Name
        values.put(KEY_ID_NAME_MEMBERS_SERVER, members.getIdNameServer()); //Id Name Server
        values.put(KEY_COMPLETE_MEMBERS, members.getComplete()); // Complete
        // updating row
        return db.update(TABLE_MEMBERS, values, KEY_ID_MEMBERS + " = ?",
                new String[]{String.valueOf(members.getIdMEMBERS())});
    }

    public int UpdatePlaceMeeting(PlaceMeeting placeMeeting)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ROUTE_PLACE_MEETING, placeMeeting.getRoute()); // Route
        values.put(KEY_STREET_NUMBER1_PLACE_MEETING, placeMeeting.getStreetNumber1()); // Street Number1
        values.put(KEY_STREET_NUMBER2_PLACE_MEETING, placeMeeting.getStreetNumber2()); // Street Number2
        values.put(KEY_NEIGHBORHOOD_PLACE_MEETING, placeMeeting.getNeighborhood()); // Neigborhood
        values.put(KEY_ADMINISTRATIVEAREALEVEL2_PLACE_MEETING, placeMeeting.getAdministrativeAreaLevel2()); // AdministrativeLevel2
        values.put(KEY_ID_FEDERAL_ENTITY_PLACE_MEETING, placeMeeting.getIdFederalEntity()); // Id Federal Entity
        values.put(KEY_POSTAL_CODE_PLACE_MEETING, placeMeeting.getPostalCode()); // Postal Code
        values.put(KEY_ID_COUNTRY_PLACE_MEETING, placeMeeting.getIdCountry()); // Id Country
        values.put(KEY_ROUTE_REFERENCE1_PLACE_MEETING, placeMeeting.getRouteReference1()); // Route Reference 1
        values.put(KEY_ROUTE_REFERENCE2_PLACE_MEETING, placeMeeting.getRouteReference2()); // Route Reference 2
        values.put(KEY_LOCATION_REFERENCE, placeMeeting.getLocationReference()); // Location References
        values.put(KEY_PHONE_PLACE_MEETING, placeMeeting.getPhone()); // Phone
        values.put(KEY_ID_PHONE_TYPES_PLACE_MEETING, placeMeeting.getIdPhoneTypes()); // Phone Types
        values.put(KEY_COMMENTS_PLACE_MEETING, placeMeeting.getComments()); // Comments
        values.put(KEY_ID_GROUP_DETAIL_PLACE_MEETING, Integer.parseInt(placeMeeting.getIdGroupDetail())); // Id Group Detail
        // updating row
        return db.update(TABLE_PLACE_MEETING, values, KEY_ID_PLACE_MEETING + " = ?",
                new String[]{String.valueOf(placeMeeting.getIdPLACEMEETING())});
    }

    public int UpdateGroupData(GroupData groupData)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_MINIUM_AMOUNT_SAVINGS, groupData.getMiniumAmountSavings()); // Minium Amount Savings
        values.put(KEY_AMOUNT_FINE_DELAY, groupData.getAmountFineDelay()); // Amount Fine Delay
        values.put(KEY_AMOUNT_MISSING_FINE, groupData.getAmountMissingFine()); // Amount Missing Fine
        values.put(KEY_MEETING_SCHEDULE, groupData.getMeetingSchedule()); // Meeting Schedule
        values.put(KEY_MEETING_DATE, groupData.getMeetingDate()); // Meeting Date
        values.put(KEY_DISBURSEMENT_DATE, groupData.getDisbursementDate()); // Disbursement Date
        values.put(KEY_PAY_DATE1, groupData.getPayDate1()); // Pay Date1
        values.put(KEY_ID_PRODUCT_GROUP_DATA, groupData.getIdProduct()); // Id Product
        values.put(KEY_ID_FREQUENCIES_GROUP_DATA, groupData.getIdFrequencies()); // Id Frequencies
        values.put(KEY_ID_DEADLINES_GROUP_DATA, groupData.getIdDeadlines()); // Id Deadlines
        values.put(KEY_ID_PLACE_MEETING_GROUP_DATA, groupData.getIdPlaceMeeting()); // Id Place Meeting
        values.put(KEY_ID_GROUP_DETAIL_GROUP_DATA, groupData.getIdGroupDetail()); // Group Detail
        values.put(KEY_ID_NAME_GROUP_DATA, groupData.getIdNameGroupData()); // Group Detail
        values.put(KEY_COMPLETE_GROUP_DATA, groupData.getComplete()); // Complete
        // updating row
        return db.update(TABLE_GROUP_DATA, values, KEY_ID_GROUP_DATA + " = ?",
                new String[]{String.valueOf(groupData.getIdGROUPDATA())});
    }

    /*Delete Table*/
    public void DeleteUser(int id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        try
        {
            db.delete(TABLE_USER, KEY_ID_USER + " = ?",
                    new String[]{String.valueOf(id)});
            db.close();
        }
        catch (Exception ex)
        {

        }
    }

    public void DeleteName(int id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        try
        {
            db.delete(TABLE_NAME, KEY_ID_NAME + " = ?",
                    new String[]{String.valueOf(id)});
            db.close();
        }
        catch (Exception ex)
        {

        }
    }

    public void DeleteSigning(int id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        try
        {
            db.delete(TABLE_SIGNING, KEY_ID_SIGNING + " = ?",
                    new String[]{String.valueOf(id)});
            db.close();
        }
        catch (Exception ex)
        {

        }
    }

    public void DeleteProspect(int id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        try
        {
            db.delete(TABLE_PROSPECT, KEY_ID_NAME_PROSPECT + " = ?",
                    new String[]{String.valueOf(id)});
            db.close();
        }
        catch (Exception ex)
        {

        }
    }

    public void DeleteAddress(int id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        try
        {
            db.delete(TABLE_ADDRESS, KEY_ID_NAME_ADDRESS + " = ?",
                    new String[]{String.valueOf(id)});
            db.close();
        }
        catch (Exception ex)
        {

        }
    }

    public void DeletePhones(int id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        try
        {
            db.delete(TABLE_PHONES, KEY_ID_NAME_PHONES + " = ?",
                    new String[]{String.valueOf(id)});
            db.close();
        }
        catch (Exception ex)
        {

        }
    }

    public void DeleteBasics(int id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        try
        {
            db.delete(TABLE_BASICS, KEY_ID_NAME_BASICS + " = ?",
                    new String[]{String.valueOf(id)});
            db.close();
        }
        catch (Exception ex)
        {

        }
    }

    public void DeleteEconomic(int id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        try
        {
            db.delete(TABLE_ECONOMIC, KEY_ID_NAME_ECONOMIC + " = ?",
                    new String[]{String.valueOf(id)});
            db.close();
        }
        catch (Exception ex)
        {

        }
    }

    public void DeleteComplementary(int id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        try
        {
            db.delete(TABLE_COMPLEMENTARY, KEY_ID_NAME_COMPLEMENTARY + " = ?",
                    new String[]{String.valueOf(id)});
            db.close();
        }
        catch (Exception ex)
        {

        }
    }

    public void DeleteAdditional(int id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        try
        {
            db.delete(TABLE_ADDITIONAL, KEY_ID_NAME_ADDITIONAL + " = ?",
                    new String[]{String.valueOf(id)});
            db.close();
        }
        catch (Exception ex)
        {

        }
    }

    public void DeleteGroupDetail(int id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        try
        {
            db.delete(TABLE_GROUP_DETAIL, KEY_ID_GROUP_DETAIL + " = ?",
                    new String[]{String.valueOf(id)});
            db.close();
        }
        catch (Exception ex)
        {

        }
    }

    public void DeleteMembers(int id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        try
        {
            db.delete(TABLE_MEMBERS, KEY_ID_NAME_MEMBERS + " = ?",
                    new String[]{String.valueOf(id)});
            db.close();
        }
        catch (Exception ex)
        {

        }
    }

    public void DeleteGroupData(int id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        try
        {
            db.delete(TABLE_GROUP_DATA, KEY_ID_GROUP_DETAIL_GROUP_DATA + " = ?",
                    new String[]{String.valueOf(id)});
            db.close();
        }
        catch (Exception ex)
        {

        }
    }

    public void DeletePlaceMeeting(int id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        try
        {
            db.delete(TABLE_PLACE_MEETING, KEY_ID_GROUP_DETAIL_PLACE_MEETING + " = ?",
                    new String[]{String.valueOf(id)});
            db.close();
        }
        catch (Exception ex)
        {

        }
    }

    public void DeleteDocuments(int id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        try
        {
            db.delete(TABLE_DOCUMENTS, KEY_ID_NAME_DOCUMENTS + " = ?",
                    new String[]{String.valueOf(id)});
            db.close();
        }
        catch (Exception ex)
        {

        }
    }

    public void DeleteSecure(int id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        try
        {
            db.delete(TABLE_SECURE, KEY_ID_NAME_SECURE + " = ?",
                    new String[]{String.valueOf(id)});
            db.close();
        }
        catch (Exception ex)
        {

        }
    }

    public void DeleteBeneficiary(int id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        try
        {
            db.delete(TABLE_BENEFICIARY, KEY_ID_SECURE_BENEFICIARY + " = ?",
                    new String[]{String.valueOf(id)});
            db.close();
        }
        catch (Exception ex)
        {

        }
    }
}
