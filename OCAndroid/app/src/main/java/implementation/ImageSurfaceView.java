package implementation;

import android.content.Context;
import android.content.res.Configuration;
import android.hardware.Camera;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.IOException;
import java.util.List;

/**
 * Created by andresaleman on 3/28/17.
 */

public class ImageSurfaceView extends SurfaceView implements SurfaceHolder.Callback {

    private Camera camera;
    private SurfaceHolder surfaceHolder;

    public ImageSurfaceView(Context context, Camera camera) {
        super(context);
        this.camera = camera;
        this.surfaceHolder = getHolder();
        this.surfaceHolder.addCallback(this);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        try
        {
            Camera.Parameters parameters = camera.getParameters();
            List<Camera.Size> sizes = parameters.getSupportedPictureSizes();

            Camera.Size result = null;
            for (int i=0;i<sizes.size();i++)
            {
                result = (Camera.Size) sizes.get(i);
                Log.i("PictureSize", "Supported Size. Width: " + result.width + "height : " + result.height);

                /*if(result.width == 640)
                {
                    //parameters.setPreviewSize(result.width, result.height);//640*480
                    parameters.setPictureSize(result.width, result.height);

                    //Now if camera support for 640*480 pictures size you will get captured image as same size
                }
                else
                {
                    //to do here
                    //parameters.setPreviewSize(result.width, result.height);//640*480
                    parameters.setPictureSize(result.width, result.height);
                }*/
            }
            if (this.getResources().getConfiguration().orientation != Configuration.ORIENTATION_LANDSCAPE)
            {
                parameters.set("orientation", "portrait");
                this.camera.setDisplayOrientation(90);

            }
            else
            {

                this.camera.setDisplayOrientation(0);

            }
            this.camera.setParameters(parameters);
            this.camera.setPreviewDisplay(holder);
            this.camera.startPreview();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        this.camera.setPreviewCallback(null);
        this.camera.stopPreview();
        this.camera.release();
        this.camera=null;
    }
}
