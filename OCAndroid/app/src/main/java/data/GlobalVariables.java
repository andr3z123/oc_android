package data;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.os.AsyncTask;
import android.os.Handler;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Patterns;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Pattern;

import implementation.DBHandler;
import implementation.FunctionJson;
import model.Additional;
import model.Additionals;
import model.Address;
import model.Addresses;
import model.Basics;
import model.Beneficiary;
import model.Complementary;
import model.Economic;
import model.EconomicActivityCatalog;
import model.GroupData;
import model.GroupDetail;
import model.Members;
import model.Name;
import model.NameUser;
import model.NumberMembers;
import model.Phone;
import model.RequestName;
import model.ResponseAdditional;
import model.ResponseAddress;
import model.ResponseBasics;
import model.ResponseComplementary;
import model.ResponseEconomic;
import model.ResponseGroupDetail;
import model.ResponseMembers;
import model.ResponseName;
import model.ResponseNameUser;
import model.ResponsePhones;
import model.ResponsePlaceMeeting;
import model.ResponseSigning;
import model.ResponseUser;
import model.Secure;
import model.User;
import nubaj.com.ocandroid.R;

/**
 * Created by Jose Luis on 10/02/2017.
 */

public class GlobalVariables
{
    public static String getFormatDate(int day, int month, int year){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendarFormatterDate = Calendar.getInstance();
        calendarFormatterDate.set(year, month, day);
        long endMillis = calendarFormatterDate.getTimeInMillis();
        String str2 = formatter.format(endMillis).toString();
        return str2;
    }

    public static String getFormatTime(int hour, int minute){
        SimpleDateFormat formatter = new SimpleDateFormat("hh:mm");
        Calendar calendarFormatterDate = Calendar.getInstance();
        calendarFormatterDate.set(hour, minute);
        long endMillis = calendarFormatterDate.getTimeInMillis();
        String str2 = formatter.format(endMillis).toString();
        return str2;
    }

    public static void getMessage(final Activity activity, final String menssage) {
        activity.runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(activity, menssage, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public static boolean curpValidate(String curp)
    {
        String regex =
                "[A-Z]{1}[AEIOU]{1}[A-Z]{2}[0-9]{2}" +
                        "(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])" +
                        "[HM]{1}" +
                        "(AS|BC|BS|CC|CS|CH|CL|CM|DF|DG|GT|GR|HG|JC|MC|MN|MS|NT|NL|OC|PL|QT|QR|SP|SL|SR|TC|TS|TL|VZ|YN|ZS|NE)" +
                        "[B-DF-HJ-NP-TV-Z]{3}" +
                        "[0-9A-Z]{1}[0-9]{1}$";

        Pattern patron = Pattern.compile(regex);
        if(!patron.matcher(curp).matches())
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public static boolean claveElectorValidate(String claveElector)
    {
        String regex =
                "[A-Z]{6}[0-9]{8}[HM]{1}[0-9]{3}";

        Pattern patron = Pattern.compile(regex);
        if(!patron.matcher(claveElector).matches())
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public static boolean rfcValidate(String rfc)
    {
        String regex = "^[A-Z]{4}([0-9]{2})(1[0-2]|0[1-9])([0-3][0-9])([ -]?)$";
        Pattern patron = Pattern.compile(regex);
        if(!patron.matcher(rfc).matches())
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public static Integer getAge(String claveElector)
    {
        //CNCNCS90032209H300

        int year;
        int data = Integer.parseInt(claveElector.substring(6,7));

        if (data == 0 || data == 1 || data == 2 || data == 3){
            year = Integer.parseInt(claveElector.substring(6,8))+2000;
        }else {
            year = Integer.parseInt(claveElector.substring(6,8))+1900;
        }
        int month = Integer.parseInt(claveElector.substring(8,10));
        int day = Integer.parseInt(claveElector.substring(10,12));

        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.set(year, month, day);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)){
            age--;
        }

        Integer ageInt = new Integer(age);
        return ageInt;
    }

    public static boolean emailValidate(String email)
    {
        String regex = "";
        Pattern patron = Pattern.compile(regex);
        if(!Patterns.EMAIL_ADDRESS.matcher(email).matches())
        {
            return  false;
        }
        else
        {
            return true;
        }
    }

    public static void capitalLetters(final EditText editText){
        //editText.setFilters(new InputFilter[]{new InputFilter.AllCaps()});



        editText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

            }
            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
            }
            @Override
            public void afterTextChanged(Editable et) {
                String s=et.toString();
                if(!s.equals(s.toUpperCase()))
                {
                    s=s.toUpperCase();
                    editText.setText(s);
                }
                editText.setSelection(editText.getText().length());
            }
        });


    }

    public static boolean validateHomoClave(final String homo_Clave) {
        if (homo_Clave.length() == 3) {
            for (int i = 0; i < homo_Clave.length(); ++i) {
                char character = homo_Clave.charAt(i);

                if (!Character.isLetterOrDigit(character)) {
                    return false;
                }

            }
        }else {
            return false;
        }
        return true;
    }

    public static boolean validateLengthPhone(String number_phone){
        if (number_phone.length() == 10){

        }else {
            return false;
        }
        return true;
    }

    public static boolean validateLengthFiel(String number_fiel){
        if (number_fiel.length() == 12){

        }else {
            return false;
        }
        return true;
    }

    public static boolean validateLengthCP(String number_cp){
        if (number_cp.length() == 5){

        }else {
            return false;
        }
        return true;
    }

    public static boolean validateLengthNumberElectoral(String number_electoral){
        if (number_electoral.length() == 18){

        }else {
            return false;
        }
        return true;
    }

    public static List listCountry() {
        // Spinner Drop down elements type Pais Nacimiento
        List<String> list_Pais_Nacimiento = new ArrayList<String>();
        list_Pais_Nacimiento.add("México");
        list_Pais_Nacimiento.add("Andorra");
        list_Pais_Nacimiento.add("E. A. U.");
        list_Pais_Nacimiento.add("Afganistán");
        list_Pais_Nacimiento.add("Antigua/Barbuda");
        list_Pais_Nacimiento.add("Anguilla");
        list_Pais_Nacimiento.add("Albania");
        list_Pais_Nacimiento.add("Armenia");
        list_Pais_Nacimiento.add("Antillas Neerl");
        list_Pais_Nacimiento.add("Angola");
        list_Pais_Nacimiento.add("Antártida");
        list_Pais_Nacimiento.add("Argentina");
        list_Pais_Nacimiento.add("Samoa americana");
        list_Pais_Nacimiento.add("Austria");
        list_Pais_Nacimiento.add("Australia");
        list_Pais_Nacimiento.add("Aruba");
        list_Pais_Nacimiento.add("Azerbaiyán");
        list_Pais_Nacimiento.add("Bosnia-Herz");
        list_Pais_Nacimiento.add("Barbados");
        list_Pais_Nacimiento.add("Bangladesh");
        list_Pais_Nacimiento.add("Bélgica");
        list_Pais_Nacimiento.add("Burkina Faso");
        list_Pais_Nacimiento.add("Bulgaria");
        list_Pais_Nacimiento.add("Bahráin");
        list_Pais_Nacimiento.add("Burundi");
        list_Pais_Nacimiento.add("Benín");
        list_Pais_Nacimiento.add("Bermudas");
        list_Pais_Nacimiento.add("Brunéi");
        list_Pais_Nacimiento.add("Bolivia");
        list_Pais_Nacimiento.add("Brasil");
        list_Pais_Nacimiento.add("Bahamas");
        list_Pais_Nacimiento.add("Bután");
        list_Pais_Nacimiento.add("Islas Bouvet");
        list_Pais_Nacimiento.add("Botsuana");
        list_Pais_Nacimiento.add("Bielorrusia");
        list_Pais_Nacimiento.add("Belice");
        list_Pais_Nacimiento.add("Canadá");
        list_Pais_Nacimiento.add("Islas Cocos");
        list_Pais_Nacimiento.add("República Congo");
        list_Pais_Nacimiento.add("Rep.Centroafr.");
        list_Pais_Nacimiento.add("Suiza");
        list_Pais_Nacimiento.add("Costa de Marfil");
        list_Pais_Nacimiento.add("Islas Cook");
        list_Pais_Nacimiento.add("Chile");
        list_Pais_Nacimiento.add("Camerún");
        list_Pais_Nacimiento.add("China");
        list_Pais_Nacimiento.add("Colombia");
        list_Pais_Nacimiento.add("Costa Rica");
        list_Pais_Nacimiento.add("Serbia/Monten.");
        list_Pais_Nacimiento.add("Cuba");
        list_Pais_Nacimiento.add("Cabo Verde");
        list_Pais_Nacimiento.add("Isla Christmas");
        list_Pais_Nacimiento.add("Chipre");
        list_Pais_Nacimiento.add("República Checa");
        list_Pais_Nacimiento.add("Alemania");
        list_Pais_Nacimiento.add("Yibuti");
        list_Pais_Nacimiento.add("Dinamarca");
        list_Pais_Nacimiento.add("Dominica");
        list_Pais_Nacimiento.add("Rep.Dominicana");
        list_Pais_Nacimiento.add("Argelia");
        list_Pais_Nacimiento.add("Ecuador");
        list_Pais_Nacimiento.add("Estonia");
        list_Pais_Nacimiento.add("Egipto");
        list_Pais_Nacimiento.add("Sáhara occid");
        list_Pais_Nacimiento.add("Eritrea");
        list_Pais_Nacimiento.add("España");
        list_Pais_Nacimiento.add("Etiopía");
        list_Pais_Nacimiento.add("Finlandia");
        list_Pais_Nacimiento.add("Fiyi");
        list_Pais_Nacimiento.add("Islas Malvinas");
        list_Pais_Nacimiento.add("Micronesia");
        list_Pais_Nacimiento.add("Islas Feroe");
        list_Pais_Nacimiento.add("Francia");
        list_Pais_Nacimiento.add("Gabón");
        list_Pais_Nacimiento.add("Reino Unido");
        list_Pais_Nacimiento.add("Granada");
        list_Pais_Nacimiento.add("Georgia");
        list_Pais_Nacimiento.add("Guayana Franc.");
        list_Pais_Nacimiento.add("Ghana");
        list_Pais_Nacimiento.add("Gibraltar");
        list_Pais_Nacimiento.add("Groenlandia");
        list_Pais_Nacimiento.add("Gambia");
        list_Pais_Nacimiento.add("Guinea");
        list_Pais_Nacimiento.add("Guadalupe");
        list_Pais_Nacimiento.add("Guinea Ecuator.");
        list_Pais_Nacimiento.add("Grecia");
        list_Pais_Nacimiento.add("Isl.S.Sandwich");
        list_Pais_Nacimiento.add("Guatemala");
        list_Pais_Nacimiento.add("Guam");
        list_Pais_Nacimiento.add("Guinea-Bissau");
        list_Pais_Nacimiento.add("Guyana");
        list_Pais_Nacimiento.add("Hong Kong");
        list_Pais_Nacimiento.add("Is.Heard/Mcdon.");
        list_Pais_Nacimiento.add("Honduras");
        list_Pais_Nacimiento.add("Croacia");
        list_Pais_Nacimiento.add("Haití");
        list_Pais_Nacimiento.add("Hungría");
        list_Pais_Nacimiento.add("Indonesia");
        list_Pais_Nacimiento.add("Irlanda");
        list_Pais_Nacimiento.add("Israel");
        list_Pais_Nacimiento.add("India");
        list_Pais_Nacimiento.add("Terr.br.Oc.Ind.");
        list_Pais_Nacimiento.add("Iraq");
        list_Pais_Nacimiento.add("Irán");
        list_Pais_Nacimiento.add("Islandia");
        list_Pais_Nacimiento.add("Italia");
        list_Pais_Nacimiento.add("Jamaica");
        list_Pais_Nacimiento.add("Jordania");
        list_Pais_Nacimiento.add("Japón");
        list_Pais_Nacimiento.add("Kenia");
        list_Pais_Nacimiento.add("Kirguizistán");
        list_Pais_Nacimiento.add("Camboya");
        list_Pais_Nacimiento.add("Kiribati");
        list_Pais_Nacimiento.add("Comoras");
        list_Pais_Nacimiento.add("S.Cris.& Nieves");
        list_Pais_Nacimiento.add("Corea del Norte");
        list_Pais_Nacimiento.add("Corea del Sur");
        list_Pais_Nacimiento.add("Kuwait");
        list_Pais_Nacimiento.add("Islas Caimán");
        list_Pais_Nacimiento.add("Kazajistán");
        list_Pais_Nacimiento.add("Laos");
        list_Pais_Nacimiento.add("Líbano");
        list_Pais_Nacimiento.add("Santa Lucía");
        list_Pais_Nacimiento.add("Liechtenstein");
        list_Pais_Nacimiento.add("Sri Lanka");
        list_Pais_Nacimiento.add("Liberia");
        list_Pais_Nacimiento.add("Lesoto");
        list_Pais_Nacimiento.add("Lituania");
        list_Pais_Nacimiento.add("Luxemburgo");
        list_Pais_Nacimiento.add("Letonia");
        list_Pais_Nacimiento.add("Libia");
        list_Pais_Nacimiento.add("Marruecos");
        list_Pais_Nacimiento.add("Mónaco");
        list_Pais_Nacimiento.add("Moldavia");
        list_Pais_Nacimiento.add("Madagascar");
        list_Pais_Nacimiento.add("de Isl.Marshall");
        list_Pais_Nacimiento.add("Macedonia");
        list_Pais_Nacimiento.add("Malí");
        list_Pais_Nacimiento.add("Myanmar");
        list_Pais_Nacimiento.add("Mongolia");
        list_Pais_Nacimiento.add("Macao");
        list_Pais_Nacimiento.add("Isl.Marianas N.");
        list_Pais_Nacimiento.add("Martinica");
        list_Pais_Nacimiento.add("Mauritania");
        list_Pais_Nacimiento.add("Montserrat");
        list_Pais_Nacimiento.add("Malta");
        list_Pais_Nacimiento.add("Mauricio (Isl.)");
        list_Pais_Nacimiento.add("Maldivas");
        list_Pais_Nacimiento.add("Malaui");
        list_Pais_Nacimiento.add("Malasia");
        list_Pais_Nacimiento.add("Mozambique");
        list_Pais_Nacimiento.add("Namibia");
        list_Pais_Nacimiento.add("Nueva Caledonia");
        list_Pais_Nacimiento.add("Níger");
        list_Pais_Nacimiento.add("Islas Norfolk");
        list_Pais_Nacimiento.add("Nigeria");
        list_Pais_Nacimiento.add("Nicaragua");
        list_Pais_Nacimiento.add("Países Bajos");
        list_Pais_Nacimiento.add("Noruega");
        list_Pais_Nacimiento.add("Nepal");
        list_Pais_Nacimiento.add("Nauru");
        list_Pais_Nacimiento.add("Islas Niue");
        list_Pais_Nacimiento.add("Nueva Zelanda");
        list_Pais_Nacimiento.add("Omán");
        list_Pais_Nacimiento.add("Panamá");
        list_Pais_Nacimiento.add("Perú");
        list_Pais_Nacimiento.add("Polinesia fran.");
        list_Pais_Nacimiento.add("PapuaNvaGuinea");
        list_Pais_Nacimiento.add("Filipinas");
        list_Pais_Nacimiento.add("Pakistán");
        list_Pais_Nacimiento.add("Polonia");
        list_Pais_Nacimiento.add("S.Pedr.,Miquel.");
        list_Pais_Nacimiento.add("Islas Pitcairn");
        list_Pais_Nacimiento.add("Puerto Rico");
        list_Pais_Nacimiento.add("Palestina");
        list_Pais_Nacimiento.add("Portugal");
        list_Pais_Nacimiento.add("Palaos");
        list_Pais_Nacimiento.add("Paraguay");
        list_Pais_Nacimiento.add("Qatar");
        list_Pais_Nacimiento.add("Reunión");
        list_Pais_Nacimiento.add("Rumanía");
        list_Pais_Nacimiento.add("Federación Rusa");
        list_Pais_Nacimiento.add("Ruanda");
        list_Pais_Nacimiento.add("Arabia Saudí");
        list_Pais_Nacimiento.add("Islas Salomón");
        list_Pais_Nacimiento.add("Seychelles");
        list_Pais_Nacimiento.add("Sudán");
        list_Pais_Nacimiento.add("Suecia");
        list_Pais_Nacimiento.add("Singapur");
        list_Pais_Nacimiento.add("Santa Helena");
        list_Pais_Nacimiento.add("Eslovenia");
        list_Pais_Nacimiento.add("Svalbard");
        list_Pais_Nacimiento.add("Eslovaquia");
        list_Pais_Nacimiento.add("Sierra Leona");
        list_Pais_Nacimiento.add("San Marino");
        list_Pais_Nacimiento.add("Senegal");
        list_Pais_Nacimiento.add("Somalia");
        list_Pais_Nacimiento.add("Surinam");
        list_Pais_Nacimiento.add("S.Tomé,Príncipe");
        list_Pais_Nacimiento.add("El Salvador");
        list_Pais_Nacimiento.add("Siria");
        list_Pais_Nacimiento.add("Suazilandia");
        list_Pais_Nacimiento.add("Isl.Turcas y C.");
        list_Pais_Nacimiento.add("Chad");
        list_Pais_Nacimiento.add("French S.Territ");
        list_Pais_Nacimiento.add("Togo");
        list_Pais_Nacimiento.add("Tailandia");
        list_Pais_Nacimiento.add("Tayikistán");
        list_Pais_Nacimiento.add("Islas Tokelau");
        list_Pais_Nacimiento.add("Timor Oriental");
        list_Pais_Nacimiento.add("Turkmenistán");
        list_Pais_Nacimiento.add("Túnez");
        list_Pais_Nacimiento.add("Tonga");
        list_Pais_Nacimiento.add("Timor oriental");
        list_Pais_Nacimiento.add("Turquía");
        list_Pais_Nacimiento.add("TrinidadyTobago");
        list_Pais_Nacimiento.add("Tuvalu");
        list_Pais_Nacimiento.add("Taiwan");
        list_Pais_Nacimiento.add("Tanzania");
        list_Pais_Nacimiento.add("Ucrania");
        list_Pais_Nacimiento.add("Uganda");
        list_Pais_Nacimiento.add("IslMenAlejEEUU");
        list_Pais_Nacimiento.add("EE.UU.");
        list_Pais_Nacimiento.add("Uruguay");
        list_Pais_Nacimiento.add("Uzbekistán");
        list_Pais_Nacimiento.add("Ciudad Vaticano");
        list_Pais_Nacimiento.add("San Vicente");
        list_Pais_Nacimiento.add("Venezuela");
        list_Pais_Nacimiento.add("Isl.Vírgenes GB");
        list_Pais_Nacimiento.add("Is.Vírgenes USA");
        list_Pais_Nacimiento.add("Vietnam");
        list_Pais_Nacimiento.add("Vanuatu");
        list_Pais_Nacimiento.add("Wallis,Futuna");
        list_Pais_Nacimiento.add("Samoa Occident.");
        list_Pais_Nacimiento.add("Yemen");
        list_Pais_Nacimiento.add("Mayotte");
        list_Pais_Nacimiento.add("Sudáfrica");
        list_Pais_Nacimiento.add("Zambia");
        list_Pais_Nacimiento.add("Zimbabue");
        return list_Pais_Nacimiento;
    }

    public static List listTypeAdicional(){
        List<String> list_Tipo_Adicional = new ArrayList<String>();
        list_Tipo_Adicional.add("");
        list_Tipo_Adicional.add("Cónyuge");
        list_Tipo_Adicional.add("Arrendador de casa");
        list_Tipo_Adicional.add("Arrendador local (Negocio)");
        list_Tipo_Adicional.add("Referencia personal/familiar");
        list_Tipo_Adicional.add("Referencia comercial");
        list_Tipo_Adicional.add("Empleador");
        return list_Tipo_Adicional;
    }

    public static List listTypeAddress(){
        List<String> list_Tipo_Domicilio = new ArrayList<String>();
        list_Tipo_Domicilio.add("");
        list_Tipo_Domicilio.add("Casa");
        list_Tipo_Domicilio.add("Familiar");
        list_Tipo_Domicilio.add("Negocio");
        list_Tipo_Domicilio.add("Domicilio fiscal");
        list_Tipo_Domicilio.add("Empresa/Trabajo");
        list_Tipo_Domicilio.add("Dirección anterior");
        list_Tipo_Domicilio.add("Domicilio correspondencia");
        list_Tipo_Domicilio.add("Principal");
        list_Tipo_Domicilio.add("Otro");
        return list_Tipo_Domicilio;
    }

    public static List listTypePhone(){
        List<String> list_Tipo_Telefono = new ArrayList<String>();
        list_Tipo_Telefono.add("");
        list_Tipo_Telefono.add("Casa");
        list_Tipo_Telefono.add("Celular");
        list_Tipo_Telefono.add("Negocio");
        list_Tipo_Telefono.add("Recados");
        list_Tipo_Telefono.add("Trabajo");
        return list_Tipo_Telefono;
    }

    public static List listIconTypePhone(){
        List<Integer> list_icon_phone = new ArrayList<Integer>();
        list_icon_phone.add(R.mipmap.ic_home_blue);
        list_icon_phone.add(R.mipmap.ic_mobile_blue);
        list_icon_phone.add(R.mipmap.ic_office_blue);
        list_icon_phone.add(R.mipmap.ic_mobile_blue);
        list_icon_phone.add(R.mipmap.ic_work_blue);
        return list_icon_phone;
    }

    public static String url_Photo = "/Android/data/com.originacion.ao/Documents";
    //public static String url_Photo = "/Documents";
    public static ResponseUser responseUser = new ResponseUser();
    public static ResponseNameUser responseNameUser = new ResponseNameUser();
    public static ResponseName responseName = new ResponseName();
    public static ArrayList<ResponsePhones> responsePhonesArrayList = new ArrayList<ResponsePhones>();
    public static ArrayList<ResponseAddress> responseAddressArrayList = new ArrayList<ResponseAddress>();
    public static ResponseBasics responseBasics = new ResponseBasics();
    public static ArrayList<ResponseAdditional> responseAdditionalArrayList = new ArrayList<ResponseAdditional>();
    public static ResponseComplementary responseComplementary = new ResponseComplementary();
    public static ResponseEconomic responseEconomic = new ResponseEconomic();
    public static ArrayList<ResponseSigning> responseSigning = new ArrayList<ResponseSigning>();
    public static ArrayList<ResponseName> responseNameList = new ArrayList<ResponseName>();
    public static ResponseGroupDetail groupDetailResponse = new ResponseGroupDetail();
    public static ArrayList<ResponseMembers> responseMembersArrayList = new ArrayList<ResponseMembers>();
    public static ResponsePlaceMeeting responsePlaceMeeting = new ResponsePlaceMeeting();

    public static ArrayList<User> userArrayList;
    public static ArrayList<NameUser> nameUserArrayList = new ArrayList<NameUser>();
    public static List<Name> getAllNameList = new ArrayList<Name>();
    public static Name name = new Name();
    public static List<Address> getAddressesList = new ArrayList<Address>();
    public static ArrayList<Phone> phonesArrayList = new ArrayList<Phone>();
    public static ArrayList<Address> addressArrayList = new ArrayList<Address>();
    public static ArrayList<Additionals> additionalArrayList = new ArrayList<Additionals>();
    public static ArrayList<Complementary> complementaryArrayList;
    public static ArrayList<Phone> phone;
    public static ArrayList<Address> address;
    public static ArrayList<Additionals> additional;
    public static ArrayList<GroupDetail> groupDetailArrayList;
    public static ArrayList<Members> membersArrayList;
    public static ArrayList<EconomicActivityCatalog> economicActivityCatalogs;

    public static Secure secures;
    public static Basics basics;
    public static Economic economic;
    public static Complementary complementary;
    public static GroupDetail groupDetail;
    public static Beneficiary beneficiary;
    public static GroupData groupData;
    public static boolean setServiceData = false;
    public static int id_Name;
    public static int id_occupation;
    public static int id_place_meeting=0;
    public static int id_signing;
    public static int position_activity_economic;
    public static int activity_economic;
    public static int id_name_group_data;

    public static int position;
    public static boolean state_Boolean = false;
    public static boolean state_New_Addrees_Boolean = false;
    public static boolean isRefresh_news_applicants_boolean = false;
    public static boolean isRefresh_phones_list_boolean = false;
    public static boolean isRefresh_additionals_list_boolean = false;
    public static boolean isRefresh_addresses_list_boolean = false;
    public static boolean isFamily_boolean = false;
    public static boolean is_add_address = false;
    public static boolean isSaveName = false;
    public static boolean is_check_place_meeting = false;
    public static boolean is_loading = false;
    public static boolean photoOCRFront=false, photoOCRReverse=false;
    public static String CURP;
    public static int get_id_secure;
    //Type document
    public static int type_Document;
    public static File file;
    public static Bitmap bitmap;
    public static Bitmap bitmapFront, bitmapReverse;
    public static File
            url0,
            url1,
            url2,
            url3,
            url4,
            url5;

    public static String
            type_Document0 = "0",
            type_Document1 = "1",
            type_Document2 = "2",
            type_Document3 = "3",
            type_Document4 = "4",
            type_Document5 = "5";

    public static Bitmap rotateImage(Bitmap source, float angle) {
        int width = source.getWidth();
        int height = source.getHeight();
        float scaleWidth = ((float) 1920.0f) / width;
        float scaleHeight = ((float) 1080.0f) / height;
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        matrix.postScale(scaleWidth, scaleHeight);
        return Bitmap.createBitmap(source, 0, 0, width, height,
                matrix, true);
    }

}
