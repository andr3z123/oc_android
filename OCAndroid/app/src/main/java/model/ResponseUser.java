package model;

/**
 * Created by andresaleman on 2/15/17.
 */

public class ResponseUser
{
    private String estado;
    private String mensaje;
    private User user;

    public ResponseUser()
    {

    }

    public String getEstado() {
        return estado;
    }
    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMensaje() {
        return mensaje;
    }
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public User getUser() {
        return user;
    }
    public void setUser(User user) {
        this.user = user;
    }
}
