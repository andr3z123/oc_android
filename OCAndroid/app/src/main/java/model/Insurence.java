package model;

/**
 * Created by Jose Luis on 03/02/2017.
 */

public class Insurence {
    public int icon_Person_Status;
    public String tipo_Status;
    public String nombre_Grupo;
    public String fecha;

    public Insurence(){
        super();
    }

    public Insurence(int icon_Person_Status, String tipo_Status, String nombre_Grupo, String fecha){
        this.icon_Person_Status = icon_Person_Status;
        this.tipo_Status = tipo_Status;
        this.nombre_Grupo = nombre_Grupo;
        this.fecha = fecha;
    }

    public int getIcon_Person_Status() {
        return icon_Person_Status;
    }

    public void setIcon_Person_Status(int icon_Person_Status) {
        this.icon_Person_Status = icon_Person_Status;
    }

    public String getTipo_Status() {
        return tipo_Status;
    }

    public void setTipo_Status(String tipo_Status) {
        this.tipo_Status = tipo_Status;
    }

    public String getNombre_Grupo() {
        return nombre_Grupo;
    }

    public void setNombre_Grupo(String nombre_Grupo) {
        this.nombre_Grupo = nombre_Grupo;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
}
