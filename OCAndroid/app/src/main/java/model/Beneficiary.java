package model;

/**
 * Created by andresaleman on 2/17/17.
 */

public class Beneficiary
{
    private String idBENEFICIARY;
    private String name1;
    private String name2;
    private String lastName;
    private String mLastName;
    private String idRelationship;
    private String birthDate;
    private String idGender;
    private String idSigning;
    private String idSecure;

    public Beneficiary()
    {

    }

    public Beneficiary(String idBENEFICIARY, String name1, String name2, String lastName, String mLastName, String idRelationship, String birthDate, String idGender, String idSigning, String idSecure)
    {
        this.idBENEFICIARY = idBENEFICIARY;
        this.name1 = name1;
        this.name2 = name2;
        this.lastName = lastName;
        this.mLastName = mLastName;
        this.idRelationship = idRelationship;
        this.birthDate = birthDate;
        this.idGender = idGender;
        this.idSigning = idSigning;
        this.idSecure = idSecure;
    }

    public String getIdBENEFICIARY() {
        return idBENEFICIARY;
    }
    public void setIdBENEFICIARY(String idBENEFICIARY) {
        this.idBENEFICIARY = idBENEFICIARY;
    }

    public String getName1() {
        return name1;
    }
    public void setName1(String name1) {
        this.name1 = name1;
    }

    public String getName2() {
        return name2;
    }
    public void setName2(String name2) {
        this.name2 = name2;
    }

    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getmLastName() {
        return mLastName;
    }
    public void setmLastName(String mLastName) {
        this.mLastName = mLastName;
    }

    public String getIdRelationship() {
        return idRelationship;
    }
    public void setIdRelationship(String idRelationship) {
        this.idRelationship = idRelationship;
    }

    public String getBirthDate() {
        return birthDate;
    }
    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getIdGender() {
        return idGender;
    }
    public void setIdGender(String idGender) {
        this.idGender = idGender;
    }

    public String getIdSigning() {
        return idSigning;
    }
    public void setIdSigning(String idSigning) {
        this.idSigning = idSigning;
    }

    public String getIdSecure() {
        return idSecure;
    }
    public void setIdSecure(String idSecure) {
        this.idSecure = idSecure;
    }
}
