package model;

/**
 * Created by Jose Luis on 25/01/2017.
 */

public class Addresses {
    public String domicilio;
    public String tipo_Domicilio;

    public Addresses(){
        super();
    }

    public Addresses(String domicilio, String tipo_Domicilio){
        this.domicilio = domicilio;
        this.tipo_Domicilio = tipo_Domicilio;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public String getTipo_Domicilio() {
        return tipo_Domicilio;
    }

    public void setTipo_Domicilio(String tipo_Domicilio) {
        this.tipo_Domicilio = tipo_Domicilio;
    }
}
