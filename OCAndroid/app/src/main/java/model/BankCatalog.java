package model;

/**
 * Created by andresaleman on 2/15/17.
 */

public class BankCatalog
{
    private String idBANK_CATALOG;
    private String descriptionBankCatalog;

    public BankCatalog()
    {

    }

    public String getIdBANK_CATALOG() {
        return idBANK_CATALOG;
    }
    public void setIdBANK_CATALOG(String idBANK_CATALOG) {
        this.idBANK_CATALOG = idBANK_CATALOG;
    }

    public String getDescriptionBankCatalog() {
        return descriptionBankCatalog;
    }
    public void setDescriptionBankCatalog(String descriptionBankCatalog) {
        this.descriptionBankCatalog = descriptionBankCatalog;
    }
}
