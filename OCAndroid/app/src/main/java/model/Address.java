package model;

/**
 * Created by andresaleman on 2/14/17.
 */

public class Address
{
    private String idADDRESS;
    private String route;
    private String streetNumber1;
    private String streetNumber2;
    private String neighborhood;
    private String administrativeAreaLevel2;
    private String idFederalEntity;
    private String postalCode;
    private String idCountry;
    private String routeReference1;
    private String routeReference2;
    private String idName;
    private String complete;
    private String idAddressTypes;

    public Address()
    {

    }

    public Address(String id_address, String route, String streetNumber1, String streetNumber2, String neighborhood, String administrativeAreaLevel2, String id_federal_entity, String postal_code, String id_country, String route_reference1, String route_reference2, String id_name, String complete, String id_address_type)
    {
        this.idADDRESS = id_address;
        this.route = route;
        this.streetNumber1 = streetNumber1;
        this.streetNumber2 = streetNumber2;
        this.neighborhood = neighborhood;
        this.administrativeAreaLevel2 = administrativeAreaLevel2;
        this.idFederalEntity = id_federal_entity;
        this.postalCode = postal_code;
        this.idCountry = id_country;
        this.routeReference1 = route_reference1;
        this.routeReference2 = route_reference2;
        this.idName = id_name;
        this.complete = complete;
        this.idAddressTypes = id_address_type;
    }

    public String getIdADDRESS() {
        return idADDRESS;
    }

    public void setIdADDRESS(String idADDRESS) {
        this.idADDRESS = idADDRESS;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getStreetNumber1() {
        return streetNumber1;
    }

    public void setStreetNumber1(String streetNumber1) {
        this.streetNumber1 = streetNumber1;
    }

    public String getStreetNumber2() {
        return streetNumber2;
    }

    public void setStreetNumber2(String streetNumber2) {
        this.streetNumber2 = streetNumber2;
    }

    public String getNeighborhood() {
        return neighborhood;
    }

    public void setNeighborhood(String neighborhood) {
        this.neighborhood = neighborhood;
    }

    public String getAdministrativeAreaLevel2() {
        return administrativeAreaLevel2;
    }

    public void setAdministrativeAreaLevel2(String administrativeAreaLevel2) {
        this.administrativeAreaLevel2 = administrativeAreaLevel2;
    }

    public String getIdFederalEntity() {
        return idFederalEntity;
    }

    public void setIdFederalEntity(String idFederalEntity) {
        this.idFederalEntity = idFederalEntity;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getIdCountry() {
        return idCountry;
    }

    public void setIdCountry(String idCountry) {
        this.idCountry = idCountry;
    }

    public String getRouteReference1() {
        return routeReference1;
    }

    public void setRouteReference1(String routeReference1) {
        this.routeReference1 = routeReference1;
    }

    public String getRouteReference2() {
        return routeReference2;
    }

    public void setRouteReference2(String routeReference2) {
        this.routeReference2 = routeReference2;
    }

    public String getIdName() {
        return idName;
    }

    public void setIdName(String idName) {
        this.idName = idName;
    }

    public String getComplete() {
        return complete;
    }

    public void setComplete(String complete) {
        this.complete = complete;
    }

    public String getIdAddressTypes() {
        return idAddressTypes;
    }

    public void setIdAddressTypes(String idAddressTypes) {
        this.idAddressTypes = idAddressTypes;
    }
}
