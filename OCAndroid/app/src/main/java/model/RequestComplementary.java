package model;

/**
 * Created by Jose Luis on 04/04/2017.
 */

public class RequestComplementary {

    private String physical_person;
    private String curp;
    private String dependent_number;
    private String id_occupation;
    private String email;
    private String homo_clave;
    private String fiel;
    private String id_name;
    private String complete;

    public RequestComplementary(){

    }

    public String getPhysical_person() {
        return physical_person;
    }

    public void setPhysical_person(String physical_person) {
        this.physical_person = physical_person;
    }

    public String getCurp() {
        return curp;
    }

    public void setCurp(String curp) {
        this.curp = curp;
    }

    public String getDependent_number() {
        return dependent_number;
    }

    public void setDependent_number(String dependent_number) {
        this.dependent_number = dependent_number;
    }

    public String getId_occupation() {
        return id_occupation;
    }

    public void setId_occupation(String id_occupation) {
        this.id_occupation = id_occupation;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHomo_clave() {
        return homo_clave;
    }

    public void setHomo_clave(String homo_clave) {
        this.homo_clave = homo_clave;
    }

    public String getFiel() {
        return fiel;
    }

    public void setFiel(String fiel) {
        this.fiel = fiel;
    }

    public String getId_name() {
        return id_name;
    }

    public void setId_name(String id_name) {
        this.id_name = id_name;
    }

    public String getComplete() {
        return complete;
    }

    public void setComplete(String complete) {
        this.complete = complete;
    }
}
