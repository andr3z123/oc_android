package model;

/**
 * Created by andresaleman on 2/17/17.
 */

public class Secure
{
    private String idSECURE;
    private String startDate;
    private String limitTime;
    private String modality;
    private String idName;
    private String complete;

    public Secure()
    {

    }

    public Secure(String idSECURE, String startDate, String limitTime, String modality, String idName, String complete)
    {
        this.idSECURE = idSECURE;
        this.startDate = startDate;
        this.limitTime = limitTime;
        this.modality = modality;
        this.idName = idName;
        this.complete = complete;
    }

    public String getIdSECURE() {
        return idSECURE;
    }
    public void setIdSECURE(String idSECURE) {
        this.idSECURE = idSECURE;
    }

    public String getStartDate() {
        return startDate;
    }
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getLimitTime() {
        return limitTime;
    }
    public void setLimitTime(String limitTime) {
        this.limitTime = limitTime;
    }

    public String getModality() {
        return modality;
    }
    public void setModality(String modality) {
        this.modality = modality;
    }

    public String getIdName() {
        return idName;
    }
    public void setIdName(String idName) {
        this.idName = idName;
    }

    public String getComplete() {
        return complete;
    }
    public void setComplete(String complete) {
        this.complete = complete;
    }
}
