package model;

/**
 * Created by andresaleman on 2/17/17.
 */

public class ResponseGroupData
{
    private String estado;
    private String mensaje;
    private GroupData group_data;

    public ResponseGroupData()
    {

    }

    public String getEstado() {
        return estado;
    }
    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMensaje() {
        return mensaje;
    }
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public GroupData getGroup_data() {
        return group_data;
    }
    public void setGroup_data(GroupData group_data) {
        this.group_data = group_data;
    }
}
