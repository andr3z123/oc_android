package model;

/**
 * Created by andresaleman on 2/16/17.
 */

public class Additionals
{
    private String idADDITIONAL;
    private String name1;
    private String name2;
    private String lastName;
    private String mLastName;
    private String idOccupation;
    private String phone;
    private String idName;
    private String idAdditionalTypes;
    private String complete;

    public Additionals()
    {

    }

    public Additionals(String idADDITIONAL, String name1, String name2, String lastName, String mLastName, String idOccupation, String phone, String idName, String idAdditionalTypes, String complete)
    {
        this.idADDITIONAL = idADDITIONAL;
        this.name1 = name1;
        this.name2 = name2;
        this.lastName = lastName;
        this.mLastName = mLastName;
        this.idOccupation = idOccupation;
        this.phone = phone;
        this.idName = idName;
        this.idAdditionalTypes = idAdditionalTypes;
        this.complete = complete;
    }

    public String getIdADDITIONAL() {
        return idADDITIONAL;
    }
    public void setIdADDITIONAL(String idADDITIONAL) {
        this.idADDITIONAL = idADDITIONAL;
    }

    public String getName1() {
        return name1;
    }
    public void setName1(String name1) {
        this.name1 = name1;
    }

    public String getName2() {
        return name2;
    }
    public void setName2(String name2) {
        this.name2 = name2;
    }

    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getmLastName() {
        return mLastName;
    }
    public void setmLastName(String mLastName) {
        this.mLastName = mLastName;
    }

    public String getIdOccupation() {
        return idOccupation;
    }
    public void setIdOccupation(String idOccupation) {
        this.idOccupation = idOccupation;
    }

    public String getPhone() {
        return phone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getIdName() {
        return idName;
    }
    public void setIdName(String idName) {
        this.idName = idName;
    }

    public String getIdAdditionalTypes() {
        return idAdditionalTypes;
    }
    public void setIdAdditionalTypes(String idAdditionalTypes) {
        this.idAdditionalTypes = idAdditionalTypes;
    }

    public String getComplete() {
        return complete;
    }
    public void setComplete(String complete) {
        this.complete = complete;
    }
}
