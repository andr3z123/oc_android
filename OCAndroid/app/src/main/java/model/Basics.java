package model;

/**
 * Created by andresaleman on 2/15/17.
 */

public class Basics
{
    private String idBASICS;
    private String birthdate;
    private String voterKey;
    private String numRegVoter;
    private String idGender;
    private String idCountryBirth;
    private String idFederalEntity;
    private String idNationality;
    private String idCivilStatus;
    private String sons;
    private String idLivingPlace;
    private String idLevelEducation;
    private String idKindLocal;
    private String idName;
    private String complete;

    public Basics()
    {

    }

    public Basics(String idBASICS, String birthdate, String voterKey, String numRegVoter, String idGender, String idCountryBirth, String idFederalEntity, String idNationality, String idCivilStatus, String sons, String idLivingPlace, String idLevelEducation, String idKindLocal, String idName, String complete)
    {
        this.idBASICS = idBASICS;
        this.birthdate = birthdate;
        this.voterKey = voterKey;
        this.numRegVoter = numRegVoter;
        this.idGender = idGender;
        this.idCountryBirth = idCountryBirth;
        this.idFederalEntity = idFederalEntity;
        this.idNationality = idNationality;
        this.idCivilStatus = idCivilStatus;
        this.sons = sons;
        this.idLivingPlace = idLivingPlace;
        this.idLevelEducation = idLevelEducation;
        this.idKindLocal = idKindLocal;
        this.idName = idName;
        this.complete = complete;
    }

    public String getIdBASICS() {
        return idBASICS;
    }

    public void setIdBASICS(String idBASICS) {
        this.idBASICS = idBASICS;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getVoterKey() {
        return voterKey;
    }

    public void setVoterKey(String voterKey) {
        this.voterKey = voterKey;
    }

    public String getNumRegVoter() {
        return numRegVoter;
    }

    public void setNumRegVoter(String numRegVoter) {
        this.numRegVoter = numRegVoter;
    }

    public String getIdGender() {
        return idGender;
    }

    public void setIdGender(String idGender) {
        this.idGender = idGender;
    }

    public String getIdCountryBirth() {
        return idCountryBirth;
    }

    public void setIdCountryBirth(String idCountryBirth) {
        this.idCountryBirth = idCountryBirth;
    }

    public String getIdFederalEntity() {
        return idFederalEntity;
    }

    public void setIdFederalEntity(String idFederalEntity) {
        this.idFederalEntity = idFederalEntity;
    }

    public String getIdNationality() {
        return idNationality;
    }

    public void setIdNationality(String idNationality) {
        this.idNationality = idNationality;
    }

    public String getIdCivilStatus() {
        return idCivilStatus;
    }

    public void setIdCivilStatus(String idCivilStatus) {
        this.idCivilStatus = idCivilStatus;
    }

    public String getSons() {
        return sons;
    }

    public void setSons(String sons) {
        this.sons = sons;
    }

    public String getIdLivingPlace() {
        return idLivingPlace;
    }

    public void setIdLivingPlace(String idLivingPlace) {
        this.idLivingPlace = idLivingPlace;
    }

    public String getIdLevelEducation() {
        return idLevelEducation;
    }

    public void setIdLevelEducation(String idLevelEducation) {
        this.idLevelEducation = idLevelEducation;
    }

    public String getIdKindLocal() {
        return idKindLocal;
    }

    public void setIdKindLocal(String idKindLocal) {
        this.idKindLocal = idKindLocal;
    }

    public String getIdName() {
        return idName;
    }

    public void setIdName(String idName) {
        this.idName = idName;
    }

    public String getComplete() {
        return complete;
    }

    public void setComplete(String complete) {
        this.complete = complete;
    }
}
