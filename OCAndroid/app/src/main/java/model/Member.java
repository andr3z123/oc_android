package model;

/**
 * Created by Jose Luis on 24/04/2017.
 */

public class Member {

    public int id_name;
    public String nombre_Solicitante;
    public String fecha_Ingreso;
    public int icon_Status;

    public Member(){
        super();
    }

    public Member(int id_name, String nombre_Solicitante, String fecha_Ingreso, int icon_Status)
    {
        this.id_name = id_name;
        this.nombre_Solicitante = nombre_Solicitante;
        this.fecha_Ingreso = fecha_Ingreso;
        this.icon_Status = icon_Status;
    }

    public int getId_name() {
        return id_name;
    }

    public void setId_name(int id_name) {
        this.id_name = id_name;
    }

    public String getNombre_Solicitante() {
        return nombre_Solicitante;
    }

    public void setNombre_Solicitante(String nombre_Solicitante) {
        this.nombre_Solicitante = nombre_Solicitante;
    }

    public String getFecha_Ingreso() {
        return fecha_Ingreso;
    }

    public void setFecha_Ingreso(String fecha_Ingreso) {
        this.fecha_Ingreso = fecha_Ingreso;
    }

    public int getIcon_Status() {
        return icon_Status;
    }

    public void setIcon_Status(int icon_Status) {
        this.icon_Status = icon_Status;
    }
}
