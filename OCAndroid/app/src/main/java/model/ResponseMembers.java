package model;

/**
 * Created by andresaleman on 2/17/17.
 */

public class ResponseMembers
{
    private String estado;
    private String mensaje;
    private Members members;

    public ResponseMembers()
    {

    }

    public String getEstado() {
        return estado;
    }
    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMensaje() {
        return mensaje;
    }
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Members getMembers() {
        return members;
    }
    public void setMembers(Members members) {
        this.members = members;
    }
}
