package model;

/**
 * Created by andresaleman on 2/16/17.
 */

public class Document
{
    private String idDOCUMENTS;
    private String descriptionDocuments;
    private String idName;

    public Document()
    {

    }

    public Document(String idDOCUMENTS, String descriptionDocuments, String idName)
    {
        this.idDOCUMENTS = idDOCUMENTS;
        this.descriptionDocuments = descriptionDocuments;
        this.idName = idName;
    }

    public String getIdDOCUMENTS() {
        return idDOCUMENTS;
    }
    public void setIdDOCUMENTS(String idDOCUMENTS) {
        this.idDOCUMENTS = idDOCUMENTS;
    }

    public String getDescriptionDocuments() {
        return descriptionDocuments;
    }
    public void setDescriptionDocuments(String descriptionDocuments) {
        this.descriptionDocuments = descriptionDocuments;
    }

    public String getIdName() {
        return idName;
    }
    public void setIdName(String idName) {
        this.idName = idName;
    }
}
