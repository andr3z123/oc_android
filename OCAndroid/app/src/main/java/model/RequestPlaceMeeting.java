package model;

/**
 * Created by andresaleman on 6/7/17.
 */

public class RequestPlaceMeeting
{
    public RequestPlaceMeeting()
    {

    }

    private String route;
    private String street_number1;
    private String street_number2;
    private String neighborhood;
    private String administrative_area_level2;
    private String id_federal_entity;
    private String id_country;
    private String postal_code;
    private String route_reference1;
    private String route_reference2;
    private String location_reference;
    private String phone;
    private String id_phone_types;
    private String comments;
    private String id_group_detail;

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getStreet_number1() {
        return street_number1;
    }

    public void setStreet_number1(String street_number1) {
        this.street_number1 = street_number1;
    }

    public String getStreet_number2() {
        return street_number2;
    }

    public void setStreet_number2(String street_number2) {
        this.street_number2 = street_number2;
    }

    public String getNeighborhood() {
        return neighborhood;
    }

    public void setNeighborhood(String neighborhood) {
        this.neighborhood = neighborhood;
    }

    public String getAdministrative_area_level2() {
        return administrative_area_level2;
    }

    public void setAdministrative_area_level2(String administrative_area_level2) {
        this.administrative_area_level2 = administrative_area_level2;
    }

    public String getId_federal_entity() {
        return id_federal_entity;
    }

    public void setId_federal_entity(String id_federal_entity) {
        this.id_federal_entity = id_federal_entity;
    }

    public String getId_country() {
        return id_country;
    }

    public void setId_country(String id_country) {
        this.id_country = id_country;
    }

    public String getPostal_code() {
        return postal_code;
    }

    public void setPostal_code(String postal_code) {
        this.postal_code = postal_code;
    }

    public String getRoute_reference1() {
        return route_reference1;
    }

    public void setRoute_reference1(String route_reference1) {
        this.route_reference1 = route_reference1;
    }

    public String getRoute_reference2() {
        return route_reference2;
    }

    public void setRoute_reference2(String route_reference2) {
        this.route_reference2 = route_reference2;
    }

    public String getLocation_reference() {
        return location_reference;
    }

    public void setLocation_reference(String location_reference) {
        this.location_reference = location_reference;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getId_phone_types() {
        return id_phone_types;
    }

    public void setId_phone_types(String id_phone_types) {
        this.id_phone_types = id_phone_types;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getId_group_detail() {
        return id_group_detail;
    }

    public void setId_group_detail(String id_group_detail) {
        this.id_group_detail = id_group_detail;
    }
}
