package model;

/**
 * Created by andresaleman on 2/17/17.
 */

public class ResponseProspect
{
    private String estado;
    private String mensaje;
    private Prospect prospect;

    public ResponseProspect()
    {

    }

    public String getEstado() {
        return estado;
    }
    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMensaje() {
        return mensaje;
    }
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Prospect getProspect() {
        return prospect;
    }
    public void setProspect(Prospect prospect) {
        this.prospect = prospect;
    }
}
