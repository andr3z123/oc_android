package model;

/**
 * Created by andresaleman on 2/14/17.
 */

public class Name
{
    private String idNAME;
    private String name1;
    private String name2;
    private String lastName;
    private String mLastName;
    private String idUser;
    private String complete;
    private String dateAdmission;
    private String dayRet;
    private int idStatus;
    private int idProduct;
    private String idNameServer;


    public Name()
    {
    }

    public Name(String idNAME, String name1, String name2, String lastName, String mLastName, String idUser, String complete, String dateAdmission, String dayRet, int idStatus, int idProduct, String idNameServer)
    {
        this.idNAME = idNAME;
        this.name1 = name1;
        this.name2 = name2;
        this.lastName = lastName;
        this.mLastName = mLastName;
        this.idUser = idUser;
        this.complete = complete;
        this.dateAdmission = dateAdmission;
        this.dayRet = dayRet;
        this.idStatus = idStatus;
        this.idProduct = idProduct;
        this.idNameServer = idNameServer;
    }

    public String getIdName() {
        return idNAME;
    }

    public void setIdName(String idNAME) {
        this.idNAME = idNAME;
    }

    public String getName1() {
        return name1;
    }

    public void setName1(String name1) {
        this.name1 = name1;
    }

    public String getName2() {
        return name2;
    }

    public void setName2(String name2) {
        this.name2 = name2;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getmLastName() {
        return mLastName;
    }

    public void setmLastName(String mLastName) {
        this.mLastName = mLastName;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getComplete() {
        return complete;
    }

    public void setComplete(String complete) {
        this.complete = complete;
    }

    public String getDateAdmission() {
        return dateAdmission;
    }

    public void setDateAdmission(String dateAdmission) {
        this.dateAdmission = dateAdmission;
    }

    public String getDayRet() {
        return dayRet;
    }

    public void setDayRet(String dayRet) {
        this.dayRet = dayRet;
    }

    public int getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(int idStatus) {
        this.idStatus = idStatus;
    }

    public int getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(int idProduct) {
        this.idProduct = idProduct;
    }

    public String getIdNameServer() {
        return idNameServer;
    }

    public void setIdNameServer(String idNameServer) {
        this.idNameServer = idNameServer;
    }

}
