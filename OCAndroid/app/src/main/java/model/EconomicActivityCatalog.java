package model;

/**
 * Created by Jose Luis on 10/02/2017.
 */

public class EconomicActivityCatalog {

    public String descripción;
    public String id_Actividad_Economica;

    public EconomicActivityCatalog(){
        super();
    }

    public EconomicActivityCatalog(String descripción, String id_Actividad_Economica){
        this.descripción = descripción;
        this.id_Actividad_Economica = id_Actividad_Economica;
    }

    public String getDescripción() {
        return descripción;
    }

    public void setDescripción(String descripción) {
        this.descripción = descripción;
    }

    public String getId_Actividad_Economica() {
        return id_Actividad_Economica;
    }

    public void setId_Actividad_Economica(String id_Actividad_Economica) {
        this.id_Actividad_Economica = id_Actividad_Economica;
    }
}
