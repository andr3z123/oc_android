package model;

/**
 * Created by andresaleman on 2/17/17.
 */

public class ResponseAdditional
{
    private String estado;
    private String mensaje;
    private Additionals additional;

    public ResponseAdditional()
    {

    }

    public String getEstado() {
        return estado;
    }
    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMensaje() {
        return mensaje;
    }
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Additionals getAdditional() {
        return additional;
    }
    public void setAdditional(Additionals additional) {
        this.additional = additional;
    }
}
