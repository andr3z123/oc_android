package model;

/**
 * Created by andresaleman on 2/15/17.
 */

public class Economic
{
    private String idECONOMIC;
    private String idEconomicActivity;
    private String contributionLevel;
    private String anotherSourceIncome;
    private String currentBusinessTime;
    private String idLocalType;
    private String idMakesActivity;
    private String idTimeActivity;
    private String email;
    private String idName;
    private String complete;

    public Economic()
    {

    }

    public Economic(String idECONOMIC, String idEconomicActivity, String contributionLevel, String anotherSourceIncome, String currentBusinessTime, String idLocalType, String idMakesActivity, String idTimeActivity, String email, String idName, String complete)
    {
        this.idECONOMIC = idECONOMIC;
        this.idEconomicActivity = idEconomicActivity;
        this.contributionLevel = contributionLevel;
        this.anotherSourceIncome = anotherSourceIncome;
        this.currentBusinessTime = currentBusinessTime;
        this.idLocalType = idLocalType;
        this.idMakesActivity = idMakesActivity;
        this.idTimeActivity = idTimeActivity;
        this.email = email;
        this.idName = idName;
        this.complete = complete;
    }

    public String getIdECONOMIC() {
        return idECONOMIC;
    }
    public void setIdECONOMIC(String idECONOMIC) {
        this.idECONOMIC = idECONOMIC;
    }

    public String getIdEconomicActivity() {
        return idEconomicActivity;
    }
    public void setIdEconomicActivity(String idEconomicActivity) {
        this.idEconomicActivity = idEconomicActivity;
    }

    public String getContributionLevel() {
        return contributionLevel;
    }
    public void setContributionLevel(String contributionLevel) {
        this.contributionLevel = contributionLevel;
    }

    public String getAnotherSourceIncome() {
        return anotherSourceIncome;
    }
    public void setAnotherSourceIncome(String anotherSourceIncome) {
        this.anotherSourceIncome = anotherSourceIncome;
    }

    public String getCurrentBusinessTime() {
        return currentBusinessTime;
    }
    public void setCurrentBusinessTime(String currentBusinessTime) {
        this.currentBusinessTime = currentBusinessTime;
    }

    public String getIdLocalType() {
        return idLocalType;
    }
    public void setIdLocalType(String idLocalType) {
        this.idLocalType = idLocalType;
    }

    public String getIdMakesActivity() {
        return idMakesActivity;
    }

    public void setIdMakesActivity(String idMakesActivity) {
        this.idMakesActivity = idMakesActivity;
    }

    public String getIdTimeActivity() {
        return idTimeActivity;
    }

    public void setIdTimeActivity(String idTimeActivity) {
        this.idTimeActivity = idTimeActivity;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIdName() {
        return idName;
    }
    public void setIdName(String idName) {
        this.idName = idName;
    }

    public String getComplete() {
        return complete;
    }
    public void setComplete(String complete) {
        this.complete = complete;
    }
}
