package model;

/**
 * Created by andresaleman on 2/17/17.
 */

public class ResponseComplementary
{
    private String estado;
    private String mensaje;
    private Complementary complementary;

    public ResponseComplementary()
    {

    }

    public String getEstado() {
        return estado;
    }
    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMensaje() {
        return mensaje;
    }
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Complementary getComplementary() {
        return complementary;
    }
    public void setComplementary(Complementary complementary) {
        this.complementary = complementary;
    }
}
