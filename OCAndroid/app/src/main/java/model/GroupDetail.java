package model;

/**
 * Created by andresaleman on 2/16/17.
 */

public class GroupDetail
{
    private String idGROUPDETAIL;
    private String groupName;
    private String groupCycle;
    private String idSponsor;
    private String idChannelDispersion;
    private String idDispersionMedium;
    private String idUser;
    private String date;
    private String idGroupDetailServer;
    private String complete;

    public GroupDetail()
    {

    }

    public GroupDetail(String idGROUPDETAIL, String groupName, String groupCycle, String idSponsor, String idChannelDispersion, String idDispersionMedium, String idUser, String date, String  idGroupDetailServer, String complete)
    {
        this.idGROUPDETAIL = idGROUPDETAIL;
        this.groupName = groupName;
        this.groupCycle = groupCycle;
        this.idSponsor = idSponsor;
        this.idChannelDispersion = idChannelDispersion;
        this.idDispersionMedium = idDispersionMedium;
        this.idUser = idUser;
        this.date = date;
        this.idGroupDetailServer = idGroupDetailServer;
        this.complete = complete;
    }

    public String getIdGROUP_DETAIL() {
        return idGROUPDETAIL;
    }
    public void setIdGROUP_DETAIL(String idGROUP_DETAIL) {
        this.idGROUPDETAIL = idGROUP_DETAIL;
    }

    public String getGroupName() {
        return groupName;
    }
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupCycle() {
        return groupCycle;
    }
    public void setGroupCycle(String groupCycle) {
        this.groupCycle = groupCycle;
    }

    public String getIdSponsor() {
        return idSponsor;
    }
    public void setIdSponsor(String idSponsor) {
        this.idSponsor = idSponsor;
    }

    public String getIdChannelDispersion() {
        return idChannelDispersion;
    }
    public void setIdChannelDispersion(String idChannelDispersion) {
        this.idChannelDispersion = idChannelDispersion;
    }

    public String getIdDispersionMedium() {
        return idDispersionMedium;
    }
    public void setIdDispersionMedium(String idDispersionMedium) {
        this.idDispersionMedium = idDispersionMedium;
    }

    public String getIdUser() {
        return idUser;
    }
    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getIdGroupDetailServer() {
        return idGroupDetailServer;
    }

    public void setIdGroupDetailServer(String idGroupDetailServer) {
        this.idGroupDetailServer = idGroupDetailServer;
    }

    public String getComplete() {
        return complete;
    }
    public void setComplete(String complete) {
        this.complete = complete;
    }
}
