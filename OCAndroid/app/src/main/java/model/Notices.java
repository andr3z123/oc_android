package model;

/**
 * Created by Jose Luis on 09/02/2017.
 */

public class Notices {

    public String nombre_Aviso;
    public String contenido_Aviso;
    public int icon_Aviso;

    public Notices(){
        super();
    }

    public Notices(String nombre_Aviso, String contenido_Aviso, int icon_Aviso){
        this.nombre_Aviso = nombre_Aviso;
        this.contenido_Aviso = contenido_Aviso;
        this.icon_Aviso = icon_Aviso;
    }

    public String getNombre_Aviso() {
        return nombre_Aviso;
    }

    public void setNombre_Aviso(String nombre_Aviso) {
        this.nombre_Aviso = nombre_Aviso;
    }

    public String getContenido_Aviso() {
        return contenido_Aviso;
    }

    public void setContenido_Aviso(String contenido_Aviso) {
        this.contenido_Aviso = contenido_Aviso;
    }

    public int getIcon_Aviso() {
        return icon_Aviso;
    }

    public void setIcon_Aviso(int icon_Aviso) {
        this.icon_Aviso = icon_Aviso;
    }
}
