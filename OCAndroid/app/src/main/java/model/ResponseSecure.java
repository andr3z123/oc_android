package model;

/**
 * Created by andresaleman on 2/17/17.
 */

public class ResponseSecure
{
    private String estado;
    private String mensaje;
    private Secure secure;

    public ResponseSecure()
    {

    }

    public String getEstado() {
        return estado;
    }
    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMensaje() {
        return mensaje;
    }
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Secure getSecure() {
        return secure;
    }
    public void setSecure(Secure secure) {
        this.secure = secure;
    }
}
