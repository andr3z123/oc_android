package model;

/**
 * Created by Jose Luis on 31/01/2017.
 */

public class Applications {
    public int icon_Tipo_Solicitud;
    public String titulo_Tipo_Solicitud;
    public String titulo_Grupo;
    public String fecha_Solicitud;

    public Applications(){
        super();
    }
    public Applications(int icon_Tipo_Solicitud, String titulo_Tipo_Solicitud, String titulo_Grupo, String fecha_Solicitud){
        this.icon_Tipo_Solicitud = icon_Tipo_Solicitud;
        this.titulo_Tipo_Solicitud = titulo_Tipo_Solicitud;
        this.titulo_Grupo = titulo_Grupo;
        this.fecha_Solicitud = fecha_Solicitud;
    }

    public int getIcon_Tipo_Solicitud() {
        return icon_Tipo_Solicitud;
    }

    public void setIcon_Tipo_Solicitud(int icon_Tipo_Solicitud) {
        this.icon_Tipo_Solicitud = icon_Tipo_Solicitud;
    }

    public String getTitulo_Tipo_Solicitud() {
        return titulo_Tipo_Solicitud;
    }

    public void setTitulo_Tipo_Solicitud(String titulo_Tipo_Solicitud) {
        this.titulo_Tipo_Solicitud = titulo_Tipo_Solicitud;
    }

    public String getTitulo_Grupo() {
        return titulo_Grupo;
    }

    public void setTitulo_Grupo(String titulo_Grupo) {
        this.titulo_Grupo = titulo_Grupo;
    }

    public String getFecha_Solicitud() {
        return fecha_Solicitud;
    }

    public void setFecha_Solicitud(String fecha_Solicitud) {
        this.fecha_Solicitud = fecha_Solicitud;
    }
}
