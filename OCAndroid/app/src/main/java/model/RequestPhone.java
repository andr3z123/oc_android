package model;

/**
 * Created by andresaleman on 4/3/17.
 */

public class RequestPhone
{
    public RequestPhone()
    {

    }

    private String number;
    private String id_phone_type;
    private String id_name;
    private String complete;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getId_phone_type() {
        return id_phone_type;
    }

    public void setId_phone_type(String id_phone_type) {
        this.id_phone_type = id_phone_type;
    }

    public String getId_name() {
        return id_name;
    }

    public void setId_name(String id_name) {
        this.id_name = id_name;
    }

    public String getComplete() {
        return complete;
    }

    public void setComplete(String complete) {
        this.complete = complete;
    }
}
