package model;

/**
 * Created by andresaleman on 2/17/17.
 */

public class ResponsePlaceMeeting
{
    private String estado;
    private String mensaje;
    private PlaceMeeting place_meeting;

    public ResponsePlaceMeeting()
    {

    }

    public String getEstado() {
        return estado;
    }
    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMensaje() {
        return mensaje;
    }
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public PlaceMeeting getPlace_meeting() {
        return place_meeting;
    }
    public void setPlace_meeting(PlaceMeeting place_meeting) {
        this.place_meeting = place_meeting;
    }
}
