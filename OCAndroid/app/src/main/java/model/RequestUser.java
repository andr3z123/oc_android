package model;

/**
 * Created by andresaleman on 3/21/17.
 */

public class RequestUser
{
    public RequestUser()
    {

    }

    private String userName;
    private String password;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
