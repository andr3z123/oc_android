package model;

/**
 * Created by andresaleman on 2/17/17.
 */

public class ResponseSigning
{
    private String estado;
    private String mensaje;
    private Signing Signing;

    public ResponseSigning()
    {

    }

    public String getEstado() {
        return estado;
    }
    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMensaje() {
        return mensaje;
    }
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Signing getSigning() {
        return Signing;
    }
    public void setSigning(Signing signing) {
        Signing = signing;
    }
}
