package model;

/**
 * Created by andresaleman on 2/17/17.
 */

public class ResponsePhones
{
    private String estado;
    private String mensaje;
    private Phone phones;

    public ResponsePhones()
    {

    }

    public String getEstado() {
        return estado;
    }
    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMensaje() {
        return mensaje;
    }
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Phone getPhones() {
        return phones;
    }
    public void setPhones(Phone phones) {
        this.phones = phones;
    }
}
