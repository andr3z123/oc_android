package model;

/**
 * Created by Jose Luis on 01/02/2017.
 */

public class ListMembers
{
    public int id_name;
    public String nombre_Integrante_Grupo;
    public String rol;
    public String lista_Control;
    public String nivel_Riesgo;
    public int icon_Persona;
    public int icon_Status;

    public ListMembers(){
        super();
    }

    public ListMembers(int id_name, String nombre_Integrante_Grupo, String rol, String lista_Control, String nivel_Riesgo, int icon_Persona, int icon_Status){
        this.id_name = id_name;
        this.nombre_Integrante_Grupo = nombre_Integrante_Grupo;
        this.rol = rol;
        this.lista_Control = lista_Control;
        this.nivel_Riesgo = nivel_Riesgo;
        this.icon_Persona = icon_Persona;
        this.icon_Status = icon_Status;
    }

    public int getId_name() {
        return id_name;
    }

    public void setId_name(int id_name) {
        this.id_name = id_name;
    }

    public String getNombre_Integrante_Grupo() {
        return nombre_Integrante_Grupo;
    }

    public void setNombre_Integrante_Grupo(String nombre_Integrante_Grupo) {
        this.nombre_Integrante_Grupo = nombre_Integrante_Grupo;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public String getLista_Control() {
        return lista_Control;
    }

    public void setLista_Control(String lista_Control) {
        this.lista_Control = lista_Control;
    }

    public String getNivel_Riesgo() {
        return nivel_Riesgo;
    }

    public void setNivel_Riesgo(String nivel_Riesgo) {
        this.nivel_Riesgo = nivel_Riesgo;
    }

    public int getIcon_Persona() {
        return icon_Persona;
    }

    public void setIcon_Persona(int icon_Persona) {
        this.icon_Persona = icon_Persona;
    }

    public int getIcon_Status() {
        return icon_Status;
    }

    public void setIcon_Status(int icon_Status) {
        this.icon_Status = icon_Status;
    }
}
