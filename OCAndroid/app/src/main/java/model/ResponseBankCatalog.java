package model;

import java.util.List;

/**
 * Created by andresaleman on 2/15/17.
 */

public class ResponseBankCatalog
{
    public int estado;
    public List<BankCatalog> bank_catalog;

    public ResponseBankCatalog()
    {

    }

    public int getEstado() {
        return estado;
    }
    public void setEstado(int estado) {
        this.estado = estado;
    }

    public List<BankCatalog> getBank_catalog() {
        return bank_catalog;
    }
    public void setBank_catalog(List<BankCatalog> bank_catalog) {
        this.bank_catalog = bank_catalog;
    }
}
