package model;

/**
 * Created by andresaleman on 2/17/17.
 */

public class ResponseEconomic
{
    private String estado;
    private String mensaje;
    private Economic economic;

    public ResponseEconomic()
    {

    }

    public String getEstado() {
        return estado;
    }
    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMensaje() {
        return mensaje;
    }
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Economic getEconomic() {
        return economic;
    }
    public void setEconomic(Economic economic) {
        this.economic = economic;
    }
}
