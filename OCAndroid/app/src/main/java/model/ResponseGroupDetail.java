package model;

/**
 * Created by andresaleman on 2/17/17.
 */

public class ResponseGroupDetail
{
    private String estado;
    private String mensaje;
    private GroupDetail group_detail;

    public ResponseGroupDetail()
    {

    }

    public String getEstado() {
        return estado;
    }
    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMensaje() {
        return mensaje;
    }
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public GroupDetail getGroup_detail() {
        return group_detail;
    }
    public void setGroup_detail(GroupDetail group_detail) {
        this.group_detail = group_detail;
    }
}
