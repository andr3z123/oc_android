package model;

/**
 * Created by andresaleman on 2/16/17.
 */

public class Members
{
    private String idMEMBERS;
    private String idGroupDetail;
    private String idName;
    private String idNameServer;
    private String complete;

    public Members()
    {

    }

    public Members(String idMEMBERS, String idGroupDetail, String idName, String idNameServer, String complete)
    {
        this.idMEMBERS = idMEMBERS;
        this.idGroupDetail = idGroupDetail;
        this.idName = idName;
        this.idNameServer = idNameServer;
        this.complete = complete;
    }

    public String getIdMEMBERS() {
        return idMEMBERS;
    }
    public void setIdMEMBERS(String idMEMBERS) {
        this.idMEMBERS = idMEMBERS;
    }

    public String getIdGroupDetail() {
        return idGroupDetail;
    }
    public void setIdGroupDetail(String idGroupDetail) {
        this.idGroupDetail = idGroupDetail;
    }

    public String getIdName() {
        return idName;
    }
    public void setIdName(String idName) {
        this.idName = idName;
    }

    public String getIdNameServer() {
        return idNameServer;
    }

    public void setIdNameServer(String idNameServer) {
        this.idNameServer = idNameServer;
    }

    public String getComplete() {
        return complete;
    }
    public void setComplete(String complete) {
        this.complete = complete;
    }
}
