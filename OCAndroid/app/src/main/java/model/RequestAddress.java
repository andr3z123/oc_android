package model;

/**
 * Created by andresaleman on 4/3/17.
 */

public class RequestAddress
{
    public RequestAddress()
    {
        
    }
    
    private String route;
    private String street_number1;
    private String street_number2;
    private String neighborhood;
    private String administrative_area_level2;
    private String id_federal_entity;
    private String postal_code;
    private String id_country;
    private String route_reference1;
    private String route_reference2;
    private String id_name;
    private String complete;
    private String id_address_types;

    public String getId_address_type() {
        return id_address_types;
    }

    public void setId_address_type(String id_address_type) {
        this.id_address_types = id_address_type;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getStreet_number1() {
        return street_number1;
    }

    public void setStreet_number1(String street_number1) {
        this.street_number1 = street_number1;
    }

    public String getStreet_number2() {
        return street_number2;
    }

    public void setStreet_number2(String street_number2) {
        this.street_number2 = street_number2;
    }

    public String getNeighborhood() {
        return neighborhood;
    }

    public void setNeighborhood(String neighborhood) {
        this.neighborhood = neighborhood;
    }

    public String getAdministrative_area_level2() {
        return administrative_area_level2;
    }

    public void setAdministrative_area_level2(String administrative_area_level2) {
        this.administrative_area_level2 = administrative_area_level2;
    }

    public String getId_federal_entity() {
        return id_federal_entity;
    }

    public void setId_federal_entity(String id_federal_entity) {
        this.id_federal_entity = id_federal_entity;
    }

    public String getPostal_code() {
        return postal_code;
    }

    public void setPostal_code(String postal_code) {
        this.postal_code = postal_code;
    }

    public String getId_country() {
        return id_country;
    }

    public void setId_country(String id_country) {
        this.id_country = id_country;
    }

    public String getRoute_reference1() {
        return route_reference1;
    }

    public void setRoute_reference1(String route_reference1) {
        this.route_reference1 = route_reference1;
    }

    public String getRoute_reference2() {
        return route_reference2;
    }

    public void setRoute_reference2(String route_reference2) {
        this.route_reference2 = route_reference2;
    }

    public String getId_name() {
        return id_name;
    }

    public void setId_name(String id_name) {
        this.id_name = id_name;
    }

    public String getComplete() {
        return complete;
    }

    public void setComplete(String complete) {
        this.complete = complete;
    }
}
