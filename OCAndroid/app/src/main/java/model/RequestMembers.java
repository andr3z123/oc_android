package model;

/**
 * Created by andresaleman on 6/7/17.
 */

public class RequestMembers
{
    public RequestMembers()
    {}

    private String id_group_detail;
    private String id_name;
    private String complete;

    public String getId_group_detail() {
        return id_group_detail;
    }

    public void setId_group_detail(String id_group_detail) {
        this.id_group_detail = id_group_detail;
    }

    public String getId_name() {
        return id_name;
    }

    public void setId_name(String id_name) {
        this.id_name = id_name;
    }

    public String getComplete() {
        return complete;
    }

    public void setComplete(String complete) {
        this.complete = complete;
    }
}
