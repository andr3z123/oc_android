package model;

/**
 * Created by andresaleman on 2/17/17.
 */

public class ResponseBasics
{
    private String estado;
    private String mensaje;
    private Basics basics;

    public ResponseBasics()
    {

    }

    public String getEstado() {
        return estado;
    }
    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMensaje() {
        return mensaje;
    }
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Basics getBasics() {
        return basics;
    }
    public void setBasics(Basics basics) {
        this.basics = basics;
    }
}
