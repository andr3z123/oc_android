package model;

/**
 * Created by Jose Luis on 07/04/2017.
 */

public class RequestAdditional {

    private String name1;
    private String name2;
    private String last_name;
    private String m_last_name;
    private String id_occupation;
    private String phone;
    private String id_name;
    private String id_additional_types;
    private String complete;

    public RequestAdditional(){

    }

    public String getName1() {
        return name1;
    }

    public void setName1(String name1) {
        this.name1 = name1;
    }

    public String getName2() {
        return name2;
    }

    public void setName2(String name2) {
        this.name2 = name2;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getM_last_name() {
        return m_last_name;
    }

    public void setM_last_name(String m_last_name) {
        this.m_last_name = m_last_name;
    }

    public String getId_occupation() {
        return id_occupation;
    }

    public void setId_occupation(String id_occupation) {
        this.id_occupation = id_occupation;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getId_name() {
        return id_name;
    }

    public void setId_name(String id_name) {
        this.id_name = id_name;
    }

    public String getId_additional_types() {
        return id_additional_types;
    }

    public void setId_additional_types(String id_additional_types) {
        this.id_additional_types = id_additional_types;
    }

    public String getComplete() {
        return complete;
    }

    public void setComplete(String complete) {
        this.complete = complete;
    }
}
