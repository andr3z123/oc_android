package model;

/**
 * Created by andresaleman on 6/7/17.
 */

public class RequestGroupData
{
    public RequestGroupData()
    {

    }

    private String minium_amount_savings;
    private String amount_fine_delay;
    private String amount_missing_fine;
    private String meeting_schedule;
    private String meeting_date;
    private String disbursement_date;
    private String pay_date1;
    private String id_product;
    private String id_frequencies;
    private String id_deadlines;
    private String id_place_meeting;
    private String id_name;
    private String id_group_detail;
    private String complete;

    public String getMinium_amount_savings() {
        return minium_amount_savings;
    }

    public void setMinium_amount_savings(String minium_amount_savings) {
        this.minium_amount_savings = minium_amount_savings;
    }

    public String getAmount_fine_delay() {
        return amount_fine_delay;
    }

    public void setAmount_fine_delay(String amount_fine_delay) {
        this.amount_fine_delay = amount_fine_delay;
    }

    public String getAmount_missing_fine() {
        return amount_missing_fine;
    }

    public void setAmount_missing_fine(String amount_missing_fine) {
        this.amount_missing_fine = amount_missing_fine;
    }

    public String getMeeting_schedule() {
        return meeting_schedule;
    }

    public void setMeeting_schedule(String meeting_schedule) {
        this.meeting_schedule = meeting_schedule;
    }

    public String getMeeting_date() {
        return meeting_date;
    }

    public void setMeeting_date(String meeting_date) {
        this.meeting_date = meeting_date;
    }

    public String getDisbursement_date() {
        return disbursement_date;
    }

    public void setDisbursement_date(String disbursement_date) {
        this.disbursement_date = disbursement_date;
    }

    public String getPay_date1() {
        return pay_date1;
    }

    public void setPay_date1(String pay_date1) {
        this.pay_date1 = pay_date1;
    }

    public String getId_product() {
        return id_product;
    }

    public void setId_product(String id_product) {
        this.id_product = id_product;
    }

    public String getId_frequencies() {
        return id_frequencies;
    }

    public void setId_frequencies(String id_frequencies) {
        this.id_frequencies = id_frequencies;
    }

    public String getId_deadlines() {
        return id_deadlines;
    }

    public void setId_deadlines(String id_deadlines) {
        this.id_deadlines = id_deadlines;
    }

    public String getId_place_meeting() {
        return id_place_meeting;
    }

    public void setId_place_meeting(String id_place_meeting) {
        this.id_place_meeting = id_place_meeting;
    }

    public String getId_name() {
        return id_name;
    }

    public void setId_name(String id_name) {
        this.id_name = id_name;
    }

    public String getId_group_detail() {
        return id_group_detail;
    }

    public void setId_group_detail(String id_group_detail) {
        this.id_group_detail = id_group_detail;
    }

    public String getComplete() {
        return complete;
    }

    public void setComplete(String complete) {
        this.complete = complete;
    }


}
