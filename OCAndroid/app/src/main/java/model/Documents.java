package model;

import android.graphics.Bitmap;

import java.io.File;

/**
 * Created by Jose Luis on 31/01/2017.
 */

public class Documents {
    public String titulo_Documento;
    public File imagen_Documento;
    public Documents(){
        super();
    }
    public Documents(String titulo_Documento, File imagen_Documento){
        this.titulo_Documento = titulo_Documento;
        this.imagen_Documento = imagen_Documento;
    }

    public String getTitulo_Documento() {
        return titulo_Documento;
    }

    public void setTitulo_Documento(String titulo_Documento) {
        this.titulo_Documento = titulo_Documento;
    }

    public File getImagen_Documento() {
        return imagen_Documento;
    }

    public void setImagen_Documento(File imagen_Documento) {
        this.imagen_Documento = imagen_Documento;
    }
}
