package model;

/**
 * Created by andresaleman on 6/7/17.
 */

public class RequestGroupDetail 
{
    public RequestGroupDetail()
    {
        
    }

    private String group_name;
    private String group_cycle;
    private String id_sponsor;
    private String id_channel_dispersion;
    private String id_dispersion_medium;
    private String id_user;
    private String complete;

    public String getGroup_name() {
        return group_name;
    }

    public void setGroup_name(String group_name) {
        this.group_name = group_name;
    }

    public String getGroup_cycle() {
        return group_cycle;
    }

    public void setGroup_cycle(String group_cycle) {
        this.group_cycle = group_cycle;
    }

    public String getId_sponsor() {
        return id_sponsor;
    }

    public void setId_sponsor(String id_sponsor) {
        this.id_sponsor = id_sponsor;
    }

    public String getId_channel_dispersion() {
        return id_channel_dispersion;
    }

    public void setId_channel_dispersion(String id_channel_dispersion) {
        this.id_channel_dispersion = id_channel_dispersion;
    }

    public String getId_dispersion_medium() {
        return id_dispersion_medium;
    }

    public void setId_dispersion_medium(String id_dispersion_medium) {
        this.id_dispersion_medium = id_dispersion_medium;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public String getComplete() {
        return complete;
    }

    public void setComplete(String complete) {
        this.complete = complete;
    }
}
