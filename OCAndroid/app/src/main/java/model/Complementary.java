package model;

/**
 * Created by andresaleman on 2/15/17.
 */

public class Complementary
{
    private String idCOMPLEMENTARY;
    private String physicalPerson;
    private String curp;
    private String dependentNumber;
    private String idOccupation;
    private String email;
    private String homoclave;
    private String fiel;
    private String idName;
    private String complete;

    public Complementary()
    {

    }

    public Complementary(String idCOMPLEMENTARY, String physicalPerson, String curp, String dependentNumber, String idOccupation, String email, String homoclave, String fiel, String idName, String complete)
    {
        this.idCOMPLEMENTARY = idCOMPLEMENTARY;
        this.physicalPerson = physicalPerson;
        this.curp = curp;
        this.dependentNumber = dependentNumber;
        this.idOccupation = idOccupation;
        this.email = email;
        this.homoclave = homoclave;
        this.fiel = fiel;
        this.idName = idName;
        this.complete = complete;
    }

    public String getIdCOMPLEMENTARY() {
        return idCOMPLEMENTARY;
    }
    public void setIdCOMPLEMENTARY(String idCOMPLEMENTARY) {
        this.idCOMPLEMENTARY = idCOMPLEMENTARY;
    }

    public String getPhysicalPerson() {
        return physicalPerson;
    }
    public void setPhysicalPerson(String physicalPerson) {
        this.physicalPerson = physicalPerson;
    }

    public String getCurp() {
        return curp;
    }
    public void setCurp(String curp) {
        this.curp = curp;
    }

    public String getDependentNumber() {
        return dependentNumber;
    }
    public void setDependentNumber(String dependentNumber) {
        this.dependentNumber = dependentNumber;
    }

    public String getIdOccupation() {
        return idOccupation;
    }
    public void setIdOccupation(String idOccupation) {
        this.idOccupation = idOccupation;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public String getHomoclave() {
        return homoclave;
    }
    public void setHomoclave(String homoclave) {
        this.homoclave = homoclave;
    }

    public String getFiel() {
        return fiel;
    }
    public void setFiel(String fiel) {
        this.fiel = fiel;
    }

    public String getIdName() {
        return idName;
    }
    public void setIdName(String idName) {
        this.idName = idName;
    }

    public String getComplete() {
        return complete;
    }
    public void setComplete(String complete) {
        this.complete = complete;
    }

}
