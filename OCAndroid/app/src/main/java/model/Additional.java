package model;

/**
 * Created by Jose Luis on 25/01/2017.
 */

public class Additional {

    public int icon_Contact;
    public String nombre;
    public String tipo;

    public Additional(){
        super();
    }

    public Additional(int icon_Contact, String nombre, String tipo){
        this.icon_Contact = icon_Contact;
        this.nombre = nombre;
        this.tipo = tipo;
    }

    public int getIcon_Contact() {
        return icon_Contact;
    }

    public void setIcon_Contact(int icon_Contact) {
        this.icon_Contact = icon_Contact;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
}
