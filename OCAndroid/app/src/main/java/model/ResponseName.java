package model;

import java.util.List;

/**
 * Created by andresaleman on 2/17/17.
 */

public class ResponseName
{
    private String estado;
    private String mensaje;
    private Name name;

    public ResponseName()
    {

    }

    public String getEstado() {
        return estado;
    }
    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMensaje() {
        return mensaje;
    }
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Name getUser() {
        return name;
    }
    public void setUser(Name name) {
        this.name = name;
    }
}
