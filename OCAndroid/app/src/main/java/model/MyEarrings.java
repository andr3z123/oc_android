package model;

/**
 * Created by Jose Luis on 09/02/2017.
 */

public class MyEarrings {

    public String id_Oportunidad;
    public String nombre_Pendiente;
    public String fecha;
    public int icon_Editar;

    public MyEarrings(){
        super();
    }

    public MyEarrings(String id_Oportunidad, String nombre_Pendiente, String fecha, int icon_Editar){
        this.id_Oportunidad = id_Oportunidad;
        this.nombre_Pendiente = nombre_Pendiente;
        this.fecha = fecha;
        this.icon_Editar = icon_Editar;
    }

    public String getId_Oportunidad() {
        return id_Oportunidad;
    }

    public void setId_Oportunidad(String id_Oportunidad) {
        this.id_Oportunidad = id_Oportunidad;
    }

    public String getNombre_Pendiente() {
        return nombre_Pendiente;
    }

    public void setNombre_Pendiente(String nombre_Pendiente) {
        this.nombre_Pendiente = nombre_Pendiente;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getIcon_Editar() {
        return icon_Editar;
    }

    public void setIcon_Editar(int icon_Editar) {
        this.icon_Editar = icon_Editar;
    }
}
