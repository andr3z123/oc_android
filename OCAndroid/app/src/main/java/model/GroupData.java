package model;

/**
 * Created by andresaleman on 2/16/17.
 */

public class GroupData
{
    private String idGROUPDATA;
    private String miniumAmountSavings;
    private String amountFineDelay;
    private String amountMissingFine;
    private String meetingSchedule;
    private String meetingDate;
    private String disbursementDate;
    private String payDate1;
    private String idProduct;
    private String idFrequencies;
    private String idDeadlines;
    private String idPlaceMeeting;
    private String idGroupDetail;
    private String idNameGroupData;
    private String complete;

    public GroupData()
    {

    }

    public GroupData(String idGROUPDATA, String miniumAmountSavings, String amountFineDelay, String amountMissingFine, String meetingSchedule, String meetingDate, String disbursementDate, String payDate1, String idProduct, String idFrequencies, String idDeadlines, String idPlaceMeeting, String idGroupDetail, String idNameGroupData, String complete)
    {
        this.idGROUPDATA = idGROUPDATA;
        this.miniumAmountSavings = miniumAmountSavings;
        this.amountFineDelay = amountFineDelay;
        this.amountMissingFine = amountMissingFine;
        this.meetingSchedule = meetingSchedule;
        this.meetingDate = meetingDate;
        this.disbursementDate = disbursementDate;
        this.payDate1 = payDate1;
        this.idProduct = idProduct;
        this.idFrequencies = idFrequencies;
        this.idDeadlines = idDeadlines;
        this.idPlaceMeeting = idPlaceMeeting;
        this.idGroupDetail = idGroupDetail;
        this.idNameGroupData = idNameGroupData;
        this.complete = complete;
    }

    public String getIdGROUPDATA() {
        return idGROUPDATA;
    }
    public void setIdGROUPDATA(String idGROUPDATA) {
        this.idGROUPDATA = idGROUPDATA;
    }

    public String getMiniumAmountSavings() {
        return miniumAmountSavings;
    }
    public void setMiniumAmountSavings(String miniumAmountSavings) {
        this.miniumAmountSavings = miniumAmountSavings;
    }

    public String getAmountFineDelay() {
        return amountFineDelay;
    }
    public void setAmountFineDelay(String amountFineDelay) {
        this.amountFineDelay = amountFineDelay;
    }

    public String getAmountMissingFine() {
        return amountMissingFine;
    }
    public void setAmountMissingFine(String amountMissingFine) {
        this.amountMissingFine = amountMissingFine;
    }

    public String getMeetingSchedule() {
        return meetingSchedule;
    }
    public void setMeetingSchedule(String meetingSchedule) {
        this.meetingSchedule = meetingSchedule;
    }

    public String getMeetingDate() {
        return meetingDate;
    }
    public void setMeetingDate(String meetingDate) {
        this.meetingDate = meetingDate;
    }

    public String getDisbursementDate() {
        return disbursementDate;
    }
    public void setDisbursementDate(String disbursementDate) {
        this.disbursementDate = disbursementDate;
    }

    public String getPayDate1() {
        return payDate1;
    }
    public void setPayDate1(String payDate1) {
        this.payDate1 = payDate1;
    }

    public String getIdProduct() {
        return idProduct;
    }
    public void setIdProduct(String idProduct) {
        this.idProduct = idProduct;
    }

    public String getIdFrequencies() {
        return idFrequencies;
    }
    public void setIdFrequencies(String idFrequencies) {
        this.idFrequencies = idFrequencies;
    }

    public String getIdDeadlines() {
        return idDeadlines;
    }
    public void setIdDeadlines(String idDeadlines) {
        this.idDeadlines = idDeadlines;
    }

    public String getIdPlaceMeeting() {
        return idPlaceMeeting;
    }
    public void setIdPlaceMeeting(String idPlaceMeeting) {
        this.idPlaceMeeting = idPlaceMeeting;
    }

    public String getIdGroupDetail() {
        return idGroupDetail;
    }
    public void setIdGroupDetail(String idGroupDetail) {
        this.idGroupDetail = idGroupDetail;
    }

    public String getIdNameGroupData() {
        return idNameGroupData;
    }

    public void setIdNameGroupData(String idNameGroupData) {
        this.idNameGroupData = idNameGroupData;
    }

    public String getComplete() {
        return complete;
    }
    public void setComplete(String complete) {
        this.complete = complete;
    }
}
