package model;

/**
 * Created by andresaleman on 2/17/17.
 */

public class ResponseAddress
{
    private String estado;
    private String mensaje;
    private Address address;

    public ResponseAddress()
    {

    }

    public String getEstado() {
        return estado;
    }
    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMensaje() {
        return mensaje;
    }
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Address getAddress() {
        return address;
    }
    public void setAddress(Address address) {
        this.address = address;
    }
}
