package model;

/**
 * Created by andresaleman on 2/14/17.
 */

public class User
{
    private String idUser;
    private String userName;
    private String password;

    public User()
    {

    }

    public User(String idUser, String userName)
    {
        this.idUser = idUser;
        this.userName = userName;
    }

    public String getIdUSERS() {
        return idUser;
    }
    public void setIdUSERS(String idUser) {
        this.idUser = idUser;
    }

    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
}
