package model;

/**
 * Created by andresaleman on 2/14/17.
 */

public class Signing
{
    private String idSIGNING;
    private String descriptionSigning;

    public Signing()
    {

    }

    public Signing(String idSIGNING, String descriptionSigning)
    {
        this.idSIGNING = idSIGNING;
        this.descriptionSigning = descriptionSigning;
    }

    public String getIdSIGNING() {
        return idSIGNING;
    }

    public void setIdSIGNING(String idSIGNING) {
        this.idSIGNING = idSIGNING;
    }

    public String getDescriptionSigning() {
        return descriptionSigning;
    }

    public void setDescriptionSigning(String descriptionSigning) {
        this.descriptionSigning = descriptionSigning;
    }
}
