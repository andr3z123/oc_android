package model;

/**
 * Created by Jose Luis on 04/04/2017.
 */

public class RequestBasics {

    private String birth_date;
    private String voter_key;
    private String num_reg_voter;
    private String id_gender;
    private String id_country_birth;
    private String id_federal_entity;
    private String id_nationality;
    private String id_civil_status;
    private String sons;
    private String id_living_place;
    private String id_level_education;
    private String id_kind_local;
    private String id_name;
    private String complete;

    public RequestBasics() {

    }

    public String getBirth_date() {
        return birth_date;
    }

    public void setBirth_date(String birth_date) {
        this.birth_date = birth_date;
    }

    public String getVoter_key() {
        return voter_key;
    }

    public void setVoter_key(String voter_key) {
        this.voter_key = voter_key;
    }

    public String getNum_reg_voter() {
        return num_reg_voter;
    }

    public void setNum_reg_voter(String num_reg_voter) {
        this.num_reg_voter = num_reg_voter;
    }

    public String getId_gender() {
        return id_gender;
    }

    public void setId_gender(String id_gender) {
        this.id_gender = id_gender;
    }

    public String getId_country_birth() {
        return id_country_birth;
    }

    public void setId_country_birth(String id_country_birth) {
        this.id_country_birth = id_country_birth;
    }

    public String getId_federal_entity() {
        return id_federal_entity;
    }

    public void setId_federal_entity(String id_federal_entity) {
        this.id_federal_entity = id_federal_entity;
    }

    public String getId_nationality() {
        return id_nationality;
    }

    public void setId_nationality(String id_nationality) {
        this.id_nationality = id_nationality;
    }

    public String getId_civil_status() {
        return id_civil_status;
    }

    public void setId_civil_status(String id_civil_status) {
        this.id_civil_status = id_civil_status;
    }

    public String getSons() {
        return sons;
    }

    public void setSons(String sons) {
        this.sons = sons;
    }

    public String getId_living_place() {
        return id_living_place;
    }

    public void setId_living_place(String id_living_place) {
        this.id_living_place = id_living_place;
    }

    public String getId_level_education() {
        return id_level_education;
    }

    public void setId_level_education(String id_level_education) {
        this.id_level_education = id_level_education;
    }

    public String getId_kind_local() {
        return id_kind_local;
    }

    public void setId_kind_local(String id_kind_local) {
        this.id_kind_local = id_kind_local;
    }

    public String getId_name() {
        return id_name;
    }

    public void setId_name(String id_name) {
        this.id_name = id_name;
    }

    public String getComplete() {
        return complete;
    }

    public void setComplete(String complete) {
        this.complete = complete;
    }
}
