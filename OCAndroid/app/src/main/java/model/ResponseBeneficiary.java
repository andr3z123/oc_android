package model;

/**
 * Created by andresaleman on 2/17/17.
 */

public class ResponseBeneficiary
{
    private String estado;
    private String mensaje;
    private Beneficiary beneficiary;

    public ResponseBeneficiary()
    {

    }

    public String getEstado() {
        return estado;
    }
    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMensaje() {
        return mensaje;
    }
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Beneficiary getBeneficiary() {
        return beneficiary;
    }
    public void setBeneficiary(Beneficiary beneficiary) {
        this.beneficiary = beneficiary;
    }
}
