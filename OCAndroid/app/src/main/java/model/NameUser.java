package model;

/**
 * Created by andresaleman on 3/30/17.
 */

public class NameUser
{
    public NameUser()
    {

    }

    private int idNAME;
    private String name1;
    private String name2;
    private String lastName;
    private String mLastName;
    private String dateAdmission;
    private String dayRet;
    private int idStatus;
    private String administrativeAreaLevel2;
    private String idADDRESS;
    private boolean complete;
    private String idNameServer;

    public int getIdNAME() {
        return idNAME;
    }

    public void setIdNAME(int idNAME) {
        this.idNAME = idNAME;
    }

    public String getName1() {
        return name1;
    }

    public void setName1(String name1) {
        this.name1 = name1;
    }

    public String getName2() {
        return name2;
    }

    public void setName2(String name2) {
        this.name2 = name2;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getmLastName() {
        return mLastName;
    }

    public void setmLastName(String mLastName) {
        this.mLastName = mLastName;
    }

    public String getDateAdmission() {
        return dateAdmission;
    }

    public void setDateAdmission(String dateAdmission) {
        this.dateAdmission = dateAdmission;
    }

    public String getDayRet() {
        return dayRet;
    }

    public void setDayRet(String dayRet) {
        this.dayRet = dayRet;
    }

    public int getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(int idStatus) {
        this.idStatus = idStatus;
    }

    public String getAdministrativeAreaLevel2() {
        return administrativeAreaLevel2;
    }

    public void setAdministrativeAreaLevel2(String administrativeAreaLevel2) {
        this.administrativeAreaLevel2 = administrativeAreaLevel2;
    }

    public String getIdADDRESS() {
        return idADDRESS;
    }

    public void setIdADDRESS(String idADDRESS) {
        this.idADDRESS = idADDRESS;
    }

    public boolean isComplete() {
        return complete;
    }

    public void setComplete(boolean complete) {
        this.complete = complete;
    }

    public String getIdNameServer() {
        return idNameServer;
    }

    public void setIdNameServer(String idNameServer) {
        this.idNameServer = idNameServer;
    }
}
