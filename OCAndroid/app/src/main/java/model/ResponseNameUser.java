package model;

import java.util.List;

/**
 * Created by andresaleman on 3/29/17.
 */

public class ResponseNameUser
{
    public ResponseNameUser()
    {

    }

    private String estado;
    private String mensaje;
    public List<NameUser> name_user;

    public List<NameUser> getName_user() {
        return name_user;
    }

    public void setName_user(List<NameUser> name_user) {
        this.name_user = name_user;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
}
