package model;

/**
 * Created by andresaleman on 3/30/17.
 */

public class RequestName 
{
    public RequestName()
    {
        
    }
    
    private String name1;
    private String name2;
    private String last_name;
    private String m_last_name;
    private String id_user;
    private String complete;
    private String id_user_type;
    private String date_admission;
    private String day_ret;
    private String status;
    private String id_product;

    public String getId_product() {
        return id_product;
    }

    public void setId_product(String id_product) {
        this.id_product = id_product;
    }

    public String getDate_admission() {
        return date_admission;
    }

    public void setDate_admission(String date_admission) {
        this.date_admission = date_admission;
    }

    public String getDay_ret() {
        return day_ret;
    }

    public void setDay_ret(String day_ret) {
        this.day_ret = day_ret;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getName1() {
        return name1;
    }

    public void setName1(String name1) {
        this.name1 = name1;
    }

    public String getName2() {
        return name2;
    }

    public void setName2(String name2) {
        this.name2 = name2;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getM_last_name() {
        return m_last_name;
    }

    public void setM_last_name(String m_last_name) {
        this.m_last_name = m_last_name;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public String getComplete() {
        return complete;
    }

    public void setComplete(String complete) {
        this.complete = complete;
    }

    public String getId_user_type() {
        return id_user_type;
    }

    public void setId_user_type(String id_user_type) {
        this.id_user_type = id_user_type;
    }
}
