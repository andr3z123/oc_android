package model;

/**
 * Created by Jose Luis on 24/01/2017.
 */

public class ApplicantsNews {

    public String nombre_Solicitante;
    public String fecha_Ingreso;
    public String id_Cliente;
    public String dias_Restantes;
    public String colonia;
    public int icon_Status;
    public boolean complete;
    public String idName;
    public int idNameServer;

    public ApplicantsNews(){
        super();
    }

    public ApplicantsNews(String nombre_Solicitante, String fecha_Ingreso, String id_Cliente, String dias_Restantes, String colonia, int icon_Status, int idNameServer){
        this.nombre_Solicitante = nombre_Solicitante;
        this.fecha_Ingreso = fecha_Ingreso;
        this.id_Cliente = id_Cliente;
        this.dias_Restantes = dias_Restantes;
        this.colonia = colonia;
        this.icon_Status = icon_Status;
        this.idNameServer = idNameServer;
    }

    public String getNombre_Solicitante() {
        return nombre_Solicitante;
    }

    public void setNombre_Solicitante(String nombre_Solicitante) {
        this.nombre_Solicitante = nombre_Solicitante;
    }

    public String getFecha_Ingreso() {
        return fecha_Ingreso;
    }

    public void setFecha_Ingreso(String fecha_Ingreso) {
        this.fecha_Ingreso = fecha_Ingreso;
    }

    public String getId_Cliente() {
        return id_Cliente;
    }

    public void setId_Cliente(String id_Cliente) {
        this.id_Cliente = id_Cliente;
    }

    public String getDias_Restantes() {
        return dias_Restantes;
    }

    public void setDias_Restantes(String dias_Restantes) {
        this.dias_Restantes = dias_Restantes;
    }

    public String getColonia() {
        return colonia;
    }

    public void setColonia(String colonia) {
        this.colonia = colonia;
    }

    public int getIcon_Status() {
        return icon_Status;
    }

    public void setIcon_Status(int icon_Status) {
        this.icon_Status = icon_Status;
    }

    public boolean isComplete() {
        return complete;
    }

    public void setComplete(boolean complete) {
        this.complete = complete;
    }

    public String getIdName() {
        return idName;
    }

    public void setIdName(String idName) {
        this.idName = idName;
    }

    public int getIdNameServer() {
        return idNameServer;
    }

    public void setIdNameServer(int idNameServer) {
        this.idNameServer = idNameServer;
    }
}
