package model;

/**
 * Created by andresaleman on 2/14/17.
 */

public class Prospect
{
    private String id_prospect;
    private String id_rol;
    private String id_status;
    private String id_product;
    private String complete;
    private String id_name;

    public Prospect()
    {

    }

    public Prospect(String id_prospect, String id_rol, String id_status, String id_product, String complete, String id_name)
    {
        this.id_prospect = id_prospect;
        this.id_rol = id_rol;
        this.id_status = id_status;
        this.id_product = id_product;
        this.complete = complete;
        this.id_name = id_name;
    }

    public String getId_prospect() {
        return id_prospect;
    }
    public void setId_prospect(String id_prospect) {
        this.id_prospect = id_prospect;
    }

    public String getId_rol() {
        return id_rol;
    }
    public void setId_rol(String id_rol) {
        this.id_rol = id_rol;
    }

    public String getId_status() {
        return id_status;
    }
    public void setId_status(String id_status) {
        this.id_status = id_status;
    }

    public String getId_product() {
        return id_product;
    }
    public void setId_product(String id_product) {
        this.id_product = id_product;
    }

    public String getComplete() {
        return complete;
    }
    public void setComplete(String complete) {
        this.complete = complete;
    }

    public String getId_name() {
        return id_name;
    }
    public void setId_name(String id_name) {
        this.id_name = id_name;
    }
}
