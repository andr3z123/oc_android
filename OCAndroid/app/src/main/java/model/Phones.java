package model;

/**
 * Created by Jose Luis on 24/01/2017.
 */

public class Phones {
    public String telefono;
    //public String tipo;
    public int icon_Contact;
    public int icon_telefono;

    public Phones(){
        super();
    }

    public Phones(int icon_Contact, String telefono, int icon_telefono){
        this.icon_Contact = icon_Contact;
        this.telefono = telefono;
        //this.tipo = tipo;
        this.icon_telefono = icon_telefono;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    /*public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }*/

    public int getIcon_Contact() {
        return icon_Contact;
    }

    public void setIcon_Contact(int icon_Contact) {
        this.icon_Contact = icon_Contact;
    }

    public int getIcon_telefono() {
        return icon_telefono;
    }

    public void setIcon_telefono(int icon_telefono) {
        this.icon_telefono = icon_telefono;
    }
}
