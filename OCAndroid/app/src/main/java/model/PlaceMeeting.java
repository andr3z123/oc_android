package model;

/**
 * Created by andresaleman on 2/16/17.
 */

public class PlaceMeeting
{
    private String idPLACEMEETING;
    private String route;
    private String streetNumber1;
    private String streetNumber2;
    private String neighborhood;
    private String administrativeAreaLevel2;
    private String idFederalEntity;
    private String idCountry;
    private String postalCode;
    private String routeReference1;
    private String routeReference2;
    private String locationReference;
    private String phone;
    private String idPhoneTypes;
    private String comments;
    private String idGroupDetail;

    public PlaceMeeting()
    {

    }

    public PlaceMeeting(String idPLACEMEETING, String route, String streetNumber1, String streetNumber2, String neighborhood, String administrativeAreaLevel2, String idFederalEntity, String postalCode, String idCountry, String routeReference1, String routeReference2, String locationReference, String phone, String idPhoneTypes, String comments, String idGroupDetail)
    {
        this.idPLACEMEETING = idPLACEMEETING;
        this.route = route;
        this.streetNumber1 = streetNumber1;
        this.streetNumber2 = streetNumber2;
        this.neighborhood = neighborhood;
        this.administrativeAreaLevel2 = administrativeAreaLevel2;
        this.idFederalEntity = idFederalEntity;
        this.idCountry = idCountry;
        this.postalCode = postalCode;
        this.routeReference1 = routeReference1;
        this.routeReference2 = routeReference2;
        this.locationReference = locationReference;
        this.phone = phone;
        this.idPhoneTypes = idPhoneTypes;
        this.comments = comments;
        this.idGroupDetail = idGroupDetail;
    }

    public String getIdPLACEMEETING() {
        return idPLACEMEETING;
    }
    public void setIdPLACEMEETING(String idPLACEMEETING) {
        this.idPLACEMEETING = idPLACEMEETING;
    }

    public String getRoute() {
        return route;
    }
    public void setRoute(String route) {
        this.route = route;
    }

    public String getStreetNumber1() {
        return streetNumber1;
    }
    public void setStreetNumber1(String streetNumber1) {
        this.streetNumber1 = streetNumber1;
    }

    public String getStreetNumber2() {
        return streetNumber2;
    }
    public void setStreetNumber2(String streetNumber2) {
        this.streetNumber2 = streetNumber2;
    }

    public String getNeighborhood() {
        return neighborhood;
    }
    public void setNeighborhood(String neighborhood) {
        this.neighborhood = neighborhood;
    }

    public String getAdministrativeAreaLevel2() {
        return administrativeAreaLevel2;
    }
    public void setAdministrativeAreaLevel2(String administrativeAreaLevel2) {
        this.administrativeAreaLevel2 = administrativeAreaLevel2;
    }

    public String getIdFederalEntity() {
        return idFederalEntity;
    }
    public void setIdFederalEntity(String idFederalEntity) {
        this.idFederalEntity = idFederalEntity;
    }

    public String getIdCountry() {
        return idCountry;
    }
    public void setIdCountry(String idCountry) {
        this.idCountry = idCountry;
    }

    public String getPostalCode() {
        return postalCode;
    }
    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getRouteReference1() {
        return routeReference1;
    }
    public void setRouteReference1(String routeReference1) {
        this.routeReference1 = routeReference1;
    }

    public String getRouteReference2() {
        return routeReference2;
    }
    public void setRouteReference2(String routeReference2) {
        this.routeReference2 = routeReference2;
    }

    public String getLocationReference() {
        return locationReference;
    }
    public void setLocationReference(String locationReference) {
        this.locationReference = locationReference;
    }

    public String getPhone() {
        return phone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getIdPhoneTypes() {
        return idPhoneTypes;
    }
    public void setIdPhoneTypes(String idPhoneTypes) {
        this.idPhoneTypes = idPhoneTypes;
    }

    public String getComments() {
        return comments;
    }
    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getIdGroupDetail() {
        return idGroupDetail;
    }
    public void setIdGroupDetail(String idGroupDetail) {
        this.idGroupDetail = idGroupDetail;
    }
}
