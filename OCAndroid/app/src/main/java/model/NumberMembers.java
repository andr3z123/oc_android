package model;

/**
 * Created by Jose Luis on 07/02/2017.
 */

public class NumberMembers {
    public String nombre_Integrante;

    public NumberMembers(){
        super();
    }

    public NumberMembers(String nombre_Integrante){
        this.nombre_Integrante = nombre_Integrante;
    }

    public String getNombre_Integrante() {
        return nombre_Integrante;
    }

    public void setNombre_Integrante(String nombre_Integrante) {
        this.nombre_Integrante = nombre_Integrante;
    }
}
