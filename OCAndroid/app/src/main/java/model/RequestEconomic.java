package model;

/**
 * Created by andresaleman on 5/24/17.
 */

public class RequestEconomic
{
    public RequestEconomic()
    {

    }

    private String id_economic_activity;
    private String contribution_level;
    private String another_source_income;
    private String current_business_time;
    private String id_local_type;
    private String id_makes_activity;
    private String id_time_activity;
    private String email;
    private String id_name;
    private String complete;

    public String getId_economic_activity() {
        return id_economic_activity;
    }

    public void setId_economic_activity(String id_economic_activity) {
        this.id_economic_activity = id_economic_activity;
    }

    public String getContribution_level() {
        return contribution_level;
    }

    public void setContribution_level(String contribution_level) {
        this.contribution_level = contribution_level;
    }

    public String getAnother_source_income() {
        return another_source_income;
    }

    public void setAnother_source_income(String another_source_income) {
        this.another_source_income = another_source_income;
    }

    public String getCurrent_business_time() {
        return current_business_time;
    }

    public void setCurrent_business_time(String current_business_time) {
        this.current_business_time = current_business_time;
    }

    public String getId_local_type() {
        return id_local_type;
    }

    public void setId_local_type(String id_local_type) {
        this.id_local_type = id_local_type;
    }

    public String getId_makes_activity() {
        return id_makes_activity;
    }

    public void setId_makes_activity(String id_makes_activity) {
        this.id_makes_activity = id_makes_activity;
    }

    public String getId_time_activity() {
        return id_time_activity;
    }

    public void setId_time_activity(String id_time_activity) {
        this.id_time_activity = id_time_activity;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getId_name() {
        return id_name;
    }

    public void setId_name(String id_name) {
        this.id_name = id_name;
    }

    public String getComplete() {
        return complete;
    }

    public void setComplete(String complete) {
        this.complete = complete;
    }
}
