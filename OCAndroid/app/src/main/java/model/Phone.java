package model;

/**
 * Created by andresaleman on 2/15/17.
 */

public class Phone
{
    private String idPHONES;
    private String number;
    private String idPhoneTypes;
    private String idName;
    private String complete;

    public Phone()
    {

    }

    public Phone(String id_phone, String number, String id_phone_types, String id_name, String complete)
    {
        this.idPHONES = id_phone;
        this.number = number;
        this.idPhoneTypes = id_phone_types;
        this.idName = id_name;
        this.complete = complete;
    }

    public String getIdPHONES() {
        return idPHONES;
    }

    public void setIdPHONES(String idPHONES) {
        this.idPHONES = idPHONES;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getIdPhoneTypes() {
        return idPhoneTypes;
    }

    public void setIdPhoneTypes(String idPhoneTypes) {
        this.idPhoneTypes = idPhoneTypes;
    }

    public String getIdName() {
        return idName;
    }

    public void setIdName(String idName) {
        this.idName = idName;
    }

    public String getComplete() {
        return complete;
    }
    public void setComplete(String complete) {
        this.complete = complete;
    }
}
