package nubaj.com.ocandroid;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;

import data.GlobalVariables;
import data.InternetConnection;
import implementation.DBHandler;
import implementation.FunctionJson;
import model.Name;
import model.RequestNameUser;
import model.RequestUser;
import model.ResponseBankCatalog;
import model.ResponseNameUser;
import model.ResponseUser;
import model.User;

public class LoginActivity extends AppCompatActivity
{
    private Button button_Loging;
    private EditText editText_User, editText_Password;
    //private ProgressBar progressBar;
    private int progressStatus = 0, id;
    private Handler handler = new Handler();
    private boolean load= false;
    ProgressDialog progressBar;
    Gson gson;
    FunctionJson functionJson;
    String response;
    DBHandler dbHandler;

    // Handler for updating progress
    Handler progressBarHandler;

    // Activity Variable
    int fileSize;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        editText_User = (EditText)findViewById(R.id.editTexUser);
        editText_Password = (EditText)findViewById(R.id.editTextPassword);
        button_Loging = (Button)findViewById(R.id.buttonSignIn);

        button_Loging.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent intentLogin = new Intent(getApplicationContext(), MainActivity.class);
                //startActivity(intentLogin);
                onLogin(v);
            }
        });
        getSupportActionBar().hide();
    }

    public void onLogin(View view)
    {
        FunctionJson functionJson = new FunctionJson();
        RequestUser requestUser = new RequestUser();
        requestUser.setUserName(editText_User.getText().toString());
        requestUser.setPassword(editText_Password.getText().toString());
        Gson gson = new GsonBuilder().create();
        String json = gson.toJson(requestUser);
        String response = "";
        if(InternetConnection.checkConnection(LoginActivity.this)) {
            response = functionJson.RequestHttp(getString(R.string.url_login), FunctionJson.POST, json);

            GlobalVariables.responseUser = new ResponseUser();
            GlobalVariables.responseUser = gson.fromJson(response, ResponseUser.class);
            dbHandler = new DBHandler(this);
            GlobalVariables.userArrayList = new ArrayList<User>();
            GlobalVariables.userArrayList = dbHandler.GetAllUser();
            User user = new User();
            if (GlobalVariables.responseUser.getUser() != null) {
                id = Integer.parseInt(GlobalVariables.responseUser.getUser().getIdUSERS());
                if (GlobalVariables.userArrayList.size() > 0) {
                    for (User user1 : GlobalVariables.userArrayList) {
                        if (!user1.getUserName().toUpperCase().equals(GlobalVariables.responseUser.getUser().getUserName().toUpperCase())) {
                            user.setIdUSERS(GlobalVariables.responseUser.getUser().getIdUSERS());
                            user.setPassword(GlobalVariables.responseUser.getUser().getPassword().toString());
                            user.setUserName(GlobalVariables.responseUser.getUser().getUserName().toString());
                            dbHandler.AddUser(user);
                            break;
                        }
                    }
                } else {
                    user.setIdUSERS(GlobalVariables.responseUser.getUser().getIdUSERS());
                    user.setPassword(GlobalVariables.responseUser.getUser().getPassword().toString());
                    user.setUserName(GlobalVariables.responseUser.getUser().getUserName().toString());
                    dbHandler.AddUser(user);
                }
                final String mensaje = GlobalVariables.responseUser.getMensaje().toString();

                if (GlobalVariables.responseUser.getEstado().toString().equals("0")) {
                    GlobalVariables.getMessage(this, mensaje);
                    new LoginActivity.get_name().execute();
                } else {
                    GlobalVariables.getMessage(this, mensaje);
                }
            } else {
                Snackbar.make(view, GlobalVariables.responseUser.getMensaje(), Snackbar.LENGTH_SHORT).setAction("Action", null).show();
            }
        }
        else
        {
            Toast.makeText(LoginActivity.this, "No hay conexión a internet", Toast.LENGTH_LONG).show();
        }


    }
    public class get_name extends AsyncTask<String, String, String>
    {
        ProgressDialog pDialog;
        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            pDialog = new ProgressDialog(LoginActivity.this);
            pDialog.setTitle(getString(R.string.message_title_applicants));
            pDialog.setMessage(getString(R.string.message_applicants));
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params)
        {
            getName();
            return "";
        }

        @Override
        protected void onPostExecute(String s)
        {
            //la respuesta del web services despues que se envio la información del movil por el web services
            super.onPostExecute(s);
            pDialog.dismiss();

        }

        private void getName()
        {
            dbHandler = new DBHandler(LoginActivity.this);
            GlobalVariables.nameUserArrayList = dbHandler.GetAllAplicants(id);
            try
            {
                Intent intentLogin = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intentLogin);
            }
            catch (Exception e)
            {
                e.getStackTrace();
            }
        }
    }
}
