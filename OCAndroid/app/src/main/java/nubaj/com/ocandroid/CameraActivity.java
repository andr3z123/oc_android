package nubaj.com.ocandroid;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import data.GlobalVariables;
import implementation.ImageSurfaceView;

public class CameraActivity extends AppCompatActivity {

    private Menu menu;
    private ImageSurfaceView mImageSurfaceView;
    private Camera camera;

    private FrameLayout cameraPreviewLayout;
    private ImageView capturedImageHolder;
    private RelativeLayout relativeLayoutCamera, relativeLayoutPreview;
    ActionBar actionBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // remove title
        View decorView = this.getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        decorView.setSystemUiVisibility(uiOptions);

        setContentView(R.layout.activity_camera);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        actionBar = getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setCustomView(getLayoutInflater().inflate(R.layout.action_bar_custom_view_documents, null),
                new ActionBar.LayoutParams(
                        ActionBar.LayoutParams.MATCH_PARENT,
                        ActionBar.LayoutParams.MATCH_PARENT
                ));
        final Drawable upArrow = getResources().getDrawable(R.mipmap.ic_close);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        if (id == android.R.id.home)
        {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    /*public void onClick(View view)
    {
        if(view.getId()==R.id.imageViewClick)
        {
            String focusMode = camera.getParameters().getFocusMode();
            if(focusMode.equals(Camera.Parameters.FOCUS_MODE_AUTO) || focusMode.equals(Camera.Parameters.FOCUS_MODE_MACRO))
            {
                camera.autoFocus(new Camera.AutoFocusCallback()
                {
                    @Override
                    public void onAutoFocus(boolean success, Camera camera)
                    {
                        camera.takePicture(null, null, pictureCallback);
                    }
                });
            }
            else
            {
                camera.takePicture(null, null, pictureCallback);
            }
        }
    }

    private Camera checkDeviceCamera(){
        Camera mCamera = null;
        try
        {
            mCamera = Camera.open();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mCamera;
    }

    Camera.PictureCallback pictureCallback = new Camera.PictureCallback() {
        @Override
        public void onPictureTaken(byte[] data, Camera camera)
        {
            Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
            if(bitmap==null)
            {
                Toast.makeText(CameraActivity.this, "Captured image is empty", Toast.LENGTH_LONG).show();
                return;
            }
            else
            {
                capturedImageHolder.setImageBitmap(GlobalVariables.bitmap);
                *//*if(GlobalVariables.photoOCRFront==true)
                {
                    GlobalVariables.bitmapFront = bitmap;
                }
                else if(GlobalVariables.photoOCRReverse)
                {
                    GlobalVariables.bitmapReverse = bitmap;
                }*//*
                camera.stopPreview();
                camera.startPreview();
                relativeLayoutCamera.setVisibility(View.GONE);
                relativeLayoutPreview.setVisibility(View.VISIBLE);
            }
        }
    };*/
}
