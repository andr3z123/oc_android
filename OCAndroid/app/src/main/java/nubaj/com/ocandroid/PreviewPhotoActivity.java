package nubaj.com.ocandroid;

import android.app.Dialog;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import data.GlobalVariables;

public class PreviewPhotoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview_photo);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.mipmap.ic_close);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        ImageView img = (ImageView) findViewById(R.id.goProDialogImage);

        Glide.with(this)
                .load(GlobalVariables.file) //load position image
                .diskCacheStrategy(DiskCacheStrategy.NONE) //Delete cache
                .skipMemoryCache(true) //Update cache
                .centerCrop() //Center
                //.placeholder(R.mipmap.ic_launcher)//Icon preview
                .into(img); //component resource
        //imageViewImagendocumento.setImageBitmap(mDataset.get(position).getImagen_Documento());
        //mageViewImagendocumento.setImageResource(mDataset.get(position).getImagen_Documento());
        //img.setImageURI(Uri.fromFile(GlobalVariables.file));
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public boolean onCreateOptionsMenu(Menu menu)
    {
        return true;
    }
}
