package nubaj.com.ocandroid;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.BoolRes;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.text.Selection;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;

import data.GlobalVariables;
import data.InternetConnection;
import fragments.ApplicantsTABFragment;
import fragments.ApplicationsFragment;
import fragments.InsuranceFragment;
import fragments.MyEarringsFragment;
import fragments.NextRenovationsFragment;
import fragments.NoticesFragment;
import fragments.RequestTabFragment;
import fragments.SyncUpFragment;
import implementation.DBHandler;
import implementation.FunctionJson;
import model.Additionals;
import model.Address;
import model.Basics;
import model.Complementary;
import model.Economic;
import model.Name;
import model.Phone;
import model.RequestAdditional;
import model.RequestAddress;
import model.RequestBasics;
import model.RequestComplementary;
import model.RequestEconomic;
import model.RequestName;
import model.RequestNameUser;
import model.RequestPhone;
import model.ResponseAdditional;
import model.ResponseAddress;
import model.ResponseBasics;
import model.ResponseComplementary;
import model.ResponseEconomic;
import model.ResponseName;
import model.ResponseNameUser;
import model.ResponsePhones;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private FloatingActionButton fab;
    ActionBar actionBar;
    Gson gson;
    FunctionJson functionJson;
    private RequestName requestName;
    private Name nameBD;
    private boolean changeFlag=false;
    private static boolean nameFlag=true, phoneFlag=true,addressFlag=true,basicFlag=true,economicFlag=true,complementaryFlag=true, additionalFlag=true;
    private int idFlag;
    private static ArrayList<Phone> phoneBD;
    private static ArrayList<Address> addressBD;
    private static Basics basicsBD;
    private static Economic economicBD;
    private static Complementary complementaryBD;
    private static ArrayList<Additionals> additionalsBD;
    private static DBHandler dbHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                /*Fragment fragment = null;
                Activity activity =  null;
                fragment = new SyncUpFragment();
                actionBar = getSupportActionBar();
                actionBar.setDisplayShowTitleEnabled(false);
                actionBar.setDisplayShowCustomEnabled(true);
                actionBar.setDisplayHomeAsUpEnabled(true);
                actionBar.setCustomView(getLayoutInflater().inflate(R.layout.action_bar_custom_view_pending, null),
                        new ActionBar.LayoutParams(
                                ActionBar.LayoutParams.MATCH_PARENT,
                                ActionBar.LayoutParams.MATCH_PARENT
                        ));*/
                if(InternetConnection.checkConnection(MainActivity.this))
                {
                    new MainActivity.request_sync_up().execute();
                }
                else
                {
                    Toast.makeText(MainActivity.this, "No hay conexión a internet", Toast.LENGTH_LONG).show();
                }
            }
        });

        actionBar = getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setCustomView(getLayoutInflater().inflate(R.layout.action_bar_custom_view_main, null),
                new ActionBar.LayoutParams(
                        ActionBar.LayoutParams.MATCH_PARENT,
                        ActionBar.LayoutParams.MATCH_PARENT
                ));

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View hView = navigationView.getHeaderView(0);
        TextView textViewUser = (TextView) hView.findViewById(R.id.textViewName);
        textViewUser.setText(GlobalVariables.responseUser.getUser().getUserName().toString());
        showFragment();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            //super.onBackPressed();
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case DialogInterface.BUTTON_POSITIVE:
                            //Yes button clicked
                            //deleteUser();
                            //GlobalVariables.logout = true;
                            startActivity(new Intent(getBaseContext(), LoginActivity.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));
                            finish();
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            //No button clicked
                            dialog.dismiss();
                            break;
                    }
                }
            };
            android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
            builder.setMessage("¿Está seguro de cerrar su sesión?").setPositiveButton("Sí", dialogClickListener).setNegativeButton("No", dialogClickListener).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();
        //noinspection SimplifiableIfStatement


        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Fragment fragment = null;
        Activity activity =  null;
        //frameLayout.setBackground(null);
        if (id == R.id.nav_solicitantes) {

            fragment = new ApplicantsTABFragment();
            actionBar = getSupportActionBar();
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setCustomView(getLayoutInflater().inflate(R.layout.action_bar_custom_view_main, null),
                    new ActionBar.LayoutParams(
                            ActionBar.LayoutParams.MATCH_PARENT,
                            ActionBar.LayoutParams.MATCH_PARENT
                    ));
            fab.setVisibility(View.VISIBLE);
            callFragment(fragment);

        } else if (id == R.id.nav_solicitudes) {

            fragment = new RequestTabFragment();
            actionBar = getSupportActionBar();
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setCustomView(getLayoutInflater().inflate(R.layout.action_bar_custom_view_request, null),
                    new ActionBar.LayoutParams(
                            ActionBar.LayoutParams.MATCH_PARENT,
                            ActionBar.LayoutParams.MATCH_PARENT
                    ));
            fab.setVisibility(View.GONE);
            callFragment(fragment);

        }
        /*else if (id == R.id.nav_proximas_renovaciones)
        {

            fragment = new NextRenovationsFragment();
            actionBar = getSupportActionBar();
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setCustomView(getLayoutInflater().inflate(R.layout.action_bar_custom_view_renewals, null),
                    new ActionBar.LayoutParams(
                            ActionBar.LayoutParams.MATCH_PARENT,
                            ActionBar.LayoutParams.MATCH_PARENT
                    ));
            fab.setVisibility(View.GONE);
            callFragment(fragment);

        } else if (id == R.id.nav_seguros) {

            fragment = new InsuranceFragment();
            actionBar = getSupportActionBar();
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setCustomView(getLayoutInflater().inflate(R.layout.action_bar_custom_view_secure, null),
                    new ActionBar.LayoutParams(
                            ActionBar.LayoutParams.MATCH_PARENT,
                            ActionBar.LayoutParams.MATCH_PARENT
                    ));
            fab.setVisibility(View.GONE);
            callFragment(fragment);

        }*/ /*else if (id == R.id.nav_mis_pendientes) {

            fragment = new MyEarringsFragment();
            actionBar = getSupportActionBar();
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setCustomView(getLayoutInflater().inflate(R.layout.action_bar_custom_view_pending, null),
                    new ActionBar.LayoutParams(
                            ActionBar.LayoutParams.MATCH_PARENT,
                            ActionBar.LayoutParams.MATCH_PARENT
                    ));
            fab.setVisibility(View.GONE);
            callFragment(fragment);

        } else if (id == R.id.nav_avisos) {

            fragment = new NoticesFragment();
            actionBar = getSupportActionBar();
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setCustomView(getLayoutInflater().inflate(R.layout.action_bar_custom_view_prompt, null),
                    new ActionBar.LayoutParams(
                            ActionBar.LayoutParams.MATCH_PARENT,
                            ActionBar.LayoutParams.MATCH_PARENT
                    ));
            fab.setVisibility(View.GONE);
            callFragment(fragment);

        } else if (id == R.id.nav_sincronizacion) {

            this.setTitle(getText(R.string.nav_sync));

        }*/ else if (id == R.id.nav_cerrar_sesion) {

            this.setTitle(getText(R.string.nav_logout));
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case DialogInterface.BUTTON_POSITIVE:
                            //Yes button clicked
                            startActivity(new Intent(getBaseContext(), LoginActivity.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));
                            finish();
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            //No button clicked
                            dialog.dismiss();
                            break;
                    }
                }
            };

            android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
            builder.setMessage("¿Está seguro de cerrar su sesión?").setPositiveButton("Sí", dialogClickListener).setNegativeButton("No", dialogClickListener).show();

        }

        /*DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);*/
        return true;
    }

    public void showFragment()
    {
        Fragment fragment = null;

        fragment = new ApplicantsTABFragment();

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        this.setTitle(getText(R.string.nav_applicants));
    }

    void callFragment(Fragment fragment)
    {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    private static void verifyName(Name name)
    {
        try
        {
            if(name!=null)
            {
                if(TextUtils.isEmpty(name.getName1()))
                {
                    nameFlag=false;
                    return;
                }
                if(TextUtils.isEmpty(name.getLastName()))
                {
                    nameFlag = false;
                    return;
                }
                if(name.getIdProduct()<=0)
                {
                    nameFlag=false;
                    return;
                }
                if(TextUtils.isEmpty(name.getDateAdmission()))
                {
                    nameFlag=false;
                    return;
                }
                if(name.getIdStatus()<=0)
                {
                    nameFlag=false;
                    return;
                }
            }
            else
            {
                nameFlag = false;
                return;
            }
        }
        catch (Exception ex)
        {
            ex.getStackTrace();
        }
    }

    private static ArrayList<Phone> getPhone(String id)
    {
        phoneBD = new ArrayList<Phone>();
        try
        {
            phoneBD = dbHandler.GetAllPhones(Integer.parseInt(id));
            if(phoneBD==null)
            {
                phoneFlag = false;
                return null;
            }
        }
        catch (Exception ex)
        {
            ex.getStackTrace();
        }
        return phoneBD;
    }

    private static ArrayList<Address> getAddress(String id)
    {
        addressBD = new ArrayList<Address>();
        try
        {
            addressBD = dbHandler.GetAllAddress(Integer.parseInt(id));
            if(addressBD!=null)
            {
                for (Address address : addressBD)
                {
                    if (TextUtils.isEmpty(address.getIdAddressTypes())) {
                        addressFlag = false;
                        return null;
                    }
                    if (TextUtils.isEmpty(address.getPostalCode())) {
                        addressFlag = false;
                        return null;
                    }
                    if (TextUtils.isEmpty(address.getIdCountry())) {
                        addressFlag = false;
                        return null;
                    }
                    if (TextUtils.isEmpty(address.getIdFederalEntity())) {
                        addressFlag = false;
                        return null;
                    }
                    if (TextUtils.isEmpty(address.getAdministrativeAreaLevel2())) {
                        addressFlag = false;
                        return null;
                    }
                    if (TextUtils.isEmpty(address.getNeighborhood())) {
                        addressFlag = false;
                        return null;
                    }
                    if (TextUtils.isEmpty(address.getRoute())) {
                        addressFlag = false;
                        return null;
                    }
                    if (TextUtils.isEmpty(address.getStreetNumber1())) {
                        addressFlag = false;
                        return null;
                    }
                }
            }
            else
            {
                addressFlag = false;
                return null;
            }
        }
        catch (Exception ex)
        {
            ex.getStackTrace();
        }
        return addressBD;
    }

    private static Basics getBasics(String id)
    {
        basicsBD = new Basics();
        try
        {
            basicsBD = dbHandler.GetBasics(Integer.parseInt(id));
            if(basicsBD!=null)
            {
                if(TextUtils.isEmpty(basicsBD.getIdGender()))
                {
                    basicFlag=false;
                    return null;
                }
                if(TextUtils.isEmpty(basicsBD.getBirthdate()))
                {
                    basicFlag=false;
                    return null;
                }
                if(TextUtils.isEmpty(basicsBD.getVoterKey()))
                {
                    basicFlag=false;
                    return null;
                }
                if(TextUtils.isEmpty(basicsBD.getNumRegVoter()))
                {
                    basicFlag=false;
                    return null;
                }
                if(TextUtils.isEmpty(basicsBD.getIdCountryBirth()))
                {
                    basicFlag=false;
                    return null;
                }
                if(TextUtils.isEmpty(basicsBD.getIdFederalEntity()))
                {
                    basicFlag=false;
                    return null;
                }
                if(TextUtils.isEmpty(basicsBD.getIdNationality()))
                {
                    basicFlag=false;
                    return null;
                }
                if(TextUtils.isEmpty(basicsBD.getIdCivilStatus()))
                {
                    basicFlag=false;
                    return null;
                }
                if (TextUtils.isEmpty(basicsBD.getSons()))
                {
                    basicFlag=false;
                    return null;
                }
                if(TextUtils.isEmpty(basicsBD.getIdLevelEducation()))
                {
                    basicFlag=false;
                    return null;
                }
                if(TextUtils.isEmpty(basicsBD.getIdLivingPlace()))
                {
                    basicFlag=false;
                    return null;
                }
            }
            else
            {
                basicFlag = false;
                return null;
            }
        }
        catch (Exception ex)
        {
            ex.getStackTrace();
        }
        return basicsBD;
    }

    private static Economic getEconomic(String id)
    {
        economicBD = new Economic();
        try
        {
            economicBD = dbHandler.GetEconomic(Integer.parseInt(id));
            if(economicBD!=null)
            {
                if(TextUtils.isEmpty(economicBD.getEmail()))
                {
                    economicFlag=false;
                    return null;
                }
                if(TextUtils.isEmpty(economicBD.getIdTimeActivity()))
                {
                    economicFlag=false;
                    return null;
                }
                if(TextUtils.isEmpty(economicBD.getIdMakesActivity()))
                {
                    economicFlag=false;
                    return null;
                }
                if(TextUtils.isEmpty(economicBD.getContributionLevel()))
                {
                    economicFlag=false;
                    return null;
                }
                if(TextUtils.isEmpty(economicBD.getAnotherSourceIncome()))
                {
                    economicFlag=false;
                    return null;
                }
                if(TextUtils.isEmpty(economicBD.getCurrentBusinessTime()))
                {
                    economicFlag=false;
                    return null;
                }
                if(TextUtils.isEmpty(economicBD.getIdLocalType()))
                {
                    economicFlag=false;
                    return null;
                }
                if (TextUtils.isEmpty(economicBD.getIdEconomicActivity()))
                {
                    economicFlag=false;
                    return null;
                }
            }
            else
            {
                economicFlag=false;
                return null;
            }
        }
        catch (Exception ex)
        {
            ex.getStackTrace();
        }
        return economicBD;
    }

    private static Complementary getComplementary(String id)
    {
        complementaryBD = new Complementary();
        try
        {
            complementaryBD = dbHandler.GetComplementary(Integer.parseInt(id));
            if(TextUtils.isEmpty(complementaryBD.getCurp()))
            {
                complementaryFlag = false;
                return null;
            }
            if(TextUtils.isEmpty(complementaryBD.getDependentNumber()))
            {
                complementaryFlag=false;
                return null;
            }
            if(TextUtils.isEmpty(complementaryBD.getIdOccupation()))
            {
                complementaryFlag=false;
                return null;
            }
            if(Boolean.parseBoolean(complementaryBD.getPhysicalPerson())==true)
            {
                if(TextUtils.isEmpty(complementaryBD.getFiel()))
                {
                    complementaryFlag=false;
                    return null;
                }
                if(TextUtils.isEmpty(complementaryBD.getHomoclave()))
                {
                    complementaryFlag=false;
                    return null;
                }
            }
        }
        catch (Exception ex)
        {
            ex.getStackTrace();
        }
        return complementaryBD;
    }

    private static ArrayList<Additionals> getAdditionals(String id)
    {
        additionalsBD = new ArrayList<Additionals>();
        try
        {
            additionalsBD = dbHandler.GetAdditional(Integer.parseInt(id));
            for(Additionals additionals: additionalsBD)
            {
                if(TextUtils.isEmpty(additionals.getName1()))
                {
                    additionalFlag = false;
                    break;
                }
                if(TextUtils.isEmpty(additionals.getLastName()))
                {
                    additionalFlag=false;
                    break;
                }
                if(TextUtils.isEmpty(additionals.getmLastName()))
                {
                    additionalFlag=false;
                    break;
                }
                if(TextUtils.isEmpty(additionals.getPhone()))
                {
                    additionalFlag=false;
                    break;
                }
                if(TextUtils.isEmpty(additionals.getIdAdditionalTypes()))
                {
                    additionalFlag=false;
                    break;
                }
            }
        }
        catch (Exception ex)
        {
            ex.getStackTrace();
        }
        return additionalsBD;
    }

    public class request_sync_up extends AsyncTask<String, String, String>
    {
        ProgressDialog pDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setTitle(getString(R.string.message_title_applicants));
            pDialog.setMessage(getString(R.string.message_applicants));
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params)
        {
            request_sync_up();
            return "";
        }

        @Override
        protected void onPostExecute(String s)
        {
            //la respuesta del web services despues que se envio la información del movil por el web services
            super.onPostExecute(s);
            pDialog.dismiss();
            showFragment();
        }

        private void request_sync_up()
        {
            dbHandler = new DBHandler(MainActivity.this);
            ArrayList<Name> nameArrayList = new ArrayList<Name>();
            boolean ok = false;
            gson = new GsonBuilder().create();
            functionJson = new FunctionJson();
            requestName = new RequestName();
            String json="",response="";
            try
            {
                nameArrayList = dbHandler.GetAllListName(Integer.parseInt(GlobalVariables.responseUser.getUser().getIdUSERS()));
                if(nameArrayList!=null)
                {
                    for (Name name1 : nameArrayList)
                    {
                        nameFlag = true;
                        phoneFlag = true;
                        addressFlag = true;
                        basicFlag = true;
                        economicFlag = true;
                        complementaryFlag = true;
                        additionalFlag = true;
                        nameBD = new Name();
                        verifyName(name1);
                        nameBD.setComplete(name1.getComplete());
                        nameBD.setName1(name1.getName1());
                        nameBD.setIdProduct(Integer.parseInt(String.valueOf(name1.getIdProduct())));
                        nameBD.setDateAdmission(name1.getDateAdmission());
                        nameBD.setDayRet(name1.getDayRet());
                        nameBD.setIdName(name1.getIdName());
                        nameBD.setIdStatus(Integer.parseInt(String.valueOf(name1.getIdStatus())));
                        nameBD.setIdUser(name1.getIdUser());
                        nameBD.setLastName(name1.getLastName());
                        nameBD.setmLastName(name1.getmLastName());
                        nameBD.setName2(name1.getName2());
                        if (Boolean.parseBoolean(nameBD.getComplete()) == false)
                        {
                            if (nameFlag == true)
                            {
                                getPhone(name1.getIdName());
                                getAddress(name1.getIdName());
                                getBasics(name1.getIdName());
                                getEconomic(name1.getIdName());
                                getComplementary(name1.getIdName());
                                getAdditionals(name1.getIdName());
                                if (phoneFlag == true && addressFlag == true && basicFlag == true && economicFlag == true && complementaryFlag == true && additionalFlag == true) {
                                    requestName.setName1(nameBD.getName1());
                                    requestName.setName2(nameBD.getName2());
                                    requestName.setLast_name(nameBD.getLastName());
                                    requestName.setM_last_name(nameBD.getmLastName());
                                    requestName.setId_user(GlobalVariables.responseUser.getUser().getIdUSERS());
                                    requestName.setComplete(getString(R.string.value_true));
                                    requestName.setId_user_type("1");
                                    requestName.setDate_admission(nameBD.getDateAdmission());
                                    requestName.setDay_ret(nameBD.getDayRet());
                                    requestName.setStatus(String.valueOf(nameBD.getIdStatus()));
                                    requestName.setId_product(String.valueOf(nameBD.getIdProduct()));

                                    json = gson.toJson(requestName);
                                    response = functionJson.RequestHttp(getString(R.string.url_insert_name), FunctionJson.POST, json);
                                    if (!TextUtils.isEmpty(response)) {
                                        gson = new GsonBuilder().create();
                                        GlobalVariables.responseName = gson.fromJson(response, ResponseName.class);
                                        nameBD.setComplete(getString(R.string.value_true));
                                        nameBD.setIdNameServer(GlobalVariables.responseName.getUser().getIdName());
                                        idFlag = updateName(nameBD);
                                        if (idFlag > 0) {
                                            functionJson = new FunctionJson();
                                            RequestPhone requestPhone = new RequestPhone();

                                            response = "";
                                            GlobalVariables.responsePhonesArrayList = new ArrayList<ResponsePhones>();
                                            for (Phone phone : phoneBD) {
                                                json = "";
                                                requestPhone.setNumber(phone.getNumber().toString());
                                                requestPhone.setId_phone_type(phone.getIdPhoneTypes());
                                                requestPhone.setId_name(GlobalVariables.responseName.getUser().getIdName());
                                                requestPhone.setComplete(getString(R.string.value_true));
                                                json = gson.toJson(requestPhone);
                                                response = functionJson.RequestHttp(getString(R.string.url_insert_phones), FunctionJson.POST, json);
                                                gson = new GsonBuilder().create();
                                                ResponsePhones responsePhones = new ResponsePhones();
                                                responsePhones = gson.fromJson(response, ResponsePhones.class);
                                                GlobalVariables.responsePhonesArrayList.add(responsePhones);
                                                phone.setIdName(GlobalVariables.responseName.getUser().getIdName());
                                                phone.setComplete(getString(R.string.value_true));
                                            }
                                            if (GlobalVariables.responsePhonesArrayList != null || GlobalVariables.responsePhonesArrayList.size() > 0) {
                                                RequestAddress requestAddress = new RequestAddress();
                                                response = "";
                                                GlobalVariables.responseAddressArrayList = new ArrayList<ResponseAddress>();
                                                for (Address address1 : addressBD) {
                                                    json = "";
                                                    requestAddress.setId_address_type(address1.getIdAddressTypes());
                                                    requestAddress.setPostal_code(address1.getPostalCode());
                                                    requestAddress.setId_country(address1.getIdCountry());
                                                    requestAddress.setId_federal_entity(address1.getIdFederalEntity());
                                                    requestAddress.setAdministrative_area_level2(address1.getAdministrativeAreaLevel2());
                                                    requestAddress.setNeighborhood(address1.getNeighborhood());
                                                    requestAddress.setRoute(address1.getRoute());
                                                    requestAddress.setStreet_number1(address1.getStreetNumber1());
                                                    requestAddress.setStreet_number2(address1.getStreetNumber2());
                                                    requestAddress.setRoute_reference1(address1.getRouteReference1());
                                                    requestAddress.setRoute_reference2(address1.getRouteReference2());
                                                    requestAddress.setId_name(GlobalVariables.responseName.getUser().getIdName());
                                                    requestAddress.setComplete(getString(R.string.value_true));
                                                    json = gson.toJson(requestAddress);
                                                    response = functionJson.RequestHttp(getString(R.string.url_insert_address), FunctionJson.POST, json);
                                                    gson = new GsonBuilder().create();
                                                    ResponseAddress responseAddress = new ResponseAddress();
                                                    responseAddress = gson.fromJson(response, ResponseAddress.class);
                                                    GlobalVariables.responseAddressArrayList.add(responseAddress);
                                                    address1.setIdName(GlobalVariables.responseName.getUser().getIdName());
                                                    address1.setComplete(getString(R.string.value_true));
                                                }

                                                if (GlobalVariables.responseAddressArrayList != null || GlobalVariables.responseAddressArrayList.size() > 0) {
                                                    RequestBasics requestBasics = new RequestBasics();
                                                    response = "";
                                                    GlobalVariables.responseBasics = new ResponseBasics();
                                                    requestBasics.setComplete(getString(R.string.value_true));
                                                    requestBasics.setId_name(GlobalVariables.responseName.getUser().getIdName());
                                                    requestBasics.setBirth_date(basicsBD.getBirthdate());
                                                    requestBasics.setId_civil_status(basicsBD.getIdCivilStatus());
                                                    requestBasics.setId_country_birth(basicsBD.getIdCountryBirth());
                                                    requestBasics.setId_federal_entity(basicsBD.getIdFederalEntity());
                                                    requestBasics.setId_gender(basicsBD.getIdGender());
                                                    requestBasics.setId_level_education(basicsBD.getIdLevelEducation());
                                                    requestBasics.setId_living_place(basicsBD.getIdLivingPlace());
                                                    requestBasics.setId_nationality(basicsBD.getIdNationality());
                                                    requestBasics.setNum_reg_voter(basicsBD.getNumRegVoter());
                                                    requestBasics.setId_kind_local(basicsBD.getIdKindLocal());
                                                    requestBasics.setSons(basicsBD.getSons());
                                                    requestBasics.setVoter_key(basicsBD.getVoterKey());
                                                    json = gson.toJson(requestBasics);
                                                    response = functionJson.RequestHttp(getString(R.string.url_insert_basics), FunctionJson.POST, json);
                                                    gson = new GsonBuilder().create();
                                                    ResponseBasics responseBasics = new ResponseBasics();
                                                    responseBasics = gson.fromJson(response, ResponseBasics.class);
                                                    GlobalVariables.responseBasics = responseBasics;

                                                    if (GlobalVariables.responseBasics != null) {
                                                        RequestComplementary requestComplementary = new RequestComplementary();
                                                        response = "";
                                                        GlobalVariables.responseComplementary = new ResponseComplementary();
                                                        requestComplementary.setComplete(getString(R.string.value_true));
                                                        requestComplementary.setCurp(complementaryBD.getCurp());
                                                        requestComplementary.setDependent_number(complementaryBD.getDependentNumber());
                                                        requestComplementary.setEmail(complementaryBD.getEmail());
                                                        requestComplementary.setFiel(complementaryBD.getFiel());
                                                        requestComplementary.setHomo_clave(complementaryBD.getHomoclave());
                                                        requestComplementary.setId_name(GlobalVariables.responseName.getUser().getIdName());
                                                        requestComplementary.setId_occupation(complementaryBD.getIdOccupation());
                                                        requestComplementary.setPhysical_person(complementaryBD.getPhysicalPerson());
                                                        json = gson.toJson(requestComplementary);
                                                        response = functionJson.RequestHttp(getString(R.string.url_insert_complementary), FunctionJson.POST, json);
                                                        gson = new GsonBuilder().create();
                                                        GlobalVariables.responseComplementary = new ResponseComplementary();
                                                        GlobalVariables.responseComplementary = gson.fromJson(response, ResponseComplementary.class);

                                                        if (GlobalVariables.responseComplementary != null)
                                                        {
                                                            RequestEconomic requestEconomic = new RequestEconomic();
                                                            response = "";
                                                            GlobalVariables.responseEconomic = new ResponseEconomic();
                                                            requestEconomic.setComplete(getString(R.string.value_true));
                                                            requestEconomic.setEmail(economicBD.getEmail());
                                                            requestEconomic.setAnother_source_income(economicBD.getAnotherSourceIncome());
                                                            requestEconomic.setContribution_level(economicBD.getContributionLevel());
                                                            requestEconomic.setCurrent_business_time(economicBD.getCurrentBusinessTime());
                                                            requestEconomic.setId_local_type(economicBD.getIdLocalType());
                                                            requestEconomic.setId_makes_activity(economicBD.getIdMakesActivity());
                                                            requestEconomic.setId_name(economicBD.getIdName());
                                                            requestEconomic.setId_time_activity(economicBD.getIdTimeActivity());
                                                            requestEconomic.setId_economic_activity(economicBD.getIdEconomicActivity());
                                                            json = gson.toJson(requestEconomic);
                                                            response = functionJson.RequestHttp(getString(R.string.url_insert_economic), FunctionJson.POST, json);
                                                            gson = new GsonBuilder().create();
                                                            GlobalVariables.responseEconomic = new ResponseEconomic();
                                                            GlobalVariables.responseEconomic = gson.fromJson(response, ResponseEconomic.class);
                                                            if (GlobalVariables.responseEconomic != null) {
                                                                RequestAdditional requestAdditional = new RequestAdditional();
                                                                response = "";
                                                                GlobalVariables.responseAdditionalArrayList = new ArrayList<ResponseAdditional>();
                                                                for (Additionals additionals : additionalsBD) {
                                                                    json = "";
                                                                    requestAdditional.setId_occupation(additionals.getIdOccupation());
                                                                    requestAdditional.setId_name(GlobalVariables.responseName.getUser().getIdName());
                                                                    requestAdditional.setComplete(getString(R.string.value_true));
                                                                    requestAdditional.setId_additional_types(additionals.getIdAdditionalTypes());
                                                                    requestAdditional.setLast_name(additionals.getLastName());
                                                                    requestAdditional.setM_last_name(additionals.getmLastName());
                                                                    requestAdditional.setName1(additionals.getName1());
                                                                    requestAdditional.setName2(additionals.getName2());
                                                                    requestAdditional.setPhone(additionals.getPhone());
                                                                    json = gson.toJson(requestAdditional);
                                                                    response = functionJson.RequestHttp(getString(R.string.url_insert_additional), FunctionJson.POST, json);
                                                                    gson = new GsonBuilder().create();
                                                                    ResponseAdditional responseAdditional = new ResponseAdditional();
                                                                    responseAdditional = gson.fromJson(response, ResponseAdditional.class);
                                                                    GlobalVariables.responseAdditionalArrayList.add(responseAdditional);
                                                                    additionals.setComplete(getString(R.string.value_true));
                                                                    additionals.setIdName(GlobalVariables.responseName.getUser().getIdName());
                                                                }
                                                            } else {
                                                            }
                                                        }
                                                    } else {

                                                    }

                                                }
                                            }
                                        }
                                        MainActivity.this.runOnUiThread(new Runnable() {
                                            public void run() {
                                                Toast.makeText(MainActivity.this, GlobalVariables.responseName.getMensaje(), Toast.LENGTH_LONG).show();
                                            }
                                        });
                                    }
                                }
                                else
                                {
                                    MainActivity.this.runOnUiThread(new Runnable() {
                                        public void run() {
                                            Toast.makeText(MainActivity.this, "Necesita tener todos los datos obligatorios registrados", Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                            }
                        }
                    }
                }
                else
                {
                    MainActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(MainActivity.this, "No hay datos para sincronizar", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
            catch (Exception e)
            {
                e.getStackTrace();
            }
        }

        private boolean changeId(int id)
        {
            boolean change = false;
            Name nameChange = new Name();
            int idChange=0, idResult=0;
            try
            {
                nameChange = dbHandler.GetName(id);
                if(nameChange!=null)
                {
                    if (!TextUtils.isEmpty(nameChange.getIdName()))
                    {
                        Log.d("Name1 Response", GlobalVariables.responseName.getUser().getName1().toString());
                        Log.d("Name2 Response", GlobalVariables.responseName.getUser().getName2().toString());
                        Log.d("LastName Response", GlobalVariables.responseName.getUser().getLastName().toString());
                        Log.d("MLastName Response", GlobalVariables.responseName.getUser().getmLastName().toString());

                        Log.d("Name1 BD", nameChange.getName1().toString());
                        Log.d("Name2 BD", nameChange.getName2().toString());
                        Log.d("LastName BD", nameChange.getLastName().toString());
                        Log.d("MLastName BD", nameChange.getmLastName().toString());
                        if(TextUtils.isEmpty(nameChange.getName1().toString()))
                        {
                            if(!nameChange.getName1().toString().equals(GlobalVariables.responseName.getUser().getName1()))
                            {
                                change = true;
                            }
                        }
                        if(TextUtils.isEmpty(nameChange.getLastName().toString()))
                        {
                            if(!nameChange.getLastName().toString().equals(GlobalVariables.responseName.getUser().getLastName()))
                            {
                                change = true;
                            }
                        }
                        if(TextUtils.isEmpty(nameChange.getmLastName().toString()))
                        {
                            if(!nameChange.getmLastName().toString().equals(GlobalVariables.responseName.getUser().getmLastName()))
                            {
                                change = true;
                            }
                        }
                        if(!TextUtils.isEmpty(nameChange.getName2()))
                        {
                            if(!TextUtils.isEmpty(GlobalVariables.responseName.getUser().getName2()))
                            {
                                if(!nameChange.getName2().equals(GlobalVariables.responseName.getUser().getName2()))
                                {
                                    change = true;
                                }
                            }
                        }
                    }
                }

                if(change==true)
                {
                    //Get LastId
                    idChange = dbHandler.GetLastId()+1;
                    if(idChange>0)
                    {
                        idResult = dbHandler.AddName(nameChange);
                        if(idResult>0)
                        {
                            ArrayList<Phone> phoneArrayList = new ArrayList<Phone>();
                            phoneArrayList = dbHandler.GetAllPhones(id);
                            if(phoneArrayList.size()>0)
                            {
                                for (Phone phoneChange: phoneArrayList)
                                {
                                    if(phoneChange.getIdName().equals(String.valueOf(id)))
                                    {
                                        phoneChange.setIdName(String.valueOf(idChange));
                                        idResult = dbHandler.UpdateIdPhone(phoneChange, idChange);
                                    }
                                }
                            }

                            ArrayList<Address> addressArrayList = new ArrayList<Address>();
                            addressArrayList = dbHandler.GetAllAddress(id);
                            if(addressArrayList.size()>0)
                            {
                                for(Address addressChange: addressArrayList)
                                {
                                    if(addressChange.getIdName().equals(String.valueOf(id)))
                                    {
                                        idResult = dbHandler.UpdateIdAddress(addressChange, idChange);
                                    }
                                }
                            }

                            Basics basicsChange = new Basics();
                            basicsChange = dbHandler.GetBasics(id);
                            if(basicsChange.getIdName()!=null)
                            {
                                idResult = dbHandler.UpdateIdBasics(basicsChange, idChange);
                            }

                            Complementary complementaryChange = new Complementary();
                            complementaryChange = dbHandler.GetComplementary(id);
                            if(complementaryChange.getIdName()!=null)
                            {
                                idResult = dbHandler.UpdateIdComplementary(complementaryChange, idChange);
                            }

                            ArrayList<Additionals> additionalsArrayList = new ArrayList<Additionals>();
                            additionalsArrayList = dbHandler.GetAdditional(id);
                            if(additionalsArrayList.size()>0)
                            {
                                for(Additionals additionalsChange: additionalsArrayList)
                                {
                                    if(additionalsChange.getIdName().equals(String.valueOf(id)))
                                    {
                                        idResult = dbHandler.UpdateIdAdditionals(additionalsChange, idChange);
                                    }
                                }
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.getStackTrace();
                Toast.makeText(MainActivity.this, "Error al cambiar id", Toast.LENGTH_LONG).show();
            }

            return change;
        }

        private int updateName(Name name)
        {
            int id = 0;
            try
            {
                id = dbHandler.UpdateName(name);
            }
            catch (Exception ex)
            {
                ex.getStackTrace();
            }
            return id;
        }

        private int updatePhone(Phone phone, int idPhone)
        {
            int id=0;
            try
            {
                id = dbHandler.UpdateIdPhone(phone, idPhone);
            }
            catch (Exception ex)
            {
                ex.getStackTrace();
            }
            return id;
        }

        private int updateAddress(Address address, int idAddress)
        {
            int id = 0;
            try
            {
                id = dbHandler.UpdateIdAddress(address, idAddress);
            }
            catch (Exception ex)
            {
                ex.getStackTrace();
            }
            return id;
        }

        private int updateBasics(Basics basics, int idBasics)
        {
            int id = 0;
            try
            {
                id = dbHandler.UpdateIdBasics(basics, idBasics);
            }
            catch (Exception ex)
            {
                ex.getStackTrace();
            }
            return  id;
        }

        private int updateComplementary(Complementary complementary, int idComplementary)
        {
            int id = 0;
            try
            {
                id = dbHandler.UpdateIdComplementary(complementary, idComplementary);
            }
            catch (Exception ex)
            {
                ex.getStackTrace();
            }
            return  id;
        }

        private int updateEconomic(Economic economic, int idEconomic)
        {
            int id = 0;
            try
            {
                id = dbHandler.UpdateIdEconomic(economic, idEconomic);
            }
            catch (Exception ex)
            {
                ex.getStackTrace();
            }
            return  id;
        }

        private int updateAdditionals(Additionals additionals, int idAdditional)
        {
            int id = 0;
            try
            {
                id = dbHandler.UpdateIdAdditionals(additionals, idAdditional);
            }
            catch (Exception ex)
            {
                ex.getStackTrace();
            }
            return id;
        }
    }
}
