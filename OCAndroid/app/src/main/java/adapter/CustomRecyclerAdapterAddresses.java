package adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import model.Addresses;
import nubaj.com.ocandroid.R;

/**
 * Created by Jose Luis on 25/01/2017.
 */

public class CustomRecyclerAdapterAddresses extends RecyclerView.Adapter<CustomRecyclerAdapterAddresses.DataObjectHolder>
{
    private static String LOG_TAG = "MyRecyclerViewAdapter";
    private ArrayList<Addresses> mDataset;
    private static MyClickListener myClickListener;
    private Context _context;

    public static class DataObjectHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        TextView label, subLabel;
        ImageView imageView;

        public DataObjectHolder(View itemView)
        {
            super(itemView);
            label = (TextView) itemView.findViewById(R.id.textViewAddresses);
            subLabel = (TextView) itemView.findViewById(R.id.textViewAddressesType);
            Log.i(LOG_TAG, "Adding Listener");
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v)
        {
            myClickListener.onItemClick(getAdapterPosition(), v);
        }
    }

    public void setOnItemClickListener(MyClickListener myClickListener)
    {
        this.myClickListener = myClickListener;
    }

    public CustomRecyclerAdapterAddresses(Context context, ArrayList<Addresses> myDataset)
    {
        _context = context;
        mDataset = myDataset;
    }

    public DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType)
    {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_card_view_addresses, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    public void onBindViewHolder(DataObjectHolder holder, int position)
    {
        TextView textView = holder.label;
        TextView textView2 = holder.subLabel;
        ImageView imageView = holder.imageView;

        textView.setText(mDataset.get(position).getDomicilio());
        textView2.setText(mDataset.get(position).getTipo_Domicilio());

    }

    public void addItem(Addresses dataObj, int index)
    {
        mDataset.add(index, dataObj);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        mDataset.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount()
    {
        return mDataset.size();
    }

    public interface MyClickListener
    {
        public void onItemClick(int position, View v);
    }
}
