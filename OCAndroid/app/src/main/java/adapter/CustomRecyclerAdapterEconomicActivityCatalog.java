package adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import model.EconomicActivityCatalog;
import nubaj.com.ocandroid.R;

/**
 * Created by Jose Luis on 10/02/2017.
 */

public class CustomRecyclerAdapterEconomicActivityCatalog extends RecyclerView.Adapter<CustomRecyclerAdapterEconomicActivityCatalog.DataObjectHolder>
{
    private static String LOG_TAG = "MyRecyclerViewAdapter";
    private ArrayList<EconomicActivityCatalog> mDataset;
    private static MyClickListener myClickListener;
    private Context _context;

    public static class DataObjectHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        TextView label, subLabel;

        public DataObjectHolder(View itemView)
        {
            super(itemView);
            label = (TextView) itemView.findViewById(R.id.textViewDescripcion);
            subLabel = (TextView) itemView.findViewById(R.id.textViewIdActividadEconomica);
            Log.i(LOG_TAG, "Adding Listener");
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v)
        {
            myClickListener.onItemClick(getAdapterPosition(), v);
        }
    }

    public void setOnItemClickListener(MyClickListener myClickListener)
    {
        this.myClickListener = myClickListener;
    }

    public CustomRecyclerAdapterEconomicActivityCatalog(Context context, ArrayList<EconomicActivityCatalog> myDataset)
    {
        _context = context;
        mDataset = myDataset;
    }

    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_card_view_economic_activity_catalog, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    public void onBindViewHolder(DataObjectHolder holder, int position)
    {
        TextView textView = holder.label;
        TextView textView2 = holder.subLabel;

        textView.setText(mDataset.get(position).getDescripción());
        textView2.setText(mDataset.get(position).getId_Actividad_Economica());

    }

    public void addItem(EconomicActivityCatalog dataObj, int index)
    {
        mDataset.add(index, dataObj);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        mDataset.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount()
    {
        return mDataset.size();
    }

    public interface MyClickListener
    {
        public void onItemClick(int position, View v);
    }
}
