package adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import model.Addresses;
import model.ApplicantsNews;
import model.Phones;
import nubaj.com.ocandroid.R;

/**
 * Created by Jose Luis on 24/01/2017.
 */

public class CustomRecyclerAdapterPhones extends RecyclerView.Adapter<CustomRecyclerAdapterPhones.DataObjectHolder>
{
    //private static String LOG_TAG = "MyRecyclerViewAdapter";
    private ArrayList<Phones> mDataset;
    private static CustomRecyclerAdapterPhones.MyClickListener myClickListener;
    private Context _context;

    public static class DataObjectHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        TextView label, subLabel;
        ImageView imageView, imageView2;

        public DataObjectHolder(View itemView)
        {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.imageViewContact);
            label = (TextView) itemView.findViewById(R.id.textViewPhone);
            //subLabel = (TextView) itemView.findViewById(R.id.textViewType);
            imageView2 = (ImageView) itemView.findViewById(R.id.imageViewPhone);
            //Log.i(LOG_TAG, "Adding Listener");
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v)
        {
            myClickListener.onItemClick(getAdapterPosition(), v);
        }
    }

    public void setOnItemClickListener(CustomRecyclerAdapterPhones.MyClickListener myClickListener)
    {
        this.myClickListener = myClickListener;
    }

    public CustomRecyclerAdapterPhones(Context context, ArrayList<Phones> myDataset)
    {
        _context = context;
        mDataset = myDataset;
    }

    public CustomRecyclerAdapterPhones.DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                                                              int viewType)
    {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_phones, parent, false);

        CustomRecyclerAdapterPhones.DataObjectHolder dataObjectHolder = new CustomRecyclerAdapterPhones.DataObjectHolder(view);
        return dataObjectHolder;
    }

    public void onBindViewHolder(CustomRecyclerAdapterPhones.DataObjectHolder holder, int position)
    {
        ImageView imageView = holder.imageView;
        TextView textView = holder.label;
        TextView textView2 = holder.subLabel;
        ImageView imageView2 = holder.imageView2;


        imageView.setImageResource(mDataset.get(position).getIcon_Contact());
        textView.setText(mDataset.get(position).getTelefono());
        //textView2.setText(mDataset.get(position).getTipo());
        imageView2.setImageResource(mDataset.get(position).getIcon_telefono());

    }

    public void addItem(Phones dataObj, int index)
    {
        mDataset.add(index, dataObj);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        mDataset.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount()
    {
        return mDataset.size();
    }

    public interface MyClickListener
    {
        public void onItemClick(int position, View v);
    }
}