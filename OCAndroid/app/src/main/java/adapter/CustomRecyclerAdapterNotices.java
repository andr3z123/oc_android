package adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import model.Notices;
import nubaj.com.ocandroid.R;

/**
 * Created by Jose Luis on 09/02/2017.
 */

public class CustomRecyclerAdapterNotices extends RecyclerView.Adapter<CustomRecyclerAdapterNotices.DataObjectHolder>
{
    private static String LOG_TAG = "MyRecyclerViewAdapter";
    private ArrayList<Notices> mDataset;
    private static MyClickListener myClickListener;
    private Context _context;

    public static class DataObjectHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        TextView textView_Nombre_Aviso, textView_Contenido_Aviso;
        ImageView imageView_Icon_Aviso;

        public DataObjectHolder(View itemView)
        {
            super(itemView);
            textView_Nombre_Aviso = (TextView) itemView.findViewById(R.id.textViewNombreAviso);
            textView_Contenido_Aviso = (TextView) itemView.findViewById(R.id.textViewContenidoAviso);
            imageView_Icon_Aviso = (ImageView) itemView.findViewById(R.id.imageViewAviso);
            Log.i(LOG_TAG, "Adding Listener");
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v)
        {
            myClickListener.onItemClick(getAdapterPosition(), v);
        }
    }

    public void setOnItemClickListener(MyClickListener myClickListener)
    {
        this.myClickListener = myClickListener;
    }

    public CustomRecyclerAdapterNotices(Context context, ArrayList<Notices> myDataset)
    {
        _context = context;
        mDataset = myDataset;
    }

    public DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_card_view_notices, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    public void onBindViewHolder(DataObjectHolder holder, int position)
    {
        TextView textView_Nombre_Aviso = holder.textView_Nombre_Aviso;
        TextView textView_Contenido_Aviso = holder.textView_Contenido_Aviso;
        ImageView imageView_Icon_Aviso = holder.imageView_Icon_Aviso;

        textView_Nombre_Aviso.setText(mDataset.get(position).getNombre_Aviso());
        textView_Contenido_Aviso.setText(mDataset.get(position).getContenido_Aviso());
        imageView_Icon_Aviso.setImageResource(mDataset.get(position).getIcon_Aviso());

    }

    public void addItem(Notices dataObj, int index)
    {
        mDataset.add(index, dataObj);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        mDataset.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount()
    {
        return mDataset.size();
    }

    public interface MyClickListener
    {
        public void onItemClick(int position, View v);
    }
}
