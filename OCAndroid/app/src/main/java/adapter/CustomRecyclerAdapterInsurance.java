package adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import model.Insurence;
import nubaj.com.ocandroid.R;

/**
 * Created by Jose Luis on 03/02/2017.
 */

public class CustomRecyclerAdapterInsurance extends RecyclerView.Adapter<CustomRecyclerAdapterInsurance.DataObjectHolder>
{
    //private static String LOG_TAG = "MyRecyclerViewAdapter";
    private ArrayList<Insurence> mDataset;
    private static CustomRecyclerAdapterInsurance.MyClickListener myClickListener;
    private Context _context;

    public static class DataObjectHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        TextView textView_Tipo_Status, textView_Nombre_Grupo, textView_Fecha;
        ImageView imageView_Icon_Status;

        public DataObjectHolder(View itemView)
        {
            super(itemView);
            imageView_Icon_Status = (ImageView) itemView.findViewById(R.id.imageViewIconPersonStatus);
            textView_Tipo_Status = (TextView) itemView.findViewById(R.id.textViewTipoStatus);
            textView_Nombre_Grupo = (TextView) itemView.findViewById(R.id.textViewNombreGrupo);
            textView_Fecha = (TextView) itemView.findViewById(R.id.textViewFecha);
            //Log.i(LOG_TAG, "Adding Listener");
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v)
        {
            myClickListener.onItemClick(getAdapterPosition(), v);
        }
    }

    public void setOnItemClickListener(CustomRecyclerAdapterInsurance.MyClickListener myClickListener)
    {
        this.myClickListener = myClickListener;
    }

    public CustomRecyclerAdapterInsurance(Context context, ArrayList<Insurence> myDataset)
    {
        _context = context;
        mDataset = myDataset;
    }

    public CustomRecyclerAdapterInsurance.DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                                                           int viewType)
    {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_card_view_insurence, parent, false);

        CustomRecyclerAdapterInsurance.DataObjectHolder dataObjectHolder = new CustomRecyclerAdapterInsurance.DataObjectHolder(view);
        return dataObjectHolder;
    }

    public void onBindViewHolder(CustomRecyclerAdapterInsurance.DataObjectHolder holder, int position)
    {
        ImageView imageView_Icon_Status = holder.imageView_Icon_Status;
        TextView textView_Tipo_Status = holder.textView_Tipo_Status;
        TextView textView_Nombre_Grupo = holder.textView_Nombre_Grupo;
        TextView textView_Fecha = holder.textView_Fecha;


        imageView_Icon_Status.setImageResource(mDataset.get(position).getIcon_Person_Status());
        textView_Tipo_Status.setText(mDataset.get(position).getTipo_Status());
        textView_Nombre_Grupo.setText(mDataset.get(position).getNombre_Grupo());
        textView_Fecha.setText(mDataset.get(position).getFecha());

    }

    public void addItem(Insurence dataObj, int index)
    {
        mDataset.add(index, dataObj);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        mDataset.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount()
    {
        return mDataset.size();
    }

    public interface MyClickListener
    {
        public void onItemClick(int position, View v);
    }
}
