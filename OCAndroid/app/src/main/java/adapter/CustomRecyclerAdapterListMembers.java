package adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import model.ListMembers;
import nubaj.com.ocandroid.R;

/**
 * Created by Jose Luis on 01/02/2017.
 */

public class CustomRecyclerAdapterListMembers extends RecyclerView.Adapter<CustomRecyclerAdapterListMembers.DataObjectHolder>
{
    //private static String LOG_TAG = "MyRecyclerViewAdapter";
    private ArrayList<ListMembers> mDataset;
    private static CustomRecyclerAdapterListMembers.MyClickListener myClickListener;
    private Context _context;

    public static class DataObjectHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        TextView textView_Nombre_Integrante_Grupo, textView_Rol, textView_Lista_Control, textView_Nivel_Riesgo;
        ImageView imageView_Icon_Persona, imageView_Icon_Status;

        public DataObjectHolder(View itemView)
        {
            super(itemView);
            textView_Nombre_Integrante_Grupo = (TextView) itemView.findViewById(R.id.textViewNombreIntegranteGrupo);
            textView_Rol = (TextView) itemView.findViewById(R.id.textViewRol);
            textView_Lista_Control = (TextView) itemView.findViewById(R.id.textViewTitleListaControl);
            textView_Nivel_Riesgo = (TextView) itemView.findViewById(R.id.textViewNiveRiesgo);
            imageView_Icon_Persona = (ImageView) itemView.findViewById(R.id.imageViewIconPersona);
            imageView_Icon_Status = (ImageView) itemView.findViewById(R.id.imageViewIconStatus);

            //Log.i(LOG_TAG, "Adding Listener");
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v)
        {
            myClickListener.onItemClick(getAdapterPosition(), v);
        }
    }

    public void setOnItemClickListener(CustomRecyclerAdapterListMembers.MyClickListener myClickListener)
    {
        this.myClickListener = myClickListener;
    }

    public CustomRecyclerAdapterListMembers(Context context, ArrayList<ListMembers> myDataset)
    {
        _context = context;
        mDataset = myDataset;
    }

    public CustomRecyclerAdapterListMembers.DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_card_view_list_members, parent, false);

        CustomRecyclerAdapterListMembers.DataObjectHolder dataObjectHolder = new CustomRecyclerAdapterListMembers.DataObjectHolder(view);
        return dataObjectHolder;
    }

    public void onBindViewHolder(CustomRecyclerAdapterListMembers.DataObjectHolder holder, int position)
    {
        TextView textView_nombre_integrante_grupo = holder.textView_Nombre_Integrante_Grupo;
        TextView textView_rol = holder.textView_Rol;
        TextView textView_lista_control = holder.textView_Lista_Control;
        TextView textView_Nivel_Riesgo = holder.textView_Nivel_Riesgo;
        ImageView imageView_Icon_Persona = holder.imageView_Icon_Persona;
        ImageView imageView_Icon_Status = holder.imageView_Icon_Status;

        textView_nombre_integrante_grupo.setText(mDataset.get(position).getNombre_Integrante_Grupo());
        textView_rol.setText(mDataset.get(position).getRol());
        textView_lista_control.setText(mDataset.get(position).getLista_Control());
        textView_Nivel_Riesgo.setText(mDataset.get(position).getNivel_Riesgo());
        imageView_Icon_Persona.setImageResource(mDataset.get(position).getIcon_Persona());
        imageView_Icon_Status.setImageResource(mDataset.get(position).getIcon_Status());

    }

    public void addItem(ListMembers dataObj, int index)
    {
        mDataset.add(index, dataObj);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        mDataset.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount()
    {
        return mDataset.size();
    }

    public interface MyClickListener
    {
        public void onItemClick(int position, View v);
    }
}
