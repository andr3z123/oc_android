package adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import model.MyEarrings;
import nubaj.com.ocandroid.R;

/**
 * Created by Jose Luis on 09/02/2017.
 */

public class CustomRecyclerAdapterMyEarrings extends RecyclerView.Adapter<CustomRecyclerAdapterMyEarrings.DataObjectHolder>
{
    private static String LOG_TAG = "MyRecyclerViewAdapter";
    private ArrayList<MyEarrings> mDataset;
    private static MyClickListener myClickListener;
    private Context _context;

    public static class DataObjectHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        TextView textView_Id_Oportunidad, textView_Nombre_Pendiente, textView_Fecha;
        ImageView imageView_Icon_Editar;

        public DataObjectHolder(View itemView)
        {
            super(itemView);
            textView_Id_Oportunidad = (TextView) itemView.findViewById(R.id.textViewIdOportunidad);
            textView_Nombre_Pendiente = (TextView) itemView.findViewById(R.id.textViewNombrePendiente);
            textView_Fecha = (TextView) itemView.findViewById(R.id.textViewFechaPendiente);
            imageView_Icon_Editar = (ImageView) itemView.findViewById(R.id.imageViewEdit);
            Log.i(LOG_TAG, "Adding Listener");
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v)
        {
            myClickListener.onItemClick(getAdapterPosition(), v);
        }
    }

    public void setOnItemClickListener(MyClickListener myClickListener)
    {
        this.myClickListener = myClickListener;
    }

    public CustomRecyclerAdapterMyEarrings(Context context, ArrayList<MyEarrings> myDataset)
    {
        _context = context;
        mDataset = myDataset;
    }

    public DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_card_view_my_earrings, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    public void onBindViewHolder(DataObjectHolder holder, int position)
    {
        TextView textView_Id_Oportunidad = holder.textView_Id_Oportunidad;
        TextView textView_Nombre_Pendiente = holder.textView_Nombre_Pendiente;
        TextView textView_Fecha = holder.textView_Fecha;
        ImageView imageView_Icon_Editar = holder.imageView_Icon_Editar;

        textView_Id_Oportunidad.setText(mDataset.get(position).getId_Oportunidad());
        textView_Nombre_Pendiente.setText(mDataset.get(position).getNombre_Pendiente());
        textView_Fecha.setText(mDataset.get(position).getFecha());
        imageView_Icon_Editar.setImageResource(mDataset.get(position).getIcon_Editar());

    }

    public void addItem(MyEarrings dataObj, int index)
    {
        mDataset.add(index, dataObj);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        mDataset.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount()
    {
        return mDataset.size();
    }

    public interface MyClickListener
    {
        public void onItemClick(int position, View v);
    }
}
