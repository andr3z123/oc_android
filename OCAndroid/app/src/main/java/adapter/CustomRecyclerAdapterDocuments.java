package adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import model.Documents;
import nubaj.com.ocandroid.R;

/**
 * Created by Jose Luis on 31/01/2017.
 */

public class CustomRecyclerAdapterDocuments extends RecyclerView.Adapter<CustomRecyclerAdapterDocuments.DataObjectHolder>
{
    private static String LOG_TAG = "MyRecyclerViewAdapter";
    private ArrayList<Documents> mDataset;
    private static MyClickListener myClickListener;
    private Context _context;

    public static class DataObjectHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        TextView titulo_Documento, documento_Frente_Reverso;
        ImageView imageViewImagenDocumento;

        public DataObjectHolder(View itemView)
        {
            super(itemView);
            titulo_Documento = (TextView) itemView.findViewById(R.id.textViewTituloDocumentos);
            imageViewImagenDocumento = (ImageView) itemView.findViewById(R.id.imageViewImageDocument);
            Log.i(LOG_TAG, "Adding Listener");
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v)
        {
            myClickListener.onItemClick(getAdapterPosition(), v);
        }
    }

    public void setOnItemClickListener(MyClickListener myClickListener)
    {
        this.myClickListener = myClickListener;
    }

    public CustomRecyclerAdapterDocuments(Context context, ArrayList<Documents> myDataset)
    {
        _context = context;
        mDataset = myDataset;
    }

    public DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_card_view_documents, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    public void onBindViewHolder(DataObjectHolder holder, int position)
    {
        TextView textViewTituloDocumento = holder.titulo_Documento;
        ImageView imageViewImagendocumento = holder.imageViewImagenDocumento;

        textViewTituloDocumento.setText(mDataset.get(position).getTitulo_Documento());

        //Glide.clear(imageViewImagendocumento);
        Glide.with(_context)
                .load(mDataset.get(position).getImagen_Documento()) //load position image
                .diskCacheStrategy(DiskCacheStrategy.NONE) //Delete cache
                .skipMemoryCache(true) //Update cache
                .centerCrop() //Center
                //.placeholder(R.mipmap.ic_launcher)//Icon preview
                .into(imageViewImagendocumento); //component resource
        //imageViewImagendocumento.setImageBitmap(mDataset.get(position).getImagen_Documento());
        //mageViewImagendocumento.setImageResource(mDataset.get(position).getImagen_Documento());
    }

    public void addItem(Documents dataObj, int index)
    {
        mDataset.add(index, dataObj);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        mDataset.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount()
    {
        return mDataset.size();
    }

    public interface MyClickListener
    {
        public void onItemClick(int position, View v);
    }
}
