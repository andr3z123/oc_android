package adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import model.Applications;
import nubaj.com.ocandroid.R;

/**
 * Created by Jose Luis on 31/01/2017.
 */

public class CustomRecyclerAdapterApplications extends RecyclerView.Adapter<CustomRecyclerAdapterApplications.DataObjectHolder> {
    //private static String LOG_TAG = "MyRecyclerViewAdapter";
    private ArrayList<Applications> mDataset;
    private static CustomRecyclerAdapterApplications.MyClickListener myClickListener;
    private Context _context;

    public static class DataObjectHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView textView_Tipo_Solicitud, textView_Solicitud_Grupo, textView_Fecha_Solicitud;
        ImageView imageView_Icon_Tipo_Solicitud;

        public DataObjectHolder(View itemView) {
            super(itemView);
            imageView_Icon_Tipo_Solicitud = (ImageView) itemView.findViewById(R.id.imageViewIconTipoSolicitud);
            textView_Tipo_Solicitud = (TextView) itemView.findViewById(R.id.textViewTipoSolicitud);
            textView_Solicitud_Grupo = (TextView) itemView.findViewById(R.id.textViewSolicitudGrupo);
            textView_Fecha_Solicitud = (TextView) itemView.findViewById(R.id.textViewFechaSolicitud);
            //Log.i(LOG_TAG, "Adding Listener");
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v)
        {
            myClickListener.onItemClick(getAdapterPosition(), v);
        }
    }

    public void setOnItemClickListener(CustomRecyclerAdapterApplications.MyClickListener myClickListener)
    {
        this.myClickListener = myClickListener;
    }

    public CustomRecyclerAdapterApplications(Context context, ArrayList<Applications> myDataset) {
        _context = context;
        mDataset = myDataset;
    }

    public CustomRecyclerAdapterApplications.DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                                                                 int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_card_view_applications, parent, false);

        CustomRecyclerAdapterApplications.DataObjectHolder dataObjectHolder = new CustomRecyclerAdapterApplications.DataObjectHolder(view);
        return dataObjectHolder;
    }

    public void onBindViewHolder(CustomRecyclerAdapterApplications.DataObjectHolder holder, int position) {
        ImageView imageView_Icon_Tipo_Solicitud = holder.imageView_Icon_Tipo_Solicitud;
        TextView textView_Tipo_Solicitud = holder.textView_Tipo_Solicitud;
        TextView textView_Solicitud_Grupo = holder.textView_Solicitud_Grupo;
        TextView textView_Fecha_Solicitud = holder.textView_Fecha_Solicitud;


        imageView_Icon_Tipo_Solicitud.setImageResource(mDataset.get(position).getIcon_Tipo_Solicitud());
        textView_Tipo_Solicitud.setText(mDataset.get(position).getTitulo_Tipo_Solicitud());
        textView_Solicitud_Grupo.setText(mDataset.get(position).getTitulo_Grupo());
        textView_Fecha_Solicitud.setText(mDataset.get(position).getFecha_Solicitud());

    }

    public void addItem(Applications dataObj, int index) {
        mDataset.add(index, dataObj);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        mDataset.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount()
    {
        if(mDataset!=null)
        {
            return mDataset.size();
        }
        else
        {
            return  0;
        }
    }

    public interface MyClickListener {
        public void onItemClick(int position, View v);
    }
}