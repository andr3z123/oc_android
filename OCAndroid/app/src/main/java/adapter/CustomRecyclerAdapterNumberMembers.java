package adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import model.ListMembers;
import model.NumberMembers;
import nubaj.com.ocandroid.R;

/**
 * Created by Jose Luis on 07/02/2017.
 */

public class CustomRecyclerAdapterNumberMembers extends RecyclerView.Adapter<CustomRecyclerAdapterNumberMembers.DataObjectHolder>
{
    private static String LOG_TAG = "MyRecyclerViewAdapter";
    private ArrayList<ListMembers> mDataset;
    private static MyClickListener myClickListener;
    private Context _context;

    public static class DataObjectHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        TextView textView_Nombre_Integrante;

        public DataObjectHolder(View itemView)
        {
            super(itemView);
            textView_Nombre_Integrante = (TextView) itemView.findViewById(R.id.textViewNombreIntegrante);
            Log.i(LOG_TAG, "Adding Listener");
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v)
        {
            myClickListener.onItemClick(getAdapterPosition(), v);
        }
    }

    public void setOnItemClickListener(MyClickListener myClickListener)
    {
        this.myClickListener = myClickListener;
    }

    public CustomRecyclerAdapterNumberMembers(Context context, ArrayList<ListMembers> myDataset)
    {
        _context = context;
        mDataset = myDataset;
    }

    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_card_view_number_members, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    public void onBindViewHolder(DataObjectHolder holder, int position)
    {
        TextView textView_Nombre_Integrante = holder.textView_Nombre_Integrante;

        textView_Nombre_Integrante.setText(mDataset.get(position).getNombre_Integrante_Grupo());

    }

    public void addItem(ListMembers dataObj, int index)
    {
        mDataset.add(index, dataObj);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        mDataset.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount()
    {
        return mDataset.size();
    }

    public interface MyClickListener
    {
        public void onItemClick(int position, View v);
    }
}
