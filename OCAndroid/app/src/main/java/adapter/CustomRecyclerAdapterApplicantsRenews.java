package adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import model.ApplicantsRenews;
import nubaj.com.ocandroid.R;

/**
 * Created by Jose Luis on 10/02/2017.
 */

public class CustomRecyclerAdapterApplicantsRenews extends RecyclerView.Adapter<CustomRecyclerAdapterApplicantsRenews.DataObjectHolder>
{
    //private static String LOG_TAG = "MyRecyclerViewAdapter";
    private ArrayList<ApplicantsRenews> mDataset;
    private static CustomRecyclerAdapterApplicantsRenews.MyClickListener myClickListener;
    private Context _context;

    public static class DataObjectHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        TextView textView_Nombre_Solicitante, textView_Fecha_Ingreso, textView_Id_Cliente, textView_Dias_Restantes, textView_Colonia;
        ImageView imageView;

        public DataObjectHolder(View itemView)
        {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.imageViewStatus);
            textView_Nombre_Solicitante = (TextView) itemView.findViewById(R.id.textViewNombreSolicitante);
            textView_Fecha_Ingreso = (TextView) itemView.findViewById(R.id.textViewFechaIngreso);
            textView_Id_Cliente = (TextView) itemView.findViewById(R.id.textViewIdCliente);
            textView_Dias_Restantes = (TextView) itemView.findViewById(R.id.textViewDiasRestantes);
            //textView_Colonia = (TextView) itemView.findViewById(R.id.textViewColonia);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v)
        {
            myClickListener.onItemClick(getAdapterPosition(), v);
        }
    }

    public void setOnItemClickListener(CustomRecyclerAdapterApplicantsRenews.MyClickListener myClickListener)
    {
        this.myClickListener = myClickListener;
    }

    public CustomRecyclerAdapterApplicantsRenews(Context context, ArrayList<ApplicantsRenews> myDataset)
    {
        _context = context;
        mDataset = myDataset;
    }

    public CustomRecyclerAdapterApplicantsRenews.DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                                                                   int viewType)
    {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_applicants_news, parent, false);

        CustomRecyclerAdapterApplicantsRenews.DataObjectHolder dataObjectHolder = new CustomRecyclerAdapterApplicantsRenews.DataObjectHolder(view);
        return dataObjectHolder;
    }

    public void onBindViewHolder(CustomRecyclerAdapterApplicantsRenews.DataObjectHolder holder, int position)
    {
        ImageView imageView = holder.imageView;
        TextView textView = holder.textView_Nombre_Solicitante;
        TextView textView2 = holder.textView_Fecha_Ingreso;
        TextView textView3 = holder.textView_Id_Cliente;
        TextView textView4 = holder.textView_Dias_Restantes;
        //TextView textView5 = holder.textView_Colonia;


        imageView.setImageResource(mDataset.get(position).getIcon_Status());
        textView.setText(mDataset.get(position).getNombre_Solicitante());
        textView2.setText(mDataset.get(position).getFecha_Ingreso());
        textView3.setText(mDataset.get(position).getId_Cliente());
        textView4.setText(mDataset.get(position).getDias_Restantes());
        //textView5.setText(mDataset.get(position).getColonia());

    }

    public void addItem(ApplicantsRenews dataObj, int index)
    {
        mDataset.add(index, dataObj);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        mDataset.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount()
    {
        return mDataset.size();
    }

    public interface MyClickListener
    {
        public void onItemClick(int position, View v);
    }
}
